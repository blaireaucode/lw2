/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

var fs = require('fs');

// read template
fn = './src/passages/allpassages_work.txt'
data = fs.readFileSync(fn, 'utf-8')

// split passage
data = data.split('::')

console.log(data.length);

for (var p of data) {

    var n = p.split('\n')[0]
    txt = p.replace(n+'\n', '')

    // [[Rendez-vous au 305|305]]
    var reg = /\[\[rendez-vous au (.*?)\|.*?\]\]/g
    txt = txt.replace(reg, '<L to="\$1">rendez-vous au $1</L>');

    var reg = /\[\[Rendez-vous au (.*?)\|.*?\]\]/g
    txt = txt.replace(reg, '<L to="\$1">Rendez-vous au $1</L>');

    fn = './src/passage_template.js';
    t = fs.readFileSync(fn, 'utf-8');

    // replace passage
    t = t.replace(/passage_number/g, n.toString());
    t = t.replace("ici", txt);
    t = t.replace("<<dead>>", "DEAD");
    t = t.replace(/\r/g, "");

    // console.log(t)

    // write
    fn = './src/passages/'+n+'.js'
    fs.copyFileSync(fn, fn+'.backup')

    fs.writeFileSync(fn, t)
    console.log("Successfully written to file "+fn);

}
