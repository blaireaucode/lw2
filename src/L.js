/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import * as bh from './book_helpers.js';

class L extends Component {

    handleClick() {
        if (this.props.to[0] !== '/')
            bh.history_add_passage(this.props, this.props.to);
        if (this.props.onClick) this.props.onClick();
    }

    componentDidMount() {
        window.scrollTo(0, 0);
    }
    
    render() {
        var u='';
        if (this.props.to[0] === '/')
            u = this.props.to;
        else
            u = "/passage/"+this.props.to;
        const dead = bh.is_dead(this.props.character);
        if (!dead && this.props.enabled)
            return (
                <Link to={u} onClick={this.handleClick.bind(this)}>{this.props.children}</Link>
            );
        else
            return (<span>
                          {this.props.children}
                        </span>
                   );
    }
}

// Set default props
L.defaultProps = {
    enabled: true
};

//export default L;
export default connect(mapStateToProps, mapDispatchToProps)(L);
