/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import * as bh from './book_helpers.js';
import Dices, { DICE_TYPES } from './Dices.js';
import L from './L.js';

class RandomTable extends Component {

    constructor(props) {
        super(props);
        this.state = { rnd: false};
    }
    
    random() {
        var bonus = 0;
        if (this.props.bonus) {
            bonus = this.props.bonus(this.props.character);
        }
        const r = Math.floor(Math.random() * 10)+bonus;
        const e = {...this.state.rnd, dice_value:r };
        const ee = {...e, bonus:bonus };
        this.props.book_set_element(ee);
        // Call the callback if any
        if (this.props.onClick) this.props.onClick(r);
        
    }

    componentWillReceiveProps(nextProps) {
        const r = bh.book_get_element(nextProps, nextProps.p);
        this.setState({rnd:r});
    }

    render() {
        const cp = this.props.p.passage_title;
        const pl = '/passage/'+cp;
        const r = this.state.rnd;
        if (this.props.enabled === false) {
            return (<span>Table de Hasard </span>);
        }
        if (!r || r.dice_value === -1)
            return (
                <span>
                  <L onClick={this.random.bind(this)} to={pl} >Table de Hasard</L> {this.props.children}
                </span>
            );
        else {
            if (!r.bonus || r.bonus === 0) {
                return (
                    <span>
                      Table de Hasard <span className='Book-randomtable'>(vous obtenez un {r.dice_value})</span>
                    </span>
                );
            }
            const n = r.dice_value-r.bonus;
            return (
                <span>
                  Table de Hasard <span className='Book-randomtable'>(vous obtenez {n},
                                    plus {r.bonus} de bonus = {r.dice_value}) </span>
                </span>
            );
        }
    }

}


class CurrentDice extends Component {
    render() {
        if (!this.props.options.display_dice) return '';
        const r = this.props.r;
        if (r >= 0) {
            return (   
                <Dices
                  height='250px'
                  posY={8}
                  scaleFactor={1.0}
                  dices={[                 
                      {
                          type: DICE_TYPES.D12,
                          backColor: "black",
                          fontColor: "white",
                          value: r+1
                      }
                  ]}
                />);
        }
        return '';
    }
}

const cCurrentDice = connect(mapStateToProps, mapDispatchToProps)(CurrentDice);
export {cCurrentDice as CurrentDice};
export default connect(mapStateToProps, mapDispatchToProps)(RandomTable);
