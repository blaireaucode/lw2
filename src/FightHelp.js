/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';

class FightHelp extends Component {

    render() {
        const h = this.props.fight.combat_help;
        if (!h) return (<span/>);
        if (h.length === 0) return (<span/>);
        var txt = [];
        for (var help of h) {
            txt.push(<span key={help}>{help}<br/></span>);
        }
        return (
            <span className='Book-fight-help'>
              {txt}
            </span>
        );
    }
}

export default FightHelp;
