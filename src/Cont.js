/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Cont extends Component {
    render() {
        const u = "/passage/"+this.props.to;
        return (
            <span style={{fontSize: '90%'}}>
              <Link to={u}>Continuer ...</Link>
            </span>
        );
    }
}

export default Cont;
