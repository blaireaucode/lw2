/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import { book_set, character_set, book_set_element, options_set} from './actions.js';

export const mapDispatchToProps = (dispatch) => ({
    character_set(character) { dispatch(character_set(character)); },
    book_set(book) { dispatch(book_set(book)); },
    options_set(options) { dispatch(options_set(options)); },
    book_set_element(element) { dispatch(book_set_element(element)); }
});

export const mapStateToProps = store => {
    return { character: store.character, book:store.book, options:store.options };
};
