/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps } from './book_default_props.js';

class Span extends Component {
    render() {
        if (!this.props.enabled)
            return (<span/>);
        else
            return (<span >
                      {this.props.children}
                   </span>
                   );
    }
}

// Set default props
Span.defaultProps = {
    enabled: true
};

//export default L;
export default connect(mapStateToProps)(Span);
