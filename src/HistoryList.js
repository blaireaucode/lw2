/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import Cached from '@material-ui/icons/Cached';
import Button from '@material-ui/core/Button';
import History from './History.js';
import update from 'immutability-helper';

class HistoryList extends Component {

    clearHistory() {
        const c = update(this.props.character, {history: {$set:[]}} );
        this.props.character_set(c);
    }
    
    render() {
        var history=[];
        var n = 0;
        const l = this.props.character.history.length;
        for (var h of this.props.character.history) {
            var i;
            if (n<l-1)
                i = (<span key={n}>
                        <History history={h}/> -  
                      </span>);
            else
                i = (<span key={n}>
                       <History history={h}/>
                     </span>);
            history.push(i);
            n += 1;
        }
        const icon_trash_style = {
            verticalAlign: 'middle',
            fontSize: '1.2em'
        };

        var b = '';
        if (l>=1)
            b = (<Button disabled={!this.props.options.history_link}
                         onClick={this.clearHistory.bind(this)}>
                <Cached style={icon_trash_style}/>
                 </Button>);
        
        return (
            <div>
              Historique  : {history} {b}              
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HistoryList);
