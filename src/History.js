/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import L from './L.js';
import * as bh from './book_helpers.js';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faFistRaised } from '@fortawesome/free-solid-svg-icons';
import { faSkullCrossbones } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
library.add(faFistRaised);
library.add(faSkullCrossbones);

class History extends Component {
      
    render() {
        const h = this.props.history;
        const icon_style = {
            verticalAlign: 'middle',
            fontSize: '0.8em'
        };

        // Icon fight ? 
        var f ='';
        if (bh.is_fight_in_passage(this.props.book, h)) {
            f = (<FontAwesomeIcon style={icon_style} icon="fist-raised" />);
        }

        // Icon dead ?
        const el = {
            passage_title:h,
            name:'dead',
            dead:false
        };
        const elem =bh.book_get_element(this.props, el);
        if (elem.dead === true) {
            f = (<FontAwesomeIcon style={icon_style} icon="skull-crossbones" />);
        }

        var thetitle = h;
        if (thetitle === "0_0") thetitle = '0.1';
        if (thetitle === "0_1") thetitle = '0.2';
        if (thetitle === "0_2") thetitle = '0.3';
        if (thetitle === "0_3") thetitle = '0.4';
        if (thetitle === "0_4") thetitle = '0.5';
        
        // reader
        return (
            <span>
              <L enabled={this.props.options.history_link} to={h}> {thetitle} {f}</L>
            </span>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(History);
