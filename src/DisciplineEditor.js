/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DisciplineList from './DisciplineList.js';
import Grid from '@material-ui/core/Grid';
import disciplines from './disciplines.js';
//import Switch from '@material-ui/core/Switch';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';

class DisciplineEditor extends Component {

    constructor(props) {
        super(props);
        this.state = {
            edit_mode: false
        };

    }

    handleToggle() {
        this.setState({edit_mode:!this.state.edit_mode});
    }

    /*
      <Switch
      onChange={this.handleToggle.bind(this)}
      checked={this.state.edit_mode}/>
    */
    
    render() {
        var start='';
        if (this.props.edit_mode)
            return this.render_edit_mode(start);
        return (
            <div>
              Disciplines : <p/>
              <DisciplineList disc={this.props.character.kai_disciplines}/>
            </div>
        );
    }

    render_edit_mode(start) {
        var disc = [];
        //function test(e,d) { return e.id === d.id; };
        for(var d of disciplines) {
            const b = this.props.character.kai_disciplines.some(e => e.id === d.id);
            //const b = this.props.character.kai_disciplines.some(test(d));
            if (!b) disc.push(d);
        }
        return (
            <div>
              
              Au cours des siècles, les Moines Kaï ont appris
              à maîtriser toutes les techniques du guerrier. Ces
              techniques sont connues sous le nom de Disciplines
              Kaï et enseignées aux Seigneurs Kaï. Pour votre
              part, vous n'avez été initié qu'à cinq des
              techniques décrites ci-dessous. Il vous appartient de
              choisir vous-même ces cinq Disciplines. Chaque
              Discipline Kaï peut vous être utile à un moment
              ou à un autre de votre quête et votre choix devra
              être le plus judicieux possible : dans certaines
              circonstances, une habile mise en pratique de l'une de
              ces techniques peut vous sauver la vie. Lorsque vous
              aurez choisi vos cinq Disciplines, inscrivez-les dans la
              case correspondante de votre Feuille d'Aventure.

              <p/>
              <Grid container>
                <Grid item sm={12} md={6}>
                  Disciplines acquises ({this.props.character.kai_disciplines.length}):<p/>
                  <DisciplineList disc={this.props.character.kai_disciplines} mode='remove'/>
                </Grid>
                <Grid item sm={12} md={6}>
                  Disciplines disponibles ({disc.length}):<p/>
                  <DisciplineList disc={disc} mode='add'/>
                </Grid>
              </Grid>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DisciplineEditor);
