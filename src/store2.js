/* * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { observable, computed } from "mobx";

class OrderLine {
    @observable price = 0;
    @observable amount = 1;

    @computed get total() {
        return this.price * this.amount;
    }
}
// //import {observable, autorun, action, decorate, computed } from "mobx";

// class StoreBook {
//     name = "John"
//     age = 42
//     showAge = false

//     get labelText() {
//         return this.showAge ? `${this.name} (age: ${this.age})` : this.name;
//     }

//     setAge(age) {
//         this.age = age;
//     }
// }

// // when using decorate, all fields should be specified (a class might have many
// // more non-observable internal fields after all)
// decorate(StoreBook, {
//     name: observable,
//     age: observable,
//     showAge: observable,
//     labelText: computed,
//     setAge: action
// })

// export default StoreBook;
