/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import { Link } from 'react-router-dom';

class Start extends Component {

    constructor(props) {
        super(props);
        this.s = require('./images/cover.png');
    }

    render() {
        return (
            <div  className='Book-character'>

              <div style={{display: 'flex',  justifyContent:'center', alignItems:'center'}}>
                <Link to='/edit'>Débuter l'aventure ...</Link>
              </div><p/>

              <map className='PassageImageDiv'>
                <img alt='2' className='PassageImage' src={this.s}/>
              </map>

              <h1> La traversée infernale</h1>

              <span>
              Joe Dever<p/>
              Illustrations de Gary Chalk<p/>
              Traduit de l'anglais par Camille Fabien<p/>
              </span>

              <p/>
              Adaptation : {' '} 
              <a href="mailto:blaireaucode@gmail.com" target="_new">blaireaucode@gmail.com</a>
              
              <p/>
              Code source : {' '} 
              <a href="https://gitlab.com/blaireaucode/lw2" target="_new">https://gitlab.com/blaireaucode/lw2</a>

              <p/>
              Cette adaptation a été faite pour le plaisir, sans
              objectif commercial. J'espère avoir fait un 'fair-use'
              du matériel récupéré. 
              
              <p/>
              
              © Joe Dever, 1984, pour le texte © Gary Chalk, 1984,
              pour les illustrations © Solar Wind Ltd, 1985, pour
              l'illustration de couverture © Éditions Gallimard, 1985,
              pour la traduction française.
              
              <p/>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Start);
