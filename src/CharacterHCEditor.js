
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import Field from './Field.js';
import { Link } from 'react-router-dom';
import update from 'immutability-helper';

class CharacterHCEditor extends Component {

    constructor(props) {
        super(props);
        this.state = { txt: ''};
    }


    newValues() {
        const d1 = Math.floor(Math.random() * 10);
        const e = d1+20;
        const d2 = Math.floor(Math.random() * 10);
        const h = d2+10;
        const c = update(this.props.character, {
            endurance: {$set: e},
            endurance_max: {$set: e},
            combat_skill: {$set: h}});
        this.props.character_set(c);
        const t = (<span>
                     Vous obtenez <span className='Book-endurance-char'>{d2}</span>
                     +10 d'Habilité et <span className='Book-endurance-char'>{d1}</span>+20 d'Endurance
                     <p/>
                   </span>);
        this.setState({...this.state, txt:t});
    }
    
    render() {
        return (
            <div>

              Au cours de l'initiation qui vous a permis de
              devenir un Seigneur Kaï, vous avez acquis une force
              exceptionnelle&nbsp;; les deux éléments essentiels de cette
              force sont représentés par votre HABILETÉ au combat et
              votre ENDURANCE. Avant d'entreprendre votre mission, il
              vous faudra mesurer exactement le degré
              d'efficacité de votre entraînement.
              
              <p/>

              Lancer deux dés 10. Le premier chiffre obtenu
              représentera votre HABILETÉ au combat. Ajoutez 10 à ce
              chiffre et inscrivez le total obtenu dans la case
              HABILETÉ. Lorsque vous aurez à combattre, il faudra
              mesurer votre HABILETÉ à celle de votre adversaire. Il
              est donc souhaitable que votre total soit le plus élevé
              possible.

              <p/>

              Le deuxième chiffre représentera votre capacité
              d'ENDURANCE. Ajoutez 20 à ce chiffre et inscrivez le
              total obtenu dans la case ENDURANCE. Si vous êtes blessé
              lors d'un combat, vous perdrez des points d'ENDURANCE et
              si jamais votre ENDURANCE tombe à zéro, vous saurez
              alors que vous venez d'être tué et que votre aventure
              est terminée. Au cours de votre mission, vous aurez la
              possibilité de récupérer des points d'ENDURANCE mais
              votre total d'ENDURANCE ne pourra en aucun cas dépasser
              celui dont vous disposiez au départ de votre mission.

              <p/>

              <Link onClick={this.newValues.bind(this)} to="edit">
                Lancez les dés ...</Link><br/><br/>
              <div className='Book-character-HE-editor'>
                {this.state.txt}
                <Field
                  label = {'Habileté'}
                  name = {'combat_skill'}
                  encounter = {this.props.character}
                  type = {'number'} />
                
                <Field
                  label = {'Endurance'}
                  name = {'endurance'}
                  encounter = {this.props.character}
                  type = {'number'}/>
                <br/>
                Endurance maximale: {this.props.character.endurance_max}
              </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CharacterHCEditor);
