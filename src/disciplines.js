/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

const disciplines = [

    { id:'camouflage', name:'Le Camouflage',
      desc:"Cette technique permet au Seigneur Kaï de se fondre dans le paysage. A la campagne, il peut se cacher parmi les arbres et les rochers et se rendre de cette façon invisible à l'ennemi même s'il passe tout près de lui. Dans une ville, cette Discipline donnera à celui qui l'utilise la faculté d'avoir l'air d'un habitant du cru, tant par l'apparence que par l'accent ou la langue employée. On peut ainsi trouver un abri où se cacher en toute sécurité."},

    { id:'chasse', name:'La Chasse',
      desc:"Cette discipline donne au Seigneur Kaï l'assurance qu'il ne mourra jamais de faim même s'il se trouve dans un environnement hostile. Il aura toujours la possibilité de chasser pour se procurer de la nourriture, sauf dans les déserts et les autres régions arides. Cette technique permet également de se déplacer sans bruit en pistant une proie. Vous êtes dispensé de Repas chaque fois qu'il est obligatoire de manger."},

    { id:'sixieme_sens', name:'Le Sixième Sens',
      desc:"Grâce à cette technique, le Seigneur Kaï devine les dangers imminents qui le menacent. Ce Sixième Sens peut également lui révéler les intentions véritables d'un inconnu ou la nature d'un objet étrange rencontré au cours d'une aventure."},

    { id:'orientation', name:"L'Orientation",
      desc:"Chaque fois qu'il se trouvera dans l'obligation de décider quelle direction il doit prendre, le Seigneur Kaï fera toujours le bon choix grâce à cette technique. Il saura ainsi quel chemin il convient d'emprunter dans une forêt et il pourra également, dans une ville, découvrir l'endroit où est caché une personne ou un objet. Par ailleurs, il saura interpréter chaque trace de pas, chaque empreinte qui pourrait lui permettre de remonter une piste."},

    { id:'guerison', name:"La Guérison",
      desc:"Cette discipline donne la faculté de récupérer des points d'ENDURANCE perdus lors d'un combat. Si vous maîtrisez cette technique, vous pourrez ajouter 1 point d'ENDURANCE à votre total chaque fois qu'il vous sera possible d'aller d'un bout à l'autre d'un paragraphe sans avoir à combattre un ennemi. (Vous n'aurez droit d'utiliser cette technique de la guérison que lorsque vos points d'ENDURANCE seront tombés en dessous de votre total initial. Rappelez-vous que vos points d'ENDURANCE ne peuvent en aucun cas excéder votre total de départ.) Si vous choisissez cette Discipline, vous gagnez 1 point d'ENDURANCE pour chaque paragraphe parcouru sans combat."},

    { id:'maitrise_arme', name:"La Maîtrise des Armes",
      desc:"En entrant au monastère Kaï, chaque élève a la possibilité d'être initié au maniement d'une arme. Si vous choisissez d'avoir la Maîtrise d'une arme, utilisez la Table de Hasard à la manière habituelle pour obtenir un chiffre qui correspondra, dans la liste ci-dessous, à l'arme dont on vous aura enseigné le maniement. Vous aurez dès lors la parfaite Maîtrise de cette arme et chaque fois que vous combattrez avec elle, vous aurez droit à 2 points d'HABILETÉ supplémentaires. Le fait que vous ayez la Maîtrise d'une arme ne signifie nullement que vous disposerez de cette arme dès le début de votre aventure ; au cours de celle-ci, cependant, l'occasion vous sera donnée de vous la procurer. Vous pourrez d'ailleurs acquérir diverses armes au cours de votre quête mais vous n'aurez pas le droit d'en posséder plus de deux à la fois. Si vous choisissez cette Discipline, vous gagnez 2 points d'HABILETÉ supplémentaires chaque fois qu'il sera fait usage de l'arme choisie."},

    { id:'bouclier_psychique', name:"Bouclier Psychique",
      desc:"Les Maîtres des Ténèbres et les nombreuses créatures malfaisantes qui leur obéissent ont la faculté de vous porter atteinte en faisant usage de leur force psychique. La technique du Bouclier psychique vous permet cependant de ne pas perdre de points d'ENDURANCE lorsque vous vous trouvez soumis à une telle agression. Si vous choisissez cette discipline, vous ne subissez pas de perte d'ENDURANCE en cas d'agression mentale."},

    { id:'puissance_psychique', name:"Puissance Psychique",
      desc:"Cette technique permet au Seigneur Kaï d'attaquer un ennemi en se servant de la force de son esprit : on peut l'utiliser en même temps qu'une arme de combat habituelle et l'on dispose alors de 2 points supplémentaires d'HABILETÉ. Cette puissance psychique, cependant, n'est pas forcément efficace avec toutes les créatures : il se peut que certaines d'entre elles y soient insensibles. Si le cas se présente, vous en serez averti au cours de votre mission. Si vous choisissez cette Discipline, ajoutez 2 points d'HABILETÉ supplémentaires."},

    { id:'communication_animale', name:"Communication Animale",
      desc:"Grâce à cette technique, un Seigneur Kaï peut communiquer avec certains animaux et deviner les intentions de certains autres."},

    { id:'maitrise_psychique_de_la_matiere', name:"Maîtrise Psychique de la Matière",
      desc:"C'est une technique qui donne à un Seigneur Kaï la faculté de déplacer de petits objets par le simple pouvoir de sa concentration mentale."},


];

export default disciplines;
