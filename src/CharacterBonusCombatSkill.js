
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import * as bh from './book_helpers.js';

class CharacterBonusCombatSkill extends Component {

   render() {
       var bonus = bh.fight_get_combat_skill_bonus(this.props.character);

       const help = bh.fight_get_combat_skill_bonus_help(this.props.character);
       const items = help.map(function(item){
           return (<span key={item}> {item} <br/> </span>);
       });
       if (bonus >=0) bonus = '+'+bonus;

       if (items.length === 0) return '';
       
       return (
           <span className='Book-fight-help'>Total bonus : {' '}
             <span className='Book-endurance-char-bonus'> {bonus} </span>
             <br/>
             <span className='Book-fight-help'> {items} </span>
           </span>);

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CharacterBonusCombatSkill);
