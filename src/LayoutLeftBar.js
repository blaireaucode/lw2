/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React from 'react';
import PropTypes from 'prop-types';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import ZoomIn from '@material-ui/icons/ZoomIn';
import ZoomOut from '@material-ui/icons/ZoomOut';
import ZoomReset from '@material-ui/icons/YoutubeSearchedFor';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import update from 'immutability-helper';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import * as bh from './book_helpers.js';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faCogs } from '@fortawesome/free-solid-svg-icons';
import { faBook } from '@fortawesome/free-solid-svg-icons';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
library.add(faCogs);
library.add(faBook);

const drawerWidth = 170;

const styles = theme => ({
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    // shift down left menu
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
        //marginLeft: 'calc(100% - 970px)' // offset left bar
        // marginRight: 'calc(100% - 170px)'
    }
});

class LayoutLeftBar extends React.Component {

    handleDrawerToggle = () => {
        this.props.handleDrawerToggle();
    };

    scaleUp() {
        const s = this.props.book.globalScale+0.05;
        const newb = update(this.props.book, {globalScale: {$set:s}});
        this.props.book_set(newb);
    }
    
    scaleReset() {
        const newb = update(this.props.book, {globalScale: {$set:1.0}});
        this.props.book_set(newb);
    }
    
    scaleDown() {
        const s = this.props.book.globalScale-0.05;
        const newb = update(this.props.book, {globalScale: {$set:s}});
        this.props.book_set(newb);
    }

    
    render() {
        const { classes, theme } = this.props;
        const current_passage = '/passage/'+this.props.book.current_passage;
        //const h = 'Livre - '+this.props.book.current_passage;
        //var paragraph = '\xA7';  
        // const icon_style = {
        //     verticalAlign: 'middle',
        //     fontSize: '1.1em',
        //     color: '#eee'
        // };

        var thetitle = this.props.book.current_passage;
        if (thetitle === "0_0") thetitle = '0';
        if (thetitle === "0_1") thetitle = '0';
        if (thetitle === "0_2") thetitle = '0';
        if (thetitle === "0_3") thetitle = '0';
        if (thetitle === "0_4") thetitle = '0';
        var book_dis = false;
        if (thetitle === " ") book_dis = true;
        
        const h = (<span> Livre {' '} {/* <br/>  */}
                     {/* <FontAwesomeIcon style={icon_style} icon="book" /> {' '} */}
                     <span className='Book-fight-help'> 
                       {thetitle}
                     </span>
                   </span>);

        // \xA7
        const dead = bh.is_dead(this.props.character);
        var s = require('./images/w/lw.png');
        if (dead)
            s = require('./images/p/skull.png');
        // Perso <br/>
        const en = bh.fight_get_endurance_total(this.props.character);
        const c = (<span>
                     <img alt='item' width={25} src={s}/><br/>
                     
                     <span className='Book-fight-help'> 
                       H {this.props.character.combat_skill}
                       {' / '}
                       E {en}
                     </span>
                   </span>);
        
        const drawer = (
            <div>
              <div className={classes.toolbar} />
              <Divider />
              <List>
                <ListItem button component={Link} to='/character'>
                  <ListItemText primary={c}></ListItemText>
                </ListItem>
                <ListItem button disabled={book_dis} component={Link} to={current_passage}>
                  <ListItemText primary={h}></ListItemText>
                </ListItem>
                <ListItem button component={Link} to='/map'>
                  <ListItemText primary={'Carte'}></ListItemText>
                </ListItem>
                <ListItem button component={Link} to='/notes'>
                  <ListItemText primary={'Notes'}></ListItemText>
                </ListItem>
                {/* <ListItem button> */}
                {/*   <ListItemText primary={'Encyclo'}></ListItemText> */}
                {/* </ListItem> */}
              </List>
              <Divider />
              <List>
                <ListItem button component={Link} to='/options'>
                  {/* <FontAwesomeIcon style={icon_style} icon="cogs" /> */}
                  <ListItemText primary={'Options'}></ListItemText> 
                </ListItem>
                <ListItem button component={Link} to='/start'>
                  <ListItemText primary={'A propos'}></ListItemText>
                </ListItem>
              </List>
              <Divider />
              <div>
                <IconButton onClick={this.scaleUp.bind(this)}> 
                  <ZoomIn onClick={this.scaleUp.bind(this)}/>
                </IconButton> 
                <IconButton onClick={this.scaleReset.bind(this)}> 
                  <ZoomReset onClick={this.scaleReset.bind(this)}/>
                </IconButton> 
                <IconButton onClick={this.scaleDown.bind(this)}> 
                  <ZoomOut onClick={this.scaleDown.bind(this)}/>
                </IconButton> 
              </div>
            </div>
        );

        return (
            <div>
              {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
              <Hidden smUp implementation="css">
                <Drawer
                  container={this.props.container}
                  variant="temporary"
                  anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                  open={this.props.mobileOpen}
                  onClose={this.handleDrawerToggle}
                  classes={{ paper: classes.drawerPaper }}
                >
                  {drawer}
                </Drawer>
              </Hidden>
              <Hidden xsDown implementation="css">
                <Drawer
                  classes={{ paper: classes.drawerPaper }}
                  variant="permanent"
                  open
                >
                  {drawer}
                </Drawer>
              </Hidden>
            </div>
        );
    }
}

LayoutLeftBar.propTypes = {
    classes: PropTypes.object.isRequired,
    // Injected by the documentation to work in an iframe.
    // You won't need it on your project.
    container: PropTypes.object,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(LayoutLeftBar));
