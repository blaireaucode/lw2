
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import CKEditor from '@ckeditor/ckeditor5-react';
import Editor from '@ckeditor/ckeditor5-build-inline';
import * as bh from './book_helpers.js';
import './custom.css';

const editorConfiguration = {
    toolbar: [ 'bold', 'italic', '|', 'numberedList', 'bulletedList', '|', 'undo', 'redo']
};


class NotesEditor extends Component {

    constructor(props) {
        super(props);
        this.data = '';
    }

    render() {
        const content = this.props.character.notes;
        return (
            <div>

              <h3>Carnet de notes</h3>
              
              <p/>

              <div className='Notes'>
                <CKEditor
                  editor={ Editor }
                  config={ editorConfiguration }
                  data={content}
                  onInit={ editor => {
                      // You can store the "editor" and use when it is needed.
                      // console.log( 'Editor is ready to use!', editor ); 
                      //editor.data ="toto";//bh.notes_get_data();                    
                  } }
                  onChange={ ( event, editor ) => {
                      const data = editor.getData(); 
                      this.data = data;
                  } }
                  onBlur={ editor => {
                      if (this.data !== '')
                          bh.notes_save(this.props, this.data);
                  } }
                  onFocus={ editor => {
                  } }
                />
              </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NotesEditor);
