/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';

class Map extends Component {

    constructor(props) {
        super(props);
        this.s = require('./images/map2.png');
    }

    render() {
        return (
            <div>
              Les royaumes libres du Nord de Magnamund.
              <p/>
              <map className='PassageImageDiv'>
                <img alt='2' className='PassageImage' src={this.s}/>
              </map>
            </div>
        );
    }
}

export default Map;
