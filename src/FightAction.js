/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import update from 'immutability-helper';
import Button from '@material-ui/core/Button';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import * as bh from './book_helpers.js';
const ft = require('./fight_table.js');

class FightAction extends Component {

    constructor(props) {
        super(props);
        //this.s = require('./images/brawl008-512.png');
        //this.s = require('./images/Knight_swords-512.png');
        this.s = require('./images/Knight_swords-512-white.png');
        this.state = { round:-1 };
    }

    attack() { // FIXME --> to put in book_helpers (?)
        const fight = this.props.fight;

        // First, only get random number to start dice animation
        // random : 0-9
        var r = Math.floor(Math.random() * 10);
        r = Math.max(-11, r);
        r = Math.min(11, r);

        // set current round to disable the fight button
        this.setState({round:fight.round});

        // update the current dice value + key for the animation
        var fdice = update(fight, {current_dice: {$set:r}} );
        const k = r+'_'+fight.round;
        fdice = update(fdice, {current_k: {$set:k}} );
        this.props.book_set_element(fdice);

        // resolve the attack in few seconds
        if (!this.props.options.display_dice)
            this.attack_resolve(r);
        else {
            setTimeout(function() {
                this.attack_resolve(r);
            }.bind(this), 3000);
        }
    }

    attack_resolve(r) {
        const character = this.props.character;
        var fight = this.props.fight;
        
        // Combat Ratio (quotient d'attaque (CR) hab_lw - hab_enc
        const cr = bh.fight_combat_ratio(character, fight);
        
        // combat result : from -11 to +12
        const block = Math.ceil((cr+11)/2);
        //console.log('block', block);
        var index = (block*10)+r-1; // magic !
        if (index < 0) index =0;
        if (index >= ft.CombatTable.length) index = ft.CombatTable.length-1;
        const damage = ft.CombatTable[index];
        var damage_character = parseInt(damage[1], 10);
        var damage_encounter = parseInt(damage[0], 10);
        // console.log('dmg', damage);

        if (fight.bonus_factor_dmg_encounter) {
            damage_encounter *= fight.bonus_factor_dmg_encounter;            
        }

        if (fight.bonus_dmg_character > 0) {
            damage_character -= parseInt(fight.bonus_dmg_character, 10);            
        }

        if (fight.no_damage_first_rounds) {
            if (fight.no_damage_first_rounds >= fight.round) {
                damage_character = 0;
            }
        }

        if (fight.psychic_attack === true) {
            const d = bh.has_discipline(character, "bouclier_psychique");
            if (d) {
                if (fight.round === 1)
                    fight.help.push("Vous résistez à l'attaque psychique");
            }
            else {
                if (fight.round === 1)
                    fight.help.push("Dommage +2 à cause de la puissance psychique");
                damage_character -= 2;
            }
        }

        /// max ?
        if (damage_encounter <= -666) damage_encounter = -this.props.fight.endurance;

        // update character
        //const ce = Math.max(0, parseInt(character.endurance)+damage_character);
        const ce = parseInt(character.endurance)+damage_character;
        const c = update(character, {endurance: {$set:ce}} );

        // update fight, encounter
        const ee = Math.max(0, parseInt(this.props.fight.endurance)+damage_encounter);
        const previous = {
            combat_ratio:cr,
            rnd_dice:r,
            damage_character:damage_character,
            damage_encounter:damage_encounter
        };
        // set new encounter endurance
        var f = update(fight, {endurance: {$set:ee}} );
        f = update(f, {previous: {$set:previous}});
        // set new round
        var round = 0;
        if (f.round) round = f.round;
        f = update(f, {round: {$set:round+1}});
        // set total points lost
        var t = 0;
        if (!f.total_damage) t = damage_character;
        else t = f.total_damage + damage_character;
        f = update(f, {total_damage: {$set:t}});
        
        // Callback
        if (this.props.handleRound)
            f = this.props.handleRound(f);

        // Update bonus and (help)
        f = bh.book_fight_update(c, f);
        this.props.book_set_element(f);
        this.props.character_set(c);
        
        if (f.status !== 'fighting') {
            if (this.props.handleEndFight)
                this.props.handleEndFight(f);
        }
    }

    render() {
        const cr = bh.fight_combat_ratio(this.props.character, this.props.fight);
        return (<div>
      <span  className={'Book-fight-button'}>
        Tour n°{this.props.fight.round}<br/>
        Quotient d'Attaque : {cr}
        <Button variant="contained"
                color="primary"
                disabled={this.state.round === this.props.fight.round}
                onClick={this.attack.bind(this)}
                style={{border:3}}>
          &nbsp;&nbsp;
          <img  alt='fight' width={30} src={this.s}/>
          &nbsp;
        </Button>
        <br/>
      </span>
    </div>
               );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FightAction);
