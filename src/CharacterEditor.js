
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import DisciplineEditor from './DisciplineEditor.js';
import CharacterHCEditor from './CharacterHCEditor.js';
import CharacterItemsEditor from './CharacterItemsEditor.js';
import { Link } from 'react-router-dom';
import L from './L.js';
import update from 'immutability-helper';
import * as bh from './book_helpers.js';

class CharacterEditor extends Component {

    constructor(props) {
        super(props);
        this.state = { step:0 };
    }

    render() {
        const style = {
            transformOrigin: 'top center',
            transform: 'scale('+this.props.book.globalScale+')'
        };

        if (this.state.step === 0) return this.render0(style);
        if (this.state.step === 1) return this.render1(style);
        if (this.state.step === 2) return this.render2(style);
        return this.render0(style);
    }

    handleNext() {
        this.setState({...this.state, step:this.state.step+1});
    }    

    handlePrevious() {
        this.setState({...this.state, step:this.state.step-1});
    }    

    reset() {
        bh.reset_book(this.props);
        const b = update(this.props.book, { current_passage: {$set:'0_0'}});
        this.props.book_set(b);
        const c = update(this.props.character, { history: {$set:[]}});
        this.props.character_set(c);
    }
    
    handleWeapon() {
        const d = Math.floor(Math.random() * 10);
        const list = ['poignard', 'lance', 'masse_d_armes',
                      'sabre', 'marteau_guerre', 'epee',
                      'hache', 'epee', 'baton', 'glaive'];
        const id = list[d];
        const c = {...this.props.character, weapon_master:id};
        this.props.character_set(c);
    }    

    render0(style) {        
        return (
            <div style={style} className='Book-character-editor'>

              <CharacterHCEditor/>

              <p/>
              <Link to={'edit'} onClick={this.handleNext.bind(this)} >Suite (équipement)</Link>
              
            </div>
        );
    }
    render1(style) {        
        return (
            <div style={style} className='Book-character-editor'>

              <CharacterItemsEditor/>

              <p/>
              <Link to={'edit'} onClick={this.handlePrevious.bind(this)} >Précédent </Link> / {' '}
              <Link to={'edit'} onClick={this.handleNext.bind(this)} > Suite (disciplines)</Link>
              
            </div>
        );
    }

    render2(style) {        
        var w = '';
        const d = bh.has_discipline(this.props.character, "maitrise_arme");
        if (d) {
            w = (<div>
                   <Link to={'edit'} onClick={this.handleWeapon.bind(this)}>
                     Choix aléatoire de l'arme maîtrisée</Link>
                   <p/>
                 </div>
                );
        }    
        return (
            <div style={style} className='Book-character-editor'>

              <DisciplineEditor edit_mode={true}/>
              <p/>
              {w}
              
              <Link to={'edit'} onClick={this.handlePrevious.bind(this)} >Précédent </Link> / {' '}
              <L onClick={this.reset.bind(this)} to={'0_0'}>Démarrer l'aventure ...</L>
              
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CharacterEditor);
