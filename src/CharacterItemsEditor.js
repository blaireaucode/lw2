
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import { Link } from 'react-router-dom';
import ItemsPicker from './ItemsPicker.js';
import update from 'immutability-helper';

class CharacterItemsEditor extends Component {

    constructor(props) {
        super(props);
        const d1 = Math.floor(Math.random() * 10);
        const n = d1+10;
        const f = {
            'name': 'obj1',
            'passage_title': 'edit',
            'dice': n-10,
            'list': [
                {id:'PO', nb:n},
                {id:'sceau_hammardal'},                
                {id:'epee'},
                {id:'sabre'},
                {id:'repas', nb:2},
                {id:'mail'},
                {id:'shield'},
                {id:'masse_d_armes'},
                {id:'baton'},
                {id:'lance'},
                {id:'glaive'},
                {id:'potion_guerison', nb:1},
                
            ],
        };
        this.items = f;
    }

    handleClick() {
        const c = this.items.passage_title;
        const newp = update(this.props.book.p, { [c]: { $set: {} } });
        const newb = update(this.props.book, {p: {$set:newp}});
        this.props.book_set(newb);
        const d1 = Math.floor(Math.random() * 10);
        const n = d1+10;
        this.items.list[0].nb= n;
        this.items.dice = n-10;
        const char = update(this.props.character, {
            items: {$set: []}});
        this.props.character_set(char);
    }    
    
    render() {
        return (
            <div>

              Le Capitaine Gayal, de la Garde du Roi, vous a conduit à
              l'Arsenal du Palais où l'on prend soin de réparer et de
              nettoyer votre tunique verte et votre cape de Seigneur
              Kaï&nbsp;; en attendant que vos vêtements vous soient rendus,
              le Capitaine Gayal vous donne pour votre voyage une
              bourse remplie d'or. Pour savoir combien d'or la bourse
              contient, lancer un dé et ajoutez 10 au
              chiffre obtenu. Le total représente le nombre de Pièces
              d'Or qui se trouvent dans la bourse ; vous inscrirez ce
              total dans la case Pièces d'Or de votre Feuille
              d'Aventure et, si vous avez mené à bien la mission du
              livre numéro 1, vous ajouterez alors cette somme au
              nombre de pièces d'or que peut-être vous possédez
              déjà. Au centre de l'Arsenal, divers objets ont été
              disposés à votre intention sur une grande table. Vous
              avez le droit d'emporter avec vous deux de ces objets, à
              choisir dans la liste suivante.

              <p/>
              <Link onClick={this.handleClick.bind(this)} to="edit">
                Lancer le dé ...</Link>
              <br/><br/>
              <ItemsPicker limit={2} items={this.items}/>

              <p/>

              Pour vous aider à trouver votre chemin, vous disposez
              d'une carte que vous avez sauvée des cendres du
              monastère détruit. Au départ de votre quête, vous
              êtes en possession du Sceau d'Hammardal. C'est un
              anneau que vous portez à un doigt de votre main droite.

              
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CharacterItemsEditor);
