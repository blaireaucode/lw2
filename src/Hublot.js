/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from './L.js';
import HelpGold from './HelpGold.js';
import update from 'immutability-helper';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import * as bh from './book_helpers.js';

class Hublot extends Component {

    go() {
        const e = bh.book_get_element(this.props, this.props.r);

        const r1_1 = Math.floor(Math.random() * 10);
        const r1_2 = Math.floor(Math.random() * 10);
        const t1 = r1_1+r1_2;
        const r2_1 = Math.floor(Math.random() * 10);
        const r2_2 = Math.floor(Math.random() * 10);
        const t2 = r2_1+r2_2;
        const r3_1 = Math.floor(Math.random() * 10);
        const r3_2 = Math.floor(Math.random() * 10);
        const t3 = r3_1+r3_2;

        var h1 = 'Le premier joueur obtient '+r1_1+' et '+r1_2+', soit un total de '+t1+'.';
        var h2 = 'Le second joueur obtient '+r2_1+' et '+r2_2+', soit un total de '+t2+'.';
        var h3 = 'Et vous-même: '+r3_1+' et '+r3_2+', soit un total de '+t3+'.';

        const bet = 3;
        var w = -bet;
        var hr = 'Vous perdez votre mise ...';
        if (r1_1 === 0 && r1_2 === 0) {
            h1 += ' HUBLOT !!';
            h2 = '';
            h3 = '';
        }
        else
            if (r2_1 === 0 && r2_2 === 0) {
                h2 += ' HUBLOT !!';
                h3 = '';
            }
        else
            if (r3_1 === 0 && r3_2 === 0) {
                h3 += ' HUBLOT !!';
            }
        else
            if (t3 > t2 && t3 > t1) {
                w = 6;
                hr = ' Total supérieur ! Vous gagnez 6 Couronnes.';
            }
        else
            if (t3 === t2 === t1) {
                w = 0;
                hr = hr + ' Egalité ! La partie est annulée, vous récupérez votre mise.';
            }
        
        const t = w+e.total_win;
        const c = bh.gold_add(this.props.character, w);
        this.props.character_set(c);
        var elem = update(e, { h1:{$set:h1}});
        elem = update(elem, { h2:{$set:h2}});
        elem = update(elem, { h3:{$set:h3}});
        elem = update(elem, { hr:{$set:hr}});
        elem = update(elem, { total_win:{$set:t}});
        this.props.book_set_element(elem);
    }
    
    render() {
        const r = this.props.r;
        if (!r) return ('');
        const e = bh.book_get_element(this.props, r);       
        const g = bh.gold_get(this.props.character);
        return (
            <div>
              <p/>

              <L enabled={g>=3} to={e.passage_title} onClick={this.go.bind(this)}>Lancez les dés !</L>
              <p/>
              {e.h1}
              <p/>
              {e.h2}
              <p/>
              {e.h3}
              <p/>
              {e.hr}
              <p/>
              Gain jusqu'à présent : {e.total_win}
              <p/>
              <HelpGold/>
              <p/>
              
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Hublot);
