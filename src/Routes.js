/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
//import { BrowserRouter as Router } from 'react-router-dom';
import { HashRouter as Router } from 'react-router-dom';
import Layout from './Layout.js';
import Character from './Character.js';
import CharacterEditor from './CharacterEditor.js';
import Book from './Book.js';
import Map from './Map.js';
import Options from './Options.js';
import Debug from './Debug.js';
import DebugItems from './DebugItems.js';
import NotesEditor from './NotesEditor.js';
import Start from './Start.js';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';

class Routes extends Component {

    render() {
        //console.log('Public url', process.env.PUBLIC_URL);
        //console.log('win loc href', window.location.href);
        //console.log('this ', this);
        
        return (
            <Router basename={process.env.PUBLIC_URL}>
              <div>
                <Layout path="/character" component={Character} />
                <Layout path="/map" component={Map} />
                <Layout path="/options" component={Options} />
                <Layout path="/debug" component={Debug} />
                <Layout path="/debugitems" component={DebugItems} />
                <Layout path="/edit" component={CharacterEditor} />
                <Layout path="/notes" component={NotesEditor} />
                <Layout path="/start" component={Start} />
                <Layout path="/passage" component={Book} />
                <Layout exact path="/" component={Book} />
              </div>
            </Router>
        );
    }
}

//export default Routes;
export default connect(mapStateToProps, mapDispatchToProps)(Routes);
