/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import Item from './Item.js';
import * as bh from './book_helpers.js';

class ItemsList extends Component {

    render() {
        const items = this.props.items;
        if (!items) return (<span/>);
        var list = [];
        var k=0;
        var first = true;
        for (var item of items) {
            const item_desc = bh.item_get_description(item.id);
            //console.log('item', item, item_desc);
            if (this.props.special && item_desc.special !== true) continue;
            if (this.props.special === false && item_desc.special === true) continue;
            if (this.props.weapon === true  && item_desc.type !== 'weapon') continue;
            if (this.props.weapon === false && item_desc.type === 'weapon') continue;
            k = k + 1;
            var t=', ';
            if (first) {
                t = '';
                first = false;
            }
            const i = (<span key={k}>{t}
                         <Item item={item}
                               itemSelected={this.props.itemSelected}/>
                       </span>);
            list.push(i);
        }
        
        return (
            <span>
              {list}
            </span>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemsList);
