/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import DisciplinePanel from './DisciplinePanel.js';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';

class DisciplineList extends Component {

    render() {
        if (this.props.disc.length === 0) {
            return (
                <span>
                  aucune discipline
                </span>
            );
        }
        const items =
              this.props.disc.map((d) =>
                                  <DisciplinePanel key={d.id} mode={this.props.mode} discipline={d}/>
                                 );
        return (
            <div className="DisciplineList">
              {items}
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DisciplineList);
