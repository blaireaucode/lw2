/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import ItemsList from './ItemsList.js';
import update from 'immutability-helper';
import * as bh from './book_helpers.js';

class ItemsPicker extends Component {

    constructor(props) {
        super(props);
        this.state = { items: false, help:'' };
    }

    componentWillReceiveProps(nextProps) {
        const items = bh.book_get_element(nextProps, this.props.items);

        // loop check cost and gold --> change if not possible
        const g = bh.gold_get(nextProps.character);
        for (var it of items.list) {
            if (it.cost >0) {
                if (g<it.cost) {
                    it.clickable = false;
                }
            }
            this.props.book_set_element(items);
        }
        
        this.setState( {items:items});
    }

    componentDidMount() {        
    }

    pick(picked_item) {
        
        this.setState({...this.state, help:''});
        // Retrieve passage element
        const p = bh.book_get_element(this.props, this.props.items);
        // Find the item number in the list 
        const index = bh.item_get_index(p.list, picked_item.id);

        if (this.props.limit && picked_item.id !=='PO' && picked_item.id !== 'sceau_hammardal') {
            var n = this.props.character.items.length;
            var ind = bh.item_get_index(this.props.character.items, 'PO');
            if (ind !== -1) n-=1;
            ind = bh.item_get_index(this.props.character.items, 'sceau_hammardal');
            if (ind !== -1) n-=1;            
            if (n>=2) {
                this.setState({...this.state, help:'(Vous ne pouvez pas prendre plus de deux éléments)'});
                return;
            }
        }
        
        // Check if possible
        if (!this.props.options.items_nolimit)  {
            const desc = bh.item_get_description(picked_item.id);
            
            if (desc.type === 'weapon') {
                const n = bh.item_get_nb_of_weapon(this.props.character);
                if (n>=2) {
                    this.setState({...this.state, help:'(Vous ne pouvez pas avoir plus de 2 armes)'});
                    return;
                }
            }

            if (bh.item_get_if_in_sacados(picked_item.id)) {
                const n = bh.item_get_nb_of_item_in_sacados(this.props.character);
                var lim = 8;              
                if (n>=lim) {
                    this.setState({...this.state,
                                   help:'(Vous ne pouvez pas avoir plus de 8 objets dans le sac à dos)'});
                    return;
                }
            }
            
        }
        
        // If cost, remove gold
        var c = this.props.character;
        if (picked_item.cost > 0) {
            c = bh.gold_add(this.props.character, -picked_item.cost);
            this.props.character_set(c);
        }
        // Remove the item from the list
        const np = update(p, {list: {$splice: [[index,1]]}});
        // Update
        this.props.book_set_element(np);
        // Pick the item
        var i = picked_item;
        if (picked_item.cost > 0) {
            i = update(picked_item, {cost: {$set:0}});
        }
        c = bh.item_pick(c, i);
        this.props.character_set(c);
    }

    drop(dropped_item) {
        this.setState({...this.state, help:''});
        const p = bh.book_get_element(this.props, this.props.items);
        // Drop item
        const c = bh.item_drop(this.props.character, dropped_item.id);
        this.props.character_set(c);
        // If nb, get only one 
        if (dropped_item.nb > 1) dropped_item.nb = 1;
        // Add in the room list
        const new_list = bh.item_list_add(p.list, dropped_item);
        const np = update(p, {list: {$set: new_list}});
        this.props.book_set_element(np);
    }

    render() {
        const items = this.state.items;
        if (!items) {
            this.componentWillReceiveProps(this.props);
            return (<span/>);
        }
        var a = (<ItemsList items={items.list} itemSelected={this.pick.bind(this)}/>);
        if (items.list.length === 0) {
            a = (<span className='Book-randomtable'>aucun</span>);
        }
        return (
            <div className='Book-item-picker'>
              <p/>
              <i>Objets :</i>  {' '}
              {a}
              <p/> 
              <i>Sac à dos :</i> {' '}
              <ItemsList items={this.props.character.items} itemSelected={this.drop.bind(this)}/>
              <p/> <span className={'Book-fight-help'} >{this.state.help}</span>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemsPicker);
