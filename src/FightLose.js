/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Dead from './Dead.js';
import FightEncounter from './FightEncounter.js';

class FightLose extends Component {

    render() {
        const fight = this.props.fight;
        var txt = 'en un seul tour';
        if (fight.round >1)
            txt = 'en '+fight.round+' tours';
        return (
            <span>
              <FightEncounter fight={fight}/><p/>
              <span className="Book-dead">
                Arrghhhhhhh ! Vous venez d'être tué par {fight.encounter}, {txt}.
                <Dead/>
              </span>
            </span>
        );
    }
}

export default FightLose;
