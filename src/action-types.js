/*
 * Copyright 2018
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

export const CHARACTER_SET = "CHARACTER_SET";
export const BOOK_SET_ELEMENT = "BOOK_SET_ELEMENT";
export const BOOK_SET = "BOOK_SET";
export const OPTIONS_SET = "OPTIONS_SET";
