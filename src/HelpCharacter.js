/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps} from './book_default_props.js';
import * as bh from './book_helpers.js';

class HelpCharacter extends Component {
    render() {

        const character = this.props.character;
        const cs =  bh.fight_get_combat_skill_total(character);
        const en = bh.fight_get_endurance_total(character);
        return (
            <span className='Book-help'>
              Vous avez actuellement {' '} <span
              className='Book-help-hl'> {' '}
              {en}{' '} </span> {' '}
              points d'ENDURANCE et {' '} <span
              className='Book-help-hl'>{' '}
              {cs}{' '} </span> {' '}
              d'HABILETÉ.
              
            </span>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HelpCharacter);
