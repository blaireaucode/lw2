
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import Item from './Item.js';
import ItemDisplay from './ItemDisplay.js';
import * as bh from './book_helpers.js';

class DebugItems extends Component {

    render() {
        const items = bh.items_description;

        var i = [];
        for(var item of items) {
            console.log('it', item);
            item.clickable = false;
            const a = (<span  key={item.id}>
                         <Item item={item} />
                         <ItemDisplay item={item} no_check={true}/>
                       </span>);
            i.push(a);
        }
        return (
            <div>
              <p/>
              {i}
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DebugItems);
