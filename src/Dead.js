/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import * as bh from './book_helpers.js';

class Dead extends Component {
    render() {
        const dead = bh.is_dead(this.props.character);
        if (!dead) {
            const e = bh.fight_get_endurance_bonus(this.props.character);
            const c = bh.endurance_set(this.props.character, -e);
            this.props.character_set(c);
            // Add dead in the history
            const el = {
                passage_title:this.props.book.current_passage,
                name:'dead',
                dead:true
            };
            bh.book_get_element(this.props, el);
            
        }        
        return ('');
        //     // <div>
        //     //   {/* <SimpleImage width={80} src={'skull.png'}/> */}
        //     //   {/* <p/> */}
        //     //   {/* <Link to='/edit'>Recommencer ...</Link> */}
        //     // </div>
        // );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dead);

