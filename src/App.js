/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import './App.css';
import Theme from './theme.json';
import store from './store.js';
import Routes from './Routes.js';

const theme = createMuiTheme(Theme);

class App extends Component {
    render() {
        return (
            <Provider store={store}>
              <MuiThemeProvider theme={theme}>
                <div className="App">
                  <header className="App-header">
                    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
                    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" />
                    <Routes/>
                  </header>
                </div>
              </MuiThemeProvider>
            </Provider>
        );
    }
}

export default  App;
