/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import update from 'immutability-helper';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import Switch from '@material-ui/core/Switch';

class SwitchOptions extends Component {

    constructor(props) {
        super(props);
        this.state = {value: this.props.options[this.props.option]};
    }

    componentDidUpdate() {
        const c = this.props.options[this.props.option];
        if (this.state.value !== c)
            this.setState({value:c});
    }
    
    toggle(name, event) {
        const options = this.props.options;
        const c = event.target.checked ;
        const o = update(options, { [name]: {$set: c}});
        this.setState({value:c});
        this.props.options_set(o);
    }

    render() {
        // console.log('switchoptions options',
        //            this.props.options,
        //             this.props.label,
        //             this.props.option,);
        const options = this.props.options;
        const op = this.props.option;
        const i={ 'aria-label': 'primary checkbox' };
        return (
            <span>
              {this.props.label}
              <Switch
                checked={this.state.value}
                onChange={this.toggle.bind(this, op)}
                color="secondary"
                inputProps={i}
              /> {options[op]===true? 'oui':'non'}
            </span>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SwitchOptions);
