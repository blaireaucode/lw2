/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

const default_character = {
    test: 0,
    kai_disciplines: [],
    combat_skill: 0,
    endurance: 1,
    endurance_max:1,
    weapon_master:'epee',
    items: [ // put nb if numerable (repas etc)
        //{ id:'baton'},
        //{ id:'epee'},
    ],
    notes: 'cliquez ici ...',
    history: []
};

export default default_character;

