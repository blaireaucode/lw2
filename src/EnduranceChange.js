/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import * as bh from './book_helpers.js';
import update from 'immutability-helper';
import L from './L.js';

class EnduranceChange extends Component {

    constructor(props) {
        super(props);
        this.state = { element: false};
    }

    componentWillReceiveProps(nextProps) {
        var element = bh.book_get_element(this.props, this.props.f);
        if (!element) {
            element = this.props.book_set_element(this.props.f);
        }
        this.setState( {element:element});
    }

    handleClick() {
        const elem = bh.book_get_element(this.props, this.props.f);
        if (!elem.already_done) {
            const e = update(elem, {already_done: {$set: true}});
            this.props.book_set_element(e);
            const c = bh.endurance_add(this.props.character, e.dmg);
            this.props.character_set(c);
            this.setState( {element:e});
        }        
    }

    render() {
        const element = bh.book_get_element(this.props, this.props.f);
        if (!element) return '';
        const p = element.passage_title;
        const r = !element.already_done && this.props.enabled;
        // console.log('enab', r, this.props.enabled);
        return (
            <span>
              <L to={p} enabled={r} onClick={this.handleClick.bind(this)}>
                {this.props.children}
              </L>
            </span>
        );
    }
}

// Set default props
EnduranceChange.defaultProps = {
    enabled: true
};


export default connect(mapStateToProps, mapDispatchToProps)(EnduranceChange);

