
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React from 'react';
import ItemPotion from './ItemPotion.js';

export const items_description = [

    {id:'glaive',
     name:'Glaive',
     name_plural:'Glaives',
     type:'weapon',
     img:'bsword.png'},
    
    {id:'glaive_sommer',
     name:'Glaive de Sommer',
     name_plural:'Glaive de Sommer',
     type:'weapon',
     img:'bsword_sommer.png',
     special:true
     //img:'a.png'
    },
    
    {id:'epee',
     name:'Épée',
     name_plural:"Épées",
     type:'weapon',
     img:'sword.png'},
    
    {id:'sabre',
     name:'Sabre',
     name_plural:"Sabres",
     type:'weapon',
     img:'ssword.png'},
    
    {id:'masse_d_armes',
     name:"Masse d'Armes",
     name_plural:"Masses d'Armes",
     type:'weapon',
     img:'mace.png'},
    
    {id:'marteau_guerre',
     name:"Marteau de Guerre",
     name_plural:"Marteaux de Guerre",
     type:'weapon',
     img:'warhammr.png'},
    
    {id:'hache',
     name:"Hache",
     name_plural:"Haches",
     type:'weapon',
     img:'axe.png'},

    {id:'potion_guerison',     
     name:"Potion de Guérison",     
     name_plural:"Potions de Guérison",     
     img:'potion.png',     
     desc:"Cette potion vous redonne 4 points d'ENDURANCE si vous la buvez après un combat.",
     f: () => { return ( <ItemPotion item_id={'potion_guerison'} bonus={4}/> );}
     //create_potion
    },
    
    {id:'baton',
     name:'Bâton',
     name_plural:"Bâtons",
     type:'weapon',
     img:'qstaff.png'},
    
    {id:'lance',
     name:'Lance',
     name_plural:"Lances",
     type:'weapon',
     img:'spear.png'},
    
    {id:'magical_spear',
     name:'Lance magique',
     name_plural:"Lances magique",
     type:'weapon',
     special:true,
     img:'magical_spear.png'},

    {id:'repas',
     name:'Repas',
     name_plural:'Repas',
     img:'food.png'},
    
    {id:'PO',
     name:"Pièce d'or",
     name_plural:"Pièces d'or",
     img:'goldcoins.png'},
    
    // {id:'sac_a_dos',
    //  name:"Sac à dos",
    //  size:100,
    //  name_plural:"Sacs à dos",
    //  img:'sacdos2.png'
    // },
    
    {id:'billet_Port_Bax',
     name:'Billet pour Port Bax',
     special:true,
     img:'pass_for_bax.png'},
    
    {id:'poignard',
     name:'Poignard',
     name_plural:"Poignards",
     type:'weapon',
     img:'dagger.png'},
    
    {id:'herbe_laumspur',
     name:'Herbe de Laumspur',
     name_plural:"Herbes de Laumspur",
     size:60,
     img:'laumspur_herb.png',
     desc:"Le Laumspur est une herbe délicieuse très recherchée d'un bout à l'autre des Fins de Terre en raison de ses vertus curatives. Elle vous rendra 3 points d'ENDURANCE lorsque vous la consommerez. ",
     f: () => { return ( <ItemPotion item_id={'herbe_laumspur'} bonus={3}/> );}
    },
    
    {id:'fiole_laumspur',
     name:'Fiole de Laumspur',
     name_plural:"Fioles de Laumspur",
     img:'potion.png',
     desc:"Il s'agit d'une puissante potion de guérison qui vous permettra de récupérer 5 points d'ENDURANCE si vous la buvez après un combat. ",
     f: () => { return ( <ItemPotion item_id={'fiole_laumspur'} bonus={5}/> );}
    },
    
    {id:'fiole_orange',
     name:"Fiole d'un liquide orange",
     name_plural:"Fioles de liquide orange",
     img:'potion.png'
    },
    
    {id:'sceau_hammardal',
     name:"Sceau d'Hammardal",
     name_plural:"Sceaux d'Hammardal",
     special:true,
     size:220,
     img:'seal.png'},
    
    {id:'mail',
     name:"Cotte de mailles",
     name_plural:"Cottes de mailles",
     special:true,
     img:'mail.png'},
    
    {id:'shield',
     name:"Bouclier",
     name_plural:"Bouclier",
     special:true,
     img:'shield.png'},
    
    {id:'pendentif_etoile_cristal',
     name:"Pendentif de l'Etoile de Cristal",
     img:'pendent_crystal_star.png',
     special:true,
     size:100,
     name_plural:"Pendentifs de l'Etoile de Cristal"},
    
    {id:'laissez_passer',
     name:"Laissez-passer",
     name_plural:"Laissez-passer",
     size:80,
     img:'pass3.png'
    },
    
    {id:'laissez_passer_rouge',
     name:"Laissez-passer rouge",
     name_plural:"Laissez-passer rouge",
     size:80,
     img:'pass3.png'
    },
    
    {id:'couverture',
     name:"Couverture",
     img: 'blanket.png',
     size:100,
     name_plural:"Couvertures"},    
    
    {id:'corde',
     name:"Corde (100m)",
     img:'rope.png',
     size:120,
     name_plural:"Cordes (100m)"},

    // {id:'anneaux_d_or',
    //  name:"Annneaux d'or",
    //  img: 'ring.png',
    //  size:100,
    //  name_plural:"Annneaux d'or"},    
    
];

