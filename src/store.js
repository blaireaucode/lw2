/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import { createStore } from 'redux';
import * as at from './action-types.js';
import default_character from './default_character.js';
import default_book from './default_book.js';
import default_options from './default_options.js';
import * as gr from './action-reducers.js';
const deepmerge = require('deepmerge');

const store = setupStore();
export default store;

export function setupStore()
{
    // read from local store (if exist)
    const initial_character = JSON.parse(global.localStorage.getItem("character")) || {};
    const initial_book = JSON.parse(global.localStorage.getItem("book")) || {};
    const initial_options = JSON.parse(global.localStorage.getItem("options")) || {};
    console.log('Store read', initial_character, initial_book, initial_options);

    // merge with initialState (to add non stored elements)
    const c = deepmerge(default_character, initial_character);
    const b = deepmerge(default_book, initial_book);
    const o = deepmerge(default_options, initial_options);
    const initialState = { character:c, book:b, options:o };
    console.log('Start character', initialState.character);
    console.log('Start book', initialState.book);
    console.log('Start options', initialState.options);
    
    // list of action
    const rootReducer = (state = initialState, action) => {
        switch (action.type) {
        case at.CHARACTER_SET:
            return gr.character_set(state, action.character);
        case at.BOOK_SET_ELEMENT:
            return gr.book_set_element(state, action.element);
        case at.BOOK_SET:
            return gr.book_set(state, action.book);
        case at.OPTIONS_SET:
            return gr.options_set(state, action.options);
        default:
            return state;
        }
    };

    return createStore(rootReducer);
}

