/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import * as bh from './book_helpers.js';
import { connect } from 'react-redux';
import L from './L.js';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';

class ItemDisplay extends Component {


    drop() {
        const item = this.props.item;
        const c = bh.item_drop(this.props.character, item.id);
        this.props.character_set(c);
    }
    
    render() {
        const item = this.props.item;

        // no item
        if (!item) return (<span/>);

        // Check item is in the character
        var it = item;
        if (this.props.no_check !== true) {
            var index = bh.item_get_index(this.props.character.items, item.id);
            if (index === -1) return (<span/>);
            it = this.props.character.items[index];
        }
       
        // get the associated image
        const desc = bh.item_get_description(it.id);
        var s = '';
        if ('img' in desc) {
            s = require('./images/wn/'+desc.img);
        }

        // get text with nb if needed 
        var item_txt = '';
        if (it.nb > 1) {
            item_txt = (<span>{it.nb}&nbsp;X </span>);//
        }

        // description
        var item_desc = desc.desc;
        if (desc.f) {
            const c = desc.f();
            item_desc = (<span className='Book-help'>
                           {desc.desc} <br/>
                           {c}
                         </span>);
        }
        
        // render without link
        var size = 250;
        if (it.id === 'PO') size=80;
        if (desc.size) size = desc.size;
        if (s === '') {
            return (
                <div className='PassageImageDiv'>
                  {item_txt}
                  {item_desc}
                </div>
            );
        }

        // If drop possible
        var drop = '';
        drop = (<L onClick={this.drop.bind(this)} to='/character'>Jeter cet objet</L>);
        return (
            <div className='PassageImageDiv'>
              {item_txt}
              <img alt='item' width={size} src={s}/>
              {item_desc}
              {drop}
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemDisplay);
