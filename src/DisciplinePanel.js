/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import update from 'immutability-helper';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Grid from '@material-ui/core/Grid';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Icon from '@material-ui/core/Icon';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import * as bh from './book_helpers.js';

class DisciplinePanel extends Component {

    add() {

        if (!this.props.options.disc_nolimit)  {
            // console.log('ici');
            const n = this.props.character.kai_disciplines.length;
            if (n >= 5) return;
        }
        
        const l = update(this.props.character.kai_disciplines,
                         {$push: [this.props.discipline]} );
        const c = update(this.props.character,
                         {kai_disciplines: {$set: l}} );
        this.props.character_set(c);
    }

    remove() {
        const d = this.props.character.kai_disciplines.slice();
        const index = d.indexOf(this.props.discipline);
        const c = update(this.props.character,
                         { kai_disciplines: {$splice: [[index,1]]}});
        this.props.character_set(c);
    }

    render() {
        const d = this.props.discipline;
        var before = '';
        var after = '';
        var n = 12;
        if (this.props.mode === 'add') {
            n = 10;
            before =
                <Grid item md={2}>
                  <Button onClick={this.add.bind(this)}><Icon>add_circle</Icon></Button>
                </Grid>;
        }
        if (this.props.mode === 'remove') {
            n = 10;
            after =
                <Grid item md={2}>
                  <Button onClick={this.remove.bind(this)}><Icon>remove_circle</Icon></Button>
                </Grid>;
        }
        var weapon = '';
        if (d.id === 'maitrise_arme' && this.props.mode !== 'add') {
            var m = 'epee';
            if (this.props.character.weapon_master)
                m = this.props.character.weapon_master;
            const w = bh.item_get_description(m);
            const n = w.name;
            weapon = (<span> : {n}</span>);
        }
        return (
            <Grid container>
              {before}
              <Grid item sm={n} md={n}>
                <ExpansionPanel className="Discipline">
                  <ExpansionPanelSummary  expandIcon={<ExpandMoreIcon/>}>
                    <span>
                      {d.name}
                      &nbsp;
                      {weapon}
                    </span>
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails  className="DisciplineDetails">
                    {d.desc}
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              </Grid>
              {after}
            </Grid>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DisciplinePanel);
