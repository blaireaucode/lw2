/*
 * Copyright 2018
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
//import InputAdornment from '@material-ui/core/InputAdornment';
import { connect } from 'react-redux';
import update from 'immutability-helper';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';

class Field extends React.Component
{
    handleChange = name => event => {
        var v = parseInt(event.target.value, 10);
        var lm = this.props.name.toLowerCase()+'_max';
        var max = v;
        if (this.props.character[lm])
            max = this.props.character[lm];
        if (v>max) v = max;
        const c = update(this.props.character,
                         {[this.props.name]: {$set:v}});
        this.props.character_set(c);
    };

    render()
    {
        const { classes } = this.props;
        let help = '';
        const val = this.props.encounter[this.props.name];

        // endAdornment: <InputAdornment position="end">{bonus}</InputAdornment>,
        var label = this.props.label;

        /*var lm = label.toLowerCase()+'_max';
        if (lm in this.props.character) {
            label = label+' (max '+this.props.character[lm]+')';
        }
        */
        
        return (
            <TextField
              disabled= {!this.props.options.can_modify}
              label = {label}
              value = {val}
              className = {classes.root}
              type = {this.props.type}
              InputProps={{
                  disableUnderline: true,
                  classes: {
                      root: classes.textFieldRoot,
                      input: classes.textFieldInput,
                      focused:classes.textFieldFormLabelFocus,
                  },
              }}
              InputLabelProps={{
                  shrink: true,
                  classes: {
                      root:classes.textFieldFormLabel,
                      focused:classes.textFieldFormLabelFocus,
                  }
              }}
              onChange = {this.handleChange(this.props.text)}
              helperText = {help}
              margin={'normal'} // none dense normal
            />
        );
    }
}

//const focusColor = '#18b26f';
// const focusColor = '#3e84f2';
const focusColor = '#18b26f';

const styles = {
    textFieldRoot: {
        //fontSize: '1.2rem',
        //padding: 0,
    },
    textFieldInput: {
        //fontSize: '1.1rem',
        borderRadius: 4,
        padding: '10px',
        border: '1px solid #ced4da',
        width: '30%',
        '&:focus': {
            borderColor: focusColor,
            color: focusColor
        },
    },
    textFieldFormLabel: {
        //fontSize:'1.5rem',
        fontSize:'calc(8px + 1.35vmin)',
        // // font-size: calc(8px + 0.9vmin);
        fontFamily: 'Verdana',
        paddingLeft: '15px',
        margin: '-10px',
        '&:focus': {
            color: focusColor
        },
    },
    textFieldFormLabelFocus: {
        color: focusColor+'!important',
    },
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Field));
