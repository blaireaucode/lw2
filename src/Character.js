/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import DisciplineEditor from './DisciplineEditor.js';
import CharacterBonusCombatSkill from './CharacterBonusCombatSkill.js';
import CharacterBonusEndurance from './CharacterBonusEndurance.js';
import default_character from './default_character.js';
import default_book from './default_book.js';
import Field from './Field.js';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';
import L from './L.js';
import ItemsList from './ItemsList.js';
import ItemDisplay from './ItemDisplay.js';
import Healing from './Healing.js';
import HistoryList from './HistoryList.js';
import * as bh from './book_helpers.js';

class Character extends Component {

    constructor(props) {
        super(props);
        this.state = { }; // will contains item
    }

    clearLocalStorage() {
        console.log('clearLocalStorage', this.props.character);
        // set default character
        this.props.character_set(default_character);
        this.props.book_set(default_book);
        // remove local storage
        global.localStorage.removeItem('character');
        global.localStorage.removeItem('book');
    }

    display(item) {
        this.setState({item:item});
    }

    // <Button onClick={this.clearLocalStorage.bind(this)}>Remove local storage</Button>
    // <p/>
    
    render() {
        const style = {
            transformOrigin: 'top center',
            transform: 'scale('+this.props.book.globalScale+')'
        };

        const dead = bh.is_dead(this.props.character);
        var d ='';
        if (dead) {
            d = (
                <div className="Book-dead">
                  <p/>
                  Vous êtes mort ... 
                  <p/>
                </div>
            );            
        }
        const nb_items = bh.item_get_nb_of_item_in_sacados(this.props.character);

        var thetitle = this.props.book.current_passage;
        if (thetitle === "0_0") thetitle = '0';
        if (thetitle === "0_1") thetitle = '0';
        if (thetitle === "0_2") thetitle = '0';
        if (thetitle === "0_3") thetitle = '0';
        if (thetitle === "0_4") thetitle = '0';

        return (
            <div style={style}  className='Book-character'>              
              Paragraphe actuel : <L to={this.props.book.current_passage}>{thetitle}</L>
              <p/>
              <Grid container>
                <Grid item md={6}>
                  <Field
                    label = {'Habileté'}
                    name = {'combat_skill'}
                    encounter = {this.props.character}
                    type = {'number'}
                  />
                  <br/>
                  <CharacterBonusCombatSkill/>
                </Grid>
                <Grid item md={6}>
                  <Field
                    label = {'Endurance'}
                    name = {'endurance'}
                    encounter = {this.props.character}
                    type = {'number'}
                  />
                  <br/>
                  <CharacterBonusEndurance/>
                </Grid>
              </Grid>
              {d}              

              <p/>
              Armes : {' '}
              <ItemsList weapon={true} special={false}
                         items={this.props.character.items} itemSelected={this.display.bind(this)}/>
              <p/>
              Sac à dos ({nb_items}) : {' '} 
              <ItemsList weapon={false} special={false}
                         items={this.props.character.items} itemSelected={this.display.bind(this)}/>
              <p/>
              Objects spéciaux : {' '}
              <ItemsList special={true}
                         items={this.props.character.items} itemSelected={this.display.bind(this)}/>
              <br/>   <br/>
              <ItemDisplay item={this.state.item}/>

              <p/>             
              <DisciplineEditor/>
              <Healing/>

              <p/>
              <HistoryList/>

              <p/>
              <p/>
              <div style={{display: 'flex',  justifyContent:'center', alignItems:'center'}}>
                <Link to='edit'>Recommencer</Link>
              </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Character);
