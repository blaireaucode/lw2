/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';

class FightEncounter extends Component {

    render() {
        const fight = this.props.fight;
        // make unique key to restart animation
        const k = 'k'+fight.endurance+fight.passage_title+fight.name;
        return (
            <span>
              {fight.encounter} {' '} &nbsp;&nbsp;
              H {fight.combat_skill} {' '} &nbsp;&nbsp;
              <span key={k} className='Book-endurance-encounter'>E {fight.endurance} </span>
            </span>
        );
    }
}

export default FightEncounter;
