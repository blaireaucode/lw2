/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

const default_options = {
    display_dice: true,
    display_cheat_top: false,
    can_modify: false,
    history_link: false,
    items_nolimit: false,
    disc_nolimit: false,
};

export default default_options;

