/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';

class FightRoundResult extends Component {
    
    render() {
        const p = this.props.fight.previous;
        if (typeof p === 'undefined') return(<span/>);
        var dmgc = -p.damage_character;
        var dmge = -p.damage_encounter;
        if (this.props.fight.bonus_factor_dmg_encounter) {
            dmge = dmge/2+' x '+this.props.fight.bonus_factor_dmg_encounter+' = '+dmge;
        }
        const round = this.props.fight.round-1;
        return (
            <span>
              Tirage aléatoire du tour n°{round}: {p.rnd_dice}
              <br/>
              Dégâts reçus : {dmgc} {' '} &nbsp;&nbsp;&nbsp;&nbsp;
              Dégâts infligés : {dmge}
            </span>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FightRoundResult);
