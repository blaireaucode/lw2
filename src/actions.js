/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import * as at from './action-types.js';

export const character_set =
    character => ({ type:at.CHARACTER_SET, character:character });

export const book_set_element =
    element => ({ type:at.BOOK_SET_ELEMENT, element:element });

export const book_set =
    book => ({ type:at.BOOK_SET, book:book });

export const options_set =
    options => ({ type:at.OPTIONS_SET, options:options });
