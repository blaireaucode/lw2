
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import L from './L.js';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import * as bh from './book_helpers.js';

class ItemPotion extends Component {

    action_potion(character) {
        var c = bh.endurance_add(this.props.character, this.props.bonus);
        c = bh.item_drop(c, this.props.item_id);
        this.props.character_set(c);
    }
    
    render() {
        const title = this.props.book.current_passage;
        const p = this.props.book.p[title.toString()];
        var d = true;
        if (p) {
            const e = p['healing_is_not_possible'];
            if (e) d = false;
        }
        
        return (
            <span>
              <L to='/character' enabled={d}
                 onClick={this.action_potion.bind(this)}>L'utiliser (+{this.props.bonus}).</L>
            </span>);
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemPotion);


