/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps} from './book_default_props.js';
import * as bh from './book_helpers.js';

class HelpGold extends Component {
    render() {
        const g = bh.gold_get(this.props.character);
        if (g>1)
            return (
                <span className='Book-help'>
                  Il vous reste {g} pièces d'or.
                </span>
            );
        if (g === 0)
            return (
                <span className='Book-help'>
                  Il ne vous reste plus aucune pièce d'or.
                </span>
            );
        return (
            <span className='Book-help'>
              Il ne vous une seule pièce d'or.
            </span>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HelpGold);
