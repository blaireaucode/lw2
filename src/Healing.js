/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import * as bh from './book_helpers.js';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';

class Healing extends Component {

    handleClick() {
        const e = {
            'name': 'healing_is_not_possible',
            'passage_title': this.props.book.current_passage,
        };
        this.props.book_set_element(e);
        const c = bh.endurance_add(this.props.character, 1);
        this.props.character_set(c);        
    }
    
    render() {
        const b = bh.has_discipline(this.props.character, 'guerison');
        if (!b) return '';

        const pt = this.props.book.current_passage.toString();
        const elements = this.props.book.p[pt] || false;
        var canHeal = true;
        var isFight = false;
         if (elements) {
             for (var e in elements) {
                 // console.log('e', e);
                 if (elements[e].name === 'healing_is_not_possible') {
                     canHeal = false;
                 }
                 if ('encounter' in elements[e]) {
                     canHeal = false;
                     isFight = true;
                 }
             }
         }

        const dead = bh.is_dead(this.props.character);
        if (dead) return '';       

        if (!canHeal && isFight) {
            return (<span className='Book-help'>
                      Vous ne pouvez pas utiliser 'Guérison' car un combat est en cours.
                    </span>);
        }
        if (!canHeal) {
            return (<span className='Book-help'>
                      Vous avez déjà utilisé 'Guérison' durant le passage {this.props.book.current_passage}.
                    </span>);
        }
        return (
            <span className='Book-help'>
              
              Utiliser votre capacité de guérison pour récupérer <Link
              to='/character'
              onClick={this.handleClick.bind(this)}>1 point
              d'ENDURANCE. </Link>
              
            </span>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Healing);
