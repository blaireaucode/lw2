/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import FightRoundResult from './FightRoundResult.js';
import L from './L.js';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import update from 'immutability-helper';

class FightWin extends Component {

    fightContinue() {
        const fight = this.props.fight;
        const f = update(fight, {continue_terminated: {$set:true}} );
        this.props.book_set_element(f);
    }
    
    render() {
        const fight = this.props.fight;
        var txt = 'en un seul tour';
        const r = fight.round-1;
        if (r > 1)
            txt = 'en '+r+' tours';

        var l = -fight.total_damage;
        var ll = '';
        if (l > 1)  
            ll = (<span> Vous avez perdu {l} points d'endurance dans ce combat. </span>);
        if (l === 1)  
            ll = (<span> Vous avez perdu 1 seul point d'endurance dans ce combat. </span>);
        if (l <= 0)  
            ll = (<span> Vous n'avez perdu aucun point d'endurance dans ce combat. </span>);

        var c = '';
        if (fight.continue) {
            c = (<span><p/><L onClick={this.fightContinue.bind(this)}
                              to={fight.passage_title}>Poursuivre le combat ...</L></span>);
        }
        
        if (fight.multiple && fight.multiple === true)
            return (
                <span>
                  <FightRoundResult fight={this.props.fight}/><p/>
                  <p/>
                  Victoire ! {fight.encounter} sont vaincus {txt}.
                  <p/>{ll}
                  {c}
                </span>
            );
        return (
            <span>
              <FightRoundResult fight={this.props.fight}/><p/>
              <p/>
              Victoire ! {fight.encounter} est vaincu {txt}.
              <p/>    {ll}
              {c}
            </span>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FightWin);
