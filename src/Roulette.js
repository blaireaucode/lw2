/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import L from './L.js';
import HelpGold from './HelpGold.js';
import update from 'immutability-helper';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import * as bh from './book_helpers.js';

class Roulette extends Component {

    handleChangeBet = name => event => {
        var v = parseInt(event.target.value, 10);
        if (v<0) return;
        const g = bh.gold_get(this.props.character);
        if (v>=g) v = g;
        const e = bh.book_get_element(this.props, this.props.r);
        const elem = update(e, { bet:{$set:v}});
        this.props.book_set_element(elem);
    };

    handleChangeNum = name => event => {
        var v = parseInt(event.target.value, 10);
        if (v<0) return;
        if (v>10) v = 10;
        const e = bh.book_get_element(this.props, this.props.r);
        const elem = update(e, { num:{$set:v}});
        this.props.book_set_element(elem);
    };

    bet() {
        const e = bh.book_get_element(this.props, this.props.r);
        const rnd = Math.floor(Math.random() * 10);
        var w = -e.bet;
        var h = 'La roue vous donne un '+rnd;
        if (rnd === e.num) {
            w = e.bet*8;
            h = h + '. Magnifique ! Vous gagnez 8x'+e.bet+' = '+w;
        }
        if (rnd === e.num-1 || rnd === e.num+1) {
            w = e.bet*5;
            h = h + '. Bravo ! Vous gagnez 5x'+e.bet+' = '+w;
        }
        if (w<=0) {
            h = h + '. Perdu !';
        }
        if (e.total_win + w > 40) {
            w = 40-e.total_win;
            h = h + ' (maximum du total des gains atteint).';
        }
        const t = w+e.total_win;
        const c = bh.gold_add(this.props.character, w);
        this.props.character_set(c);
        var elem = update(e, { previous:{$set:h}});        
        elem = update(elem, { total_win:{$set:t}});
        this.props.book_set_element(elem);
    }
    
    render() {
        const r = this.props.r;
        if (!r) return ('');
        const e = bh.book_get_element(this.props, r);       
        const g = bh.gold_get(this.props.character);
        return (
            <div>
              <p/>
              <TextField
                label = {'Numéro'}
                value = {e.num}
                type = {'number'} 
                onChange = {this.handleChangeNum()}
              />
              <TextField
                label = {'Mise'}
                value = {e.bet}
                type = {'number'} 
                onChange = {this.handleChangeBet()}
              />
              <p/>

              <L enabled={g>=e.bet} to={e.passage_title} onClick={this.bet.bind(this)}>Pariez !</L>
              <p/>

              Gain jusqu'à présent : {e.total_win}
              <p/>
              {e.previous}

              <HelpGold/>
              <p/>
              
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Roulette);
