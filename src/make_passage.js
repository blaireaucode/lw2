/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */


console.log('Hello script')

if (!process.argv[2]) {
    console.log('Please give passage nb');
    process.exit(0);
}

p = process.argv[2]
console.log('Passage', p)

var fs = require('fs');

// read template
fn = './src/passage_template.js'
data = fs.readFileSync(fn, 'utf-8')

// replace passage
data = data.replace(/passage_number/g, p.toString());

console.log(data);

// write
fn = './src/passages/'+p+'.js'
console.log(fn);
fs.writeFile(fn, data, function(err, data){
    if (err) console.log(err);
    console.log("Successfully Written to File.");
});


