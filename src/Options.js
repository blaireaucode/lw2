/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import SwitchOption from './SwitchOption.js';
import L from './L.js';
import * as bh from './book_helpers.js';

class Options extends Component {

    reset_book() {
        bh.reset_book(this.props);
    }
    
    reset_char() {
        bh.reset_character(this.props);
    }
    
    reset_options() {
        bh.reset_options(this.props);
    }
    
    render() {
        return (
            <div  className='Book-character'>
              Options
              <p/>
              <ul>
                <li> 
                  <SwitchOption label={'Afficher les dés'} option={'display_dice'}/>
                </li>
              </ul>

              Options de «&nbsp;<i>triche</i>&nbsp;» : 
              <p/>
              <ul>
                <li> 
                  <SwitchOption label={"Navigation dans l'en-tête"} option={'display_cheat_top'}/>
                </li>
                <li> 
                  <SwitchOption label={"Caractéristiques modifiables"} option={'can_modify'}/>
                </li>
                <li> 
                  <SwitchOption label={"Historique navigable"} option={'history_link'}/>
                </li>
                <li> 
                  <SwitchOption label={"Equipement illimité"} option={'items_nolimit'}/>
                </li>
                <li> 
                  <SwitchOption label={"Nombre de disciplines illimité"} option={'disc_nolimit'}/>
                </li>
              </ul>
              <p/>

              <L onClick={this.reset_book.bind(this)} to='/options'>Re-initialiser le livre</L> <br/>
              <L onClick={this.reset_char.bind(this)} to='/options'>Re-initialiser le personnage</L> <br/>
              <L onClick={this.reset_options.bind(this)} to='/options'>Re-initialiser les options</L> <br/>
              <L to='/debug'>Debug</L> <br/>              
              
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Options);
