/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import * as bh from './book_helpers.js';

class Item extends Component {

    render() {
        const item = this.props.item;

        // no item
        if (!item) return (<span/>);

        // get text with nb if needed 
        var item_txt = bh.item_get_description(item.id).name;
        if (item.nb > 1) {
            item_txt = item.nb + ' ' + bh.item_get_description(item.id).name_plural;
        }

        var txt_after = '';
        if (item.txt_after) txt_after = ' '+item.txt_after;

        if (item.cost > 0) {
            item_txt = item_txt+' ('+item.cost+' PO)';
        }

        // render with link
        const dead = bh.is_dead(this.props.character);
        if (!dead && (item.clickable || !('clickable' in item)))
            return (
                <span>
                  <Link to='#' onClick={() => this.props.itemSelected(item)}>{item_txt}</Link>{txt_after}
                </span>
        );
        // render without link
        return (
            <span>
              {item_txt}
            </span>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Item);
