/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import FightEncounter from './FightEncounter.js';
import FightCharacter from './FightCharacter.js';
import FightAction from './FightAction.js';
import FightWin from './FightWin.js';
import FightLose from './FightLose.js';
import FightHelp from './FightHelp.js';
import FightPreviousResult from './FightPreviousResult.js';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import * as bh from './book_helpers.js';
import { Shake } from 'reshake';
import FightPrevious from './FightPrevious.js';

class Fight extends Component {

    constructor(props) {
        super(props);
        this.state = { fight: false};
    }

    update() {
        var fight = bh.book_get_element(this.props, this.props.fight);
        if (!fight || !fight.previous) {
            fight = bh.book_fight_update(this.props.character, this.props.fight);
            fight = this.props.book_set_element(fight);
            const e = {
                'name': 'healing_is_not_possible',
                'passage_title': this.props.book.current_passage,
            };
            this.props.book_set_element(e);
        }
        this.setState( {fight:fight});
    }

    componentWillReceiveProps(nextProps) {
        // Init the fight if it does not exist
        // Save it to the state        
        var fight = bh.book_get_element(nextProps, nextProps.fight);
        if (!fight || !fight.combat_help) {
            fight = bh.book_fight_update(this.props.character, nextProps.fight);
            fight = this.props.book_set_element(fight);
            const e = {
                'name': 'healing_is_not_possible',
                'passage_title': this.props.book.current_passage,
            };
            this.props.book_set_element(e);
        }
        this.setState( {fight:fight});
    }

    componentDidUpdate(prevProps, prevState) {
    }

    render() {
        const fight = this.state.fight;       
        const character = this.props.character;
        if (!fight || this.props.enabled === false) {
            this.update();
            return (<div></div>);
        }
        
        if (fight.status === 'defeat' && fight.dont_die !== true)
            return (<FightLose fight={fight}/>);
        
        const dead = bh.is_dead(this.props.character);
        if (dead) return (<FightLose fight={fight}/>);
        
        if (fight.status === 'victory')
            return (<FightWin fight={fight} onContinue={this.props.onContinue}/>);
        
        var dur = 70;
        var dmax = 10;
        if (fight.round < 2) dur = 0;
        else {
            var d = fight.previous.damage_character;
            if (d> 20) d = 20;
            dur = -d*20;
            dmax = -d*4;
        }
        const dice = fight.current_dice;
        const k = fight.current_k;
        const sk = fight.round;//character.endurance+'_'+fight.endurance;
        console.log('keys', k, sk);
        return (
            <div className='Book-fight' align='center'>
              {/* <Shake key={sk} h={dmax} v={dmax} r={dmax} q={3} dur={dur} fixed> */}
                <FightEncounter fight={fight}/><br/>
                <FightCharacter character={character} fight={fight}/><br/>
                
              {/* </Shake> */}
              <p/>
              <FightAction fight={fight}
                           handleRound={this.props.handleRound}
                           handleEndFight={this.props.handleEndFight}
              />
              <br/>
              <FightPrevious key={k} dice={dice}/> 
              <FightPreviousResult fight={fight}/>
              {/*    <br/> */}
              <br/>
              <FightHelp fight={fight}/>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Fight);

