/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import * as bh from './book_helpers.js';

class FightCharacter extends Component {

    render() {
        const en = this.props.character.endurance;
        const cs = this.props.character.combat_skill;
        var csb = bh.fight_get_combat_skill_bonus(this.props.character, this.props.fight);
        if (csb === 0) csb = '';
        if (csb > 0) csb = '+'+csb;
        //if (csb < 0) csb = csb;
        var eb = bh.fight_get_endurance_bonus(this.props.character, this.props.fight);
        if (eb === 0) eb = '';
        if (eb > 0) eb = '+'+eb;
        if (eb < 0) eb = '-'+eb;
        const k = 'k'+en;       
        return (
            <span>
              Vous :
              {' '} &nbsp;&nbsp;
              H {cs}<span className='Book-endurance-char-bonus'>{csb}</span>
              {' '} &nbsp;&nbsp;
              <span key={k} className='Book-endurance-char'>
                E {en}{eb}
              </span>
            </span>
        );
    }
}

export default FightCharacter;
