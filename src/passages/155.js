/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P155 extends Component {
    render() {
        return (
            <div>

            Vous avez parcouru un kilomètre et demi sur le chemin de
            gauche lorsque vous arrivez à un long pont de pierre. Le
            fleuve qu'il enjambe semble être en crue, il menace de
            déborder de son lit. Vous vous etes rendu compte alors
            qu'il s'agit du chenal de Rynn Il fait 3 kilomètres dans
            sa plus grande largeur et 1500 mètres de profondeur sur
            presque toute sa longueur. Il a été formé à la suite d'un
            glissement de terrain qui a séparé le royaume de Durenor
            du reste des terres de Magnamund. A l'entrée du pont, un
            poteau indicateur précise :

           <p/>

           Vous poussez un soupir de soulagement en constatant que
           vous êtes sur le bon chemin : dans moins d'une heure, vous
           aurez atteint la ville.  <L to="265"> Rendez-vous au
           265</L>.


            </div>
        );
    }
}

export default P155;
