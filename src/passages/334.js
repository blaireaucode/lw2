/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P334 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, "orientation");
        return (
            <div>

              Quelques kilomètres plus loin, le sentier est recouvert
              de broussailles et disparaît complètement sous les
              herbes et les buissons d'épines. Il devient difficile
              d'avancer car les marécages et les fondrières abondent
              sur la lande. Il vous faut plusieurs heures d'un
              parcours malaisé pour atteindre enfin la lisière de la
              forêt de Durenor. Vous distinguez alors la silhouette
              d'une haute tour dressée parmi les fougères. Des volutes
              de fumée s'élèvent paresseusement d'une cheminée. Si
              vous souhaitez entrer dans cette tour, <L
              to="115">rendez-vous au 115</L>. Si vous préférez
              chercher un chemin qui traverse la forêt, <L
              to="291">rendez-vous au 291</L>. Enfin, si vous
              maîtrisez la Discipline Kaï de l'Orientation, <L
              enabled={d} to="98"> rendez-vous au 98</L>.

            </div>
        );
    }
}

export default P334;
