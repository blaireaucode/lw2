/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Dead from '../Dead.js';

class P234 extends Component {
    render() {
        return (
            <div>

              Avant que vous ayez pu esquisser un geste oour vous
              défendre, le Monstre d'Enfer s'est jeté sur vous et vous
              tombez tous deux sur la chaussée en contrebas. Vous
              éprouverez peut-être quelque consolation en apprenant
              que votre mort a été soudaine et indolore. Vous vous
              êtes rompu le cou dans votre chute et vous n'aurez donc
              pas le désagrément de sentir les doigts décharnés du
              Monstre d'Enfer s'enfoncer dans votre gorge en vous
              déchirant la peau de leurs griffes pointues. Le Sceau
              d'Hammardal retournera désormais à Holmgard et la ville
              tombera aux mains des Maîtres des Ténèbres. Votre
              mission s'achète donc ici en même temps que votre vie.

              <p/>
              <Dead/>          

            </div>
        );
    }
}

export default P234;
