/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import * as bh from '../book_helpers.js';

class P175 extends Component {

    constructor(props) {
        super(props);
        this.r = {
            passage_title: 175,
            name: 'random_table',
            dice_value: -1
        };
    }

    render() {
        const randtable = bh.book_get_element(this.props, this.r);
        const r = randtable.dice_value;
        return (
            <div>

              «&nbsp;Il semble que l'oiseau se soit envolé », dit le
              capitaine. Il vous montre alors un canot qui file à
              bonne allure en direction d'un autre navire. « Regardez
              bien ce vaisseau, il n'a pas de pavillon et sa forme me
              paraît bien étrange. Je n'en ai encore jamais vu de
              semblable. » Vous observez le canot qui rejoint en
              quelques instants le mystérieux navire. Et soudain,
              comme par magie, un brouillard venu d'on ne sait où se
              lève sur la mer et enveloppe le vaisseau. Moins d'une
              minute plus tard, le navire et le brouillard ont tous
              deux disparu. Utilisez la <RandomTable p={this.r}> pour
              obtenir un chiffre</RandomTable>. Si vous tirez un
              chiffre entre 0 et 4, <L enabled={r>=0 && r<=4}
              to="53">rendez-vous au 53</L>. Entre 5 et 9, <L
              enabled={r>=5 && r<=9} to="209">rendez-vous au 209</L>.

              <CurrentDice r={r}/>

            </div>
        );
    }
}

export default P175;
