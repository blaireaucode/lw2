/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P232 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, "sixieme_sens");
        const l = this.props.character.items.some(e => e.id === 'sceau_hammardal');        
        return (
            <div>

              Vous êtes parvenu à moins d'une vingtaine de mètres de
              la tour lorsque le garde fait un pas en avant et vous
              demande ce que vous venez faire par ici. Vous remarquez
              que le soldat porte la vareuse rouge de l'uniforme des
              armées durenoraises, ce qui signifie que vous avez
              atteint la frontière du royaume. Il vous faut à présent
              trouver le moyen de passer. Si vous souhaitez prétendre
              que vous êtes un marchand en route pour Port Bax, <L
              to="250"> rendez-vous au 250</L>. Si vous voulez essayer
              de le corrompre en lui donnant de l'or, <L
              to="68"> rendez-vous au 68</L>. Si vous pensez qu'il est
              préférable de lui montrer le Sceau d'Hammardal (en
              admettant qu'il soit toujours en votre possession), <L
              to="223" enabled={l}> rendez-vous au 223</L>. Enfin, si
              vous maîtrisez la Discipline Kaï du Sixième Sens, <L
              to="149" enabled={d}> rendez-vous au 149</L>.

            </div>
        );
    }
}

export default P232;
