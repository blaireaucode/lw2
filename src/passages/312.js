/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P312 extends Component {
    render() {
        return (
            <div>

              Lorsque l'aube se lève, il fait froid et la pluie tombe
              à verse ; votre cape de Seigneur Kaï et l'abri du
              feuillage vous ont cependant protégé en vous tenant au
              chaud et au sec la nuit durant. Vous jetez un coup d'œil
              à la route qui longe la côte et vous apercevez au loin
              une diligence qui avance dans votre direction. Si vous
              souhaitez descendre de l'arbre et faire signe au
              conducteur de l'attelage, <L to="117">rendez-vous au
              117</L>. Si vous préférez essayer de sauter sur le toit
              de la diligence lorsqu'elle passera sous les branches de
              l'arbre, <L to="89">rendez-vous au 89</L>.

            </div>
        );
    }
}

export default P312;
