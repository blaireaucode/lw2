/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import SimpleImage from '../SimpleImage.js';
import * as bh from '../book_helpers.js';

class P300 extends Component {
    render() {
        const r = bh.get_element(this, 300, {dice_value:-1});
        const d = r.dice_value;
        return (
            <div>

              En arrivant à proximité du bateau, vous constatez que
              l'échelle de coupée a été relevée. Un marin à la mine
              peu engageante est accoudé au bastingage et vous lance
              des injures. De toute évidence, il croit que vous êtes
              un réfugié qui essaie de monter à bord comme passager
              clandestin. Mais lorsque vous lui criez que vous êtes le
              Loup Solitaire et que vous venez d'être trompé par un
              imposteur, l'échelle est à nouveau baissée. En prenant
              pied sur le pont, vous êtes accueilli par un homme de
              haute taille vêtu d'un uniforme passementé d'or. Son
              visage est presque entièrement caché par une abondante
              tignasse de cneveux roux et une grande barbe également
              rousse. «&nbsp;Levez l'ancre !&nbsp;» ordonne-t-il d'une voix
              tonitruante. Les hommes d'équipage se précipitent
              aussitôt à leurs postes comme si leur vie en dépendait
              et se mettent au travail.

              <p/>
              <SimpleImage width={200} src={'300.png'}/>
              <p/>

              Le capitaine vous conduit ensuite à sa cabine et vous
              offre un verre de Wanlo, l'un des alcools les plus forts
              qu'on puisse boire dans cette région. Vous lui faites
              alors le récit de ce qui vient d'arriver et vous
              remarquez que son visage prend une expression
              soucieuse. «&nbsp;Tout cela sent la trahison, dit-il
              d'un air sombre, il est clair que l'ennemi a déjà dressé
              des plans pour faire échouer votre mission. Il ne faut
              plus compter sur l'effet de surprise et, quant à moi,
              j'ai perdu un courageux second. Espérons au moins que la
              traversée jusqu'au royaume de Durenor sera sans
              histoire...&nbsp;» Vous le quittez quelques instants
              plus tard et vous remontez sur le pont juste à temps
              pour voir disparaître à l'horizon les tours de la
              capitale ; vous descendez ensuite dans votre propre
              cabine en éprouvant un sentiment mêlé d'orgueil et
              d'appréhension.  Utilisez la <RandomTable p={r}>pour
              obtenir un chiffre</RandomTable>. Si vous tirez 0 ou 1,
              <L enabled={d>=0 && d<=1} to="224"> rendez-vous au
              224</L>. Avec 2 ou 3, <L enabled={d>=2 && d<=3}
              to="316"> rendez-vous au 316</L> ; 4 ou 5, <L
              enabled={d>=4 && d<=5} to="81"> rendez-vous au 81</L> ; 6
              ou 7, <L enabled={d>=6 && d<=7} to="22"> rendez-vous au
              22</L> ; 8 ou 9, <L enabled={d>=8 && d<=9}
              to='99'> rendez-vous au 99</L>.

              <CurrentDice r={d}/>
            </div>
        );
    }
}

export default P300;
