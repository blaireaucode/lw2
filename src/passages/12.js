/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import SimpleImage from '../SimpleImage.js';
import update from 'immutability-helper';
import HelpGold from '../HelpGold.js';
import * as bh from '../book_helpers.js';

class P12 extends Component {

    constructor(props) {
        super(props);
        this.r = {
            passage_title: '12',
            name: 'random_table',
            dice_value: -1,
            already_paid:false
        };
        this.f_bonus = (character) => {
            const disc = character.kai_disciplines;
            const d = disc.some(e => e.id === "sixieme_sens");
            const bonus = (d? 2:0);
            return bonus;
        };
    }

    handleClick() {
        // Enable
        const elem = bh.book_get_element(this.props, this.r);
        const e = update(elem, {already_paid: {$set:true}});
        this.props.book_set_element(e);
        // Pay + add ticket
        const c = bh.gold_add(this.props.character, -10);
        this.props.character_set(c);
    }
    
    render() {
        const randtable = bh.book_get_element(this.props, this.r);
        var t = " (ce n'est pas le cas)";
        if (this.f_bonus(this.props.character) !== 0) t = " (c'est le cas)";
        const r = randtable.dice_value;
        var already_paid = randtable.already_paid;
        const dice = r-this.f_bonus(this.props.character);
        return (
            <div>

              Le capitaine Kelman ouvre la porte d'une vitrine et en
              retire le damier d'un jeu de Samor. Les magnifiques
              figurines d'ivoire sculpté sont déjà disposées sur les
              cases lorsque le capitaine dépose avec précaution le
              damier sur la table. Vous acceptez, à contrecœur, de <L
              enabled={!already_paid} to='12'
              onClick={this.handleClick.bind(this)}>miser 10 Pièces
              d'Or</L> et le jeu commence. Utilisez la <RandomTable
              enabled={already_paid} p={this.r} bonus={this.f_bonus}>pour
              savoir qui va l'emporter. Si vous maîtrisez la
              Discipline Kaï du Sixième Sens, vous avez le droit
              d'ajouter 2 au chiffre obtenu{t}</RandomTable>.  Si le
              total est de 0, 1, 2 ou 3, <L enabled={r>=0 && r<=3}
              to="58">rendez-vous au 58</L>, 4, 5 ou 6, <L
              enabled={r>=4 && r<=6} to="167">rendez-vous au 167</L>,
              7, 8, 9,10 ou 11, <L enabled={r>=7} to="329">rendez-vous
              au 329</L>.

              <p/><HelpGold/>
              
              <p/>
              <SimpleImage width={150} src={'12.png'}/>
              <CurrentDice r={dice}/>

            </div>
        );
    }
}

export default P12;
