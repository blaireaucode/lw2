/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P47 extends Component {
    render() {
        return (
            <div>

              Les soldats se hâtent de descendre du toit et empoignent
              leurs lances ; puis ils s'avancent vers vous. «&nbsp;Le
              mot de passe, étranger ! » crie l'un d'eux. Si vous
              connaissez le mot de passe qui permet de franchir le
              pont, <L to="111">rendez-vous au 111</L>. Sinon, <L
              to="307">rendez-vous au 307</L>.

            </div>
        );
    }
}

export default P47;
