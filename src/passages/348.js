/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import * as bh from '../book_helpers.js';

class P348 extends Component {
    render() {
        const f = {
            name: 'f1',
            passage_title: 348,
            encounter: "BRIGANDS",
            combat_skill:16,
            endurance:25,
        };
        const r = bh.enabled_if_fight_win(this.props, f);
        const flee = bh.book_get_element(this.props, f).round > 2;
        
        return (
            <div>

              L'homme cesse de sourire et une expression de mépris
              apparaît sur son visage. D'un mouvement rapide, il
              s'éloigne de la table. «&nbsp;Peut-être que ni vous ni
              moi ne sommes celui que nous prétendons être, mais
              qu'importe, vous ne vivrez pas assez longtemps pour
              découvrir qui je suis !&nbsp;» lance-t-il avec
              hargne. Vous entendez alors une porte s'ouvrir à la
              volée derrière vous. Vous faites aussitôt volte-face et
              vous voyez trois BRIGANDS s'avancer dans votre
              direction. Chacun d'eux est armé d'un cimeterre et vous
              allez devoir les combattre en les considérant comme un
              seul et même adversaire. Vous aurez le droit de prendre
              la fuite après avoir mené contre eux deux assauts au
              moins : vous sortirez alors par la porte latérale en <L
              enabled={flee} to='125'>vous rendant au 125</L>. Si vous
              êtes vainqueur, <L enabled={r} to="333">rendez-vous au
              333</L>.

              <p/>
              <Fight fight={f}/>
            </div>
        );
    }
}
// BRIGANDS HABILETÉ: 16 ENDURANCE: 25

export default P348;
