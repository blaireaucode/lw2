/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P77 extends Component {
    render() {
        return (
            <div>

              
              Au cours de votre entraînement au monastère Kaï, vos
              maîtres vous ont enseigné de nombreux langues et
              dialectes en usage dans les régions septentrionales de
              Magnamund. L'un de ces dialectes est le squall. Or, il
              se trouve précisément que les créatures rassemblées dans
              cette clairière sont des Squalls. A grands cris, ils
              vous expliquent que l'homme blessé n'a en réalité rien
              d'humain. C'est un Monstre d'Enfer, affirment-ils, un de
              ces êtres maléfiques qui ont le pouvoir de changer de
              forme à leur guise et qui comptent parmi les plus
              fidèles serviteurs des Maîtres des Ténèbres.

              <p/>

              Si vous pensez que les Squalls disent vrai, jetez un
              coup d'œil au contenu du sac de l'homme blessé en <L
              to='320'>vous rendant au 320</L>. Si en revanche vous
              soupçonnez les Squalls de vous mentir pour vous
              dissuader d'intervenir dans leurs jeux répugnants,
              attaquez-les avec votre arme en <L to='28'>vous rendant
              au 28</L>.


            </div>
        );
    }
}

export default P77;
