/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';


import HelpCharacter from '../HelpCharacter.js';
import SimpleImage from '../SimpleImage.js';
import * as bh from '../book_helpers.js';

class P41 extends Component {

    handleClick() {
        const c = bh.endurance_add(this.props.character, 1);
        this.props.character_set(c);
    }

    render() {
        return (
            <div>

              Vous avez de la chance car votre signal de détresse a
              été aperçu par l'équipage du bateau qui met à présent le
              cap dans votre direction. C'est un petit bateau de pêche
              en provenance du port de Ragadorn. Les pêcheurs qui sont
              à bord ont une mine plutôt patibulaire, mais ils se
              montrent amicaux avec vous ; ils vous donnent une
              couverture pour vous réchauffer et vous offrent de quoi
              manger. Le capitaine vous conseille de faire un somme
              pendant les trois heures que durera le voyage de retour
              à Ragadorn. Si vous décidez de suivre ce conseil,
              reprenez un point d'ENDURANCE et <L to="194"
              onClick={this.handleClick.bind(this)}>rendez-vous au
              194</L>. Si vous préférez rester éveillé et scruter la
              mer dans l'espoir de retrouver d'autres survivants du
              naufrage, <L to="251">rendez-vous au 251</L>.

              <p/>
              <HelpCharacter/>

              <p/>
              <SimpleImage width={250} src={'41.png'}/>

            </div>
        );
    }
}

export default P41;
