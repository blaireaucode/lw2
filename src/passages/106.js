
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Span from '../Span.js';
import Fight from '../Fight.js';
import * as bh from '../book_helpers.js';
import update from 'immutability-helper';
import ItemsPicker from '../ItemsPicker.js';

class P106 extends Component {

    constructor(props) {
        super(props);
        // Fight
        var f = {
            'name': 'f1',
            'passage_title': 106,
            'encounter': "Monstre d'enfer",
            'combat_skill':22,
            'endurance':30,
            'no_bonus_psy': true,
            'bonus_dmg_character': 0,
            'no_bonus_weapon': true
        };
        const d = bh.has_discipline(this.props.character, "bouclier_psychique");        
        if (!d) f.bonus_dmg_character = 2;
        this.f = f;
        // Item
        const it = {
            'name': 'obj1',
            'passage_title': 106,
            'list': [ {id:'magical_spear'} ],
        };
        this.items = it;
        // endurance
        var fe = {
            'name': 'f1',
            'passage_title': 106,
            'endurance_done':false
        };
        this.fe=fe;
    }

    handleClick() {
        var e = bh.book_get_element(this.props, this.fe);
        e = update(e, {endurance_done:{$set:true}});
        this.props.book_set_element(e);
        const c = bh.endurance_add(this.props.character, -2);
        this.props.character_set(c);
    }

    render() {
        const e = bh.book_get_element(this.props, this.fe);
        const p1 = (e.endurance_done === false);
        const r = bh.enabled_if_fight_win(this.props, this.f);
        return (
            <div>
              
              La lance sculptée est en métal et pourtant elle est
              aussi légère que si elle avait été taillée dans du
              bois. Vous remarquez que la hampe est gravée de
              caractères runiques et de symboles magiques. Vous la
              retirez avec précaution de la poitrine de l'homme blessé
              qui pousse un long soupir de soulagement. Vous vous
              apprêtez ensuite à examiner sa blessure lorsqu'une
              subite douleur vous déchire la tête. Vous avez si mal
              que vous vous écroulez sur le sol en perdant <L to='106'
              enabled={p1} onClick={this.handleClick.bind(this)}>2
              points d'ENDURANCE</L>.
              
              <Span enabled={!p1}>
                <p/>
                
                Vous êtes alors stupéfait de voir l'homme se relever
                d'un bond, mais votre surprise se change en horreur
                quand soudain il se métamorphose devant vos yeux. La
                peau de son visage se convulsé et change de couleur ;
                elle prend une teinte de plus en plus sombre et se
                ratatine en se décomposant à vue d'œil. Le crâne de
                l'homme apparaît, ses yeux s'enflamment d'une lueur
                rouge et brillante et de longs crocs jaillissent de sa
                mâchoire. La terreur vous saisit à la gorge lorsque
                vous comprenez enfin que cette créature est en réalité
                un MONSTRE D'ENFER, un de ces effroyables serviteurs
                des Maîtres des Ténèbres.  Le monstre vous attaque en
                se servant de sa puissante Force Mentale et, si vous
                ne maîtrisez pas la Discipline Kaï du Bouclier
                Psychique, vous perdrez 2 points d'ENDURANCE
                supplémentaires à chaque assaut mené contre lui au
                cours du combat. La créature est invulnérable aux
                armes ordinaires et à la Discipline Kaï de la
                Puissance Psychique. Vous ne pouvez la blesser qu'à
                l'aide de la Lance Magique, celle-là même qui était
                fichée dans sa poitrine.

                <p/>

                Il vous est impossible de prendre la fuite et vous
                devrez poursuivre ce combat jusqu'à la mort de l'un
                des deux adversaires. Si vous êtes vainqueur, vous
                aurez le droit de conserver la lance. Inscrivez-la
                dans ce cas sur votre Feuille d'Aventure dans la case
                Lance Magique de la section Objets Spéciaux. <L
                to='320' enabled={r}>Rendez-vous ensuite au 320</L>.

                <p/>
                <Fight fight={this.f}/>                
              </Span>
              
              <Span enabled={r}>
                <p/>
                <ItemsPicker items={this.items}/>
                <p/>
              </Span>
              
            </div>
            
        );
    }
}

// MONSTRE D'ENFER HABILETÉ: 22 ENDURANCE:30

export default P106;
