/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P13 extends Component {
    render() {
        return (
            <div>

              Votre sens de l'orientation vous indique qu'il faut
              prendre le chemin de gauche pour parvenir au plus vite à
              Port Bax. Vous calez votre Sac à Dos sur vos épaules et
              vous vous remettez en route. <L to="155">Rendez-vous au
              155</L>.

            </div>
        );
    }
}

export default P13;
