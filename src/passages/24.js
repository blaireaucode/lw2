/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P24 extends Component {
    render() {
        return (
            <div>

              C'est le tombeau de Killean le Suzerain. Il était l'un
              des seigneurs de Ragadorn jusqu'au jour où, trois ans
              plus tôt, une épidémie de peste rouge l'emporta en même
              temps que nombre de ses compatriotes. Vous vous rappelez
              soudain les récits que vous a faits un certain Renard
              Agile qui occupait un emploi d'artisan au monastère
              Kaï. Cet homme s'était souvent rendu à Ragadorn et la
              peste rouge l'avait frappé tout comme elle avait frappé
              une bonne moitié de la population locale. Si vous
              souhaitez retourner dans la taverne, <L
              to="177">rendez-vous au 177</L>. Si vous préférez
              poursuivre votre chemin le long de la rue du Tombeau, <L
              to="253">rendez-vous au 253</L>. Si enfin vous désirez
              prendre la direction de l'est en empruntant la rue de la
              Tour de Guet, <L to="319">rendez-vous au 319</L>.

            </div>
        );
    }
}

export default P24;
