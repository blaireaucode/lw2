/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P291 extends Component {
    render() {
        return (
            <div>

              C'est une forêt très touffue où s'enchevêtrent parmi les
              arbres de hautes herbes et des buissons d'épines. Vous
              longez la lisière du bois pour essayer de découvrir un
              sentier, mais sans succès ; il vous sera impossible de
              traverser cette forêt à cheval et vous allez devoir
              abandonner votre monture pour continuer votre chemin à
              pied. <L to="244">Rendez-vous au 244</L>.

            </div>
        );
    }
}

export default P291;
