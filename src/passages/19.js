/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P19 extends Component {
    render() {
        return (
            <div>

              A moins d'une vingtaine de mètres, un groupe d'hommes
              marche sur le pavé mouillé ; ils sont à votre recherche
              ; pour tenter de leur échapper, vous vous précipitez
              vers l'entrée sombre d'une petite boutique dans laquelle
              vous pénétrez aussitôt. Le cœur battant à vous rompre
              les côtes, vous priez le ciel qu'on ne vous ait pas
              repéré. <L to="71">Rendez-vous au 71</L>.

            </div>
        );
    }
}

export default P19;
