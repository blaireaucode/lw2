/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P303 extends Component {
    render() {
        return (
            <div>

              Des amas d'ordures pourrissantes ont été déversés sur
              cette partie du quai et l'odeur qui s'en dégage est si
              pestilentielle que vous vous couvrez la bouche et le nez
              d'un pan de votre cape. Un peu plus loin sur votre
              gauche, vous apercevez la lueur d'une torche qui filtre
              par une porte ouverte. Une enseigne est accrochée
              au-dessus de la porte et porte cette inscription :

              <p/>
              <SimpleImage width={200} src={'303.png'}/>
              <p/>
              
              Si vous souhaitez entrer dans le magasin, <L
              to="173">rendez-vous au 173</L>. Si vous préférez
              poursuivre en direction du sud, <L to="18">rendez-vous
              au 18</L>.

            </div>
        );
    }
}

export default P303;
