/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P273 extends Component {
    render() {
        return (
            <div>

              Vous vous apprêtez à frapper, mais votre adversaire
              s'écrie: «&nbsp;Je suis Ronan, my lord, et je ne vous veux
              aucun mal !&nbsp;» Vous détournez votre coup de justesse et
              votre arme s'abat dans le vide. La sueur qui perle sur
              le visage du marin semble confirmer qu'il dit bien la
              vérité. Rengainez votre arme et <L to="160"> rendez-vous
              au 160</L>.

            </div>
        );
    }
}

export default P273;
