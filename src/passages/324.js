/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P324 extends Component {
    render() {
        return (
            <div>

              Qu'allez-vous faire ? Dire que vous vous êtes perdu et
              demander un abri pour la nuit ? <L to="135">Rendez-vous
              au 135</L>. Vous faire passer pour un paysan qui cherche
              du travail ? <L to="174">Rendez-vous au
              174</L>. Demander votre chemin pour rejoindre Port Bax ?
              <L to="288">Rendez-vous au 288</L>.

            </div>
        );
    }
}

export default P324;
