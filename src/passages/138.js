/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P138 extends Component {
    render() {
        return (
            <div>

              Au bout d'une heure de marche, vous atteignez le sommet
              d'une colline.  Devant vous s'étend la forêt de
              Durenor. La route s'oriente vers l'est et s'enfonce sous
              les arbres à proximité d'une grande tour de bois. Vous
              apercevez devant la tour un soldat en faction. Si vous
              voulez poursuivre votre chemin en direction de la tour,
              <L to="232"> rendez-vous au 232</L>. Si vous préférez
              éviter le garde, faites un large détour et pénétrez dans
              la forêt plus loin au sud en <L to="244">vous rendant au
              244</L>.

              <p/>
              <SimpleImage width={250} src={'138.png'}/>

            </div>
        );
    }
}

export default P138;
