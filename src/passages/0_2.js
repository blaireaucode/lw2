/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Cont from '../Cont.js';

class P0_2 extends Component {
    render() {
        return (
            <div>

              Vous avez affronté de grands périls en vous rendant auprès du
              souverain ; l'ennemi avait déjà soumis la plus grande partie du
              royaume et son armée s'avançait vers Holmgard, la capitale,
              mais, en dépit du danger et au prix de rudes combats, vous avez
              réussi à atteindre le Palais et à prévenir le roi. A la cour, on
              a fait l'éloge de votre habileté et de votre bravoure mais on
              vous a également averti que votre mission était loin d'être
              terminée&nbsp;: à présent que les Seigneurs Kaï sont morts, il n'y a
              plus, sur toutes les terres de Magnamund, qu'une seule force
              assez puissante pour venir à bout des Maîtres des Ténèbres et
              cette force, c'est celle du Glaive de Sommer.

              <p/>
              <Cont to='0_3'/>

            </div>
        );
    }
}

export default P0_2;
