/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P184 extends Component {
    render() {
        return (
            <div>

              Le Drakkarim rend l'âme à vos pieds et les pirates
              saisis de panique s'enfuient à bord de leur bateau en
              train de sombrer ; voir un aussi puissant guerrier ainsi
              terrassé leur a fait perdre tout courage. Le capitaine
              Kelman rassemble aussitôt ses hommes et les lance à la
              poursuite de l'ennemi en déroute. Les pirates sont jetés
              par-dessus bord par les marins déchaînés. Le Sceptre
              Vert s'éloigne ensuite du vaisseau pirate qui donne de
              la bande sur tribord. «&nbsp;Merci au nom de tout
              l'équipage, Seigneur Kaï ! s'écrie alors le capitaine
              en vous serrant la main. Nous sommes fiers et
              reconnaissants de vous avoir parmi nous.&nbsp;» Une longue
              ovation retentit sur le pont : l'équipage tout entier
              vous rend hommage en même temps que le capitaine. Vous
              aidez ensuite à soigner les blessés tandis que l'on
              répare les dégâts subis à l'avant du navire. Et une
              heure plus tard, le vent enfle à nouveau les voiles :
              vous êtes reparti vers le royaume de Durenor. <L
              to="240">Rendez-vous au 240</L>.

            </div>
        );
    }
}

export default P184;
