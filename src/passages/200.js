/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P200 extends Component {
    render() {
        return (
            <div>

              Lorsque vous arrivez au bar, tous les autres sont déjà
              assis à une grande table et vous attendent. Vous vous
              approchez d'eux et, soudain, la vérité vous apparaît
              clairement : vous savez à présent qui a tenté de vous
              assassiner et vous décidez d'attaquer cet ennemi par
              surprise, sans le laisser soupçonner que vous avez vu
              clair dans son jeu. Pendant quelques instants, vous
              examinez attentivement le visage de vos compagnons de
              voyage et vous avez alors la certitude d'avoir deviné
              juste. Il ne vous reste plus qu'à passer à
              l'attaque. Mais qui est donc, selon vous, cet assassin
              présumé sur lequel vous allez vous précipiter à la
              seconde même&nbsp;?

              <p/>
              <SimpleImage width={400} src={'200.png'}/>
              <p/>

              Le Chevalier de la Montagne Blanche qui répond au nom de
              Dorier ? <L to="7">Rendez-vous au 7</L>. Le marchand
              nommé Halvorc ? <L to="60">Rendez-vous au 60</L>. Viveka
              l'aventurière ? <L to="85">Rendez-vous au 85</L>. Le
              moine nommé Parsion ? <L to="158">Rendez-vous au
              158</L>. Le Chevalier de la Montagne Blanche qui se
              nomme Ganon ? <L to="270">Rendez-vous au 270</L>.

            </div>
        );
    }
}

export default P200;
