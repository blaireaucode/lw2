/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

import update from 'immutability-helper';
import Span from '../Span.js';
import * as bh from '../book_helpers.js';


class P108 extends Component {

    constructor(props) {
        super(props);
        var fe = {
            'name': 'f1',
            'passage_title': 108,
            'endurance_done':false
        };
        this.fe=fe;
    }

    handleClick() {
        var e = bh.book_get_element(this.props, this.fe);
        e = update(e, {endurance_done:{$set:true}});
        this.props.book_set_element(e);
        const c = bh.endurance_add(this.props.character, -2);
        this.props.character_set(c);
    }
    
    render() {
        const e = bh.book_get_element(this.props, this.fe);
        const p1 = (e.endurance_done === false);
        const d = bh.has_discipline(this.props.character, "sixieme_sens");
        return (
            <div>

              L'une des roues se coince dans une ornière et trois de
              ses gros rayons de bois se brisent sous le choc. Il vous
              faut interrompre votre voyage et remplacer la roue avant
              de pouvoir repartir vers Port Bax. Vous vous proposez
              d'aider le conducteur en soulevant la diligence à l'aide
              d'un levier, puis en plaçant un petit tronc d'arbre sous
              l'essieu afin qu'on puisse glisser la nouvelle roue sur
              son axe. Vous pesez de tout votre poids sur la grosse
              branche qui fait office de levier lorsque les chevaux se
              cabrent soudain puis s'élancent en avant. La branche
              aussitôt se détend comme un ressort et vous frappe en
              plein visage en vous projetant à terre. Vous êtes à
              moitié assommé et vous perdez <L to='108'
              enabled={p1} onClick={this.handleClick.bind(this)}>2
              points d'ENDURANCE</L>.

              <Span enabled={!p1}>
                
                <p/> Le conducteur a moins de chance que vous car la
                diligence lui a passé sur le corps. Le malheureux
                meurt dans vos bras après avoir réussi, dans un ultime
                effort, à vous murmurer ces quelques mots à l'oreille:
                «&nbsp;Pas... un accident... j'ai vu... ». Si vous
                maîtrisez la Discipline Kaï du Sixième Sens, <L
                enabled={d} to="343">rendez-vous au 343</L>. Sinon, <L
                to="168">rendez-vous au 168</L>.

              </Span>

            </div>
        );
    }
}

export default P108;
