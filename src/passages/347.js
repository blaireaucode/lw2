/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import EnduranceChange from '../EnduranceChange.js';
import * as bh from '../book_helpers.js';

class P347 extends Component {
    render() {
        const f = {
            name: 'a',
            passage_title: 347,
            already_done: false,
            dmg:-1
        };
        const d = bh.book_get_element(this.props, f).already_done;
        return (
            <div>

              Au bout de cette rue se trouve une grande écurie. A
              votre droite, la populace déchaînée est en train de
              fouiller les boutiques et les maisons pour essayer de
              vous retrouver. Soudain, un homme vous aperçoit et donne
              l'alerte. «&nbsp;Il est là !  s'écrie-t-il, c'est lui,
              c'est l'assassin !&nbsp;» Vous n'avez pas le temps de
              réfléchir : vous vous précipitez à l'intérieur de
              l'écurie et vous détachez un cheval ; vous bondissez
              aussitôt sur sa croupe et vous filez au galop. Quelqu'un
              vous lance alors une hache qui vous atteint à l'épaule
              en n'occasionnant cependant qu'une simple
              égratignure. Vous perdez malgré tout <EnduranceChange
              f={f}> 1 points d'ENDURANCE</EnduranceChange> et vous
              disparaissez dans la nuit, loin de vos poursuivants. <L
              enabled={d} to='150'>Rendez-vous au 150</L>.

            </div>
        );
    }
}

export default P347;
