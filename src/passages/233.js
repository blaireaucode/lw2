/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';
import HelpGold from '../HelpGold.js';

class P233 extends Component {

    pay(n) {
        const d = bh.get_done(this, 233);
        if (!d) {
            const c = bh.gold_add(this.props.character, n);
            this.props.character_set(c);
        }
        bh.update_done(this, 233);
    }
    
    render() {
        const g = bh.gold_get(this.props.character);
        return (
            <div>

              «&nbsp;Nous allons à Ragadorn, nous devrions arriver
              là-bas vers midi, dit-il, le visage presque entièrement
              dissimulé sous son chapeau à larges bords, le billet
              coûte 3 Couronnes, mais si vous voulez voyager sur le
              toit, vous n'aurez qu'une seule couronne à payer. » Si
              vous souhaitez faire le voyage à l'intérieur de la
              diligence, donnez 3 Couronnes au cocher et <L
              enabled={g>=3} onClick={this.pay.bind(this,-3)}
              to="37">rendez-vous au 37</L>. Si vous préférez faire le
              trajet sur le toit, donnez-lui 1 Couronne et <L
              enabled={g>=1} onClick={this.pay.bind(this,-1)}
              to="148">rendez-vous au 148</L>. Si vous n'avez pas les
              moyens de payer, il ne vous reste plus qu'à repartir à
              pied <L to='292'>en vous rendant au 292</L>.

              <p/>
              <HelpGold/>

            </div>
        );
    }
}

export default P233;
