/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P329 extends Component {
    render() {
        const f = {
            'name': 'obj1',
            'passage_title': 329,
            'list': [ {id:'PO', nb:10} ],
        };
        return (
            <div>

              «&nbsp;Félicitations, Loup Solitaire, dit bientôt le
              capitaine en essuyant la sueur qui perle à son front,
              vous êtes un joueur de première force et vous avez
              gagné.&nbsp;» Il fouille dans une poche de son gilet et vous
              tend une bourse contenant 10 Pièces d'Or. Vous le
              remerciez d'avoir joué avec vous et vous lui proposez de
              prendre sa revanche le lendemain soir. Avec un sourire
              quelque peu amer, il accepte votre offre et vous
              souhaite bonne nuit. Rentrez dormir dans votre cabine, à
              présent, <L to='197'>en vous rendant au 197</L>.

              <p/>
              <ItemsPicker items={f}/>
              
            </div>
        );
    }
}

export default P329;
