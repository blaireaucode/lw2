/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P179 extends Component {
    render() {
        return (
            <div>

            Votre maîtrise du Camouflage vous permet de vous
            dissimuler dans la charrette à foin en étant sûr de n'être
            pas découvert. Lorsque, enfin, tout danger est écarté,
            vous sortez de votre cachette. Si pour plus de sûreté,
            vous souhaitez rester caché un peu plus longtemps, vous
            pouvez vous réfugier au sommet d'une autre meule de foin
            entassée à quelque distance, <L to='82'>rendez-vous alors
            au 82</L>. Si vous préférez prendre un cheval et quitter
            le village, <L to="150">rendez-vous au 150</L>. Si enfin
            vous choisissez d'entrer dans la boutique du charron, <L
            to="71">rendez-vous au 71</L>.

            </div>
        );
    }
}

export default P179;
