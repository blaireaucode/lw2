/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P236 extends Component {
    render() {
        return (
            <div>

              La panique s'empare du navire. Saisis d'une véritable
              frénésie, les marins rassemblent tous les seaux et les
              couvertures qu'ils peuvent trouver pour combattre
              l'incendie. Les flammes jaillissent de l'écoutille et il
              faut plus d'une heure pour maîtriser le feu. Les dégâts
              sont considérables. Les vivres et les provisions d'eau
              douce ont été anéantis et la structure centrale du
              navire gravement endommagée. Le capitaine émerge alors
              de la cale enfumée et s'approche de vous, le visage noir
              de suie. Il porte un paquet sous son bras. «&nbsp;Je dois vous
              parler en privé, my Lord&nbsp;», dit-il à voix basse. Sans
              rien répondre, vous le suivez aussitôt dans sa
              cabine. <L to="222">Rendez-vous au 222</L>.

            </div>
        );
    }
}

export default P236;
