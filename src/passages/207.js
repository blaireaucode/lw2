/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P207 extends Component {
    render() {
        return (
            <div>

              Moins de 100 mètres plus loin, le sentier s'arrête au
              bord d'un précipice.  Les eaux du chenal de Ryner
              coulent au-dessous et il est impossible d'aller plus
              loin. Il ne vous reste donc plus qu'à rebrousser chemin
              et à prendre le pont qui traverse le chenal. <L
              to="47">Rendez-vous au 47</L>.

            </div>
        );
    }
}

export default P207;
