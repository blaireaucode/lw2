/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P86 extends Component {
    constructor(props) {
        super(props);
        const f = {
            'name': 'obj1',
            'passage_title': 86,
            'list': [
                {id:'masse_d_armes'},
                {id:'PO', nb:3},
            ],
        };
        this.items = f;
    }

    render() {
        return (
            <div>

              De ce côté du port, de nombreux navires sont amarrés ;
              il y a là des bateaux de toutes sortes qui battent
              pavillon de tous pays. Le fleuve Dom qui traverse la
              ville de Ragadorn connaît toujours une très grande
              activité : c'est la voie navigable la plus importante de
              la région. Vous êtes sur le point d'abandonner vos
              recherches lorsque vous repérez enfin le bateau de pêche
              de vos malandrins.

              <p/>

              Il n'y a personne à bord, mais une
              fouille en règle vous permet de découvrir une Masse
              d'Armes et trois Pièces d'Or dissimulées dans un hamac
              soigneusement plié. Une étiquette est cousue sur le
              hamac et porte ces mots : Taverne de l'Étoile du Nord
              rue de la Bernicle. Vous prenez la Masse d'Armes et les
              Pièces d'Or et vous retournez sur la place du Poteau de
              Pierre.

              <p/>
              <ItemsPicker items={this.items}/>
              <p/>


              Si vous souhaitez aller vers l'est, le long de la rue de
              la Bernicle, rendez-vous au 215. Si vous préférez aller
              au sud, en suivant le Dock de la rive Ouest, <L
              to="303">rendez-vous au 303</L>. Enfin, si vous
              choisissez plutôt de prendre la direction du nord en
              empruntant la rue du Butin, <L to="129">rendez-vous au
              129</L>.


            </div>
        );
    }
}

export default P86;
