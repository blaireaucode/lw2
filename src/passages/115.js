/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P115 extends Component {
    render() {
        return (
            <div>

              Devant la porte de la tour de guet, la végétation a été
              arrachée et le sol bien tassé par de nombreux
              passages. Vous êtes en train de chercher un trou de
              serrure sur cette porte à l'armature de fer lorsqu'elle
              s'ouvre soudain. Un Chevalier de la Montagne Blanche se
              tient devant vous, son épée levée face à son
              visage. «&nbsp;Exposez le but de votre visite et parlez
              sans détour. Si vous mentez, je vous répondrai par le
              glaive. » Si vous souhaitez révéler au chevalier le
              véritable but de votre voyage à Durenor, <L
              to="80">rendez-vous au 80</L>. Si vous voulez lui
              mentir, <L to="324">rendez-vous au 324</L>. Si enfin
              vous préférez tirer votre épée et l'attaquer,<L
              to="162"> rendez-vous au 162</L>.

            </div>
        );
    }
}

export default P115;
