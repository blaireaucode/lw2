/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P302 extends Component {
    render() {
        const f = {
            'name': 'obj1',
            'passage_title': 302,
            'list': [
                {id:'masse_d_armes'},
                {id:'baton'},
                {id:'potion_guerison'},
                {id:'repas', nb:3},
                {id:'sac_a_dos'},
                {id:'PO', nb:12},
            ],
        };
        return (
            <div>

              Vous enjambez le corps puis vous montez l'escalier pour
              fouiller la tour. Au cours d'une perquisition en règle,
              vous trouvez les objets suivants : Masse d'Armes,
              Glaive, Bâton, Potion de Guérison (une dose qui vous
              redonne 3 points d'ENDURANCE si vous la buvez après un
              combat), une quantité de nourriture équivalant à 3
              Repas, un Sac à Dos, 12 Pièces d'Or. Prenez ce dont vous
              avez besoin, modifiez en conséquence votre Feuille
              d'Aventure et hâtez-vous de quitter la tour de peur que
              quelqu'un ne découvre votre présence. La forêt qu'il
              vous faut traverser est très dense et vous allez devoir
              abandonner votre cheval pour continuer votre chemin à
              pied. <L to="244">Rendez-vous au 244</L>.

              <p/>
              <ItemsPicker items={f}/>

            </div>
        );
    }
}

export default P302;
