/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P328 extends Component {

    render() {
        const d = bh.has_discipline(this.props.character, 'orientation');
        const l = this.props.character.items.some(e => e.id === 'pendentif_etoile_cristal');        
        return (
            <div>

              Deux Zombies essaient de vous interdire le passage, mais
              vous leur tranchez le corps à tous deux d'un seul coup
              du Glaive de Sommer.  Vous vous trouvez à présent au
              pied de la tour et vous apercevez audessus de vous la
              silhouette d'un homme bossu, vêtu d'une robe écarlate et
              coiffé d'un tokmor, un turban de magicien, sur lequel
              l'image d'un serpent a été brodée. L'homme tient un
              bâton noir dans sa main droite.  Si vous possédez un
              Pendentif avec une Etoile de Cristal, <L enabled={l}
              to='113'>rendez-vous au 113</L>. Si vous maîtrisez la
              Discipline Kaï de l'Orientation, <L enabled={d}
              to="204">rendez-vous au 204</L>. Si vous souhaitez
              monter en haut de la tour pour attaquer le bossu, <L
              to="73">rendez-vous au 73</L>. Si vous préférez vous
              enfuir de ce vaisseau en sautant par-dessus bord, <L
              to="267">rendez-vous au 267</L>.

            </div>
        );
    }
}

export default P328;
