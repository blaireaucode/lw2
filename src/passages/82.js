/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P82 extends Component {
    render() {
        return (
            <div>

              Lorsque vous êtes assuré que la populace s'est bel et
              bien éloignée, vous sautez de la meule et vous courez le
              long de la rue en vous dissimulant dans l'ombre et en
              prenant bien garde à ne pas faire de bruit. Bientôt,
              vous apercevez sur votre gauche une boutique qui porte
              cette enseigne :

              <p/>
              <SimpleImage src={'82.png'}/>
              <p/>
              
              La vitrine de la boutique est éclairée et la porte
              ouverte. Si vous voulez entrer dans les lieux, <L
              to="55">rendez-vous au 55</L>. Si vous préférez
              continuer à courir le long de la rue, <L
              to="347">rendez-vous au 347</L>.

            </div>
        );
    }
}

export default P82;
