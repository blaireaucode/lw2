/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P315 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, "maitrise_psychique_de_la_matiere");
        return (
            <div>

              Tout en gardant un œil sur la porte, vous fouillez
              rapidement les tiroirs et les papiers disposés sur une
              table ouvragée mais vous ne trouvez rien de suspect. Il
              n'y a là que des cartes maritimes et des instruments de
              navigation. Vous êtes sur le point d'abandonner vos
              recherches lorsque vous découvrez un petit levier
              dissimulé sous la table. Vous l'actionnez et un panneau
              glisse aussitôt, révélant une cachette dans laquelle un
              petit coffret à la serrure de cuivre a été déposé. Si
              vous voulez forcer la serrure de cette boîte, <L
              to="190">rendez-vous au 190</L>. Si vous préférez
              remettre le coffret à sa place et rejoindre le capitaine
              sur le pont avant qu'il ne soupçonne quelque chose, <L
              to="175">rendez-vous au 175</L>. Enfin, si vous possédez
              la Discipline Kaï de la Maîtrise Psychique de la
              Matière, <L enabled={d} to='287'>rendez-vous au 287</L>.

            </div>
        );
    }
}

export default P315;
