/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P93 extends Component {

    constructor(props) {
        super(props);
        const f = {
            'name': 'obj1',
            'passage_title': 93,
            'list': [ ],
        };
        this.items = f;
    }
    
    render() {
        return (
            <div>

              Déduisez de votre Feuille d'Aventure le nombre de Pièces
              d'Or que vous voulez donner aux mendiants.

              <p/>
              <ItemsPicker items={this.items}/>
              <p/>

              Ils vous remercient, mais d'autres mendiants
              apparaissent aussitôt en demandant que vous leur fassiez
              également l'aumône. Finalement, vous parvenez à vous
              frayer un chemin dans la foule et vous poursuivez votre
              route. <L to='137'>Rendez-vous au 137</L>.

            </div>
        );
    }
}

export default P93;
