/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P39 extends Component {
    render() {
        const l = this.props.character.items.some(e => e.id === 'billet_Port_Bax');
        return (
            <div>

              A la tombée du jour, la diligence s'arrête devant une
              auberge sur la route qui longe la côte en direction de
              Port Bax. Le prix d'une chambre pour la nuit s'élève à 1
              Couronne d'Or pour les passagers de la diligence et à 3
              Couronnes pour les autres clients. Au moment où vous
              vous apprêtez à entrer, le conducteur de la diligence
              vous demande votre billet. Si vous avez un billet pour
              Port Bax, <L enabled={l} to="346">rendez-vous au
              346</L>. Si vous n'avez pas de billet, <L
              to="156">rendez-vous au 156</L>.

            </div>
        );
    }
}

export default P39;
