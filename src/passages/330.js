/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import EnduranceChange from '../EnduranceChange.js';
import * as bh from '../book_helpers.js';

class P330 extends Component {
    render() {
        const f = {
            'name': 'a',
            'passage_title': 330,
            'already_done': false,
            'dmg':-5
        };
        const d = bh.book_get_element(this.props, f).already_done;
        return (
            <div>

              Quelques secondes plus tard, vous vous sentez très mal
              et vous sombrez dans l'inconscience. Il s'est écoulé
              presque une heure lorsque vous vous réveillez. Vous êtes
              encore terriblement malade, mais vous avez survécu aux
              effets du poison. Vous perdez <EnduranceChange f={f}> 5
              points d'ENDURANCE</EnduranceChange>, cependant. Puis,
              tandis que vos forces reviennent peu à peu, la fureur
              vous envahit : vous ramassez vos affaires et vous
              quittez aussitôt la chambre d'un pas chancelant, bien
              décidé à démasquer celui ou celle qui a tenté de vous
              assassiner. <L enabled={d} to='200'>Rendez-vous au
              200</L>.

            </div>
        );
    }
}

export default P330;
