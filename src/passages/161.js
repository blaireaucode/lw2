/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P161 extends Component {
    render() {
        return (
            <div>

              La boutique est déserte. Vous attendez en examinant
              pendant cinq minutes les articles exposés, mais personne
              ne vient. Vous vous apprêtez à repartir lorsque vous
              remarquez une carte accrochée derrière la porte. C'est
              un plan du port de Ragadorn. Les écuries et le relais de
              la diligence sont clairement indiqués à proximité de la
              porte Est de la ville. C'est là que vous trouverez un
              moyen de transport qui vous permettra d'atteindre Port
              Bax. Vous repérez le trajet qui mène à la porte Est et
              vous quittez la boutique. Vous rebroussez chemin au pas
              de course dans la rue de la Hache puis vous tournez vers
              l'est, dans la rue du Sage. Le pont de Ragadorn se
              trouve tout au bout de cette voie sinueuse ; c'est là le
              seul point de passage qui relie les parties Est et Ouest
              de la ville. Vous vous frayez un chemin dans la foule
              qui se presse sur le pont, puis, dès que vous êtes
              arrivé de l'autre côté, vous vous mettez à courir sur
              les pavés le long du boulevard du Commerce, section
              Est. <L to="186">Rendez-vous au 186</L>.

            </div>
        );
    }
}

export default P161;
