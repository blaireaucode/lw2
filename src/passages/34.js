/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import * as bh from '../book_helpers.js';

class P34 extends Component {

    constructor(props) {
        super(props);
        const f = {
            'name': 'f1',
            'passage_title': 34,
            'encounter': 'GLOKS',
            'combat_skill':16,
            'endurance':14,
            'multiple': true
        };
        this.f = f;
    }

    render() {
        const r = bh.enabled_if_fight_win(this.props, this.f);
        return (
            <div>

              Tandis que vous fermez la porte de votre cabine, vous
              entendez les cris frénétiques de l'équipage qui se
              prépare à repousser les assaillants. Soudain, un bruit
              sourd ébranle le navire ; quelque chose vient de heurter
              le pont arrière ; retentissent alors des hurlements
              aigus que vous reconnaissez aussitôt : ce sont des GLOKS
              qui s'égosillent ainsi ! Les Bêtalzans ont déposé des
              GLOKS sur le bateau et bientôt la porte de votre cabine
              s'ouvre à la volée. Trois de ces hideuses créatures à la
              peau grise vous font face, en brandissant leurs épées à
              la lame tranchante et ruisselante de sang. Il vous est
              impossible de prendre la fuite et vous allez devoir les
              combattre en les considérant comme un seul et même
              ennemi. Si vous êtes vainqueur, <L enabled={r}
              to="345">rendez-vous au 345</L>.

              <p/>
              <Fight fight={this.f}/>
              
            </div>
        );
    }
}

export default P34;
