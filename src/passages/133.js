/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P133 extends Component {

    render() {
        return (
            <div>

              Vous regardez le marin droit dans les yeux et vous
              concentrez votre Puissance Psychique sur sa main
              ouverte. Soudain, l'homme tombe de sa chaise en se
              tenant la main et en hurlant comme s'il venait de saisir
              des charbons ardents. Lorsque vous lui expliquez que
              seul votre pouvoir a provoqué cette douleur, il vous
              contemple d'un air stupéfait. <L to="268">Rendez-vous
              au 268</L>.

            </div>
        );
    }
}

export default P133;
