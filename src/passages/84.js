/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P84 extends Component {
    render() {
        return (
            <div>

              Dans l'entrée principale, un vieil homme est assis ; il
              arbore une longue barbe et semble fort aimable. Penché
              sur un lutrin, il est en train d'étudier un énorme livre
              à la reliure de cuir. Sa lecture l'absorbe tant qu'il
              n'a pas remarqué votre présence dans l'enceinte de
              l'hôtel de ville.  Si vous souhaitez lui demander le
              chemin du consulat du Sommerlund, <L
              to="211"> rendez-vous au 211</L>. Si vous préférez
              repartir et trouver vous-même votre chemin, <L
              to="191"> rendez-vous au 191</L>.

            </div>
        );
    }
}

export default P84;
