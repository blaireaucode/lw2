/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P50 extends Component {
    render() {
        return (
            <div>

              Le moine se penche soudain en avant et dépose une autre
              Couronne d'Or sur l'assiette ; la diligence est alors
              autorisée à poursuivre sa route.  «&nbsp;Peut-être
              pourrez-vous rendre la pareille un jour, mon fils », dit
              le moine en reprenant place sur la banquette avant que
              vous ayez pu dire un mot. Vous remarquez alors que le
              capuchon de sa robe de bure maintient constamment son
              visage dans l'ombre : voilà qui est étrange...  Bientôt,
              la diligence traverse la rivière en crue et le voyage se
              poursuit.  <L to="249">Rendez-vous au 249</L>.

            </div>
        );
    }
}

export default P50;
