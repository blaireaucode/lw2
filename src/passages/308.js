/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Hublot from '../Hublot.js';

class P308 extends Component {
    render() {

        const r = {
            passage_title: 308,
            total_win: 0,
        };
        return (
            <div>

              Un marin du nom de Sprogg s'est assis à côté de vous et
              vous explique les règles du jeu de «&nbsp;Hublot&nbsp;». Il
              vous montre d'abord une paire de dés en forme de
              diamant, taillés dans du verre rouge. Chaque dé possède
              dix faces numérotées de 0 à 9 ; les joueurs doivent
              lancer les deux dés et ajouter les chiffres
              obtenus. Celui qui tire deux 0 crie «&nbsp;Hublot !&nbsp;»
              et gagne automatiquement. Chaque joueur mise 3
              Couronnes à chaque lancer de dés. Il y a trois joueurs
              en tout, et chacun dépose ses Pièces d'Or dans un
              chapeau avant que les dés soient jetés. Utilisez la
              Table de Hasard pour obtenir deux chiffres.  Faites
              ainsi deux tirages de deux chiffres qui correspondront
              aux scores obtenus par chacun des deux autres joueurs,
              puis notez le résultat de ces deux tirages
              consécutifs. Ensuite, la Table de Hasard vous donnera
              votre propre score (vous tirerez également deux chiffres
              pour vous-même). Si le total que vous obtenez est
              supérieur à celui de chacun des deux autres joueurs vous
              gagnerez 6 Couronnes. Si l'un des deux autres joueurs, a
              fait un tirage supérieur au vôtre, vous perdrez 3
              Couronnes.  En cas d'égalité entre deux joueurs, la
              partie est annulée et vous recommencez le tirage depuis
              le début.

              <p/>

              Vous pouvez jouer autant que vous voudrez, sans dépasser
              un gain maximum de 40 Couronnes. Vous avez également le
              droit de quitter la table de jeu quand bon vous semblera
              et vous êtes bien entendu obligé d'abandonner la partie
              si vous perdez toutes vos Pièces d'Or. Lorsque vous
              aurez décidé d'arrêter de jouer, modifiez votre Feuille
              d'Aventure en fonction de vos gains ou de vos pertes et
              rentrez dormir dans votre cabine <L to='197'>en vous
              rendant au 197</L>.

              <p/>
              <Hublot r={r}/>


            </div>
        );
    }
}

export default P308;
