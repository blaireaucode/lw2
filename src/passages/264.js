/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P264 extends Component {
    render() {
        return (
            <div>

              Vous concentrez toute l'énergie de votre pouvoir sur le
              reptile et vous lui ordonnez de partir à l'instant en
              quête d'une proie. Lentement, votre puissance de
              suggestion fait son effet et le serpent s'éloigne enfin,
              puis disparaît dans les hautes herbes. Vous poussez un
              soupir de soulagement et, pour plus de sûreté, vous
              grimpez à l'arbre où vous passerez le reste de la nuit à
              l'abri du feuillage et à bonne distance du sol. <L
              to="312">Rendez-vous au 312</L>.

            </div>
        );
    }
}

export default P264;
