/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

import HelpCharacter from '../HelpCharacter.js';
import HelpGold from '../HelpGold.js';
import EnduranceChange from '../EnduranceChange.js';
import * as bh from '../book_helpers.js';

class P72 extends Component {
    constructor(props) {
        super(props);
        this.f = {
            'name': 'a',
            'passage_title': 72,
            'already_done': false,
            'dmg':1
        };
    }
       
    handleClickGold() {
        const c = bh.gold_add(this.props.character, -2);
        this.props.character_set(c);            
    }
       
    render() {
        const g = bh.gold_get(this.props.character);
        const l = (g>=2);        
        return (
            <div>

              L'aubergiste prend votre Pièce d'Or et pose devant vous
              une chope de bière mousseuse. C'est une bière forte et
              revigorante qui vous redonne <EnduranceChange
              f={this.f}> un point d'ENDURANCE</EnduranceChange>. Si
              vous souhaitez parler à l'aubergiste, <L
              to="226">rendez-vous au 226</L>. Si vous désirez prendre
              une chambre pour la nuit, il vous en coûtera 2 Pièces
              d'Or et vous vous <L to='56'
              onClick={this.handleClickGold.bind(this)}
              enabled={l}>rendrez au 56</L>. Si enfin vous souhaitez
              engager une partie de bras de fer, <L
              to="276">rendez-vous au 276</L>.

              <p/>
              <HelpCharacter/> <br/> <HelpGold/>

            </div>
        );
    }
}

export default P72;
