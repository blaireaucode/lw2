/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import EnduranceChange from '../EnduranceChange.js';
import * as bh from '../book_helpers.js';

class P338 extends Component {

    drop() {
        const f = {
            name: 'b',
            passage_title: 338,
            already_done: false,
        };
        const d = bh.book_get_element(this.props, f);
        if (!d) {
            const c = bh.item_drop(this.props.character, "magical_spear");
            this.props.character_set(c);
        }
    }
    
    render() {
        const f = {
            name: 'a',
            passage_title: 338,
            already_done: false,
            dmg:-2
        };
        const d = bh.book_get_element(this.props, f).already_done;
        return (
            <div>

              Vous empoignez la lance et vous la levez au-dessus de
              votre tête en visant le Monstre d'Enfer qui se met à
              hurler de terreur : il sait en effet que le fer de votre
              lance lui sera fatal. Sous le choc, vous tombez tous
              deux sur la chaussée en contrebas. La chute est rude et
              vous coûte <EnduranceChange f={f}> 2 points
              d'ENDURANCE</EnduranceChange>. Quant au Monstre d'Enfer,
              il s'écrase sur la lance plantée dans sa poitrine et le
              fer lui transperce instantanément le cœur. Si vous
              souhaitez arracher votre lance du corps de la créature,
              <L enabled={d} to="269"> rendez-vous au 269</L>. Si vous préférez
              abandonner la lance et prendre la fuite aussi vite que
              possible, <L enabled={d} onClick={this.drop.bind(this)}
              to="349"> rendez-vous au 349</L>.

            </div>
        );
    }
}

export default P338;
