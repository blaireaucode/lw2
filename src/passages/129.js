/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P129 extends Component {
    render() {
        return (
            <div>

              Vous passez devant plusieurs entrepôts alignés sur le
              quai et vous arrivez au mur d'enceinte du port. Là, le
              chemin que vous suivez tourne brusquement à droite pour
              aboutir dans la rue du Tombeau. Quatre gardes en armes
              marchent au milieu de la rue. Vous ne voulez pas prendre
              le risque d'être interpellé et arrêté par ces soldats et
              vous vous réfugiez dans une ruelle en cul-de-sac, à
              votre droite. Mais soudain les gardes s'immobilisent à
              l'entrée de la ruelle. Il suffirait que l'un d'eux
              tourne la tête dans votre direction pour que vous soyez
              immédiatement repéré. Cherchant une issue, vous
              apercevez derrière vous une fenêtre ouverte : un coup
              d'oeil à l'intérieur vous permet de distinguer la salle
              comble d'une taverne. Il n'y a pas à hésiter: en un
              instant, vous enjambez le rebord de la fenêtre et vous
              entrez dans la taverne. <L to="4">Rendez-vous au 4</L>.

            </div>
        );
    }
}

export default P129;
