/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';
import SimpleImage from '../SimpleImage.js';
import * as bh from '../book_helpers.js';

class P144 extends Component {

    constructor(props) {
        super(props);
        const f = {
            'name': 'obj1',
            'passage_title': 144,
            'list': [ {id:'repas', nb:2} ],
        };
        this.items = f;
    }

    handleClickGold() {
        const g = bh.gold_get(this.props.character);
        const c = bh.gold_add(this.props.character, -g);
        this.props.character_set(c);            
    }
    
    render() {
        const g = bh.gold_get(this.props.character);
        const l = (g>=1);
        return (
            <div>

              Un grand Noudic vêtu d'une cape de soie en patchwork aux
              couleurs vives ordonne à quelques-uns de ses congénères
              de prendre leurs armes et de vous reconduire
              au-dehors. Vous leur parlez alors dans leur propre
              langue et un murmure de stupeur parcourt aussitôt la
              caverne.  Jamais encore ils n'ont rencontré d'être
              humain qui sache parler leur dialecte. Certains d'entre
              eux en sont abasourdis au point de vous contempler
              bouche bée, les yeux ronds et les pattes ballantes. Le
              grand Noudic s'adresse alors à vous en se présentant
              comme le chef de la colonie. Il déclare se nommer
              Gashiss et vous souhaite la bienvenue en vous invitant à
              le rejoindre sur une estrade dressée au centre de la
              caverne.

              <p/>
              
              «&nbsp;Vouzz n'êtezz pazz de Dzurzenorz, vouzz
              l'homme-là, heinzz ? vous demande-t-il avec un fort
              accent noudic, d'ouzz venezz vouzz donczz ? » Vous lui
              dites que vous êtes sommerlundois et que vous vous
              rendez à Hammardal. Le Noudic alors vous jette un regard
              inquiet. «&nbsp;Vouzz n'êtezz pazz unzz Zombizarre, au
              moinzz, j'espèrezz?&nbsp;» demande-t-il d'une voix
              anxieuse. Vous comprenez aussitôt que le mot
              «&nbsp;Zombizarre&nbsp;» désigne les Monstres d'Enfer
              dans la langue noudic et il vous apprend bientôt que
              deux de ces créatures malfaisantes sont arrivées à
              Tarnalin il y a deux heures et ont provoqué une panique
              générale dans le tunnel. Gashiss sait où ces deux
              monstres se cachent; ils vous attendent pour vous tendre
              une embuscade. «&nbsp;Vouzz voulezz que je vouzz
              montrezz commentzz lezz évitezz, vouzz, l'homme-là,
              heinzz ?&nbsp;» propose-t-il. Vous acceptez volontiers
              cette offre et il vous fait signe de le suivre au bas de
              l'estrade.

              <p/>
              <SimpleImage width={150} src={'144.png'}/>

              <p/>

              Les Noudics à présent ont surmonté leur stupeur et ils
              semblent vous considérer comme l'un d'eux. Avant que
              vous ne quittiez la caverne, une jolie femelle noudic
              vous offre quelques provisions. Il y a là l'équivalent
              de 2 Repas. Vous la remerciez de sa générosité et vous
              suivez Gashiss le long d'un des nombreux couloirs qui
              partent de la caverne. Au bout d'une heure de marche
              dans l'obscurité, il s'arrête et vous montre un rayon de
              lumière qui filtre par une crevasse à quelque
              distance. «&nbsp;Enzz sortantzz par làzz, vouzz n'aurezz
              plus rienzz à craindre, vouzz, l'homme-là, heinzz
              !&nbsp;» déclare votre guide. Vous le remerciez de vous
              avoir aidé mais vous remerciez surtout en votre for
              intérieur les Maîtres Kaï qui vous ont enseigné la
              Discipline de la Communication Animale. Ces longues
              années d'apprentissage vous ont sans doute sauvé la
              vie.

              <p/>
              <ItemsPicker items={this.items}/>

              <p/>

              Vous vous faufilez bientôt par une crevasse de la paroi
              rocheuse et vous vous laissez tomber sur la chaussée qui
              longe le mur à un mètre au-dessous.  Les Noudics se sont
              montrés fort serviables et vous leur en êtes très
              reconnaissant jusqu'au moment où vous vous apercevez
              qu'<L to='144' onClick={this.handleClickGold.bind(this)}
              enabled={l}>il ne vous reste plus une seule Pièce
              d'Or</L> ! Ils vous ont tout dérobé et vous n'avez plus
              qu'à modifier votre Feuille d'Aventure en
              conséquence. Vous vous trouvez toujours dans le tunnel
              de Tarnelin que vous continuez à suivre en <L to='349'
              enabled={!l}>vous rendant au 349</L>.

            </div>
        );
    }
}

export default P144;
