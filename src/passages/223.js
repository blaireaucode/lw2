/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P223 extends Component {
    render() {
        return (
            <div>

              Le garde contemple avec une stupeur mêlée de respect le
              magnifique anneau que vous lui montrez. Les habitants de
              Durenor connaissent bien la légende du Sceau d'Hammardal
              et l'on dit que de tous les trésors perdus du royaume,
              le Sceau est celui dont personne ne souhaite le
              retour. Le visage inquiet du soldat montre qu'il sait
              parfaitement ce que signifie le retour du Sceau
              d'Hammardal : c'est la guerre qu'il annonce.  «&nbsp;Je
              ne peux malheureusement rien faire pour vous aider, dit
              le garde, si ce n'est vous indiquer la route pour Port
              Bax. Suivez ce chemin forestier et vous arriverez à une
              bifurcation, à proximité d'un petit chêne. Prenez alors
              le sentier de gauche, c'est un raccourci.&nbsp;» Vous
              remerciez ce loyal soldat et vous repartez dans la
              forêt.

              <p/>

              Deux kilomètres plus loin environ, vous arrivez à la
              bifurcation et vous prenez le sentier de gauche. Il vous
              amène à un pont de pierre qui traverse le chenal de
              Ryner. Les eaux du chenal font plus de 1500 m de
              profondeur et plus de 3 kilomètres dans leur plus grande
              largeur. Près du pont un poteau indicateur précise :
              PORT BAX 5 km. Vous poussez un soupir de soulagement car
              dans moins d'une heure, vous devriez être rendu. <L
              to="265">Rendez-vous au 265</L>.

            </div>
        );
    }
}

export default P223;
