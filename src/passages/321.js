/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import EnduranceChange from '../EnduranceChange.js';
import update from 'immutability-helper';
import * as bh from '../book_helpers.js';

class P321 extends Component {

    constructor(props) {
        super(props);
        this.f = {
            'name': 'a',
            'passage_title': 321,
            'already_done': false,
            'dmg':-2
        };
    }

    handleClickMeal() {
        const elem = bh.book_get_element(this.props, this.f);
        if (!elem.already_done) {
            const e = update(elem, {already_done: {$set: true}});
            this.props.book_set_element(e);
            var c = bh.endurance_add(this.props.character, 3);
            c = bh.item_drop(c, 'repas');
            this.props.character_set(c);
        }        
    }

    render() {
        const elem = bh.book_get_element(this.props, this.f);
        const already_done = elem.already_done;
        const index = bh.item_get_index(this.props.character.items, 'repas');
        const meal = (index !== -1);
        return (
            <div>

              Le repas frugal est composé de restes de la veille qui
              n'ont rien d'appétissant. A la fin de ce piètre souper,
              le capitaine vous fait quelques confidences qui
              confirment vos craintes. «&nbsp;Je dois vous avouer
              quelque chose, Seigneur Kaï, dit-il : le feu a détruit
              tous nos vivres et il ne restait plus dans la cambuse
              que de quoi préparer ce maigre repas. D'ici à Port Bax,
              il faudra nous contenter du poisson que nous pourrons
              pêcher...&nbsp;» A moins qu'il ne vous reste <L to='321'
              onClick={this.handleClickMeal.bind(this)}
              enabled={!already_done && meal}> de quoi manger</L> dans
              votre Sac à Dos, ce détestable dîner vous laisse sur
              votre faim et vous perdez <EnduranceChange f={this.f}>
              perdrez 2 points d'ENDURANCE</EnduranceChange>.

              <p/>

              Plus tard dans la soirée, le capitaine vous propose une
              partie de Samor ; c'est un jeu semblable aux échecs qui
              demande beaucoup d'ingéniosité et d'audace. Pour ajouter
              à l'intérêt de la partie, le capitaine vous invite à
              miser un peu d'or. Si vous acceptez son offre, <L
              enabled={already_done} to="12">rendez-vous au 12</L>. Si
              en revanche vous n'avez pas envie de jouer, souhaitez
              une bonne nuit au capitaine et rentrez dormir dans votre
              cabine <L enabled={already_done} to='197'>en vous
              rendant au 197</L>.

            </div>
        );
    }
}

export default P321;
