/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P220 extends Component {
    render() {
        const f = {
            passage_title: 220,
            list: [ {id:'PO', nb:23} ] };
        return (
            <div>

              En fouillant son cadavre, vous découvrez des preuves
              accablantes : aucun doute, c'est bien lui qui a tenté de
              vous tuer. Dans l'une de ses poches, vous trouvez une
              fiole à moitié vide de sève de gandurn, le poison mortel
              qu'il avait versé dans vos aliments. Vous tombez ensuite
              sur un parchemin écrit en langue Glok et dans lequel
              sont indiqués tous les détails de votre voyage à Port
              Bax. C'est à Ragadorn qu'il a dû vous repérer et c'est
              là également qu'il a élaboré ses plans pour vous tuer.
              Vous remarquez aussi que son arme est une épée de Maître
              des Ténèbres, à la lame d'acier noir forgée dans le feu
              d'Helgedad, la cité infernale située au-delà des monts
              Durncrag. C'est le seul endroit, sur toutes les terres
              de Magnamund, où l'on peut fabriquer de l'acier noir.
              Mais la preuve irréfutable de son identité, vous la
              découvrez sur son poignet gauche : c'est un tatouage qui
              représente un serpent. Les brigands qui avaient essayé
              de vous tuer avant même que vous quittiez Holmgard
              portaient exactement la même marque. La bourse du moine
              contient 23 Pièces d'Or que vous pouvez conserver sans
              oublier de les inscrire sur votre Feuille
              d'Aventure. <L to='33'>Rendez-vous ensuite au 33</L>.

              <p/>
              <ItemsPicker items={f}/>

            </div>
        );
    }
}

export default P220;
