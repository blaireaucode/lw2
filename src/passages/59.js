/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';



class P59 extends Component {
    render() {
        const r = this.props.character.items.some(e => e.id === 'magical_spear');        
        return (
            <div>

              A coups d'éperons, vous lancez votre cheval en direction
              d'un Monstre d'Enfer qui s'apprête à frapper un soldat
              sans défense. Cette créature est insensible à la
              Discipline Kaï de la Puissance Psychique et ne peut être
              blessée que par une arme magique. Si vous possédez une
              Lance Magique, <L enabled={r} to="332">rendez-vous au
              332</L>. Dans le cas contraire, il vous faut prendre la
              fuite en plongeant dans les broussailles pour vous y
              cacher ; <L enabled={!r} to="311">rendez-vous alors au
              311</L>.

            </div>
        );
    }
}

export default P59;
