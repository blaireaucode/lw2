/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P286 extends Component {
    render() {
        return (
            <div>

              Vous tombez à la mer et vous nagez sous l'eau pendant
              plus d'une minute pour éviter de recevoir sur la tête
              les brandons qui jaillissent des navires enflammés ou
              les cadavres qu'on précipite par-dessus bord.  Lorsque
              le manque d'air vous oblige enfin à refaire surface, la
              vision qui s'offre à vous fait renaître l'espoir d'une
              issue favorable. <L to="109">Rendez-vous au 109</L>.

            </div>
        );
    }
}

export default P286;
