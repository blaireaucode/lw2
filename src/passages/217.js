/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';
import HelpGold from '../HelpGold.js';

class P217 extends Component {

    pay() {
        const d = bh.get_done(this, 217);
        if (!d) {
            const c = bh.gold_add(this.props.character, -1);
            this.props.character_set(c);
        }
        bh.update_done(this, 217);
    }
    
    render() {
        const g = bh.gold_get(this.props.character);
        return (
            <div>

              L'homme vous regarde et vous répond d'une voix bourrue:
              «&nbsp;La diligence... il faut prendre la diligence qui
              part cet après-midi pour Port Bax. Si vous me donnez 1
              Couronne, je vous dirai comment vous rendre au
              relais.&nbsp;» Si vous acceptez de payer, déduisez la
              Couronne de votre capital et <L enabled={g>0}
              onClick={this.pay.bind(this)} to="199">rendez-vous au
              199</L>. Si vous préférez quitter l'auberge sans lui
              donner la Pièce d'Or qu'il demande, <L
              to="143">rendez-vous au 143</L>.

              <p/>
              <HelpGold/>
            </div>
        );
    }
}

export default P217;
