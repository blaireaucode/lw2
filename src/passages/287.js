/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P287 extends Component {
    render() {
        return (
            <div>

              Vous concentrez toute votre énergie de Seigneur Kaï sur
              la petite serrure de cuivre et, quelques instants plus
              tard, un déclic à l'intérieur du coffret vous indique
              que le pêne vient de sortir de sa gâche ; vous soulevez
              alors le couvercle de la boîte et vous y découvrez un
              parchemin frappé du Sceau Royal du Sommerlund. Le
              document contient des instructions confidentielles
              concernant votre mission. En remettant ensuite le
              coffret à sa place, vous vous apercevez qu'un mécanisme
              secret a été aménagé dans le couvercle pour faire échec
              aux espions : sans le secours de votre Discipline Kaï,
              une aiguille empoisonnée aurait jailli de la boîte et se
              serait enfoncée dans votre épiderme, provoquant une mort
              instantanée. Vous refermez les tiroirs et vous prenez
              bien soin d'effacer toute trace de votre fouille avant
              d'aller rejoindre le capitaine sur le pont du navire. <L
              to="175">Rendez-vous au 175</L>.

            </div>
        );
    }
}

export default P287;
