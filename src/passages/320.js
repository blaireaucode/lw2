/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P320 extends Component {
    render() {
        return (
            <div>

              En ouvrant son sac, vous y découvrez avec horreur un
              parchemin en peau humaine sur lequel un message a été
              tracé dans une étrange écriture runique. Le seul mot que
              vous parvenez à reconnaître est «&nbsp;Kaï&nbsp;». Vous
              trouvez également dans le sac un poignard à la lame
              noire dont la seule vue vous fait frissonner, ainsi
              qu'un bloc d'obsidienne. Ces objets portent la marque
              des Maîtres des Ténèbres, et il n'est pas étonnant que
              vous vous sentiez soudain fort inquiet. Vous jetez le
              sac à terre comme s'il s'agissait d'un charbon ardent et
              vous vous hâtez de rejoindre votre cheval. Hélas, le
              malheur veut qu'il ait disparu: sans doute les Squalls
              l'ont-ils volé. Vous poussez alors un soupir de
              découragement et vous vous résignez à poursuivre votre
              route à pied.  <L to="138">Rendez-vous au 138</L>.

            </div>
        );
    }
}

export default P320;
