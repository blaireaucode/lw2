/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P192 extends Component {
    render() {
        return (
            <div>

              Les marins ivres poussent des grognements satisfaits, et
              l'argent change de mains tandis qu'on prend les
              paris. Vous remarquez alors que votre adversaire adresse
              un clin d'œil à deux de ses compagnons qui s'avancent
              aussitôt vers vous. Sans hésiter une seconde, vous vous
              levez d'un bond et vous frappez le marin d'un coup de
              poing au visage.  Le choc est si rude qu'il est projeté
              en arrière et s'écroule dans les bras de ses deux
              complices, les entraînant dans sa chute. Vous les
              laissez se débattre et vous vous dirigez vers la
              sortie. Mais, lorsque vous atteignez la porte, un autre
              marin au visage repoussant tire son épée et vous bloque
              le passage. Avant que vous n'ayez eu le temps de réagir,
              cependant vous entendez un bruit sourd et l'homme tombe
              à genoux.  Derrière lui se tient la serveuse, une grosse
              massue de bois à la main.  Elle vous sourit et vous la
              remerciez en lui adressant un clin d'oeil mais ce n'est
              pas le moment de vous attarder et vous filez par la
              porte dans la rue obscure recouverte de pavés.

              <p/>

              Vous courez quelques minutes dans le noir et vous
              apercevez alors une écurie et un relais de diligence dont
              les contours se dessinent dans l'ombre. Les hurlements
              furieux des marins retentissent à vos oreilles tandis que
              vous courez vers le bâtiment ; par chance une échelle
              extérieure vous permet de grimper dans un grenier à foin
              où vous vous réfugiez pour la nuit sans risque d'être
              découvert.  <L to="32">rendez-vous au 32</L>.

            </div>
        );
    }
}

export default P192;
