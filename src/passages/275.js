/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Dead from '../Dead.js';

class P275 extends Component {
    render() {
        return (
            <div>

              
              Vous vous sentez de plus en plus faible et la mort
              bientôt vous est un soulagement. Votre assassin a
              parfaitement rempli sa mission. Quant à la vôtre, elle
              s'achève ici.

              <p/>
              <Dead/>          

            </div>
        );
    }
}

export default P275;
