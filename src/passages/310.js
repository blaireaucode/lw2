/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P310 extends Component {
    render() {
        return (
            <div>

              Vous arrivez bientôt au bout de la rue du Col Vert ; une
              autre rue orientée nord-sud la croise à cet endroit,
              mais il fait si noir à présent que vous êtes bien
              incapable de lire le nom qu'elle porte. Il est temps de
              trouver un abri pour la nuit et vous apercevez alors, un
              peu plus loin, une enseigne éclairée qui indique :
              
              <p/>
              ÉCURIES DE RAGADORN RELAIS DE DILIGENCE
              <p/>

              
              Profitant de l'obscurité, vous montez quatre à quatre
              une échelle extérieure qui vous mène à un grenier :
              c'est l'endroit idéal pour passer la nuit, blotti parmi
              des bottes de foin. <L to="32">Rendez-vous au 32</L>.

            </div>
        );
    }
}

export default P310;
