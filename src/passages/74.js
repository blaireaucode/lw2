/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P74 extends Component {
    render() {
        return (
            <div>

              Vous posez vos mains sur sa poitrine et vous essayez de
              refermer sa blessure. Il a perdu beaucoup de sang et
              bien qu'il transpire abondamment, il a la peau
              froide. Ses yeux s'ouvrent alors et il prononce quelques
              mots à peine audibles. «&nbsp;Les pirates... Les pirates
              de Lakuri... Attention aux voiles rouges... Repoussez
              les pirates... » Le capitaine perd à nouveau
              connaissance. Vous l'enveloppez dans des couvertures et
              vous glissez un coussin sous sa tête, mais il a déjà
              plongé dans un sommeil dont il ne reviendra
              jamais.

              <p/>

              Pendant ce temps, les cadavres des membres de l'équipage
              ont été rassemblés sur le pont.  Le capitaine Kelman
              s'approche de vous et vous tend un cimeterre noir qui
              semble particulièrement redoutable. «&nbsp;Ce n'est pas
              une épée de pirate, Loup Solitaire, dit-il, cette lame
              vient des forges de Helgedad.  C'est une épée de Maître
              des Ténèbres. » On ne pouvait vous annoncer plus
              mauvaise nouvelle car, si les Maîtres des Ténèbres ont
              rallié les pirates de Lakuri à leur cause, le voyage
              jusqu'à Durenor sera plus périlleux encore que vous ne
              le pensiez. Vous jetez à l'eau le cimeterre noir et vous
              revenez à bord du Sceptre Vert. Et tandis que vous
              mettez le cap à l'est, le navire marchand de Durenor
              s'enfonce dans les profondeurs de la mer. <L
              to="240">Rendez-vous au 240</L>.

            </div>
        );
    }
}

export default P74;
