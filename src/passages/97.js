/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P97 extends Component {
    render() {
        return (
            <div>

              Vous avez remarqué qu'au cours de vos exercices
              d'entraînement au maniement du Glaive de Sommer, votre
              maîtrise de la Discipline Kaï du Sixième Sens s'est
              accrue : vous êtes à présent plus sensible que jamais et
              vous saviez déjà, bien avant qu'il ait parlé, quelle
              triste nouvelle Madin Rendalim allait vous
              annoncer. Sans nul doute, cette acuité exceptionnelle de
              votre Sixième Sens vous sera d'un grand secours lors de
              votre voyage de retour à Holmgard. <L
              to='152'>Rendez-vous au 152</L>.




            </div>
        );
    }
}

export default P97;
