/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P343 extends Component {
    render() {
        return (
            <div>

              Vous avez soudain la certitude que la victime désignée
              de ce prétendu accident n'était autre que
              vous-même. L'un de vos compagnons de voyage a
              l'intention de vous tuer ! <L to="168">Rendez-vous au
              168</L>.

            </div>
        );
    }
}

export default P343;
