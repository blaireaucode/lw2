/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import * as bh from '../book_helpers.js';

class P169 extends Component {

    constructor(props) {
        super(props);
        this.r = {
            passage_title: 169,
            name: 'random_table',
            dice_value: -1
        };
    }

    render() {
        const randtable = bh.book_get_element(this.props, this.r);
        const r = randtable.dice_value;
        return (
            <div>

            Découragé, vous quittez la maison de jeu et vous retournez
            au relais de diligence ; au loin, vous apercevez la porte
            Est de la ville. La diligence de Durenor attend juste à
            côté. Or, il vous faut à tout prix gagner Port Bax,
            l'avenir du Sommerlund en dépend. Vous vous arrangez donc
            pour passer derrière le garde qui surveille la diligence
            et vous montez dans le véhicule sans qu'il vous ait vu. A
            mesure que l'heure du départ approche, cinq autres
            passagers montent à leur tour et s'assoient autour de
            vous. Le garde claque alors la portière et le voyage pour
            Port Bax commence. Utilisez la <RandomTable
            p={this.r}>pour obtenir un chiffre</RandomTable>. Si vous
            tirez un chiffre entre 0 et 3, <L enabled={r>=0 && r<=3}
            to="39">rendez-vous au 39</L>. Entre 4 et 6, <L
            enabled={r>=4 && r<=6} to="249">rendez-vous au
            249</L>. Entre 7 et 9, <L to="339" enabled={r>=7 &&
            r<=9}>rendez-vous au 339</L>.

              <CurrentDice r={r}/>
            </div>
        );
    }
}

export default P169;
