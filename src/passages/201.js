/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import * as bh from '../book_helpers.js';

class P201 extends Component {

    render() {
        const r = bh.get_element(this, 201, {dice_value:-1});
        const d = r.dice_value;
        return (
            <div>

              Lorsque vous bondissez sur vos pieds, le serpent siffle
              et tente de vous mordre au bras. Vous faites un pas de
              côté pour l'éviter mais avez-vous été suffisamment
              rapide pour échapper à ses crochets mortels ? Utilisez
              la <RandomTable p={r}>pour obtenir un
              chiffre</RandomTable>. Si vous tirez un chiffre entre 0
              et 4, <L enabled={d>=0 && d<=4} to="285">rendez-vous au
              285</L>. Entre 5 et 9, <L enabled={d>=5 && d<=9}
              to="70"> rendez-vous au 70</L>.

              <CurrentDice r={d}/>
            </div>
        );
    }
}

export default P201;
