/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P262 extends Component {
    render() {
        const f = {
            passage_title: 262,
            list: [
                {id:'epee'},
                {id:'masse_d_armes'},
                {id:'baton'},
                {id:'repas', nb:1},
                {id:'PO', nb:6},
                {id:'fiole_orange'},
            ]
        };
        return (
            <div>

              
              Le garde vous écarte d'une bourrade et se met à courir
              en direction de la rue du Tombeau. En haut de l'escalier
              se trouve une petite pièce que vous décidez de fouiller
              avant le retour du soldat. Vous y découvrez les objets
              suivants : Épée, Masse d'Armes, Bâton, 1 Repas complet,
              6 Pièces d'Or, une fiole d'un liquide orange. Si l'un ou
              l'autre de ces objets vous intéresse, il vous suffit de
              les inscrire sur votre Feuille d'Aventure pour qu'ils
              vous appartiennent désormais.

              <p/>

              Lorsque vous quittez la pièce, vous vous heurtez à un
              autre garde ; le choc est plutôt rude et vous tombez
              tous deux au bas de l'escalier ; mais avant que le garde
              ait pu retrouver ses esprits, vous avez déjà pris la
              fuite en courant dans la nuit. <L to="65">Rendez-vous au
              65</L>.

              <p/>
              <ItemsPicker items={f}/>
              
            </div>
        );
    }
}

export default P262;
