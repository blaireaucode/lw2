/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import * as bh from '../book_helpers.js';

class P131 extends Component {

    constructor(props) {
        super(props);
        const f1 = {
            'name': 'f1',
            'passage_title': 131,
            'encounter': "CHEF VOLEUR",
            'combat_skill': 15,
            'endurance':23,
            'continue': true
        };
        const f2 = {
            'name': 'f2',
            'passage_title': 131,
            'encounter': "1er VOLEUR",
            'combat_skill': 13,
            'endurance':21
        };
        const f3 = {
            'name': 'f3',
            'passage_title': 131,
            'encounter': "2e VOLEUR",
            'combat_skill': 13,
            'endurance':20
        };
        this.f = [];
        this.f.push(f1);
        this.f.push(f2);
        this.f.push(f3);
    }

    render() {
        const f = bh.book_get_current_fight(this.props, this.f);
        const r = bh.enabled_if_fight_win(this.props, f);
        return (
            <div>

              Vous leur demandez ce qu'ils vous veulent. Pour toute
              réponse, ils tirent tous trois d'une poche de leur veste
              de longs poignards à la lame recourbée. Leur chef fait
              alors un pas en avant et vous ordonne de lui donner
              votre or. Comme vous hésitez, il crie : «&nbsp;A
              l'attaque ! » et les trois VOLEURS bondissent aussitôt
              sur vous. Si vous ne disposez d'aucune arme, retranchez
              4 points de votre total d'HABILETÉ et combattez-les à
              mains nues. Il vous faut les affronter un par un.  Vous
              pouvez prendre la fuite au cours du combat en <L
              to='121'>vous rendant au 121</L>. Si vous parvenez à
              tuer les trois voleurs, <L enabled={r}
              to="301">rendez-vous au 301</L>.

              <p/>
              <Fight fight={f}/>

            </div>
        );
    }
}

// HABILETÉ ENDURANCE
//Chef VOLEUR 15 23
//1er VOLEUR 13 21
//2e VOLEUR 13 20
export default P131;
