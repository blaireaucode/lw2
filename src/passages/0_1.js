/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Cont from '../Cont.js';

class P0_1 extends Component {
    render() {
        return (
            <div>
              Quant à vous, vous êtes le Loup Solitaire, un disciple du Temple
              Kaï où l'on vous a initié aux secrets des Seigneurs de la
              Guerre. Or, il y a deux jours, le paisible royaume s'est trouvé
              plongé dans la tourmente à la suite de l'invasion soudaine du pays
              par une puissante armée que commandaient les Maîtres des
              Ténèbres. Au cours de l'attaque, le monastère Kaï a été
              complètement détruit et les Seigneurs qui s'y étaient réunis pour
              préparer la grande fête de Fehmarn ont tous été tués lors des
              combats ; les murs du temple se sont écroulés sur eux, les écrasant
              sous les décombres.

              <p/>Vous êtes le seul Seigneur Kaï à avoir
              échappé au massacre et vous avez juré de venger la mort de vos
              compagnons. Votre première tâche était d'avertir le roi car, en
              l'absence des Seigneurs Kaï à la tête de l'armée, il
              devenait impossible de mobiliser à temps la population pour
              repousser les Maîtres des Ténèbres.

              <p/>
              <Cont to='0_2'/>

            </div>
        );
    }
}

export default P0_1;
