/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

import update from 'immutability-helper';

import HelpGold from '../HelpGold.js';
import * as bh from '../book_helpers.js';

class P75 extends Component {
    constructor(props) {
        super(props);
        this.f = {
            'name': 'a',
            'passage_title': '75',
            'already_done': false
        };
    }

    handleClick() {
        const item = bh.item_get_description('laissez_passer');
        var c = bh.item_pick(this.props.character, item);
        c = bh.gold_add(c, -10);        
        this.props.character_set(c);
        const elem = bh.book_get_element(this.props, this.f);
        const el = update(elem, {already_done: {$set:true}});
        this.props.book_set_element(el);
    }
    
    render() {
        const elem = bh.book_get_element(this.props, this.f);
        const g = bh.gold_get(this.props.character);
        const e = !elem.already_done && g>=10;
        var after = '';
        if (elem.already_done) 
            after = (<span className='Book-help'> Vous avez le laissez-passer.</span>);
        
        return (
            <div>

              Vous pénétrez dans un bureau aux odeurs de moisi. Deux
              hommes y sont assis, penchés sur leurs tables qui
              ploient sous des piles de livres et de
              papiers. «&nbsp;Bonsoir, monsieur», dit l'un des
              hommes. Sa longue moustache soigneusement cirée
              tressaute quand il parle. «&nbsp;Monsieur désire-t-il un
              laissez-passer de marchand ? » demande-t-il. Avant même
              que vous ayez pu répondre, l'homme vous tend une poignée
              de formulaires incompréhensibles. «&nbsp;Si Monsieur
              veut bien se donner la peine de signer ici, je me ferai
              un plaisir de donner immédiatement à Monsieur son
              laissez-passer. Il en coûtera 10 Couronnes à Monsieur. »

              <p/>
              
              Si vous désirez signer ces papiers et faire l'achat d'un
              laissez-passer blanc, inscrivez-le sur votre Feuille
              d'Aventure, soustrayez <L to='75'
              onClick={this.handleClick.bind(this)} enabled={e}>les 10
              Pièces d'Or</L> qu'il vous aura coûté et <L to="142"
              enabled={elem.already_done}>rendez-vous au 142</L>. Si
              vous n'avez pas assez d'argent ou si ce laissez-passer
              ne vous intéresse pas, quittez le bureau et retournez
              dans le hall en <L to='318'>vous rendant au 318</L>.


              <p/><HelpGold/><p/>{after}

            </div>
        );
    }
}

export default P75;
