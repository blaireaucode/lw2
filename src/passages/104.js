/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P104 extends Component {
    render() {
        return (
            <div>

              Les pêcheurs vous regardent bouche bée comme si vous
              étiez revenu d'entre les morts. Puis soudain l'un d'eux
              renverse la table d'un coup de pied et s'enfuit avec les
              autres de la taverne par la porte de derrière.  D'un
              bond, vous sautez par-dessus la table pour vous lancer à
              leur poursuite dans l'obscurité de la nuit. Si vous
              souhaitez continuer à les poursuivre, <L
              to="231">rendez-vous au 231</L>. Si vous préférez les
              laisser partir, <L to="177">rendez-vous au 177</L>.

            </div>
        );
    }
}

export default P104;
