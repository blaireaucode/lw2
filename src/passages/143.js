/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P143 extends Component {
    render() {
        return (
            <div>

              Vous marchez en direction du sud en suivant le quai et
              bientôt vous arrivez à un croisement où une rue mène
              vers l'est. Toutes les boutiques de cette rue sont
              fermées, sauf une, située à votre droite. Une enseigne
              est accrochée au-dessus de la porte : JINELDA KOOP
              ALCHIMISTE Achat et vente de potions magiques Si vous
              souhaitez entrer dans cette boutique, <L
              to="289">rendez-vous au 289</L>. Si vous préférez
              poursuivre votre chemin, <L to="186">rendez-vous au
              186</L>.

            </div>
        );
    }
}

export default P143;
