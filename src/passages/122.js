/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import * as bh from '../book_helpers.js';

class P122 extends Component {

    constructor(props) {
        super(props);
        this.r = {
            passage_title: 122,
            name: 'random_table',
            dice_value: -1
        };
    }

    render() {
        const randtable = bh.book_get_element(this.props, this.r);
        const r = randtable.dice_value;
        const d = bh.has_discipline(this.props.character, "sixieme_sens");
        return (
            <div>

              Cette rue longe les murs de la ville en direction du
              nord. Sur votre droite, vous remarquez une boutique dont
              la porte est de couleur orange. A la différence des
              autres boutiques de la rue, celle-ci ne porte aucune
              enseigne. C'est alors que vous revient en mémoire le
              récit qu'un Seigneur Kaï vous avait fait il y a environ
              un an à son retour d'un voyage dans la ville de
              Ragadorn. Il vous avait parlé de cette porte orange à
              plusieurs reprises. Utilisez la <RandomTable
              p={this.r}>pour obtenir un chiffre</RandomTable>. Si
              vous tirez un chiffre entre 0 et 4, <L enabled={r>=0 &&
              r<=4} to="46">rendez-vous au 46</L>. Entre 5 et 9, <L
              to="112" enabled={r>=5 && r<=9} >rendez-vous au
              112</L>. Si vous maîtrisez la Discipline Kaï du Sixième
              Sens, <L enabled={d} to="96">rendez-vous au 96</L>.

              <CurrentDice r={r}/>
            </div>
        );
    }
}

export default P122;
