/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P28 extends Component {
    render() {
        return (
            <div>

              Les Squalls se mettent à hurler de terreur et s'enfuient
              en tous sens pour éviter vos coups. En quelques
              instants, ils ont déserté la clairière et vous vous
              approchez du moribond pour lui porter secours. Il lui
              reste tout juste un souffle de vie et il est bien
              entendu beaucoup trop faible pour parler. Si vous voulez
              retirer avec précaution la lance de sa poitrine, <L
              to="106">rendez-vous au 106</L>. Si vous préférez
              fouiller dans son sac dans l'espoir d'y trouver quelque
              objet qui pourrait se révéler utile, <L
              to="320">rendez-vous au 320</L>.

            </div>
        );
    }
}

export default P28;
