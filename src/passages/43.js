/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P43 extends Component {
    render() {
        return (
            <div>

           Vous faites tournoyer le Glaive de Sommer d'un geste
           vigoureux et vous fauchez d'un coup quatre zombies, mais à
           peine leurs cadavres se sont-ils écroulés sur le pont que
           d'autres morts vivants viennent prendre leur place. Vous ne
           parviendrez jamais à les tuer tous et vous succomberez sous
           le nombre. Ils agrippent votre cape qu'ils commencent à
           déchirer et vous n'avez plus qu'à sauter par-dessus bord
           pour échapper à une mort certaine. <L to="286">Rendez-vous
           au 286</L>.

            </div>
        );
    }
}

export default P43;
