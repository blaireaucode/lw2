/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P230 extends Component {
    render() {
        return (
            <div>

              Vous marchez le long de la voie qui tourne brusquement à
              l'est pour aboutir à la rue du Mendiant. Cette artère
              est d'ailleurs bien nommée car des dizaines d'hommes, de
              femmes et d'enfants, tous vêtus de haillons, s'y
              rassemblent par groupes, à l'abri des portes cochères,
              en tendant des sébiles aux passants. Et tandis que vous
              suivez l'avenue en direction d'un croisement, vous êtes
              assailli de tous côtés par des miséreux qui vous
              demandent de l'or. Si vous souhaitez leur faire
              l'aumône, <L to="93">rendez-vous au 93</L>. Si vous
              préférez les repousser et poursuivre votre chemin, <L
              to="137">rendez-vous au 137</L>.

              <p/>
              <SimpleImage width={250} src={'230.png'}/>

            </div>
        );
    }
}

export default P230;
