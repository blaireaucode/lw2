/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P294 extends Component {
    render() {
        return (
            <div>

              Vous enveloppez l'homme blessé dans une couverture, mais
              il a déjà sombré dans un sommeil dont il ne s'éveillera
              jamais plus. Vous retournez alori sur le pont où l'on a
              rassemblé les corps des marins.  Le capitaine Kelman
              s'approche de vous et vous montre une épée dont la seule
              vue donne le frisson. «&nbsp;Cette épée n'est pas celle
              d'un pirate, Loup Solitaire, déclare le capitaine, elle
              a été fabriquée dans les forges d'Helgedad : c'est une
              arme de Maître des Ténèbres.&nbsp;» Il jette à la mer l'épée
              maléfique qui disparaît dans les vagues et vous revenez
              tous deux à bord du Sceptre Vert. L'équipage hisse
              aussitôt les voiles et le navire reprend sa route vers
              l'est tandis que, debout sur le pont, vous contemplez
              avec tristesse le bateau de Durenor qui s'enfonce à
              jamais dans les flots. <L to="240">Rendez-vous au
              240</L>.

            </div>
        );
    }
}

export default P294;
