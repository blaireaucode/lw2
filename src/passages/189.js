/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import HelpCharacter from '../HelpCharacter.js';
import EnduranceChange from '../EnduranceChange.js';
import * as bh from '../book_helpers.js';

class P189 extends Component {

    constructor(props) {
        super(props);
        this.f = {
            'name': 'a',
            'passage_title': 189,
            'already_done': false,
            'dmg':-2
        };
    }

    render() {
        const elem = bh.book_get_element(this.props, this.f);
        const already_done = elem.already_done;
        return (
            <div>

              «&nbsp;Vous êtes un imposteur ! » s'écrie-t-il en
              dégainant son arme. Avant que vous n'ayez pu réagir, la
              lame de son épée vous écorche le bras et vous perdez
              <EnduranceChange f={this.f}> 2 points
              d'ENDURANCE</EnduranceChange>. L'homme s'est précipité
              sur vous ; sous le choc, vous franchissez la porte
              ouverte à reculons, vous trébuchez et vous tombez tous
              deux tête la première au bas des escaliers, dans un
              échange de jurons retentissants. Vous vous relevez
              ensuite en titubant mais le chevalier, lui, est déjà
              debout et a ramassé son épée. Si vous souhaitez le
              combattre, <L enabled={already_done}
              to="162">rendez-vous au 162</L>. Si vous préférez vous
              enfuir dans la forêt en abandonnant votre cheval, <L
              enabled={already_done} to="244">rendez-vous au 244</L>.

              <p/>
              <HelpCharacter/>
            </div>
        );
    }
}

export default P189;
