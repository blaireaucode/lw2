/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P35 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, "orientation");
        return (
            <div>

              Vous enjambez le soldat évanoui et vous vous hâtez de
              fuir la tour en direction de la forêt car si d'autres
              gardes apparaissent, il y a tout à parier qu'ils vous
              attaqueront avant de vous poser des questions. Vous avez
              marché pendant plus de deux heures lorsque vous parvenez
              à une bifurcation, à proximité d'un chêne rabougri. Si
              vous décidez d'aller à gauche, <L to="155">rendez-vous
              au 155</L>. Si vous préférez aller à droite, <L
              to="293">rendez-vous au 293</L>. Enfin, si vous
              maîtrisez la Discipline Kaï de l'Orientation, <L to="13"
              enabled={d}>rendez-vous au 13</L>.

            </div>
        );
    }
}

export default P35;
