/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P209 extends Component {
    render() {
        return (
            <div>

              Vous entendez bientôt des murmures parmi les hommes
              d'équipage.  Parfois, quelques mots prononcés
              distinctement vous parviennent aux oreilles : ils
              parlent de «&nbsp;vaisseaux fantômes » et de
              «&nbsp;malédiction&nbsp;», mais les rumeurs
              s'évanouissent brusquement lorsque la voix tonnante du
              capitaine appelle tout le monde sur le pont. Et lorsque
              le capitaine Kelman monte lui-même sur le pont arrière
              pour venir parler à l'équipage, on n'entend plus alors
              que le craquement des mâts du navire qui gémissent sous
              le vent. «&nbsp;Nous sommes à trois jours de Port Bax,
              dit le capitaine. Le feu a dévoré nos provisions et nous
              n'avons plus d'eau potable. Il nous faut donc mettre le
              cap sur Ragadorn où nous pourrons faire réparer le
              navire et reconstituer nos vivres. C'est
              tout.&nbsp;»  Les hommes d'équipage semblent satisfaits
              de cette décision et ils se remettent au travail avec
              une vigueur renouvelée.

              <p/>

              Le capitaine se tourne alors vers vous. «&nbsp;Nous
              aurons rallié le port de Ragadorn dans huit heures
              environ, dit-il. J'ai reçu pour instructions de vous
              amener sain et sauf à Port Bax et de vous confier à la
              garde du Consul du Sommerlund, le Lieutenant Général
              Rhygar. Mais le temps est contre nous et j'ai bien peur
              qu'il faille une bonne huitaine de jours pour réparer le
              navire. Lorsque nous aurons jeté l'ancre, vous devrez
              alors décider si vous souhaitez poursuivre votre voyage
              à Durenor par la mer en restant avec nous ou par la
              route en allant là-bas par vos propres moyens.&nbsp;» Tandis
              que vous retournez dans votre cabine, les paroles du roi
              vous reviennent en mémoire : «&nbsp;Quarante jours, Loup
              Solitaire, tu n'as que quarante jours pour rapporter le
              Glaive. Nous aurons la force de résister à l'ennemi
              pendant ces quarante jours. Après... il sera trop
              tard...&nbsp;» Non, décidément, il ne vous reste guère de
              temps pour accomplir votre périlleuse mission. <L
              to="197">Rendez-vous au 197</L>.

            </div>
        );
    }
}

export default P209;
