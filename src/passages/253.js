/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P253 extends Component {
    render() {
        return (
            <div>

              La rue longe le mur du port en direction du fleuve Dorn
              ; au bord du fleuve, elle tourne brusquement vers le sud
              et aboutit à la rue du Butin.  Vous passez devant les
              entrepôts alignés le long du quai et vous reconnaissez
              un peu plus loin le poteau de pierre planté au milieu de
              la place. Si vous souhaitez poursuivre en direction du
              sud, <L to='303'> rendez-vous au 303</L>. Si vous
              préférez prendre la rue de la Bernicle et retourner à la
              taverne, <L to="177">rendez-vous au 177</L>.

            </div>
        );
    }
}

export default P253;
