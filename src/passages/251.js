/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P251 extends Component {
    render() {
        return (
            <div>

              Il fait presque nuit lorsque le petit bateau de pêche
              entre dans le port de Ragadorn. Vous n'avez toujours pas
              rencontré le moindre survivant au naufrage et le pire
              est à craindre. Vous remarquez bientôt que trois des
              pêcheurs ont un comportement suspect. Ils se parlent à
              l'oreille et jettent de fréquents regards à votre
              bourse. Et tandis que le bateau vogue dans l'estuaire du
              fleuve Dorn, ils vous encerclent soudain et vous
              ordonnent de leur donner votre or. Vous vous apprêtez à
              passer à l'attaque lorsque quelqu'un vous pousse
              par-derrière en vous projetant à plat ventre sur le
              pont. L'un des marins lève alors le pied et un instant
              plus tard, tout devient noir dans votre tête. <L
              to="194">Rendez-vous au 194</L>.

            </div>
        );
    }
}

export default P251;
