/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';
import SimpleImage from '../SimpleImage.js';

class P118 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, "communication_animale");
        return (
            <div>

              Vous dites adieu à Rhygar et vous entrez dans le tunnel
              de Tarnalin.  D'une largeur et d'une hauteur de 30
              mètres environ, le tunnel traverse les montagnes de la
              chaîne d'Hammardal et permet d'accéder à la
              capitale. Des torches l'éclairent sur toute sa longueur
              et les marchands sont nombreux à l'emprunter car c'est
              la seule voie qui relie Port Bax à
              Hammardal. D'ordinaire, la circulation y est intense
              mais vous constatez avec surprise qu'il est désert au
              moment où vous y pénétrez ; vous n'y trouvez qu'une
              carriole de fruits renversée sur la chaussée.  Vous
              continuez à avancer dans le tunnel et un doute alors
              vous saisit: les Monstres d'Enfer seraient-ils arrivés
              avant vous ?

              <p/>
              <SimpleImage width={200} src={'118.png'}/>
              <p/>
              
              Au bout d'une heure de marche, vous apercevez une
              étrange créature perchée sur le toit d'un chariot au
              milieu de la chaussée. L'animal mesure une soixantaine
              de centimètres de haut et ressemble à un rat géant. Vous
              pensez qu'il s'agit là d'un rongeur qui a établi ses
              quartiers dans le tunnel, mais vous remarquez en vous
              approchant que la créature porte une magnifique veste de
              cuir en patchwork et qu'elle tient une lance dans sa
              patte. L'animal se tourne soudain vers vous lorsqu'il
              entend vos pas. Les moustaches de son museau frémissent
              tandis qu'il renifle alentour et ses yeux scrutent
              l'obscurité. Dès qu'il vous voit, il prend la fuite et
              disparaît dans un tunnel plus petit situé à votre
              gauche. Si vous voulez suivre cette créature, <L
              to="23">rendez-vous au 23</L>. Si vous préférez la
              laisser filer sans vous en soucier et poursuivre votre
              chemin, <L to="340">rendez-vous au 340</L>. Si vous
              maîtrisez la Discipline Kaï de la Communication animale,
              <L enabled={d} to="279">rendez-vous au 279</L>.

            </div>
        );
    }
}

export default P118;
