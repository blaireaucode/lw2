/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P261 extends Component {
    render() {
        return (
            <div>

              Sur un bon nombre de kilomètres, la route longe une
              hauteur verdoyante, où abonde l'herbe grasse, avant de
              tourner enfin vers le nord, en direction de la
              côte. Vous arrivez dans un village dont les maisons sont
              bâties en cercle autour d'un étang et lorsque vous le
              traversez, un groupe d'enfants squalls se précipite vers
              vous en hurlant et en vous lançant des pierres. Vous
              descendez ensuite dans la profonde vallée qui s'étend
              au-delà et, peu à peu, la lande laisse place à des
              terres plus riches qui ont été défrichées et
              cultivées. La colline qui se dresse de l'autre côté est
              couverte de forêts ; vous n'êtes plus loin de la côte à
              présent, et vous apercevez déjà ses hautes falaises et
              la couleur des rocs qui surplombent la mer. Un peu plus
              tard, vous êtes en train de franchir un taillis lorsque
              des appels à l'aide retentissent à votre droite. Si vous
              souhaitez vous porter au secours de la personne qui crie
              ainsi, <L to="95"> rendez-vous au 95</L>. Si vous
              préférez ne pas prêter attention à ces hurlements
              désespérés, continuez de chevaucher le long de la route
              en <L to='198'> vous rendant au 198</L>.

            </div>
        );
    }
}

export default P261;
