/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import * as bh from '../book_helpers.js';

class P241 extends Component {
    render() {
        const f = {
            passage_title: 241,
            encounter: "TRICHEUR",
            combat_skill: 17,
            endurance: 25
        };
        const r = bh.enabled_if_fight_win(this.props, f);
        return (
            <div>

              Le silence se fait dans la taverne lorsque l'homme que
              vous venez d'accuser se tourne vers vous. «&nbsp;Tu as
              la langue un peu trop prompte, étranger, dit-il d'un air
              menaçant, il serait temps de la couper avant qu'elle ne
              t'attire d'autres ennuis. » Il dégaine alors un Poignard
              à la lame recourbée et se jette sur vous. La foule des
              clients forme aussitôt un cercle autour de vous et il
              vous est désormais impossible de prendre la fuite. Il
              vous faut combattre cet homme jusqu'à la mort de l'un
              d'entre vous. Si vous êtes vainqueur, <L enabled={r}
              to="21">rendez-vous au 21</L>.

              <p/>
              <Fight fight={f}/>

            </div>
        );
    }
}
// TRICHEUR HABILETÉ: 17 ENDURANCE: 25 
export default P241;
