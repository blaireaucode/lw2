/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import update from 'immutability-helper';
import * as bh from '../book_helpers.js';

class P136 extends Component {

    constructor(props) {
        super(props);
        const element = {
            'name': 'a',
            'passage_title': '136',
            'already_paid': false
        };
        this.element = element;
    }

    handleClick() {
        // Pay + add ticket
        const c = bh.gold_add(this.props.character, -20);
        const ticket = bh.item_get_description('billet_Port_Bax');
        const items = bh.item_list_add(c.items, ticket);
        const c2 = update(c, {items: {$set: items}});
        this.props.character_set(c2);
        // Enable
        const elem = bh.book_get_element(this.props, this.element);
        const e = update(elem, {already_paid: {$set:true}});
        this.props.book_set_element(e);
    }

    render() {
        const elem = bh.book_get_element(this.props, this.element);
        const already_paid = elem.already_paid;
        
        // Current nb of gold
        var n = bh.gold_get(this.props.character);
        
        // Second link
        var enough_gold = false; 
        if (!already_paid) 
            if (n >= 20) enough_gold = true;

        // help
        const h = (
            <span className='Book-fight-help'>Vous avez actuellement {n} Couronne(s).</span>
        );

        var after = '';
        if (already_paid) {
            after = (<span> Vous avez payé. </span>);
        }
        else {
            if (enough_gold) {
                after = (<span>
                           <L to='136' onClick={this.handleClick.bind(this)}>Payer les 20
                             Couronnes.</L>
                         </span>
                        );
            }
        }
        
        return (
            <div>
              
              «&nbsp;Il vous en coûtera 20 Couronnes pour vous rendre
              à Port Bax », lance le cocher qui s'exprime avec un fort
              accent de Ragadorn. Si vous possédez ces 20 Couronnes et
              que vous souhaitez acheter un billet, <L to="10"
              enabled={already_paid}> rendez-vous au 10</L>. Si vous
              n'avez pas assez d'argent (ou que vous ne souhaitez pas
              payer), <L to="238"> rendez-vous au 238</L>.

              <p/>

              {after}<br/>{h}
              
            </div>
        );
    }
}

export default P136;
