/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P319 extends Component {
    render() {
        return (
            <div>
              
              La rue aboutit soudain à une grande tour de guet aux
              murs de pierre. Si vous souhaitez entrer dans la tour,
              <L to="271"> rendez-vous au 271</L>. Si vous préférez
              retourner dans la taverne, <L to="177">rendez-vous au
              177</L>.

            </div>
        );
    }
}

export default P319;
