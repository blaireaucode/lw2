/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P244 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, "orientation");
        return (
            <div>

              Vous marchez dans la forêt touffue pendant près de trois
              heures avant de découvrir un sentier orienté au nord et
              parallèle au chenal de Ryner dont les flots
              bouillonnants ont plus de 1500 m de profondeur. Au loin,
              vous apercevez un pont qui enjambe les eaux sombres, là
              où le chenal se rétrécit. Une petite cabane au toit plat
              se dresse à l'entrée du pont ; deux soldats se tiennent
              debout au sommet de l'édifice. Un panneau orienté vers
              l'autre extrémité du pont indique : PORT BAX. Si vous
              souhaitez traverser ce pont, <L to="47">rendez-vous au
              47</L>. Si vous préférez l'éviter et poursuivre le long
              du sentier, <L to="207">rendez-vous au 207</L>. Enfin,
              si vous maîtrisez la Discipline Kaï de l'Orientation, <L
              enabled={d} to="147">rendez-vous au 147</L>.

            </div>
        );
    }
}

export default P244;
