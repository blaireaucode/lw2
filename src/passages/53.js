/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P53 extends Component {
    render() {
        return (
            <div>

              Vous entendez des murmures parmi l'équipage ; de temps à
              autre, vous percevez distinctement certains mots : les
              hommes parlent de « vaisseaux fantômes » et de
              «&nbsp;malédiction » mais les murmures cessent lorsque
              le capitaine appelle tout le monde sur le pont. Le
              silence alors s'installe à bord du Sceptre Vert tandis
              que le capitaine Kelman s'adresse à l'équipage d'une
              voix puissante : «&nbsp;Nous sommes à trois jours de mer
              de Port Bax, dit-il, mais avec un bon vent et du cœur au
              ventre, nous pourrons y jeter l'ancre et festoyer à
              terre dans moins de deux jours. Le feu a détruit la
              plupart de nos vivres et nous devrons par conséquent
              nous contenter d'un repas par jour. Un garde sera mis en
              faction devant le baril d'eau douce. Nous sommes forts
              cependant et nous supporterons l'épreuve mais sachez que
              quiconque sera surpris à voler recevra cent coups de
              fouet. C'est tout ce que j'avais à dire ».  L'équipage
              ne semble pas très satisfait des décisions du capitaine,
              mais personne n'ose défier son autorité.

              <p/>

              Plus tard dans l'après-midi, l'équipage et le capitaine
              vous invitent chacun de son côté à prendre votre repas
              du soir en leur compagnie. Si vous souhaitez dîner avec
              le capitaine, <L to="321">rendez-vous au 321</L>. Si
              vous préférez manger avec l'équipage, <L
              to="154">rendez-vous au 154</L>.

            </div>
        );
    }
}

export default P53;
