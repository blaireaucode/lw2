/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P186 extends Component {
    render() {
        var g = bh.gold_get(this.props.character);
        return (
            <div>

              Vous arrivez bientôt devant un grand bâtiment qui porte
              cette inscription sur sa façade :

              <p/>

              ÉCURIES DE RAGADORN RELAIS DE DILIGENCE

              <p/>

              Un cocher vêtu d'un uniforme vert est assis près d'un
              tableau d'affichage qui indique : Port Bax Durée du
              voyage : 7 jours. Si vous souhaitez demander au cocher
              un billet pour Port Bax, <L to='136'
              enabled={g>0}>rendez-vous au 136</L>. Si vous n'avez pas
              d'argent, <L to="238">rendez-vous au 238</L>.

            </div>
        );
    }
}

export default P186;
