/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P235 extends Component {
    render() {
        return (
            <div>

              Lorsque vous atteignez le palier de l'étage suivant, la
              porte cède dans un grand fracas et la populace déchaînée
              entre en force. En haut des marches, un Sabre est
              accroché à côté d'une cheminée. Vous pouvez vous emparer
              de cette arme si vous le désirez. En jetant ensuite un
              coup d'œil autour de vous, vous vous apercevez qu'il n'y
              a qu'un seul moyen de sortir d'ici : sauter par la
              fenêtre pour atterrir sur la chaussée en contrebas. Si
              vous souhaitez sauter par la fenêtre, <L
              to="132">rendez-vous au 132</L>.  Si vous préférez
              affronter la populace qui monte l'escalier, <L
              to='90'>rendez-vous au 90</L>.

            </div>
        );
    }
}

export default P235;
