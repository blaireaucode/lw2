/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P67 extends Component {
    render() {
        return (
            <div>

              Vous arrivez très vite à la conclusion que l'imposteur a
              dû s'enfuir par l'entrée principale de la taverne ; s'il
              est resté dans les environs, il doit probablement se
              trouver sur la place principale ou à proximité. Vous
              fouillez les ruelles et les maisons autour de la place,
              mais vous ne découvrez pas la moindre trace du
              fuyard. Plutôt que de perdre votre temps en vaines
              recherches, vous décidez alors de revenir sur le quai.
              Là, vous détachez l'amarre d'un canot et vous ramez en
              direction du Sceptre Vert en éprouvant un sentiment de
              malaise : vous ne vous attendiez pas à ce que vos
              ennemis se manifestent si tôt, dès les premières heures
              de votre mission. <L to="300">Rendez-vous au 300</L>.

            </div>
        );
    }
}

export default P67;
