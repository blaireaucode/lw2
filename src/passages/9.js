/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P9 extends Component {

    render() {
        return (
            <div>

                Vous êtes arrivé au quatorzième jour de votre quête. L'aube vient de
                se lever lorsque vous ouvrez les yeux ; vous contemplez alors un
                spectacle à vous couper le souffle : Hammardal, la cité des
                montagnes, se dresse devant vous. Contrairement aux autres villes
                des Fins de Terre, la capitale du royaume de Durenor n'a jamais eu
                besoin qu'on lui élève de fortifications. Les sommets montagneux qui
                l'entourent offrent une bien meilleure protection à ses habitants.
                Le carrosse qui vous emporte file parmi les riches terres des fermes
                environnantes en direction de la cité aux hautes tours et aux larges
                avenues. Au centre même d'Hammardal, la Tour du Roi s'élève sur une
                colline. C'est un magnifique édifice de pierre et de verre devant
                les portes duquel s'arrête votre attelage. Pour la première fois,
                vous prenez alors conscience que le privilège d'avoir brandi le
                Glaive de Sommer vous fera désormais entrer dans les plus anciennes
                légendes des Fins de Terre. <L to="196">Rendez-vous au 196</L>.

                <p/>
                <center>Au centre même d'Hammardal, la Tour du Roi s'élève sur une colline.</center>
                <SimpleImage width={300} src={'9.png'}/>

            </div>
        );
    }
}

export default P9;
