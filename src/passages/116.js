
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import HelpGold from '../HelpGold.js';


import * as bh from '../book_helpers.js';

class P116 extends Component {
    constructor(props) {
        super(props);
        this.r = {
            passage_title: 116,
            name: 'random_table',
            dice_value: -1
        };
        this.f_bonus = (character) => {
            return 5;
        };
    }

    handleClick(r) {
        console.log('ici', r);
        const c = bh.gold_add(this.props.character, r);
        this.props.character_set(c);
    }
    
    render() {
        const randtable = bh.book_get_element(this.props, this.r);
        const r = randtable.dice_value-5;
        return (
            <div>

              Grâce à la Discipline Kaï, vous n'avez aucune difficulté
              à découvrir sous quelle tasse la bille est cachée, car
              pour vous l'argile est aussi transparente que le
              verre. Utilisez la <RandomTable p={this.r}
              bonus={this.f_bonus}
              onClick={this.handleClick.bind(this)}> pour obtenir un
              chiffre et ajoutez-y 5</RandomTable>. Vous saurez ainsi
              combien de Pièces d'Or vous avez gagnées avant que le
              fripon vous soupçonne et mette fin au jeu.  Votre bourse
              est à nouveau remplie et vous retournez au bar où vous
              payez le prix d'une chambre, soit une Pièce d'Or. <L
              enabled={r!==-1} to="314">Rendez-vous au 314</L>.

              <p/>
              <HelpGold/>
              <CurrentDice r={r}/>

            </div>
        );
    }
}

export default P116;
