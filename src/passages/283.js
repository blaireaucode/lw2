/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';
import HelpGold from '../HelpGold.js';

class P283 extends Component {
    
    constructor(props) {
        super(props);
        const f = {
            'name': 'obj1',
            'passage_title': 283,
            'list': [
                {id:'epee', cost:4},
                {id:'poignard', cost:2},
                {id:'glaive', cost:6},
                {id:'lance', cost:5},
                {id:'repas', cost:2},
                {id:'anneaux_d_or', cost:8},
                {id:'couverture', cost:3},                
                // {id:'sac_a_dos', cost:1},
            ],
        };
        this.items = f;
    }

    render() {
        return (
            <div>

              Vous êtes impressionné par l'abondance et la diversité
              des marchandises exposées : il y a là des soies et des
              épices en provenance des bazars de Vassagonia, des
              pierres précieuses des mines de Bor, les plus belles
              armes et cuirasses forgées par les armuriers de Durenor,
              des fourrures de Kalte, des étoffes de Cloeasia et sur
              toutes les tables les mets et les boissons les plus
              variés qui s'offrent à l'appétit des visiteurs.  Au
              centre du magasin, les prix de toutes les marchandises
              sont indiqués sur de grandes peaux de chèvre suspendues
              au plafond. L'une de ces listes attire tout
              particulièrement votre attention. En voici le détail :
              
              <p/>
              <ItemsPicker items={this.items}/>
              <p/>
              <HelpGold/>
              <p/>

              Si vous avez suffisamment d'argent pour cela, vous
              pourrez acheter ce qui vous plaira dans la liste
              ci-dessus. Lorsque vous aurez modifié en conséquence
              votre Feuille d'Aventure, vous quitterez le magasin par
              une porte latérale en <L to='245'>vous rendant au 245</L>.

            </div>
        );
    }
}

export default P283;
