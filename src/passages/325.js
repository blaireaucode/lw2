/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P325 extends Component {
    render() {
        return (
            <div>

              Votre Sens de l'Orientation vous indique que le couloir
              de gauche est le chemin le plus court pour Hammardal ;
              vous gagnerez environ 3 kilomètres en l'empruntant. Un
              peu avant la bifurcation, une grande flaque d'eau s'est
              formée au milieu de la chaussée et les traces de pas de
              deux hommes qui ont marché dans la flaque sont visibles
              sur le sol.  Chacun d'eux a pris un chemin différent ;
              l'un a suivi le tunnel de gauche, l'autre celui de
              droite. Les traces sont encore humides et il est
              probable que les deux hommes sont passés là il y a moins
              de vingt minutes. Si vous souhaitez suivre les traces
              qui mènent dans le couloir de gauche, <L
              to="64">rendez-vous au 64</L>. Si vous préférez suivre
              les traces qui conduisent au couloir de droite, <L
              to="164">rendez-vous au 164</L>.

            </div>
        );
    }
}

export default P325;
