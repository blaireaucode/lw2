/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Dead from '../Dead.js';

class P317 extends Component {
    render() {
        return (
            <div>

              Au cri de détresse du Squall à l'agonie répond bientôt
              le vôtre car deux carreaux d'arbalète viennent de se
              planter dans votre dos. Votre mission s'achève ici, en
              même temps que votre vie.

              <Dead/>

            </div>
        );
    }
}

export default P317;
