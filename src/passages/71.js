/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P71 extends Component {
    render() {
        return (
            <div>

              Vous claquez la porte derrière vous et vous poussez le
              verrou. La boutique est sombre, mais vous parvenez
              cependant à distinguer un escalier à votre droite, une
              trappe au milieu du plancher et une porte dans le mur du
              fond. Soudain, vous entendez le fracas d'une hache qui
              vient de briser un panneau de la porte d'entrée. On vous
              a vu entrer dans la boutique et la populace est en train
              de défoncer la porte. Si vous souhaitez ouvrir la trappe
              et vous cacher dans la cave, <L to='11'>rendez-vous au
              11</L>. Si vous préférez quitter la boutique par la
              porte du fond, <L to="54">rendez-vous au 54</L>. Enfin,
              si vous décidez plutôt de monter l'escalier, <L
              to="235">rendez-vous au 235</L>.

            </div>
        );
    }
}

export default P71;
