/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import update from 'immutability-helper';
import HelpCharacter from '../HelpCharacter.js';
import EnduranceChange from '../EnduranceChange.js';
import SimpleImage from '../SimpleImage.js';
import * as bh from '../book_helpers.js';

class P284 extends Component {

    constructor(props) {
        super(props);
        this.f = {
            'name': 'a',
            'passage_title': 284,
            'already_done': false,
            'dmg':-3
        };
    }

    handleClickMeal() {
        const elem = bh.book_get_element(this.props, this.f);
        if (!elem.already_done) {
            const e = update(elem, {already_done: {$set: true}});
            this.props.book_set_element(e);
            var c = bh.endurance_add(this.props.character, 2);
            c = bh.item_drop(c, 'repas');
            this.props.character_set(c);
        }        
    }
    
    render() {
        const elem = bh.book_get_element(this.props, this.f);
        const already_done = elem.already_done;
        const index = bh.item_get_index(this.props.character.items, 'repas');
        const meal = (index !== -1);
        return (
            <div>

              Les soldats vous encerclent et confisquent votre Sac à
              Dos et vos armes ; puis le chevalier s'avance vers vous
              et lève la visière de son heaume. « Qui êtes-vous ? Que
              venez vous faire à Tarnalin ? » demande-t-il d'une voix
              rude. Vous lui répondez que vous êtes un Seigneur Kaï du
              Sommerlund porteur d'un message urgent destiné au roi
              Alin. Il ne semble pas très convaincu jusqu'au moment où
              vous lui montrez le Sceau d'Hammardal. Dès lors, sans la
              moindre hésitation, il ordonne à ses hommes de vous
              rendre votre bien et il vous fait franchir le barrage de
              chariots. Derrière, un carrosse est stationné, au milieu
              du tunnel encombré. «&nbsp;A Hammardal, et vite !&nbsp;»
              ordonne-t-il au cocher en vous entraînant à
              l'intérieur. Vous avez à peine eu le temps de vous
              asseoir que les chevaux s'élancent au grand galop. Le
              chevalier vous apprend bientôt qu'il se nomme Lord Axim
              de Ryner et qu'il est le commandant de la garde
              personnelle du roi. Il se rendait à Port Bax lorsque les
              Monstres d'Enfer ont envahi le tunnel. La terrible
              bataille qui s'est ensuivie n'a laissé dans ses rangs
              que onze rescapés : lui-même et dix de ses soldats. La
              faim vous tenaille tandis que vous filez dans le tunnel
              de Tarnalin et il vous faut <L to='284'
              onClick={this.handleClickMeal.bind(this)}
              enabled={!already_done && meal}> prendre aussitôt un
              repas</L>, sinon vous perdez <EnduranceChange f={this.f}> 3
              points d'ENDURANCE</EnduranceChange>.

              <p/>
              <HelpCharacter/>
              <p/>
              <SimpleImage width={200} src={'284.png'}/>
              <p/>
         
              Le voyage jusqu'à la capitale durera cinq heures et Lord
              Axim vous conseille de vous reposer quelque peu d'ici à
              votre arrivée. Vous vous laissez alors gagner par le
              sommeil et dans un songe vous vous voyez revenir
              triomphalement à Holmgard en brandissant le Glaive de
              Sommer ; la suite du rêve vous montre la défaite
              cuisante des Maîtres des Ténèbres. Peut-être s'agit-il
              d'une vision prémonitoire ? <L enabled={already_done}
              to="9">Rendez-vous au 9</L>.

            </div>
        );
    }
}

export default P284;
