/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Dead from '../Dead.js';

class P212 extends Component {
    render() {
        return (
            <div>

              Le malheur veut que vous n'ayez pas d'armes et qu'il
              soit lui-même un redoutable bretteur. Le combat est
              désespéré et fort bref. Il vous transperce d'un coup
              d'épée et vous jette à bas du chariot d'un simple coup
              de pied. Mais rassurez-vous, votre chute ne sera pas
              trop douloureuse car vous êtes déjà mort lorsque vous
              arrivez en bas. Votre mission s'achève ici en même temps
              que votre vie.

              <p/>
              <Dead/>

            </div>
        );
    }
}

export default P212;
