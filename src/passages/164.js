/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P164 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, "sixieme_sens");
        return (
            <div>

            Vous marchez depuis une heure dans ce tunnel désert
            lorsque vous apercevez à votre gauche plusieurs marches
            taillées dans la paroi rocheuse. Elles mènent à une
            plateforme qui permet d'atteindre les torches éclairant le
            tunnel. Si vous souhaitez monter ces marches pour explorer
            la plate-forme, <L to="52">rendez-vous au 52</L>. Si vous
            préférez continuer votre chemin sans vous occuper des
            marches, <L to="256">rendez-vous au 256</L>. Si vous
              maîtrisez la Discipline Kaï du Sixième Sens, <L enabled={d}
            to="172">rendez-vous au 172</L>.

            </div>
        );
    }
}

export default P164;
