/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';
import SimpleImage from '../SimpleImage.js';

class P95 extends Component {
    render() {
        const g = bh.has_discipline(this.props.character, "guerison");
        const h = bh.item_get_index(this.props.character.items, "herbe_laumspur") !== -1;
        const e = g || h;
        return (
            <div>

              Vous lancez votre cheval à grands coups d'éperons parmi
              les arbres enchevêtrés et vous arrivez bientôt dans une
              petite clairière. Là, six Squalls surexcités sont en
              train de sautiller autour du corps convulsé d'un homme
              étendu sur le sol. Une lance à la hampe sculptée de
              motifs étranges est enfoncée dans sa poitrine et le
              cadavre d'un Chevalier de la Montagne Blanche repose à
              côté de lui. Les Squalls échangent des cris perçants et
              semblent tout à fait indifférents au sort de l'homme
              blessé qui visiblement agonise sous leurs yeux.

              <p/>
              <SimpleImage width={200} src={'95.png'}/>
              <p/>

              Si vous souhaitez attaquer les Squalls, <L
              to="28">rendez-vous au 28</L>. Si vous maîtrisez la
              Discipline Kaï de la Guérison ou si vous disposez d'une
              potion de guérison ou d'herbe de Laumspur, vous pouvez
              essayer de sauver la vie de l'homme blessé en <L to='239'
              enabled={e}>vous rendant au 239</L>.

            </div>
        );
    }
}

export default P95;
