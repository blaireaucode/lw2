/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import update from 'immutability-helper';
import HelpCharacter from '../HelpCharacter.js';
import EnduranceChange from '../EnduranceChange.js';
import SimpleImage from '../SimpleImage.js';
import * as bh from '../book_helpers.js';

class P150 extends Component {

    constructor(props) {
        super(props);
        this.f = {
            'name': 'a',
            'passage_title': 150,
            'already_done': false,
            'dmg':-3
        };
    }

    handleClickMeal() {
        const elem = bh.book_get_element(this.props, this.f);
        if (!elem.already_done) {
            const e = update(elem, {already_done: {$set: true}});
            this.props.book_set_element(e);
            var c = bh.endurance_add(this.props.character, 2);
            c = bh.item_drop(c, 'repas');
            this.props.character_set(c);
        }        
    }
    
    render() {
        const elem = bh.book_get_element(this.props, this.f);
        const already_done = elem.already_done;
        const index = bh.item_get_index(this.props.character.items, 'repas');
        const meal = (index !== -1);
        return (
            <div>

              Vous lancez votre cheval dans les rues sinueuses du
              village, puis vous traversez un pont de bois ; vous
              montez ensuite un sentier escarpé qui conduit au sommet
              d'une crique. A la clarté de la lune, vous apercevez un
              poteau indicateur orienté vers l'est. Vous chevauchez
              toute la nuit sans prendre le temps de dormir et lorsque
              l'aube se lève enfin, le paysage s'est métamorphosé
              d'une manière surprenante. Les terres arides du Pays
              Sauvage ont fait place à des landes et à des marécages,
              et, aussi loin que porte le regard, une ombre noire
              s'étend à l'horizon en direction de l'est. C'est la
              forêt de Durenor, la frontière naturelle du royaume des
              montagnes qui borde à cet endroit les espaces inexplorés
              du Pays Sauvage. Voilà sans nul doute une vision
              réconfortante qui vous met quelque baume au cœur.

              <p/>
              <SimpleImage width={200} src={'150.png'}/>
              <p/>

              Vous n'êtes plus qu'à une journée de cheval de Port Bax,
              mais vous êtes épuisé après cette nuit blanche et il
              vous faut <L to='150'
              onClick={this.handleClickMeal.bind(this)}
              enabled={!already_done && meal}> prendre un repas</L>,
              sinon vous <EnduranceChange f={this.f}> 3 points
              d'ENDURANCE</EnduranceChange>.  Si vous maîtrisez la
              Discipline Kaï de la Chasse, vous pouvez en faire usage
              et capturer du gibier qui vous fournira les viandes
              nécessaires pour reprendre des forces. Vous avez
              chevauché pendant une heure lorsque vous arrivez à une
              bifurcation, mais vous ne voyez aucun poteau
              indicateur. Si vous souhaitez prendre le chemin de
              gauche, <L enabled={already_done} to="261">rendez-vous
              au 261</L>. Si vous préférez aller à droite, <L
              enabled={already_done} to="334">rendez-vous au 334</L>.

                <p/>
                <HelpCharacter/>

            </div>
        );
    }
}

export default P150;
