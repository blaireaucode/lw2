/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import * as bh from '../book_helpers.js';

class P306 extends Component {
    render() {
        const f = {
            name: 'f1',
            passage_title: 306,
            encounter: "GARDE FRONTALIER",
            combat_skill:16,
            endurance:24,
            bonus_factor_dmg_encounter: 2
        };
        const r = bh.enabled_if_fight_win(this.props, f);
        return (
            <div>

              Le soldat vous donne un coup de lance en visant votre
              poitrine, mais vous faites un pas de côté et le fer vous
              écorche à peine le bras. Le GARDE est décidé à se battre
              ; or, vous ne voulez pas le tuer, simplement essayer de
              l'assommer. Menez ce combat à la manière habituelle,
              mais en multipliant par 2 les points d'ENDURANCE perdus
              par votre adversaire. Lorsque son total d'ENDURANCE sera
              descendu à zéro, vous aurez réussi à le mettre hors de
              combat. En ce qui vous concerne, tous les points
              d'ENDURANCE que vous perdrez lors de cet affrontement
              seront normalement déduits de votre total.  Si vous
              parvenez à assommer ce soldat, <L enabled={r}
              to="35">rendez-vous au 35</L>.

              <p/>
              <Fight fight={f}/>
            </div>
        );
    }
}

// GARDE FRONTALIER HABILETÉ: 16 ENDURANCE: 24 

export default P306;
