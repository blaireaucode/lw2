/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P63 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, "communication_animale");
        return (
            <div>

              Vous êtes réveillé au milieu de la nuit par un poids qui
              pèse soudain sur votre poitrine. Vous écartez lentement
              les pans de votre cape et vous découvrez avec horreur
              qu'un Serpent des Sables s'est niché dessous. Si vous
              voulez essayer d'attraper ce serpent au venin mortel
              juste derrière la tête et le jeter au loin, <L
              to="188">rendez-vous au 188</L>. Si vous préférez vous
              lever d'un bond en essayant de faire tomber le serpent
              sur le sol, <L to="201">rendez-vous au 201</L>. Si vous
              maîtrisez la Discipline Kaï de la Communication Animale,
              <L enabled={d} to="264"> rendez-vous au 264</L>.

            </div>
        );
    }
}

export default P63;
