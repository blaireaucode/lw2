/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import * as bh from '../book_helpers.js';

class P114 extends Component {

    constructor(props) {
        super(props);
        this.r = {
            passage_title: 114,
            name: 'random_table',
            dice_value: -1
        };
    }
    
    render() {
        const randtable = bh.book_get_element(this.props, this.r);
        const r = randtable.dice_value;
        return (
            <div>

              Vous avez marché pendant trois heures sur cette route
              déserte qui longe la côte lorsque la nuit commence à
              tomber. Les terres alentour sont plates et désolées et
              vous n'avez pas vu signe de vie depuis que vous vous
              êtes mis en chemin. Vous décidez alors de prendre
              quelque repos à l'abri des branches d'un grand arbre qui
              s'élève au bord de la route.  Vous posez votre tête sur
              votre Sac à Dos en guise d'oreiller, vous vous couvrez
              de votre cape de Seigneur Kaï et vous vous laissez
              emporter dans un profond sommeil. Utilisez <RandomTable
              p={this.r}>pour obtenir un chiffre</RandomTable>. Si
              vous tirez un chiffre entre 0 et 3, <L enabled={r>=0 &&
              r<=3} to="206">rendez-vous au 206</L>. Entre 4 et 7, <L
              enabled={r>=4 && r<=7} to="63">rendez-vous au 63</L>. Si
              vous tirez 8 ou 9, <L enabled={r>=8 && r<=9}
              to="8">rendez-vous au 8</L>.

              <CurrentDice r={r}/>
            </div>
        );
    }
}

export default P114;
