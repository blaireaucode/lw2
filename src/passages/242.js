/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P242 extends Component {
    render() {
        return (
            <div>

              Pendant la plus grande partie de votre séjour à
              Hammardal, vous vous entraînez à manier le Glaive de
              Sommer. Jour après jour, votre habileté s'accroît et, à
              mesure que vous progressez, vous en apprenez davantage
              sur les vertus de cette arme fabuleuse. Chaque fois que
              vous utiliserez le glaive dans un combat, votre total
              d'HABILETÉ sera augmenté de 8 points (de 10 points si
              vous avez choisi la Discipline Kaï de la Maîtrise des
              Armes et que le sort vous a donné cette maîtrise à
              l'épée). Le glaive a en outre la propriété d'annuler les
              effets de toute pratique magique exercée contre vous par
              un adversaire ; il vous permettra aussi de multiplier
              par 2 tous les points d'ENDURANCE perdus par des morts
              vivants (les Monstres d'Enfer, par exemple) au cours des
              combats qu'il vous faudra peut-être livrer contre
              eux. Enfin, vous avez pleinement conscience que le
              Glaive de Sommer est la seule et unique arme, sur toutes
              les terres de Magnamund, qui ait le pouvoir d'ôter la
              vie à un Maître des Ténèbres ; il n'est donc pas
              étonnant que ces derniers aient résolu de vous tuer à
              tout prix. <L to="152">Rendez-vous au 152</L>.

            </div>
        );
    }
}

export default P242;
