/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import * as bh from '../book_helpers.js';

class P237 extends Component {
    render() {
        const f = {
            passage_title: 237,
            encounter: "MONSTRE D'ENFER",
            combat_skill: 23,
            endurance:30,
            no_bonus_psy: true,
            bonus_factor_dmg_encounter: 2
        };
        const r = bh.enabled_if_fight_win(this.props, f);
        return (
            <div>

              Les Zombies morts sont étendus à vos pieds. A présent,
              la peur que les soldats éprouvaient devant les morts
              vivants a fait place à la haine. Un chœur de cris de
              guerre retentit sur le pont et vous montez à l'abordage
              du vaisseau fantôme, suivis par les centaines de soldats
              ivres de rage.  Les Zombies sont fauchés sous l'assaut
              comme des épis de blé par une faux. Puis soudain, une
              silhouette drapée dans une longue cape vous interdit le
              passage, brandissant dans sa main squelettique une épée
              à la lame recourbée. C'est un MONSTRE D'ENFER et il vous
              faut le combattre jusqu'à la mort de l'un d'entre vous.
              C'est un mort vivant et vous avez donc le droit, en
              vertu de la puissance du Glaive de Sommer, de multiplier
              par 2 tous les points d'ENDURANCE qu'il perdra au
              combat. Mais rappelez-vous qu'il est insensible à la
              Discipline Kaï de la Puissance Psychique. Si vous êtes
              vainqueur, <L enabled={r} to="309">rendez-vous au
              309</L>.

              <p/>
              <Fight fight={f}/>

            </div>
        );
    }
}
// MONSTRE D'ENFER HABILETÉ: 23 ENDURANCE: 30 
export default P237;
