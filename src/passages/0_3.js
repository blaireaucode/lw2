/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Cont from '../Cont.js';

class P0_3 extends Component {
    render() {
        return (
            <div>

              Or, après la défaite de Vashna, le Glaive de Sommer fut offert aux
              alliés de Durenor comme un symbole de la confiance et de la
              solidarité qui unit les deux royaumes. En échange, le roi Alin de
              Durenor fit présent au Sommerlund d'un magnifique anneau d'or
              frappé du sceau royal de son pays. Cet anneau porte le nom de
              « Sceau d'Hammardal ». Lors de la cérémonie, le roi Alin jura
              que le royaume de Durenor viendrait aussitôt en aide à son allié
              si jamais le Sommerlund se trouvait à nouveau menacé par l'ombre
              des Ténèbres venue de l'occident.

              <p/>

              Le roi vous a confié le Sceau d'Hammardal et votre mission
              consiste désormais à vous rendre au royaume de Durenor pour y
              chercher le Glaive de Sommer. Mais entre temps, les troupes
              ennemies ont réussi à forcer les lignes de défense de la capitale
              et s'apprêtent à assiéger la ville. Tandis que le capitaine Gayal,
              de la Garde du Roi, vous conduit à l'arsenal du Palais pour y
              prendre les armes que vous emporterez dans votre quête, les
              dernières paroles de votre souverain vous reviennent sans cesse à
              l'esprit : « Quarante jours, Loup Solitaire, tu n'as que
              quarante jours pour rapporter le glaive. Nous aurons la force de
              résister à l'ennemi pendant ces quarante jours. Après... Il sera
              trop tard... »

              <p/>
              <Cont to='0_4'/>

            </div>
        );
    }
}

export default P0_3;
