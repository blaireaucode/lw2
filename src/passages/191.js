/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P191 extends Component {
    render() {
        return (
            <div>

              Un peu plus loin, la rue pavée tourne brusquement vers
              la droite. Vous vous trouvez alors devant un bâtiment de
              pierre blanche qui porte cette plaque fixée au-dessus de
              la porte :

              <p/>
              <SimpleImage width={250} src={'191.png'}/>
              <p/>

              La rue pavée aboutit à un haut mur de pierre dans lequel
              est aménagée une grande porte rouge gardée par deux
              soldats. Au-delà de cette porte, on distingue les mâts
              des navires ancrés dans le port. Si vous souhaitez
              entrer dans la tour de guet, <L to="318">rendez-vous au
              318</L>. Si vous préférez vous approcher de la porte
              rouge, <L to="246">rendez-vous au 246</L>.

            </div>
        );
    }
}

export default P191;
