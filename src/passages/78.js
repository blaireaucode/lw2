/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';


import EnduranceChange from '../EnduranceChange.js';
import HelpCharacter from '../HelpCharacter.js';
import * as bh from '../book_helpers.js';

class P78 extends Component {
    constructor(props) {
        super(props);
        this.f = {
            'name': 'a',
            'passage_title': 78,
            'already_done': false,
            'dmg':-1
        };
    }
      
    handleClickRemoveItem() {
        const c = bh.item_drop(this.props.character, 'mail');
        this.props.character_set(c);            
    }
       
    render() {
        const elem = bh.book_get_element(this.props, this.f);
        const r = !elem.already_done;
        const l = this.props.character.items.some(e => e.id === 'mail');
        const f = !l && !r;

        return (
            <div>
              
              Vous faites un bond en arrière, juste à temps pour
              éviter d'être écrasé par le mât qui s'abat sur le pont
              en passant au travers. Vous vous relevez en chancelant
              et vous examinez les débris de bois. Le corps sans vie
              du capitaine Kelman est coincé sous le mât brisé. Vous
              contemplez ce spectacle d'un regard horrifié lorsque
              soudain la tempête ouvre une large brèche dans la coque
              déjà endommagée du Sceptre Vert. Et tandis que le navire
              se disloque, vous êtes projeté par-dessus le bastingage
              et vous tombez dans les flots déchaînés. A moitié
              étouffé, vous parvenez tant bien que mal à remonter à la
              surface pour prendre une bouffée d'air, et votre tête
              heurte alors un panneau d'écoutille. Vous perdez
              <EnduranceChange f={this.f}> un point
              d'ENDURANCE</EnduranceChange> et vous vous hissez sur ce
              radeau de fortune.

              <p/><HelpCharacter/><p/>

              Si vous portez une cotte de mailles, il faut vous en
              débarrasser, sinon, vous risquez de périr noyé. <L
              onClick={this.handleClickRemoveItem.bind(this)} enabled={l}
              to='78'>Rayez-la de votre Feuille d'Aventure</L>.  Dans
              la lumière grise de la tourmente, vous contemplez la
              coque brisée du navire qui sombre dans la mer. Vous êtes
              alors pris de vertige, vous vous sentez mal et vous vous
              cramponnez de toutes vos forces au panneau d'écoutille,
              mais peu à peu votre corps faiblit et vous perdez
              conscience.

              <p/>

              Lorsque, enfin, vous vous réveillez, la tempête s'est
              calmée. Il ne reste plus du Sceptre Vert que le panneau
              d'écoutille sur lequel vous êtes toujours étendu. A en
              juger par la position du soleil, l'après-midi touche à
              sa fin. Au loin, vous apercevez un petit bateau de pêche
              et au-delà, la côte qui s'étend à l'horizon. Si vous
              voulez essayer de signaler votre présence au bateau de
              pêche en agitant votre cape, <L enabled={f} to="278">
              rendez-vous au 278</L>. Si vous préférez ne pas vous
              occuper du bateau et tenter de rejoindre la côte en
              pagayant à l'aide de vos seules mains, <L enabled={f}
              to="337"> rendez-vous au 337</L>.

            </div>
        );
    }
}

export default P78;
