/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P14 extends Component {
    render() {
        return (
            <div>
              
              Dès que la lutte s'engage, vous vous servez du pouvoir
              que vous donne la Discipline Kaï pour affaiblir la
              concentration de votre adversaire. Vous voyez la sueur
              perler à son front et ses yeux se fermer tandis qu'il
              cède peu à peu sous l'effet de votre implacable
              Puissance Psychique. Enfin, moins d'une minute plus
              tard, il s'écroule sur le sol, sans connaissance. <L
              to="305">Rendez-vous au 305</L>.

            </div>
        );
    }
}

export default P14;
