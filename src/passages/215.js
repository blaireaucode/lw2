/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P215 extends Component {
    render() {
        return (
            <div>

              Une trentaine de mètres plus loin, du côté gauche de la
              rue, des cris joyeux et des chants filtrent à travers la
              façade d'une grande bâtisse délabrée. Une enseigne
              rouillée grince au-dessus de la porte.  Si vous voulez
              entrer dans la taverne, <L to="4">rendez-vous au
              4</L>. Si vous préférez continuer de marcher le long de
              la rue de la Bernicle, <L to="83">rendez-vous au 83</L>

              <p/>
              <SimpleImage width={300} src={'215.png'}/>

            </div>
        );
    }
}

export default P215;
