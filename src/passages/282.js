/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import * as bh from '../book_helpers.js';

class P282 extends Component {

    constructor(props) {
        super(props);
        const f1 = {
            passage_title: 282,
            name:'f1',
            encounter: "1er SOLDAT DU PONT",
            combat_skill: 16,
            endurance: 24
        };
        const f2 = {
            passage_title: 282,
            name:'f2',
            encounter: "2e SOLDAT DU PONT",
            combat_skill: 16,
            endurance: 22
        };
        this.ff = [];
        this.ff.push(f1);
        this.ff.push(f2);
    }

    render() {
        const f = bh.book_get_current_fight(this.props, this.ff);
        const r = bh.enabled_if_fight_win(this.props, f);
        return (
            <div>

              De la pointe de leurs lances, les SOLDATS essaient de
              vous repousser.  Et tandis que vous levez votre arme sur
              l'un d'eux, l'autre vous contourne pour vous attaquer
              par-derrière. Il vous est impossible de prendre la fuite
              et vous allez devoir les combattre à tour de rôle
              jusqu'à la mort.  Si vous êtes vainqueur, <L enabled={r}
              to="187">rendez-vous au 187</L>.

              <p/>
              <Fight fight={f}/>

            </div>
        );
    }
}

// 1er SOLDAT DU PONT HABILETÉ : 16 ENDURANCE : 24
// 2e SOLDAT DU PONT HABILETÉ : 16 ENDURANCE : 22

export default P282;
