/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P255 extends Component {
    render() {
        return (
            <div>

              Vous empoignez votre arme et vous faites une brillante
              démonstration de vos talents. Vos maîtres Kaï vous ont
              enseigné la vitesse et le sens de l'équilibre, les deux
              éléments essentiels de la technique du combat.  Et
              lorsque vous faites tournoyer et virevolter votre arme
              autour de votre tête et de votre corps, vos mains
              bougent avec une telle rapidité qu'on ne parvient plus à
              distinguer leurs mouvements : elles se déplacent dans
              une sorte de tourbillon indistinct qui semble presque
              surnaturel. Pour achever de donner la preuve de votre
              maîtrise, vous frappez soudain le bord d'une assiette
              d'étain que vous envoyez tourbillonner à travers la
              salle avec une telle force qu'elle vient se ficher
              profondément dans la porte de la cave. L'homme a assisté
              à tout ce spectacle avec des yeux ronds de stupeur. <L
              to="268">Rendez-vous au 268</L>.

            </div>
        );
    }
}

export default P255;
