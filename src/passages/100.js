/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P100 extends Component {
    render() {
        return (
            <div>

              Un voile de brume s'est répandu sur la mer calme. Il
              vient des îles Kirlundin, un archipel rocheux situé au
              nord-est du Sommerlund. Des formes étranges et sombres
              apparaissent bientôt dans le brouillard ; elles
              grandissent peu à peu et quelques minutes plus tard, on
              parvient à en distinguer plus nettement les contours :
              ce sont des navires. « Branle-bas de combat ! » crie
              aussitôt l'amiral et son ordre est répété comme en écho
              sur tous les navires de la flotte de
              Durenor. «&nbsp;Tout le monde sur le pont ! » A mesure
              que les bateaux ennemis s'approchent dans le brouillard,
              un spectacle terrifiant vous frappe de stupeur : ce sont
              en effet des vaisseaux fantômes qui s'avancent vers
              vous, des épaves menées par les cadavres vivants de
              marins péris en mer. Et ces navires renfloués par
              quelque effrayant prodige de haute sorcellerie se
              préparent à combattre la flotte de Durenor.

              <p/>

              Soudain, la brume se dissipe et vous voyez distinctement
              les bateaux fantômes se disposer en ligne pour interdire
              l'entrée du golfe de Holm. Le vaisseau amiral de cette
              flotte maléfique a pris place au milieu de la rangée et
              fait voile vers vous, son rostre immense pointant à
              l'extrémité de sa proue noire. Un instant plus tard, le
              puissant éperon déchire la coque du Durenor et vous
              entendez la voix désespérée de l'amiral lancer un ordre
              : «&nbsp;Sauve qui peut ! Abandonnez le navire ! » Le
              Durenor à présent est encerclé par la flotte ennemie et
              sombre rapidement.

              <p/>
              <SimpleImage width={200} src={'100.png'}/>
              <p/>

              Si vous souhaitez sauter sur le pont du vaisseau amiral
              de la flotte fantôme, <L to="30">rendez-vous au
              30</L>. Si vous préférez plonger dans la mer et tenter
              de gagner à la nage un autre navire de la flotte de
              Durenor, <L to="267">rendez-vous au 267</L>.

            </div>
        );
    }
}

export default P100;
