/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';

import { character_set } from '../actions.js';

const mapStateToProps = store => {
    return { character: store.character };
};

const mapDispatchToProps = (dispatch) => ({
    character_set(character) { dispatch(character_set(character)); }
});

class PTest extends Component {

    hello() {
        let character = JSON.parse(JSON.stringify(this.props.character));
        character.test = character.test+1;
        this.props.character_set(character);
    }

    render() {
        return (
            <div>

            This is a test

              <p/>

              test value = {this.props.character.test}

              <p/>

              <Button onClick={this.hello.bind(this)}>Test modify character</Button>

              <p/>
              <p/>
              <Link to="/passage/1">Rendez-vous au 1</Link>.

            </div>
        );
    }
}

export default PTest;
