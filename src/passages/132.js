/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P132 extends Component {

    constructor(props) {
        super(props);
        const f = {
            'name': 'obj1',
            'passage_title': 132,
            'list': [ {id:'lance'} ],
        };
        this.items = f;
    }

    render() {
        return (
            <div>

              Vous atterrissez dans la rue boueuse au milieu d'une
              pluie de verre brisé. La chute vous a quelque peu secoué
              mais vous êtes indemne. Un villageois furieux, armé
              d'une matraque, essaie de vous fracasser le crâne, mais
              vous roulez sur vous-même et vous vous relevez d'un bond
              ; avant qu'il ait eu le temps de vous atteindre, vous
              êtes déjà en train de courir le long de la rue sinueuse
              ; vous n'êtes pas au bout de vos peines cependant, car
              un Squall à cheval se précipite sur vous, sa lance
              levée.  Il s'apprête à vous frapper lorsque vous faites
              un pas de côté qui vous permet d'éviter le coup. Vous
              saisissez alors la hampe de son arme et vous
              déséquilibrez le Squall qui glisse de sa selle. Si vous
              souhaitez frapper le Squall à l'aide de sa propre lance,
              <L to="317"> rendez-vous au 317</L>. Si vous préférez
              vous emparer de son cheval pour prendre la fuite, <L
              to="150">rendez-vous au 150</L>. Si vous décidez de
              garder la lance, n'oubliez pas de l'inscrire sur votre
              Feuille d'Aventure.

              <p/>
              <ItemsPicker items={this.items}/>
            </div>
        );
    }
}

export default P132;
