/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P91 extends Component {

    constructor(props) {
        super(props);
        const f = {
            'name': 'obj1',
            'passage_title': 91,
            'list': [
                {id:'baton'},
                {id:'couverture'},
                {id:'repas', nb:2},
                {id:'poignard'},
                {id:'corde'},
            ],
        };
        this.items = f;
    }

    
    render() {
        return (
            <div>

              Le garçon est expulsé du magasin par deux gardes vêtus
              d'un uniforme noir. Le marchand vous remercie et vous
              offre 2 objets que vous devrez choisir dans la liste
              suivante. Faites votre choix (deux objets à votre
              convenance) et inscrivez vos nouvelles acquisitions sur
              votre Feuille d'Aventure dans la case Sac à Dos.

              <p/>
              <ItemsPicker items={this.items}/>
              <p/>
              
              Vous remerciez ensuite le marchand et vous
              sortez par une porte latérale. <L to="245">Rendez-vous
              au 245</L>.

            </div>
        );
    }
}

export default P91;
