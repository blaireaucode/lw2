/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P224 extends Component {
    render() {
        return (
            <div>

              Le lendemain matin, vous êtes réveillé par les cris des
              goélands qui tournoient au-dessus du clipper. Un fort
              vent enfle les voiles. Quelques instants plus tard, vous
              prenez votre petit déjeuner en compagnie du capitaine
              Kelman qui semble plus optimiste que la veille. Il vous
              annonce que le Sceptre Vert vogue à bonne allure et que
              vous devriez arriver dans une semaine à Port Bax, le
              port principal du royaume de Durenor. Puis soudain, un
              cri retentit dans le nid-de-pie. «&nbsp;Terre par bâbord
              avant ! hurle la vigie, terre par bâbord !&nbsp;» Le
              capitaine et vous-même montez alors sur le pont,
              affrontant la froideur de la brise. «&nbsp;C'est Mannon,
              l'île la plus au sud de l'archipel des Kirlundin, dit le
              capitaine en montrant une côte rocheuse et accidentée
              qui se dessine au loin, les marchands l'appellent la
              "Pointe des Naufragés" ; nombreux sont les navires qui
              ont fini leur carrière sur ces rochers de granit.&nbsp;» Le
              capitaine vous tend une lunette d'approche pour vous
              permettre de mieux observer l'île. Les rocs pointus de
              son rivage sont parsemés d'épaves : ce sont les
              carcasses fracassées des navires qui s'y sont échoués ou
              que la tempête y a précipités. Vous êtes fasciné par le
              spectacle de ces coques déchirées et vous imaginez les
              scènes terrifiantes qui ont dû se dérouler lors de
              chacun de ces naufrages.

              <p/>

              Puis, brusquement, vous apercevez une ombre noire
              suspendue au-dessus des pointes rocheuses de Mannon ; on
              dirait un petit nuage qui semble se déplacer dans votre
              direction. Mais un instant plus tard, vous comprenez de
              quoi est fait ce «nuage». Il s'agit en fait d'une nuée
              de Bêtalzans auxquels se sont probablement mêlés des
              Kraans. Aussitôt, l'alerte est donnée. «&nbsp;Parez au
              combat !&nbsp;» Si vous souhaitez rester sur le pont,
              préparez votre arme et <L to="146">rendez-vous au
              146</L>. Si vous préférez retourner dans votre cabine,
              <L to="34"> rendez-vous au 34</L>.

            </div>
        );
    }
}

export default P224;
