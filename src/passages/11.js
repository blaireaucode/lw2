/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Dead from '../Dead.js';

class P11 extends Component {
    render() {
        return (
            <div>

            Vous vous cachez dans un grand tonneau en vous dissimulant sous
            votre cape de Seigneur Kaï. Mais votre tentative reste vaine car
            moins d'une minute plus tard la trappe s'ouvre à la volée et les
            villageois furieux sautent sur le sol de pierre en brandissant des
            torches et des épées. Ils vous arrachent à votre tonneau et vous
            font rouler à terre à grands coups de pied. Les cris de la foule
            étouffent vos supplications, inutile d'espérer la moindre pitié.
            Votre quête s'achève ici, en même temps que votre vie.

            <Dead/>

            </div>
        );
    }
}

export default P11;
