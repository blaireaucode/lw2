/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P70 extends Component {
    render() {
        const l = this.props.character.items.some(e => e.id === 'pendentif_etoile_cristal');
        return (
            <div>

              Vous haletez de douleur lorsque le serpent plonge ses
              crochets dans votre bras. Vous saisissez le reptile
              juste derrière sa tête repoussante, vous l'arrachez à
              votre bras et vous le jetez dans l'herbe. Mais le
              serpent a eu le temps de vous infliger une profonde
              morsure et son venin commence à faire de l'effet. Si
              vous possédez le Pendentif à l'Étoile de Cristal, <L
              enabled={l} to="219">rendez-vous au 219</L>. Dans le cas
              contraire, <L to="44"> rendez-vous au 44</L>.

            </div>
        );
    }
}

export default P70;
