/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P336 extends Component {
    render() {
        return (
            <div>

              En une fraction de seconde, l'éclair change de
              direction, attiré par le Glaive de Sommer qui en absorbe
              aussitôt l'énergie aussi facilement qu'une éponge
              absorbe une goutte d'eau. C'est là un des pouvoirs du
              Glaive, comme vous l'avez appris au cours de votre
              entraînement : il vous protège de toute magie et vient
              ainsi de vous épargner une mort certaine. Le sorcier
              lance alors un juron, arrache une pierre précieuse de
              son turban richement orné et la jette à vos pieds. Une
              flamme en jaillit instantanément et un nuage vert
              s'élève vers vous : c'est un gaz puissant dont l'odeur
              acide vous fait suffoquer. Pour échapper à ce gaz
              délétère, vous êtes contraint de sauter sur le pont et
              le sorcier profite de cette diversion pour s'enfuir du
              vaisseau fantôme à bord d'un canot dont il actionne les
              rames avec une véritable frénésie. Si vous souhaitez
              vous aussi quitter le vaisseau fantôme en plongeant dans
              la mer, <L to="109">rendez-vous au 109</L> Si vous
              préférez essayer de rejoindre un navire de la flotte de
              Durenor au prix de quelques combats, <L
              to="185">rendez-vous au 185</L>.

            </div>
        );
    }
}

export default P336;
