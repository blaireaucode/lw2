/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P218 extends Component {
    render() {
        return (
            <div>

              Le capitaine Zombie est mort, mais vous vous trouvez
              encerclé par les visages macabres des membres de
              l'équipage ; ils sont au nombre de vingt, armés de
              coutelas et de haches. Si vous voulez les combattre, <L
              to="43">rendez-vous au 43</L>. Si vous préférez vous
              enfuir en saisissant une corde qui pend à proximité et
              en l'utilisant pour vous élancer sur le pont d'un navire
              de Durenor, <L to="105">rendez-vous au 105</L>.

            </div>
        );
    }
}

export default P218;
