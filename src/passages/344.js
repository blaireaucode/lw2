/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P344 extends Component {
    render() {
        return (
            <div>

              Votre Sixième Sens vous révèle que ces étrangers sont
              des Monstres d'Enfer, les féroces serviteurs des Maîtres
              des Ténèbres, et qu'ils ont pour mission de vous
              assassiner. Ces immondes créatures ont le pouvoir de
              prendre à leur guise une apparence humaine et sont par
              ailleurs invulnérables aux armes habituelles tout autant
              qu'à la Puissance Psychique. Vous criez à Rhygar et à
              ses hommes de prendre garde à ces monstres, puis vous
              vous enfuyez vers la forêt. <L to="183">Rendez-vous au
              183</L>.

            </div>
        );
    }
}

export default P344;
