/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P170 extends Component {
    render() {
        return (
            <div>

            Le garde jette un coup d'œil à votre carte de couleur
            blanche et renifle avec mépris. «&nbsp;C'est un
            laissez-passer de marchand, dit-il, il ne vous sera
            d'aucune utilité ici. Il vous faut un laissez-passer rouge
            pour avoir accès à la base navale.&nbsp;» Il vous rend votre
            carte et retourne à son poste de garde. <L
            to="327">Rendez-vous au 327</L>.

            </div>
        );
    }
}

export default P170;
