/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P252 extends Component {
    render() {
        return (
            <div>

              Vous essayez de vous rappeler certains récits que vous a
              faits il y a quelque temps un Maître Kaï surnommé
              «Faucon Raisonnable».  Pendant des années, il avait
              occupé un poste de diplomate à Port Bax et il en était
              arrivé à connaître et à aimer la ville tout autant qu'un
              natif de l'endroit. Vous vous souvenez de ce qu'il vous
              avait dit : le consulat du Sommerlund se trouve sur la
              place Alin, à l'intérieur du quartier maritime. Or, un
              panneau indicateur, à votre gauche, signale : QUARTIER
              MARITIME 800 mètres. Certain d'être dans la bonne
              direction, vous marchez d'un pas confiant le long de
              l'avenue bordée d'arbres. <L to="191">Rendez-vous au
              191</L>.

            </div>
        );
    }
}

export default P252;
