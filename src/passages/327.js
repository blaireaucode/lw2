
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';
import HelpGold from '../HelpGold.js';

class P327 extends Component {

    pay() {
        const d = bh.get_done(this, 327);
        if (!d) {
            var c = bh.gold_add(this.props.character, -6);
            const item = bh.item_get_description("laissez_passer_rouge");
            c = bh.item_pick(c, item);
            this.props.character_set(c);
            bh.update_done(this, 327);
        }
    }
    
    render() {
        const g = bh.gold_get(this.props.character);
        const d = bh.get_done(this, 327);
        return (
            <div>

              Vous rebroussez chemin le long de la rue pavée en vous
              demandant ce qu'il convient de faire lorsqu'un
              jeune-garçon s'approche de vous. «Je peux vous faire
              entrer dans le port, dit-il, mais il faudra payer. » Il
              vous montre alors une enveloppe remplie de papiers
              officiels ou qui semblent tels. «&nbsp;Grâce à ces
              papiers, poursuit-il, vous obtiendrez un laissez-passer
              rouge au poste de garde du port. Ils sont à vous pour 6
              Couronnes seulement.&nbsp;» Si vous souhaitez acheter
              ces documents, <L enabled={g>=6 && !d}
              onClick={this.pay.bind(this)} to='327'>payez-les 6
              Couronnes</L>. Si vous refusez, le garçon vous laisse là
              et disparaît bientôt.

              <p/>
              <HelpGold/>
              <p/>

              Lorsque vous aurez pris une décision, vous retournerez à
              la tour. Si vous souhaitez y entrer, <L
                       to="318">rendez-vous au 318</L>. Si vous préférez
              emprunter l'avenue bordée d'arbres pour retourner à
              l'hôtel de ville et y demander comment faire pour vous
              rendre au Consulat du Sommerlund, <L to='84'>allez dans
           ce cas au 84</L>.

            </div>
        );
    }
}

export default P327;
