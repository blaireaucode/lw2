/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P112 extends Component {
    render() {
        return (
            <div>

              Vous vous rappelez soudain ce qu'il vous a dit au sujet
              de la boutique à la porte orange. C'est le Quartier
              Général de la Fraternité du Silence, la célèbre police
              secrète de Lachelan. Entrer dans cette boutique serait
              plus dangereux encore que de pénétrer dans une pièce
              remplie de Drakkarim ! Vous vous détournez aussitôt de
              la porte orange et vous vous hâtez en direction du
              nord. <L to="230">Rendez-vous au 230</L>.

            </div>
        );
    }
}

export default P112;
