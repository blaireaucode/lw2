/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P2 extends Component {

    render() {
        const d = bh.has_discipline(this.props.character, "sixieme_sens");
        return (
            <div>

              Vous suivez une route étroite et sinueuse, le long de la côte. De
              hautes falaises en surplomb se dressent sur un côté du chemin. A
              quelque distance, un éboulement interdit le passage et il vous
              faut vous arrêter pour dégager la voie. Mais, alors que vous aidez
              le conducteur à soulever un énorme rocher en vous servant d'un
              levier, vous en entendez tomber un autre et, un instant plus tard,
              une gigantesque masse de pierre se fracasse sur la chaussée,
              écrasant le conducteur sous son poids. L'homme est tué sur le coup
              avant que vous ayez pu faire le moindre geste pour essayer de le
              sauver. Le rocher est tombé du haut de la falaise et vous avez
              bien failli vous-même le recevoir sur la tête, car le malheureux
              qui vient de périr ne se trouvait qu'à deux mètres de vous. Si
              vous maîtrisez la Discipline Kaï du Sixième Sens, <L to='42'
              enabled={d}>rendez-vous au 42</L>. Dans le cas contraire, <L
              to='168' enabled={!d}> rendez-vous au 168</L>.

            </div>
        );
    }
}

export default P2;
