/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

import update from 'immutability-helper';

import HelpCharacter from '../HelpCharacter.js';
import EnduranceChange from '../EnduranceChange.js';
import * as bh from '../book_helpers.js';
import SimpleImage from '../SimpleImage.js';

class P127 extends Component {
    constructor(props) {
        super(props);
        this.f = {
            'name': 'a',
            'passage_title': 127,
            'already_done': false,
            'dmg':-3
        };
    }

    handleClickMeal() {
        const elem = bh.book_get_element(this.props, this.f);
        if (!elem.already_done) {
            const e = update(elem, {already_done: {$set: true}});
            this.props.book_set_element(e);
            var c = bh.endurance_add(this.props.character, 2);
            c = bh.item_drop(c, 'repas');
            this.props.character_set(c);
        }        
    }
    
    render() {
        const elem = bh.book_get_element(this.props, this.f);
        const already_done = elem.already_done;
        const index = bh.item_get_index(this.props.character.items, 'repas');
        const meal = (index !== -1);
        return (
            <div>

              Vous vous éveillez à l'aube, au son de la pluie qui
              tombe à verse sur les pavés de la rue. Il y a maintenant
              six jours que vous avez quitté Holmgard et il vous faut
              <L to='127' onClick={this.handleClickMeal.bind(this)}
              enabled={!already_done && meal}> prendre un repas</L>,
              sinon vous <EnduranceChange f={this.f}> 3 points
              d'ENDURANCE</EnduranceChange>. Vous rassemblez ensuite
              vos affaires et vous quittez la pièce. Tandis que vous
              descendez l'escalier branlant, vous apercevez
              l'aubergiste qui est en train de nettoyer le carrelage à
              l'aide d'une serpillière. Si vous voulez demander à
              l'aubergiste quel chemin prendre pour gagner Durenor, <L
              to="217" enabled={already_done}>rendez-vous au
              217</L>. Si vous préférez quitter les lieux sans lui
              adresser la parole, <L to="143"
              enabled={already_done}>rendez-vous au 143</L>.

              <p/>
              <HelpCharacter/>
              <p/>
              <SimpleImage width={250} src={'127.png'}/>
              <p/>

            </div>
        );
    }
}

export default P127;
