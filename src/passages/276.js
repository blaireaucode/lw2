/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import SimpleImage from '../SimpleImage.js';
import * as bh from '../book_helpers.js';

class P276 extends Component {

    handleRound(fight) {
        if (fight.round === 2) {
            fight.start_e = this.props.character.endurance - fight.previous.damage_character;
        }
        return fight;
    }

    handleEndFight(fight) {
        const c = bh.endurance_set(this.props.character, fight.start_e);
        this.props.character_set(c);
    }

    render() {
        const f = {
            passage_title: 276,
            encounter: "MARIN PATIBULAIRE",
            combat_skill: 18,
            endurance: 25,
            no_bonus_psy: true,
            dont_use_weapon: true,
            dont_die: true
        };
        const d = bh.has_discipline(this.props.character, "puissance_psychique");

        const win = bh.enabled_if_fight_win(this.props, f);
        var loose = false;
        const ff = bh.book_get_element(this.props, f);
        if (ff.status === 'defeat') loose = true;

        var fight='';
        if (!d && !win && !loose)
            fight = (<Fight fight={f}
                            handleRound={this.handleRound.bind(this)}
                            handleEndFight={this.handleEndFight.bind(this)}
                     />);
        if (win) {
            fight = ("Vous parvenez à écraser le bras du marin sur la table, victoire ! ");
        }

        if (loose) {
            fight = ("Le marin vous écrase le bras sur la table !");
        }

        return (
            <div>

              Un MARIN à la mine patibulaire défie quiconque veut
              l'entendre d'engager avec lui une partie de bras de
              fer. Il a une telle confiance dans sa force qu'il se
              déclare prêt à payer 5 Pièces d'Or à celui qui réussira
              à le vaincre. Lorsque vous vous approchez de sa table,
              une servante vous glisse quelques mots à
              l'oreille. «&nbsp;Méfiez-vous, étranger, dit-elle, cet
              homme est dangereux, il casse le bras de tous ceux qui
              perdent contre lui et il tue ceux qui parviennent à le
              battre.&nbsp;» Et tandis que vous vous asseyez à la
              table, face au marin, les autres clients de la taverne
              prennent les paris sur l'issue de la partie. Si vous
              maîtrisez la Discipline Kaï de la Puissance Psychique,
              <L to="14" enabled={d}> rendez-vous au 14</L>. Dans le
              cas contraire, menez cette partie de bras de fer comme
              s'il s'agissait d'un combat. Le premier dont le total
              d'ENDURANCE sera descendu à zéro aura perdu.

              <p/>
              <SimpleImage width={200} src={'276.png'}/>
              <p/>              
              
              Si vous perdez, vous récupérerez bien entendu tous les
              points d'ENDURANCE que vous aviez au début de la
              partie&nbsp;; <L enabled={loose} to="192">rendez-vous
              alors au 192</L>. Si vous remportez la victoire, <L
              to="305" enabled={win}>rendez-vous au 305</L>.

              <p/>{fight}

            </div>
        );
    }
}

// MARIN PATIBULAIRE HABILETÉ 18 ENDURANCE 25
export default P276;
