/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import EnduranceChange from '../EnduranceChange.js';
import update from 'immutability-helper';
import SimpleImage from '../SimpleImage.js';
import * as bh from '../book_helpers.js';

class P314 extends Component {

    constructor(props) {
        super(props);
        this.f = {
            'name': 'a',
            'passage_title': 314,
            'already_done': false,
            'dmg':-3
        };
    }

    handleClickMeal() {
        const elem = bh.book_get_element(this.props, this.f);
        if (!elem.already_done) {
            const e = update(elem, {already_done: {$set: true}});
            this.props.book_set_element(e);
            var c = bh.endurance_add(this.props.character, 3);
            c = bh.item_drop(c, 'repas');
            this.props.character_set(c);
        }        
    }

    render() {    
        const d = bh.has_discipline(this.props.character, "chasse");
        const elem = bh.book_get_element(this.props, this.f);
        const already_done = elem.already_done;
        const index = bh.item_get_index(this.props.character.items, 'repas');
        const meal = (index !== -1);
        return (
            <div>

              L'aubergiste est un vieil homme maigre, sec et
              borgne. Il vous tend une clé et vous désigne du doigt un
              escalier qui mène à une galerie. «&nbsp;Chambre 2, c'est la
              porte rouge&nbsp;», dit-il. Les autres voyageurs paient chacun
              leur Couronne, prennent la clé de leur chambre puis
              traversent la salle bondée de la taverne en direction de
              l'escalier. «&nbsp;Il nous faut établir un programme
              pour demain, dit alors Dorier. Je suggère que nous nous
              retrouvions au bar dans une heure pour décider de ce
              qu'il convient de faire.&nbsp;» Tous les autres approuvent
              d'un signe de tête.  Lorsque vous refermez la porte de
              votre chambre, les paroles du capitaine Kelman vous
              reviennent soudain en mémoire : «&nbsp;Tout cela sent la
              trahison, avait-il dit d'un air sombre, il est clair que
              l'ennemi a déjà dressé des plans pour faire échouer
              votre mission.&nbsp;»

              <p/>
              <SimpleImage width={200} src={'314.png'}/>
              <p/>

              Il s'est écoulé presque une heure lorsque des coups
              frappés à la porte viennent interrompre le fil de vos
              pensées. C'est l'aubergiste qui vous apporte un repas
              chaud. «&nbsp;Avec les compliments d'un de vos
              amis&nbsp;», dit-il en déposant un plateau devant
              vous. Puis il quitte la chambre avant que vous ayez pu
              lui demander le nom de ce mystérieux ami. Le plat qu'il
              vous a apporté est fort appétissant et d'ailleurs vous
              n'avez pas mangé de la journée. Il est donc temps de <L
              to='314' onClick={this.handleClickMeal.bind(this)}
              enabled={!already_done && meal}> prendre un Repas</L>,
              sinon, vous <EnduranceChange f={this.f}> perdrez 3
              points d'ENDURANCE</EnduranceChange>. Si vous souhaitez
              manger ce que l'aubergiste vous a apporté, <L
              enabled={already_done} to="36">rendez-vous au 36</L>. Si
              vous ne voulez pas toucher à cette nourriture, <L
              enabled={already_done}  to="178">rendez-vous au 178</L>. Enfin, si vous
              maîtrisez la Discipline Kaï de la Chasse, <L enabled={already_done && d}
              to="290">rendez-vous au 290</L>.

            </div>
        );
    }
}

export default P314;
