/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Fight from '../Fight.js';
import L from '../L.js';
import update from 'immutability-helper';
import * as bh from '../book_helpers.js';

class P7 extends Component {

    constructor(props) {
        super(props);
        const f = {
            'name': 'f1',
            'passage_title': 7,
            'encounter': "Les frères DORIER/GANON",
            'combat_skill':28,
            'endurance':30,
            'combat_skill_bonus':2,
            'help': ['Bonus initial de +2'],
            'multiple':true,
            'no_bonus_psy': true
        };
        this.f = f;
    }

    handleRound(fight) {
        if (fight.round >= 2) {
            const f = update(fight, {combat_skill_bonus: {$set:0}});
            const f2 = update(f, {help: {$set:['Plus de bonus initial.']}});
            return f2;
        }
        if (fight.round > 2) {
            const f2 = update(fight, {help: {$set:[]}});
            return f2;
        }
        return fight;
    }

    render() {
        const r = bh.enabled_if_fight_win(this.props, this.f);
        return (
            <div>

              DORIER s'écarte d'un bond de la table et tire son
              épée. Un instant plus tard, son frère GANON vient à sa
              rescousse. Il vous faut les combattre tous deux comme
              s'il s'agissait d'un seul adversaire.  Votre attaque
              soudaine vous donne un avantage en raison de l'effet de
              surprise. Vous ajouterez de ce fait 2 points à votre
              total d'HABILETÉ, mais seulement lors du premier
              assaut. Au cours des assauts suivants, votre total
              d'HABILETÉ reviendra à son niveau
              antérieur. L'entraînement que les deux hommes ont suivi
              pour devenir Chevaliers leur a donné une force mentale
              qui les met à l'abri de la Puissance Psychique. Si vous
              sortez vainqueur de ce combat, <L enabled={r}
              to="33">rendez-vous au 33</L>.

              <p/>
              <Fight fight={this.f} handleRound={this.handleRound.bind(this)}/>

            </div>
        );
    }
}

// DORIER GANON HABILETÉ: 28 ENDURANCE: 30
export default P7;
