/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';
import HelpGold from '../HelpGold.js';

class P289 extends Component {

    pay() {
        const d = bh.get_done(this, 289);
        if (!d) {
            var c = bh.gold_add(this.props.character, -40);
            c = bh.item_drop(c, "sceau_hammardal");
            this.props.character_set(c);
        }
        bh.update_done(this, 289);
    }
    
    render() {
        const g = bh.gold_get(this.props.character);
        const l = this.props.character.items.some(e => e.id === 'sceau_hammardal');
        return (
            <div>

              Vous êtes accueilli par une vieille femme vêtue de blanc
              des pieds à la tête. Elle vous sourit et vous offre une
              tasse de délicieux Jala. Mais les mésaventures que vous
              avez vécues à Ragadorn vous ont rendu méfiant et vous
              refusez poliment de boire le liquide sombre contenu dans
              la tasse qu'elle vous tend. Vous avez fait un geste de
              la main pour décliner son offre et elle a vu alors le
              Sceau d'Hammardal passé à votre doigt. «&nbsp;Quelle
              bague magnifique ! Est-elle à vendre ?&nbsp;»
              demande-t-elle le regard brillant de convoitise. Vous
              lui répliquez d'un ton ferme qu'il n'en est rien mais
              elle ne se contente pas de cette réponse. Elle vous
              propose, en échange de l'anneau, l'une des centaines de
              potions qui remplissent les vitrines alignées derrière
              le comptoir. Vous haussez les épaules sans même prendre
              la peine de répondre et vous vous tournez vers la porte
              avec la ferme intention de quitter aussitôt la
              boutique. A ce moment, elle vous offre 40 Pièces d'Or
              pour prix de l'anneau. Allez-vous cette fois, accepter
              le marché ? Rendez-vous <L enabled={g>=40 && l}
              onClick={this.pay.bind(this)} to='165'>dans ce
              cas au 165</L>.  Si cette proposition ne vous fait pas
              changer d'avis, sortez de la boutique et <L
              to="186">rendez-vous au 186</L>.

              <p/>
              <HelpGold/>

            </div>
        );
    }
}

export default P289;
