/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import * as bh from '../book_helpers.js';

class P22 extends Component {

    constructor(props) {
        super(props);
        this.r = {
            passage_title: '22',
            name: 'random_table',
            dice_value: -1
        };
    }

    render() {
        const randtable = bh.book_get_element(this.props, this.r);
        const r = randtable.dice_value;
        return (
            <div>

              Au matin, vous êtes réveillé par un cri de l'homme de
              quart : «&nbsp;Naufrage par tribord avant !&nbsp;» Vous
              vous habillez en hâte et vous montez sur le pont pour
              rejoindre le capitaine qui se tient debout devant le
              bastingage de la proue. Utilisez la <RandomTable
              p={this.r}/> pour obtenir un chiffre. Si vous tirez
              0,1,2,3 ou 4, <L to="119" enabled={r>=0 &&
              r<=4}>rendez-vous au 119</L>. Si vous tirez 5, 6, 7, 8
              ou 9, <L to="341" enabled={r>=5}>rendez-vous au 341</L>.

              <CurrentDice r={r}/>
            </div>
        );
    }
}

export default P22;
