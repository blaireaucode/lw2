/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P227 extends Component {
    render() {
        return (
            <div>

              Quatre gardes de la ville, armés de pied en cap,
              marchent au milieu de la rue. Vous ne voulez pas courir
              le risque d'être interpellé et vous vous réfugiez dans
              une ruelle à votre gauche. Mais les soldats
              s'immobilisent juste à l'entrée du passage et il
              suffirait que l'un d'eux tourne la tête pour que vous
              soyez immédiatement repéré. Derrière vous, une petite
              fenêtre ouverte vous permet de distinguer l'intérieur
              d'une taverne bondée. Sans la moindre hésitation, vous
              enjambez aussitôt le rebord de la fenêtre et vous entrez
              à l'intérieur. <L to="4">Rendez-vous au 4</L>.

            </div>
        );
    }
}

export default P227;
