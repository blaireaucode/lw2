/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P142 extends Component {
    
    constructor(props) {
        super(props);
        const f = {
            'name': 'obj1',
            'passage_title': 142,
            'list': [ {id:'laissez_passer'} ],
        };
        this.items = f;
    }

    render() {
        return (
            <div>

              L'homme prend votre argent et vous tend un
              laissez-passer valable sept jours. Vous le remerciez
              puis vous quittez les lieux. Au-dehors, vous prenez à
              gauche et vous vous approchez des gardes qui se tiennent
              en faction au bout de la rue. <L to="246">Rendez-vous au
              246</L>.

              <p/>
              <ItemsPicker items={this.items}/>

            </div>
        );
    }
}

export default P142;
