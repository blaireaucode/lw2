/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';
import HelpGold from '../HelpGold.js';

class P266 extends Component {
    render() {
        const f = {
            'name': 'obj1',
            'passage_title': 266,
            'list': [
                {id:'epee', cost:4},
                {id:'poignard', cost:2},
                {id:'glaive', cost:7},
                {id:'sabre', cost:3},
                {id:'marteau_guerre', cost:6},
                {id:'lance', cost:5},
                {id:'masse_d_armes', cost:4},
                {id:'hache', cost:3},
            ],
        };
        
        return (
            <div>

              A votre entrée, une cloche retentit et un petit homme
              vêtu d'une veste de cuir matelassée vous souhaite la
              bienvenue. Il est occupé à frotter une armure rouillée à
              l'aide d'un tampon de paille de fer. Un petit tableau de
              bois posé sur le comptoir indique le prix de chacune des
              armes exposées : 

              <p/>
              <ItemsPicker items={f}/>
              <p/>
              <HelpGold/>
              
              Si vous possédez l'argent nécessaire, vous pouvez
              acheter l'une ou l'autre de ces armes; et si vous
              souhaitez vendre une arme dont vous voulez vous séparer,
              l'armurier vous l'achètera au prix indiqué sur son
              tableau, moins 1 Couronne. Si vous désirez lui vendre
              une Masse d'Armes par exemple, il vous en donnera 4-1 =
              3 Couronnes. Apportez à votre Feuille d'Aventure toutes
              les modifications nécessaires en fonction de vos
              transactions, puis quittez la boutique après avoir
              souhaité une bonne nuit au petit homme.

              <p/>

              Au bout de la rue du Col Vert se trouvent à votre droite
              une grande écurie et un relais de diligence. Il fait
              noir à présent et il vous faut un abri pour la
              nuit. Vous apercevez alors une échelle, à l'extérieur du
              bâtiment ; vous y grimpez et vous arrivez dans un
              grenier à foin où vous pourrez vous installer
              confortablement et dormir jusqu'au lendemain. <L
              to="32">Rendez-vous au 32</L>.

            </div>
        );
    }
}

export default P266;
