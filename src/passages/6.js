/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P6 extends Component {

    render() {
        return (
            <div>


              Le garçon a remarqué que vous le suiviez et dès qu'il est sorti, il
              tourne le coin du bâtiment et se met à courir en direction du sud.
              Vous vous lancez à sa poursuite, mais il a tôt fait de disparaître
              dans le dédale des allées qui longent les entrepôts du port. Vous
              vous dirigez vers l'est en empruntant la rue du Col Vert et vous
              passez devant une autre entrée du magasin. Un peu plus loin, vous
              remarquez une enseigne au-dessus de la porte d'une petite boutique.
              On peut y lire l'inscription suivante :

              <p/> <center>MEKI MAJENOR, MAÎTRE ARMURIER</center> <p/>
              <SimpleImage width={300} src={'6.png'}/>

              <p/>
              Si vous souhaitez entrer dans cette boutique, <L
              to="266">rendez-vous au 266</L>. Si vous préférez poursuivre votre
              chemin en direction de l'est, <L to="310">rendez-vous au 310</L>.

            </div>
        );
    }
}

export default P6;
