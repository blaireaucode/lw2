/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P297 extends Component {
    render() {
        return (
            <div>

              A mi-chemin de la rue, vous apercevez sur la gauche une
              grande écurie et un relais de diligence. Il fait
              complètement nuit à présent et vous décidez d'y entrer
              par une échelle extérieure. Vous allez pouvoir passer la
              nuit en toute sécurité, caché dans le grenier à foin du
              relais. <L to="32">Rendez-vous au 32</L>.

            </div>
        );
    }
}

export default P297;
