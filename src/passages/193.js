/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P193 extends Component {
    render() {
        return (
            <div>

              Le voyage de retour au royaume du Sommerlund se déroule
              sous de mauvais auspices. De gros nuages noirs
              s'amoncellent à l'horizon et un vent violent agite la
              mer sans relâche. A la nuit tombée, de grands éclairs
              aveuglants déchirent l'obscurité, suivis par des
              roulements de tonnerre si fracassants que le navire
              amiral en est tout ébranlé depuis l'extrémité de sa
              quille jusqu'à la pointe de ses mâts. La plupart des
              soldats qui voyagent à bord de la flotte sont des
              montagnards qui n'ont aucune expérience de la mer et au
              bout du troisième jour, une bonne moitié d'entre eux
              sont cloués au lit, incapables de se lever. Lord Axim
              semble au bord du désespoir. «&nbsp;Puisse cette tempête
              se calmer, dit-il, car même si la flotte arrivait
              intacte au bout du voyage, nos hommes seraient trop
              faibles pour pouvoir combattre, après avoir subi une
              telle épreuve.&nbsp;»

              <p/>

              Et le lendemain, comme si sa prière avait été entendue,
              l'aube se lève dans un ciel apaisé qui annonce la fin de
              la tourmente.  Mais les eaux calmes à présent où navigue
              la flotte cachent un péril plus redoutable encore que la
              tempête des jours passés. <L to='100'>Rendez-vous au
              100</L>.

            </div>
        );
    }
}

export default P193;
