/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P156 extends Component {
    render() {
        return (
            <div>

            Le cocher se met en colère. «&nbsp;C'est une longue marche
            qui t'attend, étranger&nbsp;», lance-t-il en vous claquant la
            portière au nez. Vous n'avez pas les moyens de louer une
            chambre pour la nuit et vous décidez donc d'aller coucher
            avec les chevaux dans l'écurie. <L to="213">Rendez-vous au
            213</L>.

            </div>
        );
    }
}

export default P156;
