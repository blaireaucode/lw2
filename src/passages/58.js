/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';


import HelpGold from '../HelpGold.js';
import update from 'immutability-helper';
const bh = require('../book_helpers.js');

class P58 extends Component {

    constructor(props) {
        super(props);
        this.r = {
            passage_title: '58',
            name: 'gold',
            already_done: false
        };
    }

    handleClick(r) {
        const g = bh.book_get_element(this.props, this.r);
        if (g.already_done) return;
        const c = bh.gold_add(this.props.character, -10);
        this.props.character_set(c);
        const e = update(g, {already_done: {$set:true}});
        this.props.book_set_element(e);
    }
    
    render() {
        const g = bh.book_get_element(this.props, this.r);
        const r = g.already_done;
        return (
            <div>

             «&nbsp;Pas de chance, Loup Solitaire, votre stratégie ne
             manquait pas d'audace, mais je crois bien que j'ai gagné
             à présent », déclare bientôt votre adversaire. Le
             capitaine avance alors une de ses pièces sculptées sur le
             damier et vous vous rendez compte que vous avez
             perdu. Vous le félicitez pour sa maîtrise du jeu de Samor
             et vous lui <L enabled={!r} to="58"
             onClick={this.handleClick.bind(this)}>donnez 10 Pièces
             d'Or</L>. «&nbsp;Peut-être voudrez-vous engager une autre
             partie demain soir ?  demande-t-il, je suis homme à vous
             offrir une deuxième chance. » « Peut-être »,
             répondez-vous sans vous avancer. Vous souhaitez bonne
             nuit au capitaine qui vous adresse un sourire et vous
             rejoignez votre cabine. <L to="197"
             enabled={r}>Rendez-vous au 197</L>.

              <p/><HelpGold/>

            </div>
        );
    }
}

export default P58;
