/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';


import * as bh from '../book_helpers.js';
import HelpGold from '../HelpGold.js';

class P117 extends Component {

    handleClick3gold() {
        const c = bh.gold_add(this.props.character, -3);
        this.props.character_set(c);
    }
    
    handleClick1gold() {
        const c = bh.gold_add(this.props.character, -1);
        this.props.character_set(c);        
    }
    
    render() {
        const g = bh.gold_get(this.props.character);
        
        
        return (
            <div>

              C'est une de ces grosses diligences, semblables à celles
              qui transportent les voyageurs de grand chemin au
              royaume du Sommerlund. Le cocher tire les rênes et
              arrête ses chevaux en vous observant de sous le large
              bord de son chapeau. Vous lui demandez où il
              va. «&nbsp;Nous allons à Ragadorn, répond-il ; nous
              arriverons là-bas vers midi. Il vous en coûtera 3
              Couronnes pour un billet mais vous pouvez voyager sur le
              toit pour une Couronne seulement.&nbsp;» Si vous
              souhaitez voyager à l'intérieur de la diligence, payez 3
              Couronnes au cocher et <L enabled={g>=3} to='37'
              onClick={this.handleClick3gold.bind(this)}>rendez-vous
              au 37</L>. Si vous préférez voyager sur le toit,
              donnez-lui 1 Couronne et <L enabled={g>=1}
              onClick={this.handleClick1gold.bind(this)}
              to="148">rendez-vous au 148</L>. Enfin, si vous n'avez
              pas de quoi payer le voyage, vous n'aurez plus qu'à
              laisser repartir la diligence et à continuer à pied ; <L
              to='292'>rendez-vous dans ce cas au 292</L>.

              <p/>
              <HelpGold/>
              
            </div>
        );
    }
}

export default P117;
