/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P277 extends Component {
    render() {
        return (
            <div>

              Tandis que Rhygar et ses hommes se rapprochent des
              cavaliers, l'un d'eux tire de sous sa cape un bâton
              noir. Une flamme bleue étincelante jaillit alors de son
              extrémité et vient frapper le cheval du Lieutenant
              Général qui est aussitôt projeté à bas de sa monture et
              tombe cul pardessus tête dans l'épaisseur des
              broussailles. Les hommes de Rhygar se lancent à
              l'attaque, leurs épées brandies, et pourfendent les
              cavaliers aux longues capes. Mais les lames d'acier
              n'ont aucun effet sur l'ennemi, car ce ne sont pas des
              hommes que vous avez devant vous, ce sont des Monstres
              d'Enfer, les féroces serviteurs des Maîtres des
              Ténèbres. Ces créatures redoutables ont la faculté
              d'adopter l'apparence des hommes, mais restent
              invulnérables aux armes ordinaires. L'être au bâton noir
              éclate alors d'un rire terrifiant et une douleur
              fulgurante vous déchire la tête. Il vient d'utiliser
              contre vous sa formidable Puissance Psychique ; la
              situation est inquiétante : vous êtes en effet dominé
              par un ennemi supérieur en nombre et il va falloir agir
              vite si vous voulez survivre à cette
              attaque. Souhaitez-vous abandonner votre cheval et
              plonger dans les broussailles pour vous y cacher ? <L
              to="311"> Rendez-vous au 311</L>. Si vous préférez
              prêter main forte aux hommes de Rhygar, <L
              to='59'>rendez-vous au 59</L>.

            </div>
        );
    }
}

export default P277;
