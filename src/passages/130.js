/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P130 extends Component {
    render() {
        return (
            <div>

              Le moine qui voyageait en votre compagnie dans la
              diligence s'est approché de vous. «&nbsp;Vous avez
              besoin de vous reposer, comme nous tous, dit-il, je
              comprends votre embarras, mon fils; aussi permettez-moi
              de mettre en pratique ce que je m'efforce de prêcher. »
              Il vous conduit alors au bar puis dépose une Pièce d'Or
              dans la main de l'aubergiste. « Veuillez donner une
              chambre à mon ami&nbsp;», dit-il avec un sourire.  <L
              to="314">rendez-vous au 314</L>.

            </div>
        );
    }
}

export default P130;
