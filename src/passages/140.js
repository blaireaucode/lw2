/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P140 extends Component {
    render() {
        return (
            <div>

            Les deux gardes contemplent le Sceau avec une stupeur
            mêlée de respect. Tous les habitants de Durenor
            connaissent bien la légende du Sceau d'Hammardal et l'on
            dit que, de tous les trésors perdus du royaume, le Sceau
            d'Hammardal est celui dont personne ne souhaite le
            retour. L'inquiétude qu'exprime le visage des deux gardes
            montre qu'ils savent parfaitement ce que l'anneau
            signifie. L'un des soldats vous accompagne sur l'autre
            rive du chenal de Ryner et le long d'une route forestière
            qui aboutit à un croisement. Un panneau indicateur orienté
            vers l'est précise : PORT BAX 5 km. «&nbsp;Il me faut vous
            quitter à présent et retourner au chenal, dit le
            garde. J'ai bien peur que la guerre ne vienne bientôt
            assombrir ce royaume et mon devoir est de surveiller la
            frontière. Que Dieu vous accorde son aide, homme du
            Sommerlund.&nbsp;» Vous le regardez s'éloigner le long du
            chemin forestier puis vous vous remettez en route en
            direction de l'est. Vous devriez avoir atteint Port Bax
            dans une heure tout au plus. <L to="265">Rendez-vous au
            265</L>.

            </div>
        );
    }
}

export default P140;
