/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P180 extends Component {
    render() {
        return (
            <div>

              Vous arrivez à la conclusion que les marins du bateau
              sont aveugles ou qu'ils n'ont pas la moindre intention
              de vous porter secours. En effet, le bateau de pêche
              poursuit sa course et disparaît bientôt à l'horizon sans
              s'occuper de vous. En désespoir de cause, vous arrachez
              une planche du panneau d'écoutille et vous vous en
              servez comme d'une rame pour pagayer en direction de la
              côte. <L to="337">Rendez-vous au 337</L>.

            </div>
        );
    }
}

export default P180;
