/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import update from 'immutability-helper';
import * as bh from '../book_helpers.js';

class P270 extends Component {

    handleRound(fight) {
        if (fight.round >= 2) {
            const f = update(fight, {combat_skill_bonus: {$set:0}});
            const f2 = update(f, {help: {$set:['Plus de bonus initial.']}});
            return f2;
        }
        if (fight.round > 1) {
            const f2 = update(fight, {help: {$set:[]}});
            return f2;
        }
        return fight;
    }

    render() {
        const f = {
            passage_title: 270,
            encounter: "GANON & DORIER",
            combat_skill: 28,
            endurance: 30,
            no_bonus_psy: true,
            combat_skill_bonus:2,
            help: ['Bonus initial de +2']
        };
        const r = bh.enabled_if_fight_win(this.props, f);
        return (
            <div>

              GANON bondit de sa chaise et tire son épée. Un instant
              plus tard, son frère DORIER est à son côté. Il vous faut
              les combattre tous deux en les considérant comme un seul
              et même ennemi.  La soudaineté de votre attaque vous
              permet d'ajouter 2 points à votre total d'HABILETÉ, mais
              lors du premier assaut seulement. Sachez également qu'en
              raison de la force exceptionnelle de leur volonté, ils
              sont insensibles à la Discipline Kaï de la Puissance
              Psychique. Si vous sortez vainqueur de ce combat, <L enabled={r}
              to="33">rendez-vous au 33</L>.

              <p/>
              <Fight fight={f} handleRound={this.handleRound.bind(this)}/>

            </div>
        );
    }
}

// GANON DORIER HABILETÉ: 28 ENDURANCE: 30
export default P270;
