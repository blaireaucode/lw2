/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import * as bh from '../book_helpers.js';

class P316 extends Component {
    render() {
        const r = bh.get_element(this, 316, {dice_value:-1});
        const d = r.dice_value;
        return (
            <div>

              Le lendemain matin, vous êtes réveillé par les cris de
              la vigie postée dans le nid-de-pie. «&nbsp;Navire en vue
              par tribord avant !&nbsp;» annonce-t-il.  Vous montez
              aussitôt sur le pont en affrontant vaillamment la
              fraîcheur de la brise marine. A l'horizon, on aperçoit
              les rivages boisés qui s'étendent à l'est du Sommerlund
              et à mi-chemin, un navire marchand qui semble
              sérieusement endommagé. Un seul de ses mâts reste encore
              intact et il est de toute évidence en train de
              sombrer. Le navire bat pavillon de Durenor, mais le
              drapeau a été hissé à l'envers en signe de
              détresse. Utilisez la <RandomTable p={r}> pour obtenir
              un chiffre</RandomTable>. Si vous tirez un chiffre entre
              0 et 4, <L enabled={d>=0 && d<=4} to="107">rendez-vous
              au 107</L>. Entre 5 et 9, <L enabled={d>=5 &&
              d<=9}to="94">rendez-vous au 94</L>.

              <CurrentDice r={d}/>
            </div>
        );
    }
}

export default P316;
