/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P265 extends Component {
    render() {
        const disc = this.props.character.kai_disciplines;
        const d = disc.some(e => e.id === "orientation");
        return (
            <div>

              Le soleil se couche sur le dixième jour de votre quête
              lorsque vous apercevez pour la première fois la
              magnifique cité de Port Bax. Les tours de la ville
              luisent dans la pâle clarté d'un croissant de lune comme
              autant de diamants nichés au creux du rivage. Au nord se
              trouve le port lui-même où sont rassemblés les vaisseaux
              de la puissante flotte de guerre du royaume de
              Durenor. A l'est, au-delà des murs de la cité couverts
              de mousse, s'étend la forêt. Enfin, au sommet d'une
              colline se dresse un château de fière apparence, une
              haute citadelle qui donne à la ville le plus glorieux
              fleuron de sa couronne. Vous pénétrez dans Port Bax en
              franchissant l'une des portes aménagées dans les murs de
              la cité.  Il n'y a aucun garde en faction et vous passez
              sans difficulté. Dans la nuit tombante, les rues qui
              mènent au port s'obscurcissent peu à peu.  Vous
              empruntez une avenue bordée d'arbres et vous remarquez
              bientôt un bâtiment au toit en forme de dôme. Un large
              escalier de pierre donne accès à la porte de
              l'édifice. Vous vous approchez et vous lisez cette
              inscription gravée sur une plaque de cuivre :

              <p/>HÔTEL DE               VILLE
              
              <p/> En dépit de l'heure tardive, la porte principale
              est toujours ouverte. Si vous souhaitez entrer dans
              l'hôtel de ville, <L to="84">rendez-vous au 84</L>. Si
              vous préférez poursuivre votre chemin en direction du
              port, <L to="191">rendez-vous au 191</L>. Enfin, si vous
              maîtrisez la Discipline de l'Orientation, <L to="191"
              enabled={d}>rendez-vous au 252</L>.

            </div>
        );
    }
}

export default P265;
