/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import * as bh from '../book_helpers.js';

class P298 extends Component {

        constructor(props) {
        super(props);
        this.ff = [];
        this.ff.push( {passage_title: 298,
                       name:'f1',
                       encounter: "Chef des VOLEURS",
                       combat_skill: 15,
                       endurance: 23 });
        this.ff.push( {passage_title: 298,
                       name:'f2',
                       encounter: "1er VOLEUR",
                       combat_skill: 13,
                       endurance: 21 });
        this.ff.push( {passage_title: 298,
                       name:'f3',
                       encounter: "2e VOLEUR",
                       combat_skill: 13,
                       endurance: 20});
    }

    render() {
        const f = bh.book_get_current_fight(this.props, this.ff);
        const r = bh.enabled_if_fight_win(this.props, f);
        return (
            <div>

              Vous entendez derrière vous leurs pas se précipiter et
              vous faites brusquement volte-face, au moment même où
              ils dégainent chacun un poignard pour vous attaquer. Si
              vous n'avez pas d'armes, vous devrez déduire 4 points de
              votre total d'ENDURANCE et les combattre à mains
              nues. Vous les affronterez un par un.  Vous avez le
              droit de prendre la fuite à tout moment <L to='121'>en
              vous rendant au 121</L>. Si vous parvenez à tuer les
              trois voleurs, <L enabled={r} to="301">rendez-vous au
              301</L>.

              <p/> <Fight fight={f}/>

            </div>
        );
    }
}

// HABILETÉ ENDURANCE
// Chef des VOLEURS 15 23
// 1er VOLEUR 13 21
// 2e VOLEUR 13 20

export default P298;
