/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';


import * as bh from '../book_helpers.js';
import L from '../L.js';
import update from 'immutability-helper';

class P194 extends Component {
    
    constructor(props) {
        super(props);
        this.f = {
            'name': 'a',
            'passage_title': 194,
            'already_done': false,
        };
    }
      
    handleClickRemoveItem() {
        const c = update(this.props.character, { items: {$set: []}});
        this.props.character_set(c);
        const elem = bh.book_get_element(this.props, this.f);
        if (!elem.already_done) {
            const e = update(elem, {already_done: {$set: true}});
            this.props.book_set_element(e);
        }        
    }
       
    render() {
        const elem = bh.book_get_element(this.props, this.f);
        const r = elem.already_done;
        return (
            <div>

              Lorsque vous vous réveillez, vous avez la désagréable
              surprise de vous retrouver étendu sous une jetée en
              bois, dans une puanteur insupportable qui monte des eaux
              environnantes. Vous vous relevez avec une douleur
              lancinante dans la tête, comme si on vous avait
              assommé. C'est d'ailleurs très exactement ce qui vous
              est arrivé ; mais, plus grave encore, les pêcheurs <L
              to='194' onClick={this.handleClickRemoveItem.bind(this)}
              enabled={!r}>vous ont tout volé</L> : Or, Sac à Dos,
              Armes, ainsi que tous vos Objets Spéciaux, y compris,
              hélas, le Sceau d'Hammardal. Avec un gémissement
              désespéré, vous vous arrachez à la puanteur des eaux
              croupies et vous vous hissez sur la jetée. En levant les
              yeux, vous apercevez alors un écriteau délavé qui porte
              ces mots :

              <p/>BIENVENUE À RAGADORN

              <p/>

              Pour votre malheur, toutes les rumeurs qui circulent au
              sujet de cette ville maudite se sont révélées exactes ;
              il fait presque noir, à présent, et la pluie s'est mise
              à tomber. Dans l'immédiat, il vous faut à tout prix
              retrouver le Sceau d'Hammardal si vous voulez convaincre
              le Roi de Durenor de vous confier le Glaive de
              Sommer. Vous jetez un coup d'œil autour de vous et vous
              apercevez une place sur laquelle est installé un
              marché. Au centre de cette place, un poteau indicateur
              en pierre signale diverses rues qui mènent dans toutes
              les directions. Si vous voulez aller vers l'est le long
              de la rue de la Bernicle, <L enabled={r}
              to='215'>rendez-vous au 215</L>. Si vous voulez aller au
              sud, le long du Dock de la rive Ouest, <L enabled={r}
              to="303">rendez-vous au 303</L>. Si vous voulez aller au
              nord, en empruntant la rue du Butin, <L enabled={r}
              to="129">rendez-vous au 129</L>. Si enfin vous préférez
              retourner vers la jetée, en direction de l'ouest, et
              chercher le bateau de pêche, <L enabled={r}
              to="86">rendez-vous au 86</L>.

            </div>
        );
    }
}

export default P194;
