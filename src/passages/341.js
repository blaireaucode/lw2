/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P341 extends Component {
    render() {
        return (
            <div>

              Il ne reste plus du malheureux navire qu'une coque
              fracassée et les lambeaux de voiles. Vous insistez
              auprès du capitaine pour qu'il fasse rechercher
              d'éventuels survivants, mais il ignore votre demande et
              ordonne à ses hommes d'équipage de poursuivre leur
              tâche. Alors, tandis que vous vous éloignez de l'épave,
              un sentiment d'appréhension vous envahit peu à peu : et
              si un sort semblable vous attendait, vous aussi ? La
              gorge sèche, vous descendez sur le pont inférieur pour
              rejoindre votre cabine en prenant bien soin d'en fermer
              la porte à clé.  <L to="240">Rendez-vous au 240</L>.

            </div>
        );
    }
}

export default P341;
