/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P318 extends Component {
    render() {
        return (
            <div>

              Vous vous trouvez dans un vaste hall entièrement
              désert. Face à vous, vous apercevez deux portes sur
              lesquelles sont fixées des plaques de cuivre. Si vous
              voulez franchir la porte dont la plaque indique
              «&nbsp;laissez-passer blancs&nbsp;», <L
              to="75">rendez-vous au 75</L>. Si vous préférez pousser
              la porte dont la plaque indique «&nbsp;laissez-passer
              rouges&nbsp;», <L to="62">rendez-vous au 62</L>.  Enfin,
              si vous préférez ressortir et vous approcher des gardes
              postés au bout de la rue, <L to="246">rendez-vous au
              246</L>.

            </div>
        );
    }
}

export default P318;
