/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P228 extends Component {
    render() {
        const f = { passage_title:228,
                    list: [ {id:'repas', nb:2} ] };
        return (
            <div>

              Ce sont des Larnumiers, des arbres dont les fruits
              juteux et sucrés sont très nourrissants. Après avoir
              avalé quelques-uns de ces fruits, vous vous sentez tout
              revigoré et vous en cueillez l'équivalent de 2 Repas que
              vous rangez dans votre Sac à Dos pour les manger plus
              tard. Au-delà des arbres, une large route longe la côte
              en s'étendant des deux côtés de l'horizon, à droite et à
              gauche. Il n'y a pas de poteau indicateur et vous devez
              choisir quelle direction prendre. Si vous souhaitez
              aller à gauche, <L to="27">rendez-vous au 27</L>. Si
              vous préférez aller à droite, <L to='114'>rendez-vous au
              114</L>.

              <p/>
              <ItemsPicker items={f}/>

            </div>
        );
    }
}

export default P228;
