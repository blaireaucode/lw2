/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Dead from '../Dead.js';

class P190 extends Component {
    render() {
        return (
            <div>

              Vous vous servez d'une règle de fer comme d'un levier
              pour forcer la serrure et vous ressentez soudain une
              douleur cuisante dans la poitrine.  Le coffret
              comportait un piège : une petite aiguille enduite de
              poison qui vient de se planter dans votre chair tandis
              que vous tentiez de faire sauter la serrure. Cette
              minuscule fléchette vous est fatale et vous mourez sur
              le coup. Votre mission s'achève ici, en même temps que
              votre vie.

              <p/>
              <Dead/>

            </div>
        );
    }
}

export default P190;
