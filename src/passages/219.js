/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';
import HelpCharacter from '../HelpCharacter.js';
import EnduranceChange from '../EnduranceChange.js';

class P219 extends Component {
    render() {
        const e = bh.get_element(this, 219, { already_done:false, dmg:-3 });
        return (
            <div>

              Le venin commence à faire son effet. Votre bras mordu
              s'engourdit et une sueur froide perle à votre
              front. Vous ôtez aussitôt de votre cou le pendentif que
              Banedon vous a donné dans les Ruines de Raumas et à
              l'aide d'une des pointes de l'étoile de cristal, vous
              incisez la peau de votre bras à l'endroit de la
              morsure. Vous posez ensuite vos lèvres sur la plaie et
              vous aspirez le venin. Le porte-bonheur se révèle
              efficace et la chance est avec vous, car vous survivez à
              la morsure, bien que vous perdiez <EnduranceChange
              f={e}> 3 points d'ENDURANCE</EnduranceChange>. Vous
              décidez ensuite de grimper dans l'arbre et de passer le
              reste de la nuit à l'abri de son feuillage, à bonne
              distance du sol. <L enabled={e.already_done}
              to="312">Rendez-vous au 312</L>.

              <p/>
              <HelpCharacter/>
            </div>
        );
    }
}

export default P219;
