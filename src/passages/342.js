/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';
import SimpleImage from '../SimpleImage.js';

class P342 extends Component {
    pay_bier() {
        const d = bh.get_done(this, 342, 'a');
        if (!d) {
            const c = bh.gold_add(this.props.character, -1);
            this.props.character_set(c);
        }
        bh.update_done(this, 342, 'a');
    }
    
    pay_room() {
        const d = bh.get_done(this, 342,'b');
        if (!d) {
            const c = bh.gold_add(this.props.character, -2);
            this.props.character_set(c);
        }
        bh.update_done(this, 342, 'b');        
    }
    
    render() {
        const g = bh.gold_get(this.props.character);
        const d1 = bh.get_done(this, 342, 'a');
        const d2 = bh.get_done(this, 342, 'b');
        const e1 = d1 || g>0;
        const e2 = d2 || g>1;
        return (
            <div>

              C'est une véritable montagne humaine, le crâne
              complètement chauve et les oreilles ornées de gros
              anneaux d'or. Il vous regarde d'un air soupçonneux avant
              de vous adresser enfin la parole : «&nbsp;Une bière
              coûte 1 Pièce d'Or, une chambre 2 Pièces. Qu'est-ce que
              vous choisissez ?&nbsp;» Si vous souhaitez prendre une
              bière, payez une Pièce d'Or et <L enabled={e1}
              onClick={this.pay_bier.bind(this)} to='72'>rendez-vous
              au 72</L>. Si vous préférez louer une chambre pour la
              nuit, payez 2 Pièces d'Or à l'aubergiste et <L enabled={e2}
              onClick={this.pay_room.bind(this)} to="56">rendez-vous au
              56</L>. Si vous n'avez besoin ni de l'une ni de l'autre,
              vous pouvez demander plutôt à cet homme de vous parler
              de Ragadorn ; <L to='226'>rendez-vous pour cela au
              226</L>.

              <p/>
              <SimpleImage width={200} src={'342.png'}/>
              
            </div>
        );
    }
}

export default P342;
