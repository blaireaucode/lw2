/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Dead from '../Dead.js';

class P292 extends Component {
    render() {
        return (
            <div>

              Vous marchez sous la pluie depuis trois bonnes heures
              lorsque vous rencontrez soudain sept hommes à cheval qui
              vous barrent le passage.  Ce sont des mercenaires au
              service du Suzerain de Ragadorn dont ils arborent
              l'emblème gravé sur leurs écussons : un vaisseau noir
              surmonté d'une crête rouge. Ils vous ordonnent de leur
              donner tout votre or, sinon, ils vous tueront sur place
              ; et lorsqu'ils s'aperçoivent que vous n'avez plus la
              moindre Couronne dans votre bourse, vous avez beau
              essayer de prendre la fuite, ils ont tôt fait de vous
              rattraper et de vous tailler en pièces. Alors, tandis
              que vous agonisez sur le bord de la route, les contours
              de Ragadorn se dessinent au loin : c'est la dernière
              vision que vous emporterez de ce monde car, un instant
              plus tard, vos yeux se ferment à jamais. Votre mission
              s'achève donc ici, en même temps que votre vie.

              <p/>
              <Dead/>

            </div>
        );
    }
}

export default P292;
