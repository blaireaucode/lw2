/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';
import * as bh from '../book_helpers.js';

class P231 extends Component {
    render() {
        const f = {
            'passage_title': 231,
            'list': [
                {id:'poignard'},
                {id:'sceau_hammardal'},
                {id:'PO', nb:5},
            ],
        };
        const d = bh.has_discipline(this.props.character, "orientation");
        return (
            <div>

              La porte de derrière ouvre sur une petite place au
              centre de laquelle se dresse une haute sépulture. Les
              pêcheurs ont disparu dans les rues sombres, sauf un qui
              est tombé en glissant sur le pavé mouillé et s'est
              assommé dans sa chute. Il est étendu dans le caniveau,
              le visage dans une flaque d'eau. Vous le retournez du
              bout du pied et vous le fouillez.  Dans ses poches, vous
              trouvez 5 Pièces d'Or et 1 Poignard, mais mieux que
              tout, vous découvrez, passé à son doigt, le Sceau
              d'Hammardal; vous poussez un long soupir de soulagement
              et vous inscrivez sur votre Feuille d'Aventure toutes
              ces trouvailles.

              <p/>
              <ItemsPicker items={f}/>
              <p/>


              Si vous souhaitez à présent retourner dans la taverne,
              <L to="177"> rendez-vous au 177</L>. Si vous préférez
              examiner la sépulture, <L to="24"> rendez-vous au
              24</L>. S'il vous semble plus judicieux de suivre la rue
              du Tombeau en direction de l'ouest, <L
              to="253"> rendez-vous au 253</L>. Vous pouvez également
              aller vers l'est en empruntant la rue de la Tour de Guet
              ; <L to='319'> rendez-vous pour cela au 319</L>. Enfin,
              si vous maîtrisez la Discipline Kaï de l'Orientation, <L
              to="182" enabled={d}> rendez-vous au 182</L>.

            </div>
        );
    }
}

export default P231;
