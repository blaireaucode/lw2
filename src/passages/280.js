/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import * as bh from '../book_helpers.js';

class P280 extends Component {

    render() {
        const r = bh.get_element(this, 280, {dice_value:-1});
        const d = r.dice_value;
        return (
            <div>

              Vous dormez profondément jusqu'à l'aube sans être
              dérangé. A votre réveil, vous ramassez vos affaires et
              vous allez rejoindre les autres à bord de la
              diligence. Pendant deux jours, la diligence file sur la
              route qui traverse les étendues plates et désolées du
              Pays Sauvage, en ne faisant halte, de temps à autre, que
              pour permettre au cocher de prendre quelque repos. Vous
              êtes arrivé au matin du 9e jour de votre quête
              lorsqu'un malheureux accident se produit. Utilisez la &nbsp;
              <RandomTable p={r}>pour obtenir un
              chiffre</RandomTable>. Si vous tirez un chiffre entre 0
              et 4, <L enabled={d>=0 && d<=4} to="2">rendez-vous au
              2</L>. Entre 5 et 9, <L enabled={d>=5}
              to="108">rendez-vous au 108</L>.

              <CurrentDice r={d}/>
            </div>
        );
    }
}

export default P280;
