/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P335 extends Component {
    render() {
        return (
            <div>

              Vous remarquez une enseigne accrochée à la façade d'une
              petite boutique : KLASPAR, ASTRONOME et SAGE.

              <p/>
              <SimpleImage width={200} src={'335.png'}/>
              <p/>
              
              Si vous souhaitez entrer dans cette boutique et y
              demander votre chemin pour Durenor, <L
              to="161">rendez-vous au 161</L>. Si vous préférez
              continuer tout droit, <L to="61">rendez-vous au 61</L>.

            </div>
        );
    }
}

export default P335;
