/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P202 extends Component {
    render() {
        return (
            <div>

              Le soldat vous salue et vous laisse franchir la porte
              rouge. Vous arrivez alors sur une place éclairée par les
              lumières du port. A votre grand soulagement, vous
              apercevez un drapeau familier qui flotte au vent frais
              de la nuit : un soleil surmonté d'une couronne; c'est
              l'étendard du Sommerlund, et ces colonnes de marbre qui
              se dressent devant vous marquent l'entrée du
              consulat. Lorsque vous montez les marches de pierre qui
              mènent à la porte du bâtiment, les gardes sommerlundois
              en faction vous reconnaissent aussitôt. Ils
              disparaissent à l'intérieur et reviennent peu après en
              compagnie d'un homme de haute taille, aux cheveux
              grisonnants : c'est un fonctionnaire du
              consulat. L'expression inquiète de son visage se
              métamorphose en un sourire épanoui lorsqu'il aperçoit
              votre cape et votre tunique de Seigneur Kaï. «&nbsp;Dieu
              soit loué, vous êtes vivant, Seigneur Kaï. Les rares
              nouvelles qui sont parvenues jusqu'ici nous ont plongés
              dans l'angoisse.&nbsp;» Vous êtes immédiatement conduit à
              l'intérieur du bâtiment et introduit dans le bureau
              occupé par le représentant du Sommerlund, le Lieutenant
              Général Rhygar. <L to="31">Rendez-vous au 31</L>.

              <p/>
              <SimpleImage width={400} src={'202.png'}/>

            </div>
        );
    }
}

export default P202;
