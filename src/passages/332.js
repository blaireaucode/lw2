/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import * as bh from '../book_helpers.js';

class P332 extends Component {
    render() {
        const f = {
            passage_title: 332,
            encounter: "MONSTRE D'ENFER",
            combat_skill: 21,
            endurance: 30,
            psychic_attack: true,
        };
        const w = bh.enabled_if_fight_win(this.props, f);
        return (
            <div>

              Il vous faut à présent combattre le MONSTRE D'ENFER.  Il
              vous attaque en faisant usage de sa Puissance Psychique
              et si vous ne maîtrisez pas la Discipline Kaï du
              Bouclier Psychique, vous perdrez 2 points d'ENDURANCE
              supplémentaire au cours de chaque assaut. Si vous êtes
              vainqueur, <L enabled={w} to="92">rendez-vous au
              92</L>. Vous pouvez prendre la fuite à tout moment en
              vous réfugiant dans la forêt ; <L to='183'>rendez-vous
              dans ce cas au 183</L>.

              <p/>
              <Fight fight={f}/>
              
            </div>
        );
    }
}

// MONSTRE D'ENFER HABILETÉ: 21 ENDURANCE: 30


export default P332;
