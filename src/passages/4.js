/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P4 extends Component {

    render() {
        return (
            <div>

                La taverne est remplie de brigands et d'ivrognes. Il y a là tous
                les bons à rien qui ont réussi à se faire engager dans les
                équipages des navires marchands amarrés dans le port. La plupart
                d'entre eux sont en train de boire et de chanter tandis que
                d'autres se mesurent au bras de fer. Tous sont si occupés que
                votre entrée passe inaperçue. Dans un coin, vous apercevez les
                pêcheurs qui vous ont volé. Ils sont assis autour d'une table
                encombrée de chopes de bière vides. Si vous voulez atteindre le
                Royaume de Durenor à temps, il vous faut à tout prix récupérer
                le Sceau d'Hammardal, ainsi que vos Pièces d'Or. Si vous
                souhaitez affronter les pêcheurs, <L to="104">rendez-vous au
                104</L>. Si vous jugez préférable de parler à l'aubergiste, <L
                to="342">rendez-vous au 342</L>. Enfin, si vous voulez plutôt
                essayer de gagner quelques Pièces d'Or en engageant une partie
                de bras de fer, <L to="276">rendez-vous au 276</L>.

                <p/>
                <SimpleImage width={200} src={'4.png'}/>

            </div>
        );
    }
}

export default P4;
