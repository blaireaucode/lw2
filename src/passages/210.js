/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import * as bh from '../book_helpers.js';

class P210 extends Component {
    render() {
        const r = bh.get_element(this, 210, {dice_value:-1});
        const d = r.dice_value;
        return (
            <div>

              Vous posez les deux mains sur votre estomac et vous vous
              concentrez de toute la force que vous donne la
              Discipline Kaï pour tenter de vaincre la douleur. Votre
              pouvoir de guérison vous soulage bientôt, mais le poison
              est puissant et vous n'êtes pas au bout de vos peines.
              Utilisez la <RandomTable p={r}>pour obtenir un
              chiffre</RandomTable>. Puisse la clémence des dieux
              guider votre main, car votre vie dépend désormais du
              chiffre que vous aurez tiré ! Si la table vous donne
              entre 0 et 4, <L enabled={d>=0 && d<=4}
              to='275'>rendez-vous au 275</L>. Si elle vous donne
              entre 5 et 9, <L enabled={d>=5 && d<=9}
              to="330">rendez-vous au 330</L>.

              <CurrentDice r={d}/>
            </div>
        );
    }
}

export default P210;
