/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';
import * as bh from '../book_helpers.js';

class P160 extends Component {
    render() {
        const d1 = bh.has_discipline(this.props.character, "guerison");
        const d2 = bh.has_discipline(this.props.character, "puissance_psychique");
        const d3 = bh.has_discipline(this.props.character, "maitrise_arme");
        const d4 = bh.has_discipline(this.props.character, "communication_animale");
        const d5 = bh.has_discipline(this.props.character, "maitrise_psychique_de_la_matiere");
        return (
            <div>
              
              «&nbsp;Pardonnez-moi, my lord, je ne voulais pas vous
              faire peur.&nbsp;»

              <p/>

              L'homme semble inquiet et la main ouverte
              qu'il tend vers vous ne cesse de trembler. Sans vous
              départir de votre prudence, vous acceptez son geste
              amical et quelques instants plus tard vous vous asseyez
              avec lui à l'une des tables de l'auberge dans laquelle
              vous êtes entré par une porte latérale. L'endroit est
              désert, à l'exception d'un couple de souris qui rongent
              un gros morceau de fromage.

              <p/>
              <SimpleImage width={150} src={'160.png'}/>
              <p/>

              «&nbsp;Le capitaine Kelman m'a chargé de vous amener à
              bord du Sceptre Vert mais je dois tout d'abord m'assurer
              que vous êtes bien le Seigneur Kaï qu'on appelle le Loup
              Solitaire, dit l'homme ; pouvez-vous me donnez la preuve
              de votre identité ?&nbsp;»

              <p/>

              Le meilleur moyen de prouver que vous êtes
              bien le Loup Solitaire consiste à faire la démonstration
              que vous maîtrisez l'une des Disciplines Kaï. Vous avez
              le choix entre les Disciplines suivantes : Guérison <L
              enabled={d1} to="16">rendez-vous au 16</L>, Puissance Psychique <L
              enabled={d2} to="133">rendez-vous au 133</L>, Maîtrise des Armes <L
              enabled={d3} to="255">rendez-vous au 255</L>, Communication Animale&nbsp;
              <L enabled={d4} to="203">rendez-vous au 203</L>, Maîtrise Psychique
              de la Matière <L enabled={d5} to="48">rendez-vous au 48</L>.

              <p/>

              Si vous ne maîtrisez aucune des Disciplines de cette
              liste, ou si vous ne souhaitez pas faire de
              démonstration, <L to="348">rendez-vous au 348</L>.

            </div>
        );
    }
}

export default P160;
