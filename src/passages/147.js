/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P147 extends Component {
    render() {
        return (
            <div>

              La mise en pratique de votre Discipline Kaï vous indique
              que le chemin aboutit à un cul-de-sac. Seul le pont peut
              vous permettre de franchir le chenal de Ryner et
              d'atteindre Port Bax. <L to="47">Rendez-vous au 47</L>.

            </div>
        );
    }
}

export default P147;
