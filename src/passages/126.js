/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Dead from '../Dead.js';

class P126 extends Component {
    render() {
        return (
            <div>

              L'homme tire sur la corde d'une clochette dissimulée aux
              regards et, soudain, quatre gardes armés font irruption
              dans la pièce. «&nbsp;Ces documents sont des faux. Vous
              êtes sans aucun doute un espion, peut-être même
              pire. Quoi qu'il en soit, vous n'allez pas tarder à
              apprendre ce que nous faisons des criminels dans votre
              genre, à Port Bax.  Emmenez-le !&nbsp;» Avant que vous n'ayez
              pu fournir la moindre explication, les gardes vous
              saisissent et vous emmènent à la prison de la
              ville. Tout votre équipement est confisqué, y compris
              les Objets Spéciaux et les armes, et on vous jette dans
              une cellule remplie de canailles à l'aspect
              patibulaire. Vous remarquez aussitôt que plusieurs de
              ces fripons portent au poignet gauche un tatouage qui
              représente un serpent : c'est le signe de Vonatar le
              Traître. Et lorsque, enfin, les gardes découvrent votre
              véritable identité en examinant votre équipement, il est
              trop tard : les séides du sorcier vous ont déjà
              étranglé. Votre mission s'achève donc tragiquement en
              même temps que votre vie dans un cul-de-basse-fosse de
              Port Bax.

              <p/>
              <Dead/>

            </div>
        );
    }
}

export default P126;
