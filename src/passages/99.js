/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import * as bh from '../book_helpers.js';

class P99 extends Component {
    
    constructor(props) {
        super(props);
        this.r = {
            passage_title: '99',
            name: 'random_table',
            dice_value: -1
        };
    }

    render() {
        const randtable = bh.book_get_element(this.props, this.r);
        const r = randtable.dice_value;
        return (
            <div>

              Le lendemain matin, vous êtes réveillé par la vigie
              postée dans le nidde-pie. «&nbsp;Navire par bâbord avant
              ! » annonce l'homme à grands cris.  Vous grimpez une
              échelle étroite et vous rejoignez le capitaine qui se
              tient à la proue. «&nbsp;Vos yeux sont plus jeunes que
              les miens, dit-il en vous tendant une longue-vue
              ciselée, essayez de voir quel est ce bateau.  » Vous
              distinguez alors à l'horizon les voiles rouges et le
              pavillon noir d'un navire de guerre mené par des pirates
              Lakuri. Utilisez la <RandomTable p={this.r}> pour
              obtenir un chiffre</RandomTable>. Si vous tirez un
              chiffre entre 0 et 4, <L to="326" enabled={r>=0 &&
              r<=4}>rendez-vous au 326</L>. Si vous tirez un 5, un 6,
              un 7, un 8 ou un 9, <L to="163"
              enabled={r>=5}>rendez-vous au 163</L>.

              <CurrentDice r={r}/>

            </div>
        );
    }
}

export default P99;
