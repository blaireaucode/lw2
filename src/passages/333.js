/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P333 extends Component {
    render() {
        return (
            <div>

              Le marin qui se faisait passer pour Ronan semble avoir
              pris la fuite au cours du combat. Vous fouillez
              rapidement les cadavres des autres brigands, mais vous
              ne découvrez rien d'intéressant. Vous remarquez
              cependant que chacun des malfaiteurs porte au poignet
              gauche un tatouage représentant un serpent. Il est clair
              que celui qui leur a donné l'ordre de vous tuer, quel
              qu'il soit, connaît déjà la nature de votre
              mission. Vous quittez la taverne par la porte latérale
              et vous découvrez en passant devant un escalier le
              cadavre d'un marin dissimulé sous les marches. Cousue à
              l'intérieur du col de sa veste tachée de sang, une
              étiquette porte ce nom : Ronan. Voilà donc celui avec
              qui vous aviez rendez-vous et que vos ennemis ont
              assassiné pour prendre sa place.  Vous couvrez son corps
              et vous sortez de la taverne en prenant la direction du
              port dans lequel est ancré le Sceptre Vert, à quelque
              300 mètres du quai.

              <p/>

              Si vous souhaitez rejoindre le navire en empruntant l'un
              des canots qui sont amarrés le long du quai, <L
              to="300">rendez-vous au 300</L>. Si vous préférez
              essayer de retrouver l'imposteur qui s'est fait passer
              pour Ronan, <L to="67">rendez-vous au 67</L>.

            </div>
        );
    }
}

export default P333;
