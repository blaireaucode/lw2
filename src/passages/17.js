/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';

import update from 'immutability-helper';

import * as bh from '../book_helpers.js';

class P17 extends Component {

    constructor(props) {
        super(props);
        const f = {
            'name': 'f1',
            'passage_title': '17',
            'encounter': "MONSTRE D'ENFER",
            'combat_skill':22,
            'endurance':30,
            'injury_already_done': false,
            'bonus_factor_dmg_encounter': 2,
            'no_bonus_psy': true
        };
        this.f = f;
    }

    componentDidUpdate(nextProps) {
        const elem = bh.book_get_element(nextProps, this.f);
        if (!elem.injury_already_done) {
            const e = update(elem, {injury_already_done: {$set: true}});
            this.props.book_set_element(e);
            const c = bh.endurance_add(nextProps.character, -5);
            nextProps.character_set(c);
        }
    }

    render() {
        const r = bh.enabled_if_fight_win(this.props, this.f);
        return (
            <div>

              Vous avez réussi à vous hisser à mi-corps lorsque la porte de la
              cale, au-dessous de vous, s'ouvre à la volée. Un MONSTRE D'ENFER
              se précipite alors et vous blesse aux jambes d'un coup de son épée
              noire avant même que vous ayez pu tenter de vous enfuir. La
              blessure est sérieuse et vous perdez 5 points d'ENDURANCE. Vous
              tombez ensuite au fond de la cale, et il vous faut combattre la
              créature jusqu'à ce que mort s'ensuive.

              Il s'agit là d'un mort vivant et la puissance du Glaive
              de Sommer vous permet de multiplier par deux tous les
              points d'ENDURANCE que perdra la créature. Elle reste
              cependant insensible à la Puissance Psychique. Si vous
              parvenez à tuer le Monstre d'Enfer, vous pourrez quitter
              la cale par la porte ouverte. <L to='166'
              enabled={r}>Rendez-vous alors au 166</L>.

              <p/>
              <Fight fight={this.f}/>

            </div>
        );
    }
}
// MONSTRE D'ENFER HABILETÉ: 22 ENDURANCE:30
export default P17;
