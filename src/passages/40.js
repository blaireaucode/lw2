/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import update from 'immutability-helper';
import ItemsPicker from '../ItemsPicker.js';
import * as bh from '../book_helpers.js';

class P40 extends Component {

    constructor(props) {
        super(props);
        this.i = {
            'passage_title': '40',
            'name': 'obj1',
            'list': [ {id:'fiole_laumspur', nb:1} ],
            'already_done': false
        };
    }

    handleClick() {
        const elem = bh.book_get_element(this.props, this.i);
        if (elem.already_done) return;
        const c = bh.endurance_set(this.props.character, this.props.character.endurance_max);
        this.props.character_set(c);
        const e = update(elem,{already_done:{$set:true}});
        this.props.book_set_element(e);
    }

    render() {
        const elem = bh.book_get_element(this.props, this.i);       
        const already_done = elem.already_done;
        return (
            <div>

              II faut quatorze jours pour mobiliser l'armée et
              préparer la flotte de Durenor. Pendant tout ce temps,
              vous êtes l'hôte du roi, et vous demeurez à Hammardal,
              la capitale du royaume. Chaque jour qui passe accroît
              votre inquiétude : que deviennent vos compatriotes dans
              la ville assiégée de Holmgard ? Auront-ils la force de
              résister encore longtemps aux Maîtres des Ténèbres ?
              Puissent-ils tenir jusqu'à votre retour, c'est la prière
              que vous formulez sans cesse. Vous vivez cet exil à
              contrecœur en faisant chaque jour les exercices
              nécessaires pour vous maintenir en bonne condition
              physique ; vous vous adonnez également à la
              méditation.

              <p/>

              Un herboriste de Durenor, un certain Madin Rendalim est
              venu vous rendre visite. Sa science dans l'art de guérir
              est célèbre d'un bout à l'autre des Fins de Terre et il
              <L to='40' onClick={this.handleClick.bind(this)}
              enabled={!already_done}> vous redonne tous vos points
              d'ENDURANCE</L> ; vous disposez donc à présent du même
              total d'ENDURANCE qu'au tout début de votre
              mission. Madin Rendalim vous fait don par la même
              occasion d'une fiole de Laumspur: il s'agit d'une
              puissante potion de guérison qui vous permettra de
              récupérer 5 points d'ENDURANCE si vous la buvez après un
              combat. La fiole, cependant, ne contient qu'une seule
              dose.

              <p/>
              <ItemsPicker items={this.i}/>
              <p/>

              Malheureusement, l'herboriste vous apporte également de
              mauvaises nouvelles: on a découvert le cadavre du
              Lieutenant Général Rhygar dans la forêt, près de
              l'entrée du tunnel de Tarnalin. Il a été tué par un
              Monstre d'Enfer. Si vous maîtrisez la Discipline Kaï du
              Sixième Sens, <L to="97">rendez-vous au 97</L>. Dans le
              cas contraire, <L to="242">rendez-vous au 242</L>.

            </div>
        );
    }
}

export default P40;
