/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P281 extends Component {

    render() {
        return (
            <div>

              Vous demandez avec insistance au capitaine de recueillir
              les malheureux naufragés, mais il reste indifférent à
              vos prières et ordonne aux hommes d'équipage de
              poursuivre leurs tâches comme si de rien n'était. Vous
              voyez le canot disparaître à l'horizon et vous avez
              alors la prémonition qu'un sort semblable vous
              attend. Troublé par cette pensée, vous descendez au pont
              inférieur pour vous retirer dans votre cabine.  <L
              to="240">Rendez-vous au 240</L>.

            </div>
        );
    }
}

export default P281;
