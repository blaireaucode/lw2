/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P15 extends Component {

    constructor(props) {
        super(props);
        const f = {
            'name': 'obj1',
            'passage_title': 15,
            'list': [
                {id:'glaive'},
                {id:'masse_d_armes'},
                {id:'baton'},
                {id:'potion_guerison', nb:1},
                {id:'repas', nb:3},
                // {id:'sac_a_dos'},
                {id:'PO', nb:12},
            ],
        };
        this.items = f;
    }

    render() {
        return (
            <div>

              Il n'a plus l'air soupçonneux, à présent, mais
              surpris.
              
              <p/>

              « Je pensais que vous étiez un imposteur, Seigneur Kaï,
              dit-il, et je dois vous avouer que j'avais l'intention
              de vous donner une leçon que vous n'auriez jamais
              oubliée votre vie durant. Pardonnez-moi d'avoir douté de
              vous mais votre récit m'a paru si effrayant que je n'ai
              pu me résoudre à y croire, de peur qu'il ne fût
              vrai. J'ai fait le serment de défendre cette frontière
              et il m'est impossible de quitter la tour, mais si l'un
              quelconque des objets que je possède peut être utile à
              votre quête, sachez que je vous en fait volontiers
              don. »

              <p/>

              Il dispose alors sur une grande table de chêne les
              objets suivants et vous invite à choisir l'un
              d'eux. Une dose de Potion de Guérison vous
              redonne 3 points d'ENDURANCE.

              <p/>
              <ItemsPicker items={this.items}/>
              <p/>
              
              Vous faites votre choix et vous vous apprêtez à
              quitter la tour. L'homme vous indique du doigt la
              direction à prendre. « Lorsque vous serez parvenu
              au chenal de Ryner, suivez le chemin orienté au
              nord. Vous arriverez alors à un pont que gardent des
              soldats du roi. Quand ils vous demanderont le mot de
              passe, vous répondrez : <i>Crépuscule</i>. La route
              au-delà du pont mène à Port Bax. Que Dieu vous aide,
              Loup Solitaire. »

              <p/>

              Vous remerciez ce valeureux guerrier et vous vous mettez
              en chemin. Il vous faut cependant abandonner votre
              cheval car il vous serait impossible de franchir sur son
              dos la forêt dense qui s'étend devant vous. <L
              to="244">Rendez-vous au 244</L>.

            </div>
        );
    }
}

export default P15;
