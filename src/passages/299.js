/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P299 extends Component {

    giveSpear() {
        const c = bh.item_drop(this.props.character, 'magical_spear');
        this.props.character_set(c);
    }
    
    render() {
        const l = this.props.character.items.some(e => e.id === 'magical_spear');        
        return (
            <div>

              Vous courez pendant six heures sans vous arrêter. Les
              Monstres d'Enfer vous attendent sur le grand chemin et
              il vous faut les éviter en passant par les forêts
              escarpées qui s'étendent au flanc des collines. Souvent,
              vous vous sentez si fatigué, vos jambes vous font si mal
              que vous avez la tentation de tout abandonner. Mais
              chaque fois que vous faiblissez, le Lieutenant Général
              Rhygar parvient à vous redonner courage. Son endurance
              vous émerveille car ce n'est plus un jeune homme et il
              porte par surcroît la lourde armure des chevaliers du
              Sommerlund.

              <p/>

              A la nuit tombée, vous arrivez à l'entrée du tunnel de
              Tarnalin qui traverse les monts d'Hammardal. Il y a en
              tout trois tunnels qui mènent à la capitale de
              Durenor. Tous trois ont été creusés au temps de la Lune
              Noire et chacun d'eux fait plus de 60 kilomètres de
              long. Ils constituent les seules voies d'accès à la
              ville qui est entièrement encerclée par les
              montagnes. Vous pouvez à présent faire une courte halte
              et le Lieutenant Général Rhygard s'assied à côté de vous
              en prenant dans son sac du pain et des
              viandes. «&nbsp;Mangez, Loup Solitaire, dit-il alors en
              vous tendant cette nourriture, car vous allez avoir
              besoin de forces ; il vous faudra en effet parcourir
              seul ce tunnel qui mène à Hammardal, tandis que je
              resterai ici pour contenir l'ennemi aussi longtemps que
              je pourrai combattre. Et ne protestez pas, le succès de
              votre mission est la seule chose qui compte.&nbsp;» Mais si
              Rhygard veut arrêter les Monstres d'Enfer, il lui faudra
              une arme magique car sa propre épée ne lui servira à
              rien contre ces créatures.

              <p/>

              Si vous souhaitez lui donner votre Lance Magique pour
              qu'il puisse défendre l'entrée du tunnel, <L
              onClick={this.giveSpear.bind(this)} to='102'
              enabled={l}>rendez-vous au 102</L>. Si vous ne possédez
              pas cette Lance Magique ou si vous ne voulez pas vous en
              séparer, <L to="118">rendez-vous au 118</L>.

            </div>
        );
    }
}

export default P299;
