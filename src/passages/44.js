/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Dead from '../Dead.js';

class P44 extends Component {
    render() {
        return (
            <div>

              Le venin se répand dans votre sang, vos membres
              s'engourdissent et vous vous mettez à transpirer. Le
              clapotis des vagues et le cri des vautours au-dessus de
              votre tête sont les derniers bruits qui vous
              parviennent. Votre quête s'achève ici en même temps que
              votre vie.

              <Dead/>

            </div>
        );
    }
}

export default P44;
