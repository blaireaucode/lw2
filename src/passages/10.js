/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import * as bh from '../book_helpers.js';

class P10 extends Component {

    constructor(props) {
        super(props);
        this.r = {
            passage_title: 10,
            name: 'random_table',
            dice_value: -1
        };
    }

    render() {
        const randtable = bh.book_get_element(this.props, this.r);
        const r = randtable.dice_value;
        return (
            <div>

              Vous empochez le billet (inscrivez-le sur votre Feuille d'Aventure
              dans la case d'Objets Spéciaux) et l'homme vous conduit à la
              diligence qui attend à proximité de la porte Est du port. La
              diligence est vide et vous vous asseyez près d'une des fenêtres de
              forme circulaire. Vous constatez avec soulagement que le siège est
              très confortable ; c'est un avantage que vous appréciez car il vous
              faudra voyager sept jours durant pour atteindre Port Bax. Vous
              rangez votre équipement sous la banquette, vous vous adossez
              confortablement et vous vous laissez gagner par le sommeil.

              <p/>
              
              Lorsque vous vous éveillez, cinq autres passagers ont
              pris place dans la diligence qui fait route en direction
              de Durenor. Utilisez la <RandomTable ref='dice' p={this.r}>pour
              obtenir un chiffre</RandomTable>. Si vous tirez 0, 1, 2
              ou 3, <L to="51" enabled={r>=0 && r<=3}> rendez-vous au
              51</L>. 4, 5 ou 6, <L to="195" enabled={r>=4 && r<=6}>
              rendez-vous au 195</L>. 7, 8 ou 9, <L to="339"
              enabled={r>6}> rendez-vous au 339</L>.

              <CurrentDice r={r}/>
              
            </div>
        );
    }
}

export default P10;
