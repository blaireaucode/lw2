/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P88 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, "camouflage");
        return (
            <div>

              Bien que la nuit soit tombée, la pleine lune projette
              une brillante clarté sur tout le village. Derrière la
              taverne, vous apercevez la petite boutique d'un charron
              ; deux chevaux sont attelés à une charrette à foin
              stationnée juste devant la porte. Si vous souhaitez
              prendre l'un des chevaux pour vous enfuir au galop, <L
              to="150">rendez-vous au 150</L>. Si vous préférez vous
              cacher dans la boutique du charron, <L
              to="71">rendez-vous au 71</L>. Si enfin vous maîtrisez
              la Discipline Kaï du Camouflage, <L enabled={d}
              to='179'>rendez-vous au 179</L>.

            </div>
        );
    }
}

export default P88;
