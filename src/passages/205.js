/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P205 extends Component {
    render() {
        return (
            <div>

              L'aubergiste fronce les sourcils et vous montre du doigt
              une porte latérale. «&nbsp;Si vous ne pouvez pas vous
              payer une chambre, dit-il, allez donc dormir dans
              l'écurie.&nbsp;» En vous dirigeant vers la sortie, vous
              sentez dans votre dos le regard des autres passagers de
              la diligence. La porte claque sur vos talons et vous
              vous retrouvez seul dans la nuit froide, le corps
              parcouru de frissons. <L to="213">Rendez-vous au
              213</L>.

            </div>
        );
    }
}

export default P205;
