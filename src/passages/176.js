/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';
import SimpleImage from '../SimpleImage.js';


class P176 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, "sixieme_sens");
        return (
            <div>

              Vous avez chevauché pendant trois jours et trois nuits
              le long du grand chemin qui remonte la vallée du
              Durenon. Au loin, vous apercevez le sommet des monts
              d'Hammardal, l'une des plus hautes chaînes de montagnes
              de Magnamund. La capitale du royaume de Durenor est
              nichée au creux de ces montagnes. L'aube vient de se
              lever sur le quatorzième jour de votre quête. Vous avez
              établi votre camp près d'une chute d'eau ; à cet
              endroit, les flots du fleuve Durenon plongent au bas
              d'un à-pic de 40 mètres de hauteur.

              <p/>
              Vous vous apprêtez à vous remettre en route lorsque six
              cavaliers au visage encapuchonné apparaissent sur la
              route forestière et vous bloquent le passage. Le
              Lieutenant Général Rhygar leur intime l'ordre de vous
              laisser passer en leur précisant que vous êtes porteur
              d'une dépêche royale. Au royaume de Durenor, faire
              obstacle au passage d'un messager du roi est considéré
              comme un acte de trahison ; malheureusement,
              l'avertissement du Lieutenant Général laisse
              indifférents les six cavaliers qui refusent de bouger
              d'un pouce. «&nbsp;Si vous ne voulez pas entendre
              raison, nos épées vous convaincront peut-être&nbsp;»,
              dit alors Rhygar. Il dégaine aussitôt son arme et
              ordonne à ses hommes de passer à l'attaque. Si vous
              souhaitez prêter main forte à Rhygar, <L
              to="45">rendez-vous au 45</L>. Si vous préférez ne pas
              attaquer les cavaliers, <L to='277'>rendez-vous au
              277</L>. Enfin, si vous maîtrisez la Discipline Kaï du
              Sixième Sens, <L enabled={d} to="322">rendez-vous au
              322</L>.

              <p/>
              <SimpleImage width={200} src={'176.png'}/>

            </div>
        );
    }
}

export default P176;
