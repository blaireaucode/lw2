/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import * as bh from '../book_helpers.js';

class P158 extends Component {
    
    constructor(props) {
        super(props);
        const f = {
            'name': 'f1',
            'passage_title': 158,
            'encounter': "MOINE",
            'combat_skill':16,
            'endurance':23
        };
        this.f = f;
    }
    
    render() {
        const r = bh.enabled_if_fight_win(this.props, this.f);
        return (
            <div>

            Le MOINE ne semble pas surpris par votre attaque et il
            tire lui-même une épée noire de sous sa robe de bure.  Si
            vous êtes vainqueur, <L enabled={r} to="220">rendez-vous au 220</L>.

              <p/>
              <Fight fight={this.f}/>

            </div>
        );
    }
}

// MOINE HABILETÉ: 16 ENDURANCE: 23 
export default P158;
