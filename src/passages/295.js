/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P295 extends Component {
    render() {
        return (
            <div>

              L'une des créatures, plus grande que les autres et vêtue
              d'une magnifique robe de soie en patchwork, crie un
              ordre dans son étrange dialecte. Tous ses congénères
              saisissent alors des lances et des épées qui semblent
              avoir été taillées dans les rayons d'une roue de chariot
              ou dans des manches à balai. Puis ils se précipitent sur
              vous en poussant de curieux cris de guerre, quelque
              chose comme «&nbsp;Gashiss, Nashiss&nbsp;».  Vous n'avez
              cependant pas le temps de vous intéresser à leur
              langage, car bientôt vous serez piétiné à mort par une
              véritable armée de ces petits êtres hargneux, si vous ne
              prenez pas immédiatement la fuite.  Vous faites donc
              volte-face et vous courez à perdre haleine le long d'un
              couloir étroit, jusqu'à ce que leurs cris ne soient plus
              derrière vous qu'une faible rumeur. Quelques instants
              plus tard, vous parvenez au bout du passage qui débouche
              sur le tunnel principal. Vous pouvez continuer votre
              chemin à une allure plus tranquille. <L
              to="340">Rendez-vous au 340</L>.

            </div>
        );
    }
}

export default P295;
