/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P23 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, "communication_animale");
        return (
            <div>

              Pendant près de dix minutes, vous poursuivez la créature
              qui s'enfuit le long d'un passage étroit et
              sinueux. Vous êtes sur le point d'abandonner lorsque le
              passage s'ouvre soudain sur une immense caverne éclairée
              par des torches enflammées. Un spectacle saisissant
              s'offre alors à vous. L'endroit, en effet, abrite une
              colonie entière de ces étranges créatures, les Noudics,
              qui sont fort occupées à examiner et à trier toutes
              sortes d'objets insolites empilés les uns sur les autres
              au beau milieu de la caverne. Si vous maîtrisez la
              Discipline Kaï de la Communication Animale, <L to="144"
              enabled={d}>rendez-vous au 144</L>. Dans le cas
              contraire, <L to="295" enabled={!d}>rendez-vous au
              295</L>.

            </div>
        );
    }
}

export default P23;
