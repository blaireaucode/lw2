/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import * as bh from '../book_helpers.js';

class P278 extends Component {

    render() {
        const r = bh.get_element(this, 278, {dice_value:-1});
        const d = r.dice_value;
        return (
            <div>

              Vous agitez désespérément votre cape au-dessus de vous
              jusqu'à ce que vous soyez au bord de
              l'épuisement. Utilisez la <RandomTable p={r}>pour
              obtenir un chiffre</RandomTable> qui vous indiquera si
              vos efforts ont été couronnés de succès. Si vous tirez
              un chiffre entre 0 et 6, <L enabled={d>=0 && d<=6}
              to="41">rendez-vous au 41</L>. Si vous obtenez 7, 8 ou
              9, <L enabled={d>=7} to="180">rendez-vous au 180</L>.

              <CurrentDice r={d}/>

            </div>
        );
    }
}

export default P278;
