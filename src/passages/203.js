/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P203 extends Component {
    render() {
        return (
            <div>

              «&nbsp;Vous avez faim ! Voulez-vous un peu de fromage ?
              » Vous avez posé cette question au marin après avoir
              jeté un coup d'œil aux deux souris qui s'affairent à
              l'autre bout de la salle. Utilisant alors la Discipline
              Kaï de la Communication Animale, vous ordonnez aux deux
              rongeurs de vous apporter leur fromage et, un instant
              plus tard, l'homme constate avec stupéfaction que les
              souris viennent effectivement déposer le fromage à vos
              pieds avant de disparaître en toute hâte. <L to='268'>Rendez-vous au
              268</L>.

            </div>
        );
    }
}

export default P203;
