/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import HelpCharacter from '../HelpCharacter.js';
import EnduranceChange from '../EnduranceChange.js';
import * as bh from '../book_helpers.js';

class P154 extends Component {

    constructor(props) {
        super(props);
        this.f = {
            'name': 'a',
            'passage_title': 154,
            'already_done': false,
            'dmg':-2
        };
    }

    render() {
        const elem = bh.book_get_element(this.props, this.f);
        const already_done = elem.already_done;
        return (
            <div>

            Dans leur poste d'équipage, les hommes du Sceptre Vert
            sont entassés les uns sur les autres; il règne là une
            atmosphère étouffante, surchauffée. Mais, en dépit du
            manque d'espace et de la frugalité du repas (une frugalité
            telle qu'elle vous coûte <EnduranceChange f={this.f}> 2
            points d'ENDURANCE</EnduranceChange>), les marins sont
            contents que vous ayez accepté leur invitation et ils vous
            traitent comme un hôte d'honneur. Après dîner, ils vous
            invitent à jouer avec eux aux «&nbsp;Hublots ». Il s'agit
            d'un jeu de dés où l'on mise un peu d'or. Si vous voulez
            tenter votre chance, <L enabled={already_done}
            to="308">rendez-vous au 308</L>. Si vous préférez décliner
            leur offre et leur souhaiter bonne nuit avant de regagner
            votre cabine, <L to="197" enabled={already_done}
            >rendez-vous au 197</L>.

              <p/>
              <HelpCharacter/>
            </div>
        );
    }
}

export default P154;
