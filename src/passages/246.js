/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P246 extends Component {
    render() {
        const lb = this.props.character.items.some(e => e.id === 'laissez_passer');
        const lr = this.props.character.items.some(e => e.id === 'laissez_passer_rouge');
        return (
            <div>

              L'un des gardes s'avance vers vous et demande à voir
              votre laissez-passer. Si vous avez un laissez-passer
              blanc, <L enabled={lb} to="170">rendez-vous au
              170</L>. Si votre laissez-passer est rouge, <L
              enabled={lr} to="202">rendez-vous au 202</L>. Si vous
              n'avez pas de laissez-passer, l'entrée du port vous sera
              interdite et vous vous <L to='327'>rendrez alors au
              327</L>.

            </div>
        );
    }
}

export default P246;
