/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import * as bh from '../book_helpers.js';

class P45 extends Component {

    constructor(props) {
        super(props);
        this.r = {
            passage_title: '45',
            name: 'random_table',
            dice_value: -1
        };
    }

    render() {
        const randtable = bh.book_get_element(this.props, this.r);
        const r = randtable.dice_value;
        return (
            <div>

              Vous galopez le long du chemin forestier en direction
              des cavaliers vêtus de capes ; soudain, l'un d'eux lève
              un épieu de couleur noire audessus de sa tête. A
              l'extrémité de l'épieu est fixé un cône d'acier, noir
              également, d'où s'échappe une flamme bleue
              étince-lante. Vous vous apprêtez à porter votre premier
              coup lorsqu'un éclair aveuglant jaillit du bâton
              maléfique et explose juste à côté de vous. La force de
              la déflagration est telle que vous êtes projeté à terre
              dans les broussailles.  Utilisez la <RandomTable
              p={this.r}/> pour obtenir un chiffre. Si vous tirez un
              chiffre de 0 à 7, <L to="311" enabled={r>=0 &&
              r<=7}>rendez-vous au 311</L>. Si vous tirez un 8 ou un
              9, <L to="159" enabled={r>=8}>rendez-vous au 159</L>.

              <CurrentDice r={r}/>
            </div>
        );
    }
}

export default P45;
