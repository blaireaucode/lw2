/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import HelpCharacter from '../HelpCharacter.js';
import SimpleImage from '../SimpleImage.js';
import EnduranceChange from '../EnduranceChange.js';
import * as bh from '../book_helpers.js';

class P31 extends Component {

    constructor(props) {
        super(props);
        this.f = {
            'name': 'a',
            'passage_title': 31,
            'already_done': false,
            'dmg': 6
        };
        this.r = {
            passage_title: 31,
            name: 'r',
            dice_value: -1
        };
    }

    render() {
        const randtable = bh.book_get_element(this.props, this.r);
        const d = randtable.dice_value;
        
        return (
            <div>

              Votre première rencontre avec le Lieutenant Général vous
              surprend. Vous vous attendiez sans doute à voir un vieil
              homme servile, semblable à ces émissaires des contrées
              méridionales qui viennent sans cesse encombrer le palais
              du roi. Mais l'homme qui se tient devant vous, vêtu
              d'une lourde cotte de mailles, n'est ni vieux ni
              servile. C'est même un personnage tout à fait
              exceptionnel comme vous n'allez pas tarder à le
              découvrir.

              <p/>

              Né d'un père sommerlundois et d'une mère durenoraise, le
              Lieutenant Général Rhygar est devenu dans cette ville
              une figure de légende. Au cours des dix dernières
              années, il a pris la tête d'une armée formée par
              l'Alliance des Nations et sous son commandement, les
              Barbares des Glaces venus du pays de Kalte ont été
              repoussés et taillés en pièces. Sage en temps de paix,
              implacable lorsque la guerre fait rage, c'est là le
              meilleur compagnon que vous puissiez souhaiter. Nul ne
              saurait mieux vous aider dans votre quête du Glaive de
              Sommer. Rhygar fait servir un somptueux repas ; jamais
              vous n'avez aussi bien mangé depuis que la guerre a
              commencé. Au cours du festin, vous repensez à tous les
              événements qui se sont déroulés entre votre départ du
              Sommerlund et votre arrivée à Port Bax. Vous songez
              également aux terribles périls qui vous attendent
              encore.

              <p/>

              A la fin du repas, Rhygar fait venir son médecin
              personnel qui s'empresse de soigner vos blessures. Les
              potions qu'il vous fait boire vous rendent
              <EnduranceChange f={this.f}> 6 points
                d'ENDURANCE</EnduranceChange>. Le praticien vous
              conseille ensuite de prendre une bonne nuit de repos
              car, au matin, vous partirez pour Hammardal en compagnie
              du Lieutenant Général.

              <p/>
              <HelpCharacter/>
              <p/>

              Le lendemain de bonne heure, on vous conduit dans un
              jardin clôturé, à l'arrière du consulat. Rhygar et trois
              de ses meilleurs soldats vous y attendent. Ils sont déjà
              montés sur leurs chevaux, prêts à vous accompagner
              jusqu'à Hammardal, la capitale de Durenor, distante de
              370 kilomètres. Les rues de Port Bax s'éveillent à peine
              tandis que vous parcourez la ville à cheval. En passant
              la porte de pierre moussue qui marque la limite de la
              cité, vous vous sentez confiant dans le succès de votre
              mission : vous êtes quasiment sûr désormais de
              réussir. Utilisez la <RandomTable p={this.r}/> pour
              obtenir un chiffre. Si vous tirez 0,1, 2, 3 ou 4, <L
              to="176" enabled={d>=0 && d<=4}>rendez-vous au
              176</L>. Si vous tirez 5, 6, 7, 8 ou 9, <L to="254"
              enabled={d>=5 && d<=9}>rendez-vous au 254</L>.

              <p/>
              <SimpleImage src={'31.png'}/>
              <CurrentDice r={d}/>

            </div>
        );
    }
}

export default P31;
