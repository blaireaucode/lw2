/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';


import * as bh from '../book_helpers.js';

class P60 extends Component {
    constructor(props) {
        super(props);
        const f = {
            'name': 'f1',
            'passage_title': '60',
            'encounter': "HALVORC",
            'combat_skill':8,
            'endurance':11,
            'no_damage_first_rounds':2
        };
        this.f = f;
    }

    render() {
        const r = bh.enabled_if_fight_win(this.props, this.f);
        return (
            <div>

              HALVORC vous contemple d'un air stupéfait et incrédule.
              Il est incapable de se défendre au cours des deux
              premiers assauts en raison de l'effet de surprise de
              votre attaque. Vous ne perdrez donc aucun point
              d'ENDURANCE lors de ces deux assauts. Si votre
              adversaire est toujours en vie au moment du troisième
              assaut, il s'élancera sur vous armé d'un Poignard. Si
              vous sortez vainqueur du combat, <L to="76"
              enabled={r}>rendez-vous au 76</L>.

              <p/>
              <Fight fight={this.f}/>              

            </div>
        );
    }
}

// HALVORC HABILETÉ: 8 ENDURANCE: 11 
export default P60;
