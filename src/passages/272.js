/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P272 extends Component {
    render() {
        return (
            <div>

              Quelqu'un ou quelque chose s'approche de la porte de la
              cale, de l'autre côté du panneau. Si vous essayez de
              vous hisser sur le pont délabré, vos jambes seront
              exposées et vous serez vulnérable à toute attaque venant
              du fond de la cale. Il n'y a cependant pas d'autre
              issue. Compte tenu de la situation, la meilleure chose à
              faire est de dégainer le Glaive de Sommer et de vous
              préparer à combattre la créature malfaisante et
              redoutable dont vous percevez la présence. <L
              to="5">Rendez-vous au 5</L>.

            </div>
        );
    }
}

export default P272;
