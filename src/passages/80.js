/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P80 extends Component {
    render() {
        const l = this.props.character.items.some(e => e.id === 'sceau_hammardal');
        return (
            <div>

              Le chevalier remet son épée au fourreau et vous conduit
              à l'intérieur de la tour. Vous le suivez le long d'un
              escalier de pierre qui mène à une vaste salle ; un feu
              de bois brûle dans une cheminée en répandant une
              agréable chaleur. «&nbsp;Si vous êtes vraiment celui que
              vous prétendez être, vous devez avoir en votre
              possession le Sceau d'Hammardal. Dans ce cas,
              montrez-le-moi », ordonne le chevalier. Si vous acceptez
              de lui montrer le Sceau, <L enabled={l} to="15">rendez-vous au
              15</L>. Si vous n'avez plus le Sceau ou si vous ne
              voulez pas le lui montrer, <L to="189">rendez-vous au
              189</L>.

            </div>
        );
    }
}

export default P80;
