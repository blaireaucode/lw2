/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

import update from 'immutability-helper';

import HelpCharacter from '../HelpCharacter.js';
import SimpleImage from '../SimpleImage.js';
import EnduranceChange from '../EnduranceChange.js';
import * as bh from '../book_helpers.js';

class P37 extends Component {

    constructor(props) {
        super(props);
        this.f = {
            'name': 'a',
            'passage_title': 37,
            'already_done': false,
            'dmg':-3
        };
    }

    handleClickMeal() {
        const elem = bh.book_get_element(this.props, this.f);
        if (!elem.already_done) {
            const e = update(elem, {already_done: {$set: true}});
            this.props.book_set_element(e);
            var c = bh.endurance_add(this.props.character, 2);
            c = bh.item_drop(c, 'repas');
            this.props.character_set(c);
        }        
    }
    
    render() {
        const elem = bh.book_get_element(this.props, this.f);
        const already_done = elem.already_done;
        const index = bh.item_get_index(this.props.character.items, 'repas');
        const meal = (index !== -1);
        return (
            <div>

              A l'intérieur de la diligence, il fait chaud et
              sec. Vous secouez votre cape de Seigneur Kaï pour la
              débarrasser des gouttes de pluie qui la recouvrent et
              vous remarquez alors la présence de trois autres
              passagers : deux femmes et un homme qui ronfle avec
              bruit. L'une des femmes lève les yeux vers vous et vous
              adresse un sourire. «Nous arriverons à Ragadorn dans six
              heures», dit-elle, puis elle dépose son panier sur le
              plancher pour que vous puissiez vous asseoir à côté
              d'elle. Elle vous apprend ensuite qu'elle habite
              Ragadorn et vous donne quelques renseignements sur sa
              ville. Depuis que Killean le Suzerain est mort il y a
              trois ans, raconte-t-elle, son fils Lachelan règne sur
              Ragadorn ; c'est un être malfaisant entouré de
              mercenaires qui sont en fait de purs et simples
              brigands. Ils saignent à blanc toute la population en
              levant de lourds impôts et si quelqu'un a le malheur de
              se plaindre, il a tôt fait de disparaître on ne sait
              où. La vie est bien dure là-bas et, si vous voulez mon
              avis, vous feriez bien de quitter Ragadorn le plus vite
              possible. Au cours de ce voyage, vous allez devoir <L
              to='37' onClick={this.handleClickMeal.bind(this)}
              enabled={!already_done && meal}> prendre un repas</L> ;
              à défaut, vous perdrez <EnduranceChange f={this.f}> 3
              points d'ENDURANCE</EnduranceChange>. Quelques heures
              plus tard, vous entendez au loin sonner une cloche. En
              jetant un coup d'œil par la fenêtre de la diligence,
              vous apercevez le mur d'enceinte de Ragadorn.

              <p/>
              <HelpCharacter/>
              <p/>
              
              L'attelage franchit bientôt la porte Ouest, puis
              s'arrête. Vous sautez à terre et la terrible puanteur
              qui baigne ce port sordide vous monte aussitôt aux
              narines. Une enseigne rouillée, clouée à un mur porte
              ces mots : Bienvenue à Ragadorn. La femme vous indique
              alors que vous pouvez prendre une autre diligence pour
              Port Bax au relais situé près de la porte Est de la
              ville. Si vous voulez marcher en direction du nord, le
              long de la rue de la porte Ouest, <L to="122"
              enabled={already_done}>rendez-vous au 122</L>. Si vous
              préférez aller vers le sud en empruntant la promenade du
              quai de l'Est, <L to="373" enabled={already_done}>
              rendez-vous au 373</L>. Enfin, si vous décidez plutôt de
              vous orienter vers l'est en prenant la rue de la Hache,
              <L to="257" enabled={already_done}> rendez-vous au
              257</L>.

              <p/>
              <SimpleImage src={'37.png'}/>

            </div>
        );
    }
}

export default P37;
