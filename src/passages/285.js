/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P285 extends Component {
    render() {
        return (
            <div>

              Vous sentez les crochets du serpent s'enfoncer dans la
              manche de votre tunique, mais rien de plus. Vous avez de
              la chance : seul votre vêtement a souffert de la
              morsure. Le serpent s'enfuit aussitôt et disparaît dans
              l'herbe haute ; vous vous hâtez alors de grimper à
              l'arbre pour passer le reste de la nuit à l'abri de son
              feuillage, à bonne distance du sol.  <L
              to="312">Rendez-vous au 312</L>.

            </div>
        );
    }
}

export default P285;
