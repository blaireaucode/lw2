/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';
import Roulette from '../Roulette.js';
import * as bh from '../book_helpers.js';

class P238 extends Component {
    render() {

        const r = {
            passage_title: 238,
            total_win: 0,
            num:0,
            bet:0
        };
        const g = bh.gold_get(this.props.character);

        return (
            <div>

              Face au relais de diligence, une rue étroite mène à une
              maison de jeu sur la façade de laquelle est placardé cet
              avis:

              <p/>
              LES ARMES SONT INTERDITES A L'INTÉRIEUR DE CET
              ÉTABLISSEMENT
              <p/>

              La perspective de pouvoir gagner un peu d'or vous décide
              à y entrer sans attendre. Si vous avez des armes, vous
              devrez les déposer au vestiaire ; vous aurez le droit de
              les reprendre en quittant les lieux. En échange d'une
              Pièce d'Or, on vous donne un jeton d'argent qui vous
              permet d'entrer dans l'établissement. Le hall mène à une
              vaste salle où se pratiquent toutes sortes de jeux de
              hasard. L'un d'eux vous semble particulièrement
              intéressant : on l'appelle la «&nbsp;Roue du
              Carrosse&nbsp;». Au bout d'une longue table, une jeune femme
              fort séduisante fait tourner une sorte de disque noir
              qui a été divisé en dix tranches égales numérotées de 0
              à 9. Lorsque le disque tourne, elle y laisse tomber une
              petite boule d'argent qui finit par s'immobiliser sur
              l'une des tranches numérotées. Plusieurs marchands sont
              assis autour de la table où se déroule ce jeu et misent
              de grosses sommes en essayant de deviner sur quel numéro
              la boule s'arrêtera.

              <p/>
              <SimpleImage width={200} src={'238.png'}/>
              <p/>
              
              Pour jouer à la «&nbsp;Roue du Carrosse&nbsp;», il vous faut
              tout d'abord choisir le numéro sur lequel vous voulez
              miser ; ensuite, vous devrez décider combien de
              Couronnes d'Or vous allez mettre en jeu. Notez bien ces
              deux chiffres, puis utilisez la Table de Hasard pour
              savoir si vous avez gagné. Si la Table vous donne le
              chiffre sur lequel vous avez parié, vous empocherez 8
              Pièces d'Or pour chaque Couronne mise en jeu. Si le
              chiffre que vous obtenez se situe immédiatement avant ou
              immédiatement après celui choisi par vous, chaque
              Couronne mise en jeu vous rapportera 5 Pièces d'Or. Vos
              gains cependant devront se limiter à 40 Pièces d'Or
              maximum. Vous pouvez jouer aussi longtemps que vous
              voulez, jusqu'à ce que vous ayez perdu tout votre or ou
              que vous décidiez d'emporter vos gains (40 Couronnes
              maximum).

              <p/>
              <Roulette r={r}/>

              Si vous avez perdu tout votre or, <L enabled={g===0}
              to="169">rendez-vous au 169</L>. Si vous décidez de
              partir avec vos gains ou l'or qui vous reste, quittez la
              maison de jeu et <L to="186">rendez-vous au 186</L>.


            </div>
        );
    }
}

export default P238;
