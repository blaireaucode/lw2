/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P204 extends Component {
    render() {
        return (
            <div>

              L'exceptionnelle acuité visuelle que vous avez acquise
              au cours de votre entraînement à la Discipline de
              l'Orientation vous permet de distinguer nettement le
              talisman fixé à l'extrémité du bâton noir. C'est
              l'emblème de la Guilde des Magiciens de Toran : un
              croissant et une étoile de cristal. Cet homme est un
              renégat qui a trahi tout à la fois la Guilde et votre
              patrie. Si vous souhaitez monter en haut de cette tour
              pour attaquer le magicien félon, <L to="73">rendez-vous
              au 73</L>. Si vous ne voulez pas risquer votre vie en
              affrontant ce puissant sorcier, sautez par-dessus bord
              et <L to="267">rendez-vous au 267</L>.

            </div>
        );
    }
}

export default P204;
