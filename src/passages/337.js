/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P337 extends Component {

    constructor(props) {
        super(props);
        this.f = {
            name: 'a',
            passage_title: 337,
            already_done: false
        };
        this.f1 = {
            'name': 'b',
            'passage_title': 337,
            'already_done': false
        };
    }

    go() {
        const elem = bh.book_get_element(this.props, this.f);
        if (elem.already_done) return;
        const items = this.props.character.items;
        var c = JSON.parse(JSON.stringify(this.props.character));
        for (var item of items) {
            if (item.id === 'PO') continue;
            if (item.id === 'sac_a_dos') continue;
            const desc = bh.item_get_description(item.id);
            if (desc.special === true) continue;
            c = bh.item_drop(c, item.id, true);     
        }
        this.props.character_set(c);
        bh.update_done(this, 337, 'a');
    }
    
    endu() {
        const elem = bh.book_get_element(this.props, this.f1);
        if (elem.already_done) return;
        bh.update_done(this, 337, 'b');
        const c = bh.endurance_add(this.props.character, -3);
        this.props.character_set(c);
    }
    
    render() {
        const elem = bh.book_get_element(this.props, this.f);
        const done = elem.already_done;
        const d = bh.has_discipline(this.props.character, "chasse");
        return (
            <div>

              Lorsque vous n'êtes plus qu'à une cinquantaine de mètres
              du rivage, vous vous laissez glisser dans l'eau et vous
              nagez vers la terre ferme.  Bientôt, vous atteignez
              enfin la plage ; vous êtes épuisé et vous vous traînez
              sur le sable jusqu'aux dunes qui s'élèvent un peu plus
              loin et à l'abri desquelles vous pouvez reprendre
              haleine. En plus de la fatigue, la faim vous tenaille
              mais il vous faut d'abord faire l'inventaire de ce qui
              vous reste. Vous avez réussi à conserver vos Pièces
              d'Or, votre Sac à Dos et les Objets Spéciaux dont vous
              n'avez pas été contraint de vous séparer au cours de la
              tempête. Vos armes en revanche sont perdues.  <L
              enabled={!done} onClick={this.go.bind(this)}
              to='337'>Modifiez votre Feuille d'Aventure</L> en
              conséquence et prenez quelques minutes de repos.

              <p/>

              Vous vous relevez ensuite et vous parcourez à pied
              quelques centaines de mètres. Là, vous trouvez de petits
              arbres aux branches contournées qui portent des fruits
              violets. Si vous souhaitez manger ces fruits, <L
              enabled={done} to="228"> rendez-vous au 228</L>. Si vous
              préférez ne pas les manger, vous perdrez 3 points
              d'ENDURANCE avant de <L enabled={done}
              onClick={this.endu.bind(this)} to='171'>vous rendre au
              171</L>. Enfin, si vous maîtrisez la Discipline Kaï de
              la Chasse, <L enabled={done && d} to="139">
              rendez-vous au 139</L>.

            </div>
        );
    }
}

export default P337;
