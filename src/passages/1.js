/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P1 extends Component {
    render() {
        return (
            <div>

              L'homme désigne un navire marchand qui mouille dans le port, près
              du quai d'embarquement. C'est une caravelle de belle apparence,
              soigneusement entretenue. «&nbsp;<i>Le capitaine en second s'appelle
              Ronan, </i>poursuit le conducteur,<i> il vous attend de l'autre côté de
              la place, à l'auberge du Joyeux Drille </i>».  Puis l'homme vous salue
              et disparaît dans la foule en se faufilant avec son chariot dans
              les rues étroites de la ville.

              <p/>

              Vous traversez la place mais lorsque vous parvenez devant
              l'auberge, vous constatez que la porte en est fermée à clé et que
              les volets des fenêtres sont clos. Tandis que vous vous demandez
              ce qu'il convient de faire, une main vous agrippe le bras et vous
              pousse dans un coin obscur. Si vous désirez dégainer votre arme et
              combattre l'inconnu qui vous a ainsi agressé, <L
              to="273"> rendez-vous au 273</L>. Si vous préférez
              essayer d'échapper à son emprise en vous débattant, <L
              to="160"> rendez-vous au 160</L>.

              <p/>
              <SimpleImage width={200} src={'1.png'}/>

            </div>
        );
    }
}

export default P1;
