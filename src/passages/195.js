/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';
import HelpGold from '../HelpGold.js';

class P195 extends Component {

    pay() {
        const d = bh.get_done(this, 195);
        if (!d) {
            const c = bh.gold_add(this.props.character, -1);
            this.props.character_set(c);
        }
        bh.update_done(this, 195);
    }
    
    render() {
        var g = bh.gold_get(this.props.character);
        return (
            <div>

              Au bout d'une heure de voyage, le cocher annonce:
              «&nbsp;Pont à péage, une Couronne par personne. » Vous
              jetez un coup d'œil par la portière : la pluie tombe à
              verse mais vous parvenez malgré tout à distinguer au
              loin un pont de bois et une cabane en rondins. Un peu
              plus tard, le cocher arrête la diligence devant la
              cabane et une créature repoussante apparaît à la
              porte. C'est un Squall à la peau couverte de
              verrues. Les Squalls appartiennent à la famille des
              Gloks mais ce sont des êtres peureux et inoffensifs. Ils
              habitaient le Pays Sauvage au temps de la Lune Noire,
              lorsque des milliers d'entre eux émigrèrent, abandonnant
              les Monts Durncrag, pour échapper à la tyrannie de
              Vashna, le plus puissant des Maîtres des Ténèbres. Le
              Squall demande à chaque passager de la diligence de
              payer une Couronne le droit de franchir le pont. Vos
              compagnons de voyage déposent chacun une Couronne sur
              une petite assiette qu'ils vous tendent ensuite. Si vous
              avez de quoi payer votre passage, donnez une Couronne à
              votre tour et poursuivez votre route en <L enabled={g>0}
              onClick={this.pay.bind(this)} to='249'>vous rendant au
              249</L>. Si vous n'avez pas d'argent, <L
              to="50">rendez-vous au 50</L>.

              <p/>
              <HelpGold/>
              
            </div>
        );
    }
}

export default P195;

