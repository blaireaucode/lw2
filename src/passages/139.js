/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P139 extends Component {

    constructor(props) {
        super(props);
        const f = {
            'name': 'obj1',
            'passage_title': 139,
            'list': [ {id:'repas', nb:2} ],
        };
        this.items = f;
    }

    render() {
        return (
            <div>

              L'entraînement que vous avez suivi dans l'art de la
              chasse vous permet de reconnaître les fruits comestibles
              ou vénéneux qui poussent dans les régions
              septentrionales de Magnamund. Ces fruits violets sont
              des Larnumes. C'est là un mets de choix, sucré et
              nourrissant. Vous en mangez à satiété et vous en faites
              provision pour l'équivalent de 2 repas. Conservez-les
              dans votre Sac à Dos. Au-delà des larnumiers, les arbres
              qui portent ces fruits, vous distinguez une large route
              qui suit la côte en menant, au choix, vers l'est ou vers
              l'ouest. Si vous voulez aller vers l'est, <L
              to="27">rendez-vous au 27</L>. Si vous préférez prendre
              la direction de l'ouest, <L to="114">rendez-vous au
              114</L>.

              <p/>
              <ItemsPicker items={this.items}/>

            </div>
        );
    }
}

export default P139;
