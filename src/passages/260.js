/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P260 extends Component {
    render() {
        const f = {
            passage_title: 260,
            list: [ {id:'epee'} ]
        };
        return (
            <div>

              Le capitaine ordonne qu'on mette le cap sur les trois
              hommes et qu'on les hisse à bord. Ce sont des pêcheurs
              de Tyso, un port du Sommerlund.  Leur bateau a été
              attaqué par des pirates la nuit précédente et ils sont
              les seuls survivants. Vous leur donnez à manger et des
              vêtements chauds ; les trois hommes alors retiennent
              leurs larmes à grand peine et l'un d'eux vous fait
              présent d'une magnifique Epée en témoignage de sa
              reconnaissance. Si vous souhaitez accepter ce cadeau,
              n'oubliez pas de l'inscrire sur votre Feuille
              d'Aventure. <L to='240'>Rendez-vous ensuite au 240</L>.

              <p/>
              <ItemsPicker items={f}/>
              
            </div>
        );
    }
}

export default P260;
