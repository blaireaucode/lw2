/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P245 extends Component {
    render() {
        return (
            <div>

              Vous prenez la direction de l'est en longeant la rue du
              Col Vert et vous remarquez bientôt, à votre gauche, une
              enseigne accrochée au-dessus de la porte d'une petite
              boutique ; elle porte ces mots : MEKI MAJENOR MAÎTRE ARMURIER.

              <p/>
              <SimpleImage width={250} src={'245.png'}/>
              <p/>

              Si vous souhaitez entrer dans cette boutique, <L
              to="266">rendez-vous au 266</L>. Si vous préférez
              poursuivre votre chemin vers l'est, <L
              to="310">rendez-vous au 310</L>.

            </div>
        );
    }
}

export default P245;
