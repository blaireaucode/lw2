/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P121 extends Component {
    render() {
        return (
            <div>

              Vous courez le long de la rue de la Vigie et vous
              atteignez bientôt le quai, là où le fleuve Dorn sépare
              les parties Est et Ouest de la ville. A votre gauche,
              vous apercevez le pont de Ragadorn, un ouvrage d'une
              grande laideur dont le fer a rouillé et qui constitue le
              seul point de passage entre les deux moitiés de
              Ragadorn. Les cris des voleurs retentissent encore à vos
              oreilles tandis que vous vous frayez un chemin parmi la
              foule qui encombre le pont. Mais, lorsque vous êtes
              parvenu de l'autre côté, les voleurs ont abandonné la
              poursuite et vous vous engagez dans une large avenue qui
              porte le nom de boulevard du Commerce, section Est. <L
              to="186">Rendez-vous au 186</L>.

            </div>
        );
    }
}

export default P121;
