/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import SimpleImage from '../SimpleImage.js';

class P350 extends Component {
    render() {
        return (
            <div>

              La ville de Holmgard a beaucoup souffert depuis votre
              départ. Le long des quais, nombre de maisons et de
              boutiques ne sont plus que cendres désormais. L'armée
              maléfique des Maîtres des Ténèbres encercle les
              murailles et leurs effroyables machines de guerre
              maintiennent la cité sous un déluge de feu qui déchire
              la nuit sans relâche. Les habitants épuisés et affamés
              combattent du mieux qu'ils peuvent les incendies qui se
              déclarent un peu partout dans la ville sous l'effet des
              projectiles enflammés. Lorsqu'elle entre dans le port,
              la flotte durenoraise est tout d'abord accueillie par
              des cris de désespoir; les assiégés ont cru en effet
              qu'il s'agissait là de vaisseaux ennemis venus en
              renfort. Mais, lorsque les premiers soldats descendent
              sur le quai en déployant l'étendard de Durenor, la
              nouvelle a tôt fait de se répandre de l'arrivée des
              alliés. Les cris de désespoir se changent alors en
              hurlements de joie. «&nbsp;Le Seigneur Kaï est de retour
              !&nbsp;» s'exclame-t-on bientôt dans toute la capitale.

              <p/>

              Vous vous tenez debout au sommet d'une haute tour, qui
              défend la plus grande porte de la ville lorsque les
              premières lueurs de l'aube naissent à l'horizon. Des
              milliers et des milliers d'ennemis aux uniformes noirs
              sont massés autour des murs de la cité, grouillant comme
              des cancrelats le long des tranchées qui sillonnent la
              plaine Au milieu de cette horde, une tente rouge a été
              dressée qui porte l'emblème de Zagarna, Seigneur de
              Kaag, l'un des Maîtres des Ténèbres, venu
              d'Heldegad. L'emblème représente un crâne
              fracassé. Zagarna a pour ambition de détruire Holmgard
              et il souhaite plus que tout conduire son armée à la
              victoire sur la Maison d'Ulnar pour se proclamer ensuite
              roi du Sommerlund.

              <p/>

              Mais la victoire ne sera pas pour aujourd'hui, car
              bientôt vous levez au-dessus de votre tête le Glaive de
              Sommer : au même instant, un rayon de soleil vient se
              refléter sur la pointe de l'épée d'or et un
              jaillissement de flammes blanches et aveuglantes
              parcourt toute la longueur de sa lame. La puissance du
              Glaive vous emplit d'une fantastique énergie.  Tout
              votre corps s'anime, vous vous sentez frémir de la tête
              au pied et d'un geste ample vous abaissez l'Arme
              fantastique en pointant sa lame sur la tente de
              Zagarna. Dans un formidable roulement de tonnerre, un
              rayon blanc jaillit alors de l'épée magique et vient
              frapper la tente qui explose dans une tempête de feu, un
              champignon enflammé s'élevant jusqu'au ciel. Un
              épouvantable hurlement retentit aussitôt dont l'écho
              semble déchirer les nuées: c'est Zagarna, le Maître des
              Ténèbres, qui vient de succomber sous la vengeance du
              Glaive. Saisis de terreur, les soldats aux uniformes
              noirs se lèvent des tranchées et se précipitent en
              déroute loin des murs de Holmgard. L'impossible est
              survenu : leur chef invincible a été terrassé. Le Glaive
              de Sommer est revenu chasser l'envahisseur, et l'armée
              du Sommerlund aidée de ses alliés de Durenor se lance
              sans attendre à la poursuite des ennemis défaits qui
              courent aveuglément en direction des Monts Durncrag ; le
              triomphe est total : Holmgard est libérée et vos frères
              Kaï vengés.

              <p/>
              <SimpleImage width={700} src={'350.png'}/>
              <p/>

              Votre vie d'aventures, cependant, ne fait que commencer,
              car un nouveau défi vous attend, vous et le Glaive de
              Sommer, dans le troisième volume de la série du Loup
              Solitaire : LES GROTTES DE KALTE

              <p/>FIN.

            </div>
        );
    }
}

export default P350;
