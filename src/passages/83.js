/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P83 extends Component {
    render() {
        return (
            <div>

              Au bout de la rue de la Bernicle, vous arrivez à un
              croisement. Il fait déjà nuit à présent et il va bientôt
              falloir vous trouver un abri. Si vous souhaitez tourner
              à gauche, dans la rue du Tonnelier, <L
              to='227'>rendez-vous au 227</L>. Si vous préférez
              prendre à droite la rue de la Licorne, <L
              to='297'>rendez-vous au 297</L>.

            </div>
        );
    }
}

export default P83;
