/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import update from 'immutability-helper';
import SimpleImage from '../SimpleImage.js';
import HelpGold from '../HelpGold.js';
import * as bh from '../book_helpers.js';

class P55 extends Component {

    constructor(props) {
        super(props);
        this.f = {
            'name': 'a',
            'passage_title': '55',
            'already_done': false
        };
        this.state={help:''};
    }

    handleClick() {
        if (!this.props.options.items_nolimit)  {
            const n = bh.item_get_nb_of_weapon(this.props.character);
            if (n>=2) {
                this.setState({...this.state, help:'(Vous ne pouvez pas avoir plus de 2 armes)'});
                return;
            }
        }        
        const item = bh.item_get_description('glaive');
        var c = bh.item_pick(this.props.character, item);
        c = bh.gold_add(c, -12);        
        this.props.character_set(c);
        const elem = bh.book_get_element(this.props, this.f);
        const el = update(elem, {already_done: {$set:true}});
        this.props.book_set_element(el);
    }
    
    render() {
        const elem = bh.book_get_element(this.props, this.f);
        const g = bh.gold_get(this.props.character);
        const e = !elem.already_done && g>=12;
        var after = '';
        if (elem.already_done) 
            after = (<span className='Book-help'> Vous avez acquis le glaive.</span>);
        const help = (<span className='Book-help'> {this.state.help} </span>);
        
        return (
            <div>

              Un homme de haute taille vêtu d'un tablier de cuir est
              en train d'aiguiser un glaive de belle
              apparence. L'homme est assis devant une meule qui
              projette des gerbes d'étincelles chaque fois que la lame
              de l'épée entre en contact avec la pierre. Le forgeron
              vous souhaite le bonsoir et vous offre le
              glaive. «&nbsp;Cest une belle lame, dit-il, forgée dans
              un pur acier de Durenor. Pour douze Couronnes, elle est
              à vous. » Si vous souhaitez <L to='55' enabled={e}
              onClick={this.handleClick.bind(this)}> acheter ce
              glaive</L>, inscrivez-le sur votre Feuille d'Aventure et
              soustrayez douze Couronnes de votre total de Pièces
              d'Or.

              <p/><HelpGold/><p/>{after} {help}

              <p/>

              Si vous décidez de quitter la boutique par la porte
              principale, <L to="347">rendez-vous au 347</L>. Si vous
              préférez passer par la porte de derrière, <L
              to="3">rendez-vous au 3</L>.

              <p/>
              <SimpleImage width={250} src={'55.png'}/>


            </div>
        );
    }
}

export default P55;
