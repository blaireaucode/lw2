/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P307 extends Component {
    render() {
        const l = this.props.character.items.some(e => e.id === 'sceau_hammardal');        
        return (
            <div>

            Les soldats se montrent menaçants et prêts à attaquer. Il
            vous faut prendre une décision rapide. Si vous voulez
            essayer de les corrompre en leur offrant de l'or, <L
            to="57">rendez-vous au 57</L>. Si vous préférez leur
            montrer le Sceau d'Hammardal (en admettant qu'il soit
            toujours en votre possession), <L enabled={l}
            to="140">rendez-vous au 140</L>. Si vous estimez enfin
            qu'il vaut mieux dégainer votre arme et les combattre, <L
            to="282">rendez-vous au 282</L>.

            </div>
        );
    }
}

export default P307;
