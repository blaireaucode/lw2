/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Dead from '../Dead.js';

class P213 extends Component {
    render() {
        return (
            <div>

              Vous grimpez au sommet d'une meule de foin et vous vous
              emmitouflez dans votre cape de Seigneur Kaï pour vous
              protéger du vent frisquet. Vous vous endormez alors sans
              vous douter le moins du monde que vous ne vous
              réveillerez plus jamais. En effet, l'un de vos
              compagnons de voyage est un agent des Maîtres des
              Ténèbres et, dans la fraîcheur de la nuit, il vient
              silencieusement vous assassiner sans même que vous vous
              en rendiez compte. Votre quête s'achève donc ici en même
              temps que votre vie.

              <p/>
              <Dead/>

            </div>
        );
    }
}

export default P213;
