/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P323 extends Component {
    render() {
        return (
            <div>

              Vous longez cette rue sordide qui, bientôt, tourne
              brusquement vers l'est pour aboutir dans la rue de la
              Vigie. Au loin, vous apercevez les eaux du Fleuve Dorn
              qui sépare les parties Est et Ouest de Ragadorn.  Vous
              poursuivez votre chemin sous la pluie battante lorsque
              trois hommes d'allure louche surgissent soudain d'une
              ruelle et vous emboîtent le pas. Si vous souhaitez
              interrompre votre marche et affronter ces trois
              individus, <L to="131">rendez-vous au 131</L>. Si vous
              préférez continuer à marcher, <L to="298">rendez-vous au
              298</L>. Enfin, si vous estimez plus judicieux de courir
              en direction du fleuve, <L to="121">rendez-vous au
              121</L>.

            </div>
        );
    }
}

export default P323;
