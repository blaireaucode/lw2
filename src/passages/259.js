/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P259 extends Component {
    render() {
        return (
            <div>

              A travers la pluie qui tombe à verse, vous parvenez à
              distinguer la silhouette d'un groupe de soldats qui
              s'avancent dans votre direction.  Vous ne voulez pas
              prendre le risque d'être interpellé et peut-être arrêté,
              vous décidez donc de vous réfugier dans une boutique
              proche. <L to="161">Rendez-vous au 161</L>.

            </div>
        );
    }
}

export default P259;
