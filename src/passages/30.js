/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';


import * as bh from '../book_helpers.js';

class P30 extends Component {

    constructor(props) {
        super(props);
        const f = {
            'name': 'f1',
            'passage_title': '30',
            'encounter': "4 ZOMBIES",
            'combat_skill':13,
            'endurance':16,
            'multiple':true,
            'bonus_factor_dmg_encounter': 2,
            'no_bonus_psy': true
        };
        this.f = f;
    }

    render() {
        const r = bh.enabled_if_fight_win(this.props, this.f);
        return (
            <div>

              Vous tombez sur les planches moisies du pont et vous
              passez au travers pour atterrir enfin au fond d'une
              cale. Vous êtes indemne, mais la puanteur dans laquelle
              baigne le navire est insupportable. Vous vous remettez
              sur pied et vous dégainez le Glaive de Sommer. Quatre
              ZOMBIES aux allures de fantômes sortent alors de l'ombre
              d'un pas chancelant en tendant vers votre gorge des
              mains noueuses et décharnées. Il vous faut les combattre
              en les considérant comme un seul et même ennemi. Ce sont
              des morts vivants, et la puissance du Glaive de Sommer
              vous permet de multiplier par deux tous les points
              d'ENDURANCE qu'ils perdront au cours du combat. Ils sont
              cependant insensibles à la Discipline Kaï de la
              Puissance Psychique. Si vous êtes vainqueur, <L
              enabled={r} to="258">rendez-vous au 258</L>.

              <p/>
              <Fight fight={this.f}/>

            </div>
        );
    }
}

// LES ZOMBIES HABILETÉ: 13 ENDURANCE: 16 
export default P30;
