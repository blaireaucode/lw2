/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P113 extends Component {
    render() {
        return (
            <div>

              Lorsque Banedon vous a donné le Pendentif à l'Etoile de
              Cristal, il vous a parlé de cet homme&nbsp;: c'est Vonotar le
              Traître - un sorcier renégat de la Guilde des Magiciens
              de Toran. Il est passé maître dans l'art de la magie
              noire et les Maîtres des Ténèbres en personne l'ont
              investi d'un grand pouvoir. Ce sont ses agents qui ont
              essayé de vous tuer au cours de votre mission et c'est
              lui qui commande la flotte des vaisseaux fantômes. Si
              vous anéantissez Vonotar, vous anéantirez par là même la
              force maléfique qui donne son pouvoir à la flotte des
              bateaux fantômes et à son équipage. Vous pouvez grimper
              en haut de la tour et attaquer Vonotar <L to="73">en
              vous rendant au 73</L>. Si vous préférez ne pas risquer
              votre vie en vous mesurant à ce puissant magicien, fuyez
              ce navire en sautant par-dessus bord et <L
              to="267">rendez-vous au 267</L>.

            </div>
        );
    }
}

export default P113;
