/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Dead from '../Dead.js';

class P304 extends Component {
    render() {
        return (
            <div>

              Vous éprouverez peut-être quelque consolation en
              apprenant que votre mort a été quasiment instantanée. En
              quelques secondes, les doigts du Monstre d'Enfer vous
              ont déchiré la gorge et le Sceau d'Hammardal ne tardera
              pas à parvenir à Helgedad, la ville des Maîtres des
              Ténèbres.  Votre mission s'achève ici en même temps que
              votre vie.

              <Dead/>

            </div>
        );
    }
}

export default P304;
