/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';
import HelpCharacter from '../HelpCharacter.js';
import EnduranceChange from '../EnduranceChange.js';
import SimpleImage from '../SimpleImage.js';

class P240 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, "guerison");
        const end1 = (this.props.character.endurance_max-this.props.character.endurance);
        const e1 = bh.get_element(this, 240, { already_done:false, dmg:end1 }, 'g1');
        const end2 = Math.ceil(end1/2);
        const e2 = bh.get_element(this, 240, { already_done:false, dmg:end2 }, 'g2');
        const done = (d && e1.already_done) ||(!d && e2.already_done);
        return (
            <div>

              Après trois jours en mer durant lesquels il ne s'est
              rien passé, vous commencez à trouver le temps long. Si
              vous maîtrisez la Discipline Kaï de la Guérison, vous
              pouvez récupérer tous les points d'ENDURANCE que vous
              avez éventuellement perdus depuis le début de votre
              aventure.  Vous retrouverez dans ce cas le
              <EnduranceChange enabled={d} f={e1}> total
              d'ENDURANCE</EnduranceChange> dont vous disposiez au
              départ. Si vous ne maîtrisez pas cette Discipline, vous
              ne récupérerez que <EnduranceChange enabled={!d} f={e2}> la moitié des
              points d'ENDURANCE</EnduranceChange> perdus (arrondissez
              au chiffre supérieur si le nombre à diviser par deux est
              impair).

              <p/>
              <HelpCharacter/>
              <p/>

              Dans l'après-midi du quatrième jour, vous êtes sur le
              pont du navire en train de bavarder avec un homme
              d'équipage lorsqu'une odeur de brûlé se dégage soudain
              d'une des cales. Si vous souhaitez descendre dans cette
              cale, <L enabled={done} to="29">rendez-vous au 29</L>. Si vous pensez
              qu'il est préférable de crier «&nbsp;Au feu !&nbsp;», <L
              enabled={done} to="236">rendez-vous au 236</L>. Enfin, si vous décidez
              plutôt d'aller prévenir le capitaine, <L enabled={done} 
              to="101">rendez-vous au 101</L>.

              <p/>
              <SimpleImage width={200} src={'240.png'}/>
              
            </div>
        );
    }
}

export default P240;
