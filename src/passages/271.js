/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P271 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, "camouflage");
        return (
            <div>

              Vous entrez dans la tour et vous montez un escalier de
              pierre ; soudain, un garde vêtu d'une armure surgit
              devant vous. Il est coiffé d'un heaume et il porte un
              écusson gravé d'un vaisseau noir et d'une crête
              rouge. Il s'avance vers vous et tire son
              épée. «&nbsp;Halte-là ! lance-t-il, donnez le mot de
              passe !&nbsp;» Si vous maîtrisez la Discipline Kaï du
              Camouflage, <L enabled={d} to="151">rendez-vous au
              151</L>. Si vous souhaitez l'attaquer, <L
              to="157">rendez-vous au 157</L>. Si vous préférez
              prendre la fuite en vous précipitant hors de la tour, <L
              to="65">rendez-vous au 65</L>.

            </div>
        );
    }
}

export default P271;
