/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import * as bh from '../book_helpers.js';

class P183 extends Component {
    constructor(props) {
        super(props);
        this.r = {
            passage_title: 183,
            name: 'random_table',
            dice_value: -1
        };
    }

    render() {
        const randtable = bh.book_get_element(this.props, this.r);
        const r = randtable.dice_value;
        return (
            <div>

              Au bord du terrain où vous avez établi votre camp, la
              forêt descend en pente raide ; dans votre hâte, vous
              trébuchez et vous tombez tête la première parmi les
              arbres. Utilisez la <RandomTable p={this.r}/> pour
              obtenir un chiffre. Si vous tirez un chiffre entre 0 et
              8, <L enabled={r>=0 && r<=8} to="311">rendez-vous au
              311</L>. Si le chiffre obtenu est un 9, <L
              enabled={r===9} to="159">rendez-vous au 159</L>.

              <CurrentDice r={r}/>              
            </div>
        );
    }
}

export default P183;
