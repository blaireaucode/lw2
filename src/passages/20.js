/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P20 extends Component {
    render() {
        return (
            <div>

              Cette rue infestée de rats descend en pente raide en
              direction des docks et des embarcadères du Fleuve
              Dorn. Lorsque vous parvenez sur le quai, vous apercevez
              le pont de Ragadorn, le seul et unique lieu de passage
              qui relie les rives est et ouest de ce port
              sordide. Vous vous frayez un chemin parmi la foule qui
              se presse sur le pont et vous empruntez une avenue au
              sol jonché d'ordures. Elle porte le nom de Boulevard du
              Commerce, section Est. <L to="186">Rendez-vous au
              186</L>.

            </div>
        );
    }
}

export default P20;
