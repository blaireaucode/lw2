/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P36 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, 'guerison');
        const l = this.props.character.items.some(e => e.id === 'herbe_laumspur');        
        return (
            <div>

              Cette nourriture vous semble délicieuse et en quelques
              minutes, vous avez vidé votre assiette. Vous décidez
              alors de faire un petit somme avant d'aller rejoindre
              les autres au bar. Mais comme vous vous apprêtez à vous
              étendre, une terrible douleur vous tord soudain
              l'estomac. Vos jambes se dérobent et vous vous écroulez
              sur le sol, le corps saisi de tremblements. Vous avez
              l'impression qu'un feu vous brûle tout entier. Un mot
              vous revient sans cesse à l'esprit, tournant dans votre
              tête d'une manière lancinante:
              poison... poison... poison... Si vous avez de l'herbe de
              Laumspur avec vous, <L to="145" enabled={l}>rendez-vous
              au 145</L>. Si vous maîtrisez la Discipline Kaï de la
              Guérison, <L to="210" enabled={d}>rendez-vous au
              210</L>. Si vous n'avez ni l'un ni l'autre, <L
              to="275">rendez-vous au 275</L>.

            </div>
        );
    }
}

export default P36;
