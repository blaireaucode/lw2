/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P48 extends Component {
    render() {
        return (
            <div>

              Vous désignez du doigt une chope de bière posée sur le
              bar et vous demandez au marin de l'observer
              attentivement. Vous fermez alors les yeux et vous vous
              concentrez jusqu'à ce que l'image de la chope se forme
              dans votre esprit. Sous l'effet de votre volonté, la
              chope s'élève bientôt dans les airs sous le regard
              médusé de votre interlocuteur.  <L to="268">Rendez-vous
              au 268</L>.

            </div>
        );
    }
}

export default P48;
