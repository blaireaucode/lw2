/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P18 extends Component {
    render() {
        return (
            <div>

              Un peu plus loin, la rue est complètement bloquée par des chariots
              que l'on décharge pour transborder la marchandise sur un navire de
              commerce. Vous poursuivez votre chemin ; la rue tourne bientôt
              vers l'est pour aboutir à la rue du Col Vert. A votre gauche, vous
              remarquez une autre entrée par laquelle on peut pénétrer dans le
              magasin de Ragadorn. Au-delà se trouve une petite boutique avec
              cette enseigne accrochée au-dessus de la porte :

              <p/>
              <SimpleImage width={320} src={'18.png'}/>
              <p/>

              Si vous souhaitez entrer dans le magasin de Ragadorn, <L
              to='173'>rendez-vous au 173</L>. Si vous préférez pénétrer dans la
              boutique de l'armurier, <L to="266">rendez-vous au 266</L>. Si
              enfin vous décidez plutôt de poursuivre votre chemin en direction
              de l'est, <L to="310">rendez-vous au 310</L>.

            </div>
        );
    }
}

export default P18;
