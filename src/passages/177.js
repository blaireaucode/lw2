/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P177 extends Component {
    render() {
        return (
            <div>

              Lorsque vous entrez à nouveau dans la taverne, vous
              voyez les marins rassemblés autour d'une table où se
              déroule une partie de bras de fer. Si vous souhaitez
              vous aussi engager une partie de bras de fer, <L
              to="276">rendez-vous au 276</L>. Si vous préférez parler
              à l'aubergiste, <L to="342">rendez-vous au 342</L>.

            </div>
        );
    }
}

export default P177;
