/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P107 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, "guerison");
        return (
            <div>

              Le capitaine donne l'ordre d'aborder le navire marchand
              et une vision d'horreur s'offre alors à vous : des
              cadavres de marins jonchent le pont, nombre d'entre eux
              ont le corps percé de flèches et il semble qu'ils ont dû
              livrer un combat désespéré pour sauver leur cargaison;
              les cales du navire sont vides, cependant, tout a été
              emporté. En descendant sur le pont inférieur, vous
              découvrez le capitaine dans sa cabine ; il est
              grièvement blessé et sa fin est proche. Si vous
              maîtrisez la Discipline Kaï de la Guérison, <L
              enabled={d} to="74">rendez-vous au 74</L>. Sinon, <L
              to="294">rendez-vous au 294</L>.

            </div>
        );
    }
}

export default P107;
