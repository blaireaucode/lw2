/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Fight from '../Fight.js';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';
import * as bh from '../book_helpers.js';

class P146 extends Component {
    
    constructor(props) {
        super(props);
        const f = {
            'name': 'f1',
            'passage_title': 146,
            'encounter': "GLOKS",
            'combat_skill':15,
            'endurance':15
        };
        this.f = f;
    }
    
    render() {
        const r = bh.enabled_if_fight_win(this.props, this.f);
        return (
            <div>

             Vous aviez raison. Ce nuage est formé par une nuée
             d'énormes Bêtalzans et de Kraans, une espèce plus petite,
             mais tout aussi mortelle.  Pendant sous leur ventre noir,
             ils tiennent dans leurs serres d'immenses filets dans
             lesquels s'entassent des GLOKS. Les Bêtalzans fondent
             alors sur le Sceptre Vert et un filet rempli de Gloks
             hurlants s'écrase sur le pont.

              <p/>
              <SimpleImage width={150} src={'146.png'}/>
              <p/>

              Certains n'ont pas survécu à la chute mais la plupart
              sont indemnes et vous attaquent sans tarder. Il vous
              faut les combattre en les considérant comme un seul et
              même ennemi.  Si vous êtes vainqueur, <L enabled={r}
              to="345">rendez-vous au 345</L>.

              <p/>
              <Fight fight={this.f}/>

            </div>
        );
    }
}

// GLOKS HABILETÉ: 15 ENDURANCE: 15
export default P146;
