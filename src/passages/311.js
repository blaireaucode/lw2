/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P311 extends Component {
    render() {
        return (
            <div>

              Vous tombez à plat ventre dans un enchevêtrement
              d'épaisses fougères tandis que résonnent à vos oreilles
              le cliquetis des épées et les cris terrifiants des
              Monstres d'Enfer. Vous êtes à moitié assommé et vous ne
              pouvez plus faire un geste. Enfin, une main vous saisit
              le bras et vous remet debout d'un geste vigoureux. C'est
              le Lieutenant Général Rhygar, le visage ensanglanté, son
              armure bosselée et noircie. «&nbsp;Il faut fuir ces démons !
              s'exclame-t-il, la force de nos épées ne peut rien
              contre eux.&nbsp;» Vous apercevez alors les silhouettes de
              six Monstres d'Enfer occupés à anéantir par leur seule
              Puissance Psychique les malheureux soldats du Lieutenant
              Général. Or, tandis qu'ils se concentrent ainsi, vous
              parvenez à vous échapper, Rhygar et vous, en vous
              glissant dans les broussailles pour atteindre l'abri de
              la forêt. <L to="299">Rendez-vous au 299</L>.

            </div>
        );
    }
}

export default P311;
