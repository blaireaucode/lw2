/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P134 extends Component {
    
    render() {
        const l = this.props.character.items.some(e => e.id === 'magical_spear');        

        return (
            <div>

              Un cri à vous glacer le sang jaillit tout à coup de
              l'obscurité et vous vous retrouvez face à un Monstre
              d'Enfer aux yeux étincelants. Ses mains vous attrapent à
              la gorge et il essaie de vous étrangler; dans un
              hurlement de terreur, vous tombez à terre : l'immonde
              créature déchire alors votre tunique de ses doigts noirs
              aux griffes crochues. Si vous possédez une Lance
              Magique, <L enabled={l} to="38">rendez-vous au
              38</L>. Sinon, <L enabled={!l} to="304">rendez-vous au
              304</L>.

              <p/>
              <SimpleImage width={250} src={'134.png'}/>

            </div>
        );
    }
}

export default P134;
