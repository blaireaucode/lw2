/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Fight from '../Fight.js';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P157 extends Component {
    
    constructor(props) {
        super(props);
        const f = {
            'name': 'f1',
            'passage_title': 157,
            'encounter': "GARDE",
            'combat_skill':15,
            'endurance':22
        };
        this.f = f;
    }
    
    render() {
        const r = bh.enabled_if_fight_win(this.props, this.f);
        return (
            <div>

            Le GARDE est furieux et il se précipite sur vous en
            dévalant l'escalier, son épée levée au-dessus de sa
            tête. Si vous ne possédez pas d'arme, réduisez de 4 points
            votre total d'HABILETÉ et battez-vous à mains nues.  Vous
            avez le droit de prendre la fuite à tout moment <L
            to='65'>en vous rendant au 65</L>. Si vous êtes vainqueur,
            <L enabled={r} to="331">rendez-vous au 331</L>.

              <p/>
              <Fight fight={this.f}/>

            </div>
        );
    }
}

// GARDE HABILETÉ: 15 ENDURANCE 22 

export default P157;
