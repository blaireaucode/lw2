/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P103 extends Component {

    constructor(props) {
        super(props);
        const f = {
            'name': 'obj1',
            'passage_title': 103,
            'list': [ {id:'herbe_laumspur'} ],
        };
        this.items = f;
    }
    render() {
        return (
            <div>

              Le Laumspur est une herbe délicieuse très recherchée
              d'un bout à l'autre des Fins de Terre en raison de ses
              vertus curatives. Vous en avez ramassé l'équivalent d'un
              Repas et ce Repas vous rendra 3 points d'ENDURANCE
              lorsque vous le prendrez (inscrivez sur votre Feuille
              d'Aventure votre moisson de Laumspur).

              <p/>
              <ItemsPicker items={this.items}/>
              <p/>
              
              Vous enveloppez soigneusement l'herbe que vous venez de
              ramasser et vous retournez dans la diligence en
              compagnie des autres voyageurs. <L to='249'>Rendez-vous
              au 249</L>.

            </div>
        );
    }
}

export default P103;
