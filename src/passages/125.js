/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P125 extends Component {
    render() {
        return (
            <div>

              Vous vous précipitez par la porte latérale de la taverne
              et vous courez tout au long d'une ruelle qui aboutit à
              la place principale. Vous apercevez, au-delà de la foule
              qui se presse en tous sens, de nombreux bateaux amarrés
              aux quais. Les brigands vous suivent de près et il vous
              faut agir vite, sinon ils vous tueront comme ils ont
              sans doute tué Ronan. Vous défaites alors l'amarre d'un
              canot puis vous sautez du quai et vous atterrissez
              lourdement dans l'embarcation, en fracassant dans votre
              chute le petit siège de bois aménagé au milieu. Vous
              trouvez une rame au fond du canot et vous pagayez ferme
              pour rejoindre le Sceptre Vert qui mouille à 300 mètres
              de là. <L to="300">Rendez-vous au 300</L>.

            </div>
        );
    }
}

export default P125;
