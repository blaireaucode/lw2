/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import * as bh from '../book_helpers.js';

class P105 extends Component {

    constructor(props) {
        super(props);
        this.r = {
            passage_title: 105,
            name: 'random_table',
            dice_value: -1
        };
    }

    render() {
        const randtable = bh.book_get_element(this.props, this.r);
        const r = randtable.dice_value;
        return (
            <div>

              Malheureusement pour vous, la corde a été presque coupée
              en deux par un coup d'épée et il faut craindre qu'elle
              ne casse sous votre poids.  Utilisez la <RandomTable
              p={this.r}> pour obtenir un chiffre qui vous indiquera
              si la corde a tenu bon ou pas</RandomTable>. Si vous
              tirez un chiffre entre 0 et 4, <L enabled={r>=0 &&
              r<=4}to="286">rendez-vous au 286</L>. De 5 à 9, <L
              to="120" enabled={r>=5}>rendez-vous au 120</L>.

              <CurrentDice r={r}/>
              
            </div>
        );
    }
}

export default P105;
