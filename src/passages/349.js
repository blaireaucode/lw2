/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P349 extends Component {
    render() {
        return (
            <div>

              Vous avez parcouru environ 5 km lorsque vous apercevez
              au loin une rangée de chariots. Ils ont été placés en
              travers de la chaussée pour interdire le passage, et des
              soldats en uniforme rouge ont pris position sur les
              toits de chaque véhicule. Une foule considérable est
              rassemblée derrière ce barrage et vous entendez la
              rumeur de conversations animées, répercutées en écho le
              long du tunnel. Tandis que vous vous approchez, le
              silence se fait soudain et tous les regards se tournent
              dans votre direction. Un détachement d'une dizaine de
              soldats mené par un chevalier qui porte un écusson
              frappé aux armes du royaume de Durenor s'avance alors
              vers vous. Si vous souhaitez attaquer ces soldats, <L
              to="87">rendez-vous au 87</L>. Si vous préférez lever
              les mains en continuant de vous approcher d'eux, <L
              to="284">rendez-vous au 284</L>.

            </div>
        );
    }
}

export default P349;
