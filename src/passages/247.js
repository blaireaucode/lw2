/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Dead from '../Dead.js';

class P247 extends Component {
    render() {
        return (
            <div>

              Vous êtes comme hypnotisé par ce mât qui tombe sur vous
              et vous n'avez même plus la force de faire un geste. Le
              capitaine et ses hommes d'équipage, impuissants à vous
              porter secours, voient avec horreur l'énorme masse de
              bois s'écraser sur vous. La mort est instantanée.  Votre
              mission s'achève ici en même temps que votre vie.

              <p/>
              <Dead/>          

            </div>
        );
    }
}

export default P247;
