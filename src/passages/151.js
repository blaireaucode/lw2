/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import * as bh from '../book_helpers.js';

class P151 extends Component {

    constructor(props) {
        super(props);
        this.r = {
            passage_title: 151,
            name: 'random_table',
            dice_value: -1
        };
    }

    render() {
        const randtable = bh.book_get_element(this.props, this.r);
        const r = randtable.dice_value;
        return (
            <div>

              Vous utilisez votre technique du camouflage pour imiter
              l'accent rocailleux des habitants de Ragadorn et vous
              essayez de faire croire au soldat qu'une bagarre a
              éclaté dans la rue du Tombeau. Vous affirmez que les
              gardes de la ville ont été submergés par le noinlue ri
              qu'il doit immédiatement courir à leur secours. Vous
              saurez si votre mensonge a réussi en utilisant la&nbsp;
              <RandomTable p={this.r}> pour obtenir un
              chiffre</RandomTable>. Si vous tirez un chiffre entre 0
              et 4, <L enabled={r>=0 && r<=4} to="262">rendez-vous au
              262</L>. Si vous tirez un chiffre entre 5 et 9, <L
              to="110" enabled={r>=5 && r<=9} >rendez-vous au 110</L>.

              <CurrentDice r={r}/>

            </div>
        );
    }
}

export default P151;
