/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P305 extends Component {
    render() {
        const f = {
            'name': 'obj1',
            'passage_title': 305,
            'list': [ {id:'PO', nb:5} ],
        };
        return (
            <div>

              Un silence pesant s'installe dans la taverne, il en faut
              davantage cependant pour vous impressionner et c'est
              avec le plus grand calme que vous ramassez les 5 Pièces
              d'Or posées sur la table.

              <p/>
              <ItemsPicker items={f}/>
              <p/>

              Vous vous dirigez ensuite vers
              la porte, mais, au moment où vous allez sortir, un marin
              d'une laideur repoussante vous bloque le passage en
              brandissant une épée. Un instant plus tard, alors que
              vous vous demandez ce qu'il convient de faire, un coup
              sourd résonne dans le silence de la salle et l'homme
              tombe à genoux sur le plancher. Vous avez la surprise de
              reconnaître, debout derrière lui, la servante qui tient
              fermement des deux mains une grosse massue de bois. Vous
              la remerciez d'un sourire complice, mais le temps n'est
              pas aux effusions et vous vous hâtez de disparaître dans
              l'ombre de la rue, tandis qu'à l'intérieur de la taverne
              des voix s'élèvent pour vous maudire. Après avoir couru
              pendant dix minutes dans le noir, vous apercevez un peu
              plus loin une grande écurie et un relais de diligence ;
              derrière vous retentissent des cris de marins furieux
              qui vous poursuivent dans la rue : pour leur échapper,
              vous montez quatre à quatre une échelle extérieure qui
              mène à un grenier.  Là, vous pourrez passer la nuit en
              toute sécurité, blotti parmi des bottes de foin. <L
              to="32">Rendez-vous au 32</L>.


            </div>
        );
    }
}

export default P305;
