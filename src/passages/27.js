/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import HelpCharacter from '../HelpCharacter.js';
import EnduranceChange from '../EnduranceChange.js';

class P27 extends Component {

    constructor(props) {
        super(props);
        const f = {
            'name': 'a',
            'passage_title': 27,
            'already_done': false,
            'dmg': 2
        };
        this.f = f;
    }
   
    render() {
        return (
            <div>

              Vous marchez pendant plus de trois heures le long de la
              route déserte qui suit la côte. Lorsque enfin la nuit
              tombe, vous êtes épuisé et vous décidez de prendre
              quelque repos jusqu'à l'aube. Vous vous remettrez alors
              en chemin. Mais bientôt, certains récits que les Maîtres
              Kaï vous ont faits vous reviennent en mémoire : il y
              était question du Pays Sauvage qui s'étend entre le
              Sommerlund et Durenor ; la nuit, des hordes de chiens
              féroces parcourent ces terres désolées, en quête de
              nourriture. Le souvenir de ces contes vous incite à la
              prudence et vous décidez de passer la nuit à l'abri d'un
              grand arbre au feuillage touffu, planté au bord du
              chemin. Vous prenez là un repos réparateur qui vous rend
              <EnduranceChange f={this.f}> 2 points
              d'ENDURANCE</EnduranceChange> (si tant est que vous en
              ayez perdu). Rendez-vous ensuite au <L to='312'>312</L>.

              <p/>
              <HelpCharacter/>

            </div>
        );
    }
}

export default P27;
