/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Fight from '../Fight.js';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P128 extends Component {

    constructor(props) {
        super(props);
        const f = {
            name: 'f1',
            passage_title: 128,
            encounter: "Six zombies",
            combat_skill:13,
            endurance:19,
            no_bonus_psy: true,
            bonus_factor_dmg_encounter: 2,
            multiple: true
        };
        this.f = f;
    }

    render() {
        const r = bh.enabled_if_fight_win(this.props, this.f);
        return (
            <div>

              Une lueur dorée parcourt la lame du Glaive lorsque vous
              le levez au-dessus de votre tête pour faire face à
              l'ennemi. Vous êtes attaqué par six ZOMBIES terrifiants
              que vous devez combattre en les considérant comme un
              seul et même adversaire. Ce sont des morts vivants et la
              puissance du Glaive de Sommer vous permet donc de
              multiplier par deux tous les point d'ENDURANCE qu'ils
              perdront au cours du combat. Ils restent cependant
              insensibles à la Discipline Kaï de la Puissance
              Psychique. Si vous êtes vainqueur, <L to="237"
              enabled={r}>rendez-vous au 237</L>.

              <p/>
              <Fight fight={this.f}/>

            </div>
        );
    }
}

// LES ZOMBIES HABILETÉ : 13 ENDURANCE : 19
export default P128;
