/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import * as bh from '../book_helpers.js';


class P197 extends Component {

    constructor(props) {
        super(props);
        this.r = {
            passage_title: 197,
            name: 'random_table',
            dice_value: -1
        };
    }

    render() {
        const randtable = bh.book_get_element(this.props, this.r);
        const r = randtable.dice_value;
        return (
            <div>

              Lorsque l'aube paraît, une terrible tempête se lève sur
              h mer et vous êtes réveillé par le violent roulis du
              navire. Le plancher de votre cabine est inondé cm li s
              hurlements du vent laissent à peine percevoir de temps à
              autre les cris de l'équipage. Vous vous habillez en
              hâte, vous rassemblez vos affaires et vous montez sur le
              pont. Le capitaine vous rejoint bientôt ; il vous prend
              par le bras et vous donne l'ordre de retourner dans
              votre cabine. Vous revenez donc sur vos pas, mais
              soudain un craquement effroyable retentit ; vous levez
              la tête : la partie supérieure du grand mât vient de se
              rompre dans la tourmente et tombe droit sur
              vous. Utilisez la <RandomTable p={this.r}>pour obtenir
              un chiffre</RandomTable>. Si vous tirez un chiffre entre
              1 et 4, <L enabled={r>=1 && r<=4} to="78">rendez-vous au
              78</L>. Entre 5 et 9, <L enabled={r>=5 && r<=9}
              to="141">rendez-vous au 141</L>. Enfin, si vous tirez le
              0, <L to="247" enabled={r===0} >rendez-vous au 247</L>.

              <CurrentDice r={r}/>
            </div>
        );
    }
}

export default P197;
