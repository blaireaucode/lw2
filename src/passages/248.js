/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Dead from '../Dead.js';

class P248 extends Component {
    render() {
        return (
            <div>

              Lorsque vous posez le Glaive d'Or sur le pont, le
              capitaine zombie se rue sur vous et vous projette à
              terre. Il est animé d'une force surnaturelle, impossible
              d'échapper à son étreinte ; il vous plonge alors un
              poignard dans la gorge en éclatant d'un rire
              terrifiant. Votre quête s'achève ici en même temps que
              votre vie.

              <p/>
              <Dead/>          

            </div>
        );
    }
}

export default P248;
