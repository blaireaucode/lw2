/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P208 extends Component {
    render() {
        return (
            <div>

              Vous passez devant le chariot et vous entendez soudain
              un bruit, juste derrière vous. Vous faites volte-face en
              observant attentivement les parois du tunnel, mais il
              fait trop sombre pour distinguer quoi que ce soit. <L
              to="134">Rendez-vous au 134</L>.

            </div>
        );
    }
}

export default P208;
