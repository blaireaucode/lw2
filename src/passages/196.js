/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P196 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, "sixieme_sens");
        return (
            <div>

              Le roi Alin IV est assis, seul, dans sa tour surmontée
              d'un dôme et contemple les montagnes à travers une haute
              fenêtre aux vitres de couleur. Un huissier vous annonce,
              Lord Axim et vous-même, puis vous pénétrez dans la
              Chambre Royale en vous inclinant respectueusement devant
              Sa Majesté. Lord Axim retire alors le Sceau d'Hammardal
              de votre doigt et s'approche du roi. Tous deux
              s'entretiennent pendant presque une heure, leur visage
              soucieux exprimant toute la gravité de la
              situation.

              <p/>

              Enfin, après un bref silence, le roi Alin se lève
              soudain de son trône et, pour la première fois, vous
              adresse la parole. «&nbsp;Hélas, dit-il, les Maîtres des
              Ténèbres se sont levés à nouveau et, à nouveau, le
              Royaume du Sommerlund vient demander notre aide. J'ai
              longtemps prié le ciel que mon règne soit placé sous le
              signe de la paix et de l'harmonie, mais au fond de mon
              cœur, j'avais malheureusement la certitude qu'il en
              serait autrement.&nbsp;» Le roi tire alors d'une poche
              de sa pelisse blanche une clé d'or qu'il introduit dans
              la serrure d'un coffre de marbre posé sur une estrade au
              centre de la pièce. Un faible bourdonnement s'élève
              aussitôt, tandis que le couvercle du coffre glisse
              latéralement, laissant apparaître le pommeau d'une épée
              en or massif.  «&nbsp;Prend ce glaive, Loup Solitaire,
              commande le roi, car il est dit que seul un vrai fils du
              Sommerlund saura révéler la puissance qui se cache dans
              sa lame.&nbsp;» Lorsque vous empoignez le pommeau
              étincelant, un frémissement vous parcourt le bras puis
              se répand dans tout votre corps. Si vous maîtrisez la
              Discipline Kaï du Sixième Sens, <L enabled={d}
              to="79">rendez-vous au 79</L>. Dans le cas contraire, <L
              enabled={!d} to="123">rendez-vous au 123</L>.

            </div>
        );
    }
}

export default P196;
