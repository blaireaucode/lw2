/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P279 extends Component {

    render() {
        return (
            <div>

              Cette créature est un Noudic. Les Noudics sont des êtres
              doués d'intelligence qui vivent dans un dédale de puits
              et de couloirs creusés au sein des Monts d'Hammardal. De
              tempérament malicieux, ils subsistent en volant de la
              nourriture dans les chariots des marchands qui
              empruntent le tunnel de Tarnalin.  Les Noudics
              pourraient peut-être vous renseigner sur la présence
              éventuelle de Monstres d'Enfer cachés dans le tunnel. Si
              vous souhaitez suivre cet animal, <L to="23">rendez-vous
              au 23</L>. Si vous préférez le laisser filer et
              poursuivre votre chemin, <L to="340">rendez-vous au
              340</L>.

            </div>
        );
    }
}

export default P279;
