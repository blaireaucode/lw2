/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P73 extends Component {
    render() {
        return (
            <div>

              
              L'escalade se révèle malaisée car vous n'avez qu'une
              seule main libre, l'autre tenant le pommeau du Glaive de
              Sommer. Finalement, vous parvenez quand même au sommet
              de la tour et vous vous hâtez d'enjamber le muret qui
              tient lieu de garde-fou. Vous vous apprêtez à sauter à
              l'intérieur de la tour et à passer à l'attaque
              lorsqu'une petite voix vous fige sur place&nbsp;:
              «&nbsp;Votre mort sera pour moi un spectacle tout à fait
              délectable, Loup Solitaire.&nbsp;»

              <p/>

              Vous apercevez alors le sorcier qui se tient dans le
              coin opposé de la tour, sa main gauche tendue vers
              vous. «&nbsp;Votre Mission a échoué, Loup Solitaire, dit-il&nbsp;;
              à présent, il faut songer à mourir.&nbsp;» Un éclair
              s'échappe aussitôt de sa main et une flamme orange
              jaillit en direction de votre visage. <L
              to="336">Rendez-vous au 336</L>.

              <p/>
              <SimpleImage width={250} src={'73.png'}/>


            </div>
        );
    }
}

export default P73;
