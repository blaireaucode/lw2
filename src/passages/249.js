/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P249 extends Component {
    render() {
        return (
            <div>

              Au cours de l'après-midi, vous bavardez avec vos
              compagnons de voyage tandis que la diligence file bon
              train. Au bout de quelques heures, vous avez appris
              beaucoup de choses à leur sujet. Les deux hommes assis
              face à vous sont frères. Ils se nomment Ganon et Dorier
              et ce sont des Chevaliers de l'Ordre de la Montagne
              Blanche, des guerriers du Royaume de Durenor, qui ont
              fait serment de protéger leur patrie contre les brigands
              du Pays Sauvage. Ils possèdent un château et des terres
              près de Port Bax. A côté d'eux est assis un certain
              Halvorc, marchand de son état. Il a le nez enflé et le
              visage couvert de bleus. Ce sont les gardes de Lachelan,
              le Suzerain de Ragadorn, qui l'ont mis dans cet état. A
              la suite d'un léger malentendu avec les autorités de la
              ville à propos de taxes portuaires, toute sa marchandise
              et la plus grande partie de son or lui ont été
              confisqués.

              <p/>
              <SimpleImage width={200} src={'249.png'}/>
              <p/>

              Près de la portière opposée est assis un moine du nom de
              Parsion, un compatriote du Sommerlund qui a traversé le
              Pays Sauvage en diligence pour se rendre à Port Bax. La
              jeune femme assise à côté de vous a pour nom Viveka.
              C'est une aventurière mercenaire qui gagne son or les
              armes à la main en vendant ses services au plus
              offrant. Elle retourne à Port Bax avec en poche le prix
              de ses derniers exploits, accomplis victorieusement dans
              la ville de Ragadorn. Quant à vous, vous n'avez
              nullement l'intention de révéler votre véritable
              identité et vous vous êtes fait passer pour un simple
              paysan. Les passagers de la diligence semblent tout
              ignorer de la guerre qui ravage le Royaume du
              Sommerlund.  <L to="39">Rendez-vous au 39</L>.

            </div>
        );
    }
}

export default P249;
