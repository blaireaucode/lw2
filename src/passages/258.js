/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P258 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, "sixieme_sens");
        return (
            <div>

              La puanteur que dégage le navire vous étouffe à
              demi. Vous perdez 1 point d'ENDURANCE et il vous faut à
              tout prix vous échapper de cette cale répugnante où vous
              finirez par succomber à la pestilence. Si vous voulez
              essayer de vous hisser sur le pont, <L
              to="17"> rendez-vous au 17</L>. Si vous préférez quitter
              la cale par la porte aménagée dans la cloison opposée,
              <L to="5"> rendez-vous au 5</L>. Enfin, si vous maîtrisez
              la Discipline Kaï du Sixième Sens, <L enabled={d}
              to="272">rendez-vous au 272</L>.

            </div>
        );
    }
}

export default P258;
