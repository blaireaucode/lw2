/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P98 extends Component {
    render() {
        return (
            <div>

              Votre Sens de l'Orientation vous indique qu'il n'y a
              aucun sentier dans cette partie de la forêt de Durenor
              mais il vous permet de savoir quelle direction il
              convient de prendre. La forêt qui s'étend devant vous
              est si dense cependant qu'il vous sera impossible de la
              traverser à cheval.  Vous allez donc être contraint
              d'abandonner votre monture devant la tour de guet, avant
              de poursuivre votre chemin. Si vous souhaitez vous
              mettre en route en direction de Port Bax, <L
              to="244">rendez-vous au 244</L>. Si vous préférez entrer
              dans la tour de guet, <L to="115">rendez-vous au
              115</L>.

            </div>
        );
    }
}

export default P98;
