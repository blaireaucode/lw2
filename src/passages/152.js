/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import * as bh from '../book_helpers.js';

class P152 extends Component {

    constructor(props) {
        super(props);
        this.r = {
            passage_title: 152,
            name: 'random_table',
            dice_value: -1
        };
    }

    render() {
        const randtable = bh.book_get_element(this.props, this.r);
        const r = randtable.dice_value;
        return (
            <div>

              L'aube vient de se lever sur le 33e jour de votre quête
              lorsque vous entrez à cheval dans Port Bax en compagnie
              de Lord Axim. Les préparatifs de guerre sont achevés ;
              les vaisseaux de la flotte de Durenor mouillent dans le
              port, attendant l'ordre de mettre les voiles en
              direction du Sommerlund. A bord des navires, une
              puissante armée de soldats courageux et bien entraînés
              attend avec impatience d'affronter au combat les Maîtres
              des Ténèbres. Chacun de ces hommes a juré de libérer ses
              alliés assiégés par l'ennemi ou de mourir sur le champ
              de bataille.

              <p/>

              Vous-même prenez place à bord du vaisseau amiral
              Durenor, un grand navire à la proue arrondie et à la
              haute mâture dont la seule présence donne une impression
              de force imposante. L'amiral Calfen qui commande la
              flotte vous accueille sur le pont lorsque vous vous y
              présentez accompagné de Lord Axim. Il ne reste plus à
              présent qu'à donner l'ordre du départ. En moins d'une
              heure, les navires ont laissé le port loin derrière eux
              et les dômes de Port Bax ne sont plus que de simples
              points à l'horizon. Utilisez la <RandomTable p={this.r}>
              pour obtenir un chiffre</RandomTable>. Si vous tirez un
              chiffre entre 0 et 3, <L enabled={r>=0 && r<=3}
              to="216">rendez-vous au 216</L>. Entre 4 et 6, <L
              to="49" enabled={r>=4 && r<=6} >rendez-vous au
              49</L>. Entre 7 et 9, <L to="193" enabled={r>=7 && r<=9}
              >rendez-vous au 193</L>.

              <CurrentDice r={r}/>
            </div>
        );
    }
}

export default P152;
