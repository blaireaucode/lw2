/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import * as bh from '../book_helpers.js';

class P90 extends Component {
    constructor(props) {
        super(props);
        const f1 = {
            'name': 'f1',
            'passage_title': '90',
            'encounter': "1er SQUALL",
            'combat_skill': 6,
            'endurance':9,
            'continue': true
        };
        const f2 = {
            'name': 'f2',
            'passage_title': '90',
            'encounter': "1er VILLAGEOIS",
            'combat_skill': 10,
            'endurance':16
        };
        const f3 = {
            'name': 'f3',
            'passage_title': '90',
            'encounter': "2e VILLAGEOIS",
            'combat_skill': 11,
            'endurance':14
        };
        const f4 = {
            'name': 'f4',
            'passage_title': '90',
            'encounter': "2e SQUALL",
            'combat_skill': 5,
            'endurance':8
        };
        const f5 = {
            'name': 'f5',
            'passage_title': '90',
            'encounter': "3e VILLAGEOIS",
            'combat_skill': 11,
            'endurance':17
        };
        this.f = [];
        this.f.push(f2);
        this.f.push(f1);
        this.f.push(f3);
        this.f.push(f4);
        this.f.push(f5);
    }

    onContinue() {
        // needed to refresh
        // console.log('onContinue');
    }

    render() {
        var f = '';
        for (var fi of this.f) {
            const fight = bh.book_get_element(this.props, fi);
            const r = bh.enabled_if_fight_win(this.props, fi);
            if (!r) {
                f = fight;
                break;
            }
        }
        var cont = this.onContinue.bind(this);
        const r = (f === '');
        if (f ==='') {
            f = this.f[this.f.length-1];
            cont = false;
        }
        return (
            <div>

              Deux SQUALLS et trois VILLAGEOIS en colère montent les
              marches quatre à quatre, bien décidés à vous faire un
              mauvais sort. Il vous faut les combattre un par un.
              Vous pouvez à tout moment prendre la fuite en sautant
              par une fenêtre.  Dans ce cas, <L to="132">rendez-vous
              au 132</L>. Si vous parvenez à vaincre tous ces
              adversaires, <L enabled={r} to="274">rendez-vous au
              274</L>.

              <p/>
              <Fight fight={f} onContinue={cont}/>

            </div>
        );
    }
}

//HABILETÉ ENDURANCE
// 1er VILLAGEOIS 10 16
// 1er SQUALL 6 9
// 2e VILLAGEOIS 11 14
// 2e SQUALL 5 8
// 3e VILLAGEOIS 11 17
// <p/>
// 1er VILLAGEOIS H 10 E 16<br/>
// 1er SQUALL H 6 E 9<br/>
// 2e VILLAGEOIS H 11 E 14<br/>
// 2e SQUALL H 5 E 8<br/>
// 3e VILLAGEOIS H 11 E 17<br/>


export default P90;
