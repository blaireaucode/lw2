/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import HelpCharacter from '../HelpCharacter.js';
import EnduranceChange from '../EnduranceChange.js';
import * as bh from '../book_helpers.js';

class P29 extends Component {

    constructor(props) {
        super(props);
        const f = {
            'name': 'a',
            'passage_title': 29,
            'already_done': false,
            'dmg': -2
        };
        this.f = f;
    }

    render() {
        const r = bh.book_get_element(this.props, this.f).already_done;
        return (
            <div>

              Vous soulevez le loquet et vous faites glisser le
              panneau de l'écoutille. L'ouverture provoque un brusque
              appel d'air et des flammes jaillissent aussitôt de la
              cale. Vous reculez en titubant et en tenant à deux mains
              votre visage brûlé par le feu. Vous perdez
              <EnduranceChange f={this.f}> 2 points
              d'ENDURANCE</EnduranceChange>. « Au feu ! Au feu ! »
              crie alors une voix. L'équipage saisi de panique
              s'efforce d'éteindre les flammes mais il faut plus d'une
              heure pour venir à bout de l'incendie. Les dégâts sont
              considérables : c'est en effet dans la cale qui a pris
              feu qu'étaient entreposées les réserves d'eau douce et
              les vivres ; il n'en reste plus rien désormais. Mais,
              peut-être plus grave encore, l'incendie a sérieusement
              endommagé la structure même du navire. Tandis que vous
              examinez les dégâts, le capitaine s'approche de vous, le
              visage noirci par la fumée. Il porte un paquet sous son
              bras. «&nbsp;Il faut que je vous parle en privé, my
              lord&nbsp;», dit-il à voix basse. Sans rien répondre,
              vous vous tournez vers lui et vous le suivez jusqu'à sa
              cabine. <L to="222" enabled={r}>rendez-vous au 222</L>.

              <p/>
              <HelpCharacter/>

            </div>
        );
    }
}

export default P29;
