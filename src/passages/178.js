/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P178 extends Component {
    render() {
        return (
            <div>

              
            Bien que la délicieuse odeur de cette nourriture vous
            fasse saliver, vous soupçonnez quelque chose de louche et
            vous posez le plateau à terre, près de la porte. Vous êtes
            fatigué à force d'avoir faim et vous décidez de faire un
            somme avant d'aller rejoindre les autres au bar. Lorsque
            vous vous réveillez, vous apercevez les cadavres de deux
            rats étendus près du plateau : ils sont morts
            empoisonnés. Vous êtes alors saisi de fureur, car c'est à
            vous que cette nourriture était destinée. Vous vous hâtez
            de ramasser vos affaires et vous quittez la chambre, bien
            décidé à retrouver celui ou celle qui a tenté de vous
            assassiner. <L to='200'>Rendez-vous au 200</L>.

            </div>
        );
    }
}

export default P178;
