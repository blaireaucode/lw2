/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P222 extends Component {
    render() {
        return (
            <div>

              Après avoir soigneusement refermé la porte de sa cabine,
              le capitaine ouvre son mystérieux paquet et en répand le
              contenu sur une table. 11 s'agit d'une cruche de faïence
              noircie et de lambeaux d'étoffe calcinés qui dégagent
              une étrange odeur d'huile. «&nbsp;Cet incendie n'est pas
              un accident, déclare le capitaine Kelman d'une voix
              solennelle, c'est un acte de sabotage. Cette cruche
              d'huile et ces chiffons que j'ai trouvés sur le plancher
              de la cale n'avaient rien à y faire ; quelqu'un à bord
              de ce navire est prêt à risquer sa vie pour nous
              empêcher d'atteindre Durenor.&nbsp;» Vous contemplez tous
              deux les chiffons brûlés, comme s'ils pouvaient répondre
              aux questions que vous vous posez. Et soudain un cri
              retentit au-dessus de vos têtes, brisant le silence qui
              règne dans la cabine. «&nbsp;Navire en vue ! Navire en
              vue sur bâbord avant !&nbsp;» Le capitaine saisit aussitôt sa
              lunette d'approche et se hâte de monter sur le pont par
              une échelle d'écoutille. Si vous désirez le suivre, <L
              to="175">rendez-vous au 175</L>. Si vous préférez
              fouiller rapidement sa cabine, <L to="315">rendez-vous
              au 315</L>.

              <p/>
              <SimpleImage width={200} src={'222.png'}/>
              
            </div>
        );
    }
}

export default P222;
