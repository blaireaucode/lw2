/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P254 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, "sixieme_sens");
        return (
            <div>

              Pendant trois jours et trois nuits, vous avez suivi la
              route parallèle au fleuve Durenon, en direction de la
              capitale. La vallée où coule la rivière est une vaste
              région de riches terres cultivées adossées au flanc des
              Monts d'Hammardal, l'une des chaînes de montagnes les
              plus hautes de tout Magnamund. L'aube s'est levée sur le
              quatorzième jour de votre quête lorsque six hommes vêtus
              de capes apparaissent en bordure de votre camp. Le
              Lieutenant Général Rhygar est le premier à tirer son
              épée. D'une voix forte, il demande à ces hommes ce
              qu'ils viennent faire ici. Pour toute réponse, les six
              inconnus dégainent chacun une épée noire. Rhygar alors
              appelle ses hommes au combat tandis que les six
              étrangers vêtus de capes s'avancent vers nous. Si vous
              souhaitez à votre tour dégainer votre arme et vous
              préparer à combattre, <L to="69">rendez-vous au
              69</L>. Si vous ne voulez pas vous battre, vous pouvez
              prendre la fuite en courant vers la forêt ; <L to='183'>
              rendez-vous pour cela au 183</L>. Enfin, si vous
              maîtrisez la Discipline Kaï du Sixième Sens, <L
              enabled={d} to="344"> rendez-vous au 344</L>.

            </div>
        );
    }
}

export default P254;
