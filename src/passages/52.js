/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P52 extends Component {
    render() {
        const l = this.props.character.items.some(e => e.id === 'magical_spear');        
        return (         
            <div>

              Soudain un cri à vous glacer le sang retentit dans
              l'obscurité, au-dessus de vous. Vous levez la tête et
              vous apercevez deux lueurs rougeâtres : ce sont les yeux
              d'un Monstre d'Enfer qui descend les marches quatre à
              quatre pour se jeter sur vous. Vous hurlez de terreur en
              cherchant frénétiquement une arme pour vous défendre. Si
              vous possédez une Lance Magique, <L to="338"
              enabled={l}>rendez-vous au 338</L>. Sinon, <L
              to="234">rendez-vous au 234</L>.

            </div>
        );
    }
}

export default P52;
