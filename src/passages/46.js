/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P46 extends Component {
    render() {
        return (
            <div>

              Vous vous efforcez de vous rappeler la signification de
              cette porte orange, mais sans succès. Si vous décidez
              d'entrer dans la boutique, <L to="214">rendez-vous au
              214</L>. Si vous préférez poursuivre votre chemin, <L
              to="230">rendez-vous au 230</L>.

            </div>
        );
    }
}

export default P46;
