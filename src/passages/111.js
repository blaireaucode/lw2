/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P111 extends Component {
    render() {
        return (
            <div>

              A contrecœur, les gardes baissent les armes et vous
              autorisent à franchir le pont. Au moment où vous passez
              devant eux, ils vous fixent du regard puis se chuchotent
              quelques mots à l'oreille. Dès que vous avez franchi le
              chenal de Ryner, vous vous hâtez de poursuivre votre
              chemin, de peur qu'ils ne changent d'avis et vous
              arrêtent. Au bout d'une heure de marche sur la route qui
              traverse la forêt, vous arrivez à un croisement où un
              poteau de signalisation indique la direction de l'est :
              PORT BAX 5 km. Vous souriez et vous suivez la flèche :
              dans une heure tout au plus, vous devriez être arrivé à
              destination. <L to='265'>Rendez-vous au 265</L>.

            </div>
        );
    }
}

export default P111;
