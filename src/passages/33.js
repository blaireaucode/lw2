/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P33 extends Component {
    render() {
        return (
            <div>

              Les autres voyageurs contemplent d'un air horrifié et
              incrédule le résultat du combat que vous venez de
              livrer. Et avant que vous ayez pu fournir la moindre
              explication, la porte de l'auberge s'ouvre brusquement
              dans un grand fracas. Six soldats revêtus d'armures se
              précipitent à l'intérieur, conduits par l'aubergiste en
              personne. Les soldats sont des gardes de la ville et le
              tenancier borgne les exhorte à vous arrêter en poussant
              de grands cris. Si vous souhaitez affronter les soldats,
               <L to="296"> rendez-vous au 296</L>. Si vous préférez
              vous enfuir par la porte de derrière, <L
              to="88">rendez-vous au 88</L>.

            </div>
        );
    }
}

export default P33;
