/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import * as bh from '../book_helpers.js';

class P345 extends Component {
    render() {
        const f = {
            passage_title: 345,
            encounter: "DRAKKARIM",
            combat_skill: 16,
            endurance: 24
        };
        const w = bh.enabled_if_fight_win(this.props, f);
        return (
            <div>

              Une bataille féroce s'est engagée sur le navire tandis
              que les Gloks s'efforcent de prendre le contrôle du
              Sceptre Vert. Perchés en haut des mâts, les Kraans sont
              en train de déchirer les voiles avec leurs serres et
              leurs dents tranchantes comme des rasoirs ; pendant ce
              temps, les Bêtalzans retournent vers la Pointe des
              Naufragés pour aller remplir leurs filets d'autres Gloks
              avides de participer aux combats. Bientôt, une
              silhouette menaçante apparaît sur le pont jonché de
              cadavres. C'est un DRAKKARIM, un cruel guerrier à la
              solde des Maîtres des Ténèbres. Il taille en pièces
              quiconque se trouve sur son chemin, marin ou Glok, et
              s'avance vers vous en brandissant un glaive d'un noir de
              jais. Parvenu devant vous, il se lance à l'attaque et il
              vous faut le combattre jusqu'à la mort de l'un de vous
              deux.  Si vous êtes vainqueur, <L enabled={w}
              to="243">rendez-vous au 243</L>.

              <p/>
              <Fight fight={f}/>
              
            </div>
        );
    }
}

// DRAKKARIM HABILETÉ : 16 ENDURANCE : 24 
export default P345;
