/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P199 extends Component {
    render() {
        return (
            <div>

              «&nbsp;C'est très simple, dit alors l'aubergiste d'une
              voix moqueuse en empochant la Pièce d'Or, il vous suffit
              de mettre un pied devant l'autre.  Comme ça !&nbsp;»
              ajoute-t-il avec un rire sonore en se dirigeant vers la
              cuisine dans laquelle il disparaît bientôt. Vous
              maudissez la canaille et vous quittez aussitôt l'auberge
              en prenant le temps toutefois de renverser d'un coup de
              pied le seau d'eau sale. <L to="143">Rendez-vous au
              143</L>.

            </div>
        );
    }
}

export default P199;
