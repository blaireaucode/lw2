/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P135 extends Component {
    render() {
        return (
            <div>

              « Voici votre abri », dit le chevalier d'un ton bourru en
              montrant du doigt les bois qui s'étendent derrière
              vous. Avant que vous ayez pu répondre quoi que ce soit,
              il fait un pas en arrière et ferme à clé la lourde porte
              de la tour. La forêt qu'il vous a montrée est très
              dense; des herbes et des buissons d'épines
              s'enchevêtrent dans les sous-bois et il faut renoncer à
              y pénétrer à cheval. Il ne vous reste donc plus qu'à
              abandonner votre monture et à poursuivre votre route à
              pied. <L to="244">Rendez-vous au 244</L>.

            </div>
        );
    }
}

export default P135;
