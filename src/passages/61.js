/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P61 extends Component {
    render() {
        return (
            <div>

              La pluie tombe si dru qu'il vous est difficile de voir
              distinctement ; vous apercevez cependant les silhouettes
              sombres de gardes en patrouille qui s'avancent dans
              votre direction. S'ils vous arrêtaient pour vous
              demander ce que vous êtes venu faire à Ragadorn, vous
              pourriez bien finir dans l'une des nombreuses geôles de
              Lachelan le Suzerain. Il vaut mieux ne pas courir ce
              risque et vous décidez donc de battre en retraite le
              long de la rue du Chevalier Noir et de bifurquer le plus
              vite possible dans la rue du Sage ; votre tactique
              réussit et les gardes passent sans vous
              voir. <L to='181'>Rendez-vous à présent au 181</L>.

            </div>
        );
    }
}

export default P61;
