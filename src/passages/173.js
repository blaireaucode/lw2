/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P173 extends Component {
    render() {
        return (
            <div>

              Vous pénétrez dans un somptueux magasin où sont exposées
              les marchandises les plus raffinées qu'on puisse trouver
              au nord de Magnamund. Même à cette heure tardive,
              l'endroit est animé : des capitaines et de riches
              commerçants marchandent en effet l'achat ou l'échange de
              leurs denrées. Le propriétaire du magasin est un jeune
              guerrier qui préside aux enchères du haut d'un fauteuil
              de bois sculpté suspendu par quatre chaînes. Ces hommes
              sont tous vêtus d'armures noires et leurs boucliers
              portent pour emblème l'image d'un vaisseau noir surmonté
              d'une crête rouge. Vous surprenez alors un jeune garçon
              en train de voler la bourse accrochée à la ceinture d'un
              marchand. Son forfait accompli, le garnement glisse son
              butin dans sa botte. Si vous décidez d'attraper le
              garçon pour lui faire rendre la bourse, <L to='91'>rendez-vous au
              91</L>. Si vous préférez suivre le jeune homme au-dehors et
              lui dérober la bourse à votre tour, <L
              to="6">rendez-vous au 6</L>. Enfin, si vous choisissez
              de faire comme si vous n'aviez rien vu pour consacrer
              plutôt votre attention aux marchandises exposées, <L
              to="283">rendez-vous au 283</L>.

              <p/>
              <SimpleImage width={200} src={'173.png'}/>

            </div>
        );
    }
}

export default P173;
