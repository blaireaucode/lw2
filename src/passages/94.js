/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P94 extends Component {
    render() {
        return (
            <div>

              Vous insistez auprès du capitaine pour qu'on aille voir
              ce qui se passe à bord du bateau, mais il ignore votre
              demande et ordonne à ses hommes de poursuivre leurs
              tâches habituelles. Vous contemplez le navire marchand
              qui bientôt disparaît à l'horizon en vous demandant
              pourquoi le capitaine a refusé de faire quoi que ce
              soit, puis vous descendez dans la coursive et vous vous
              enfermez dans votre cabine en prenant bien soin de
              verrouiller la porte. <L to="240">Rendez-vous au
              240</L>.

            </div>
        );
    }
}

export default P94;
