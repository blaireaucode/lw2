/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P102 extends Component {

    render() {
        const d = bh.has_discipline(this.props.character, "orientation");
        return (
            <div>

              Le tunnel de Tarnalin est une véritable merveille. Il
              fait plus de 30 mètres de hauteur et de largeur et
              traverse les montagnes de la chaîne d'Hammardal en
              donnant accès à la capitale. D'ordinaire, chariots et
              piétons s'y pressent, car c'est la voie obligée entre
              Port Bax et Hammardal. Mais lorsque vous y pénétrez,
              vous avez la surprise de constater qu'il est désert ;
              vous n'y découvrez qu'une carriole de fruits renversée
              sur la chaussée. La route qui s'enfonce dans les
              profondeurs du tunnel éclairé par des torches est vide
              et silencieuse. Et tandis que vous avancez sur la
              chaussée recouverte de pavés, un sentiment d'inquiétude
              vous saisit : et si les Monstres d'Enfer étaient arrivés
              à Tarnalin avant vous ?  Vous marchez pendant une heure
              et vous arrivez alors à une bifurcation. Si vous
              souhaitez prendre la voie de gauche, <L
              to="64">rendez-vous au 64</L>. Si vous préférez aller à
              droite, <L to="164">rendez-vous au 164</L>.  Enfin, si
              vous maîtrisez la Discipline Kaï du Sens de
              l'Orientation, <L enabled={d} to="325">rendez-vous au
              325</L>.

            </div>
        );
    }
}

export default P102;
