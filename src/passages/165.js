/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P165 extends Component {
    render() {
        return (
            <div>

              Vous rangez l'or dans votre bourse, puis vous ôtez
              l'Anneau de votre doigt et vous le lui tendez. Elle vous
              le prend des mains et l'examine attentivement. Vous
              quittez ensuite la boutique mais au moment où vous
              franchissez la porte, vous l'entendez ricaner sous cape
              et vous vous demandez alors si vous avez bien fait
              d'agir ainsi. <L to='186'>Rendez-vous au 186</L>.

            </div>
        );
    }
}

export default P165;
