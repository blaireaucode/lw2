/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P76 extends Component {
    constructor(props) {
        super(props);
        const f = {
            'name': 'obj1',
            'passage_title': 76,
            'list': [
                {id:'poignard'},
                {id:'PO', nb:2},
            ],
        };
        this.items = f;
    }

    render() {
        return (
            <div>

              En fouillant ses longs vêtements tachés de sang, vous
              vous rendez compte avec un sentiment de malaise
              qu'aucune preuve ne permet d'affirmer que cet homme
              était bien celui qui cherchait à vous tuer.  Vous ne
              trouvez sur lui qu'un Poignard et 2 Pièces d'Or que vous
              pouvez vous approprier si vous le désirez. <L
              to="33">Rendez-vous au 33</L>.

              <p/>
              <ItemsPicker items={this.items}/>

            </div>
        );
    }
}

export default P76;
