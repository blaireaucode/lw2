/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P331 extends Component {
    render() {
        const f = {
            passage_title: 331,
            list: [ {id:'epee'},
                    {id:'poignard'},
                    {id:'PO', nb:3},
                  ],
        };
        return (
            <div>

              En fouillant le cadavre du soldat, vous découvrez une
              Epée, un Poignard et 3 Pièces d'Or. Vous pouvez garder
              l'une ou l'autre de ces trouvailles en modifiant en
              conséquence votre Feuille d'Aventure. Puis soudain, vous
              entendez le bruit de semelles cloutées qui descendent
              les marches de pierre de l'escalier. Vous levez alors la
              tête et vous apercevez un autre soldat à l'étage
              au-dessus. Vous vous précipitez aussitôt hors de la tour
              et vous prenez vos jambes à votre cou, tandis que le
              soldat vous abreuve d'injures. <L to="65">Rendez-vous au
              65</L>.

              <p/>
              <ItemsPicker items={f}/>              

            </div>
        );
    }
}

export default P331;
