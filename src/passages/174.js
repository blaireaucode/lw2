/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P174 extends Component {
    render() {
        return (
            <div>

              «Je n'ai encore jamais rencontré un paysan qui ait les
              moyens de s'acheter un cheval, dit le chevalier en
              s'avançant vers vous, vous n'êtes d'ailleurs sûrement
              pas un paysan, j'ai plutôt l'impression que vous êtes un
              voleur. » Puis, d'un coup de son épée, il vous
              désarçonne et vous tombez lourdement sur le
              sol. Instinctivement, vous tirez votre épée dans un
              geste de défense tandis que le chevalier vous
              attaque. <L to="162">Rendez-vous au 162</L>.

            </div>
        );
    }
}

export default P174;
