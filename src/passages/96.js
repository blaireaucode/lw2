/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P96 extends Component {
    render() {
        return (
            <div>

              Votre Sixième Sens vous indique que cet endroit est
              maléfique. Vous vous tenez devant la porte orange
              lorsque quelque chose vous revient soudain en
              mémoire. <L to="112">Rendez-vous au 112</L>.

            </div>
        );
    }
}

export default P96;
