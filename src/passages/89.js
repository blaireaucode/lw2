
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P89 extends Component {
    render() {
        return (
            <div>

              Au moment où vous sautez, le conducteur vous aperçoit et
              arrête aussitôt la diligence. Puis il se tourne vers
              vous, une épée à la main. Si vous souhaitez lui payer le
              prix d'un billet, <L to="233"> rendez-vous au 233</L>. Si
              vous préférez l'attaquer, <L to="212"> rendez-vous au
              212</L>.

            </div>
        );
    }
}

export default P89;
