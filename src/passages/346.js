
/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';
import update from 'immutability-helper';

class P346 extends Component {

    constructor(props) {
        super(props);
        this.f1 = {
            'name': 'a',
            'passage_title': 346,
            'already_done': false
        };
        this.f2 = {
            'name': 'b',
            'passage_title': 346,
            'already_done': false
        };
        this.f3 = {
            'name': 'c',
            'passage_title': 346,
            'already_done': false,
            'dmg':-3
        };
        this.f4 = {
            'name': 'd',
            'passage_title': 346,
            'already_done': false,
        };
    }

    pay_1() {
        const elem = bh.book_get_element(this.props, this.f1);
        if (!elem.already_done) {
            const e = update(elem, {already_done: {$set: true}});
            this.props.book_set_element(e);
            const c = bh.gold_add(this.props.character, -1);
            this.props.character_set(c);
        }        
    }

    pay_2() {
        const elem = bh.book_get_element(this.props, this.f2);
        if (!elem.already_done) {
            const e = update(elem, {already_done: {$set: true}});
            this.props.book_set_element(e);
            const c = bh.item_drop(this.props.character, 'repas');            
            this.props.character_set(c);
        }        
    }

    pay_3() {
        const elem = bh.book_get_element(this.props, this.f3);
        if (!elem.already_done) {
            const e = update(elem, {already_done: {$set: true}});
            this.props.book_set_element(e);
            const c = bh.endurance_add(this.props.character, -3);
            this.props.character_set(c);
        }        
    }

    pay_4() {
        const elem = bh.book_get_element(this.props, this.f4);
        if (!elem.already_done) {
            const e = update(elem, {already_done: {$set: true}});
            this.props.book_set_element(e);
            const c = bh.gold_add(this.props.character, -1);
            this.props.character_set(c);
        }        
    }

    render() {
        const index = bh.item_get_index(this.props.character.items, 'repas');
        const meal = (index !== -1);
        const g = bh.gold_get(this.props.character);
        const d1 = bh.get_done(this, 346, 'a');
        const d2 = bh.get_done(this, 346, 'b');
        const d3 = bh.get_done(this, 346, 'c');
        const s1 = d1 || d2 || d3;
        return (
            <div>

              Le cocher hoche la tête et vous rend le
              billet. L'auberge est bien chauffée, mais pauvrement
              meublée. Vous allez devoir prendre ici un repas qui <L
              to='346' enabled={!s1 && g>0}
              onClick={this.pay_1.bind(this)}> vous coûtera 1
              Couronne</L>, à moins que vous n'ayez <L to='346'
              enabled={!s1 && meal} onClick={this.pay_2.bind(this)}>
              de quoi manger dans votre Sac à Dos</L>. Si vous ne
              possédez ni or ni nourriture, vous <L enabled={!s1}
              to='346' onClick={this.pay_3.bind(this)}> perdez 3
              points d'ENDURANCE</L>. Si vous maîtrisez la Discipline
              Kaï de la Chasse, vous ne pourrez pas vous en servir
              tant que vous traverserez le Pays Sauvage, car c'est un
              désert entièrement aride où ne vivent que des Squalls,
              des créatures chétives et couardes apparentées aux Gloks
              et tout à fait impropres à la consommation. Si vous avez
              les moyens de vous offrir une chambre au prix de 1
              Couronne, <L enabled={s1 && g>0} to='280'
              onClick={this.pay_4.bind(this)}> rendez-vous au
              280</L>. Si vous n'avez pas d'argent, <L to="205"
              enabled={s1}> rendez-vous au 205</L>.

            </div>
        );
    }
}

export default P346;
