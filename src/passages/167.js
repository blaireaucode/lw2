/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P167 extends Component {
    render() {
        return (
            <div>

              «&nbsp;Votre stratégie ne manque pas d'audace, Loup Solitaire,
              mais je crois bien que je vais vous battre à présent&nbsp;»,
              lance soudain votre adversaire.  Le capitaine Kelman
              déplace alors une de ses pièces d'ivoire sculpté de
              votre côté du damier en arborant un sourire
              triomphant. Mais son sourire s'efface et une expression
              de contrariété apparaît sur son visage lorsque vous
              contre-attaquez d'une manière tout à fait inattendue.
              «&nbsp;Échec et mat&nbsp;», répliquez-vous d'une voix calme. Le
              capitaine contemple le damier d'un air
              incrédule. «&nbsp;Décidément, le talent des Seigneurs
              Kaï ne cessera jamais de m'étonner&nbsp;», dit-il en se
              grattant la tête. Il a toujours les yeux fixés sur le
              damier du Samor lorsque vous retournez dans votre cabine
              après lui avoir souhaité bonne nuit.  <L
              to="197">Rendez-vous au 197</L>.

            </div>
        );
    }
}

export default P167;
