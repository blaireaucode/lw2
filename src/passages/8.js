/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Dead from '../Dead.js';

class P8 extends Component {
    render() {
        return (
            <div>

              A la vérité, vous êtes plongé dans un sommeil si profond que vous ne
              vous réveillerez jamais plus car au cours de la nuit un Serpent des
              Sables vous a mordu et son venin mortel a eu raison de vous en
              quelques secondes. Votre mission s'achève ici, en même temps que
              votre vie.

              <Dead/>

            </div>
        );
    }
}

export default P8;
