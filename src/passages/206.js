/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P206 extends Component {
    render() {
        return (
            <div>

              Au cours de la nuit vous êtes réveillé par des loups qui
              hurlent au loin.  Vous préférez ne pas prendre le risque
              d'être dévoré pendant votre sommeil et vous montez donc
              dans l'arbre pour passer le reste de la nuit à l'abri de
              son feuillage, à bonne distance du sol. <L
              to="312">Rendez-vous au 312</L>.

            </div>
        );
    }
}

export default P206;
