/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import HelpGold from '../HelpGold.js';
import * as bh from '../book_helpers.js';

class P57 extends Component {

    constructor(props) {
        super(props);
        this.r = {
            passage_title: '57',
            name: 'random_table',
            dice_value: -1
        };
    }

    handleClick(r) {
        const randtable = bh.book_get_element(this.props, this.r);
        if (randtable.r === 0) r = 10;
        const c = bh.gold_add(this.props.character, -r);
        this.props.character_set(c);
    }
    
    render() {
        const randtable = bh.book_get_element(this.props, this.r);
        const r = randtable.dice_value;
        return (
            <div>

              Du dos de sa main gantée, l'un des gardes fait sauter de
              votre paume l'or que vous lui offrez et les pièces
              tombent dans les eaux sombres du chenal de
              Ryner. Utilisez la <RandomTable p={this.r}
              onClick={this.handleClick.bind(this)}/> pour savoir
              combien de pièces vous avez perdues, en remplaçant le
              zéro par un 10.

              <p/><HelpGold/>
              <CurrentDice r={r}/>
              <p/>

              «Nous n'allons pas vendre la sécurité de notre royaume à
              si vil prix, dit le garde, seul un brigand ou un
              imbécile songerait à corrompre un soldat de Durenor et
              j'ai bien l'impression que vous êtes les deux à la
              fois. » Vous avez eu le tort de porter atteinte à leur
              honneur et ils sont en train de vous donner une rude
              leçon. <L to="282" enabled={r!==-1}>Rendez-vous au
              282</L>.

            </div>
        );
    }
}

export default P57;
