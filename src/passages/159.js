/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Dead from '../Dead.js';

class P159 extends Component {
    render() {
        return (
            <div>

           Vous vous arrêtez enfin au pied d'un immense pin et vous
           essayez de vous maintenir debout, mais vous avez si mal aux
           jambes et au côté que vous tombez par terre en perdant
           connaissance. Ce sommeil vous épargne d'être pourfendu par
           le Monstre d'Enfer, mais il ne vous sauvera pas de la mort
           car plus jamais vous ne vous réveillerez. Votre quête
           s'achève ici en même temps que votre vie.

                <p/>
                <Dead/>

            </div>
        );
    }
}

export default P159;
