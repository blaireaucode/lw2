/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Cont from '../Cont.js';

class P0_0 extends Component {
    render() {
        return (
            <div>
              <b>Quarante jours pour un Loup Solitaire</b>

              <p/>

              Au nord du royaume du Sommerlund, il est de tradition depuis des
              siècles d'envoyer les fils des Seigneurs de la Guerre au monastère
              Kaï. C'est là qu'on leur enseigne l'art et la science de leurs
              nobles ancêtres. <p/>

              Au temps jadis, à l'époque de la Lune Noire, les Maîtres des
              Ténèbres menèrent une guerre sans merci contre le royaume du
              Sommerlund. Ce fut une longue et douloureuse épreuve de force à
              l'issue de laquelle les guerriers du Sommerlund remportèrent la
              victoire lors de la grande bataille de Maaken. Le roi Ulnar et ses
              alliés de Durenor anéantirent l'armée des Maîtres des Ténèbres
              dans le défilé de Moytura et précipitèrent l'ennemi au fond du
              gouffre de Maaken. Vashna, le plus puissant parmi les Maîtres des
              Ténèbres, périt d'un coup mortel que le roi Ulnar lui porta de sa
              puissante épée, l'épée du soleil, que l'on désigne généralement
              sous le nom de «Glaive de Sommer». Depuis ce temps, les Maîtres
              des Ténèbres ont juré de prendre leur revanche sur le royaume du
              Sommerlund et la Maison d'Ulnar.

              <p/>
                <Cont to='0_1'/>

            </div>
        );
    }
}

export default P0_0;
