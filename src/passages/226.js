/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';
import HelpGold from '../HelpGold.js';

class P226 extends Component {

    pay() {
        const d = bh.get_done(this, 226);
        if (!d) {
            const c = bh.gold_add(this.props.character, -2);
            this.props.character_set(c);
        }
        bh.update_done(this, 226);
    }
    
    render() {
        const g = bh.gold_get(this.props.character);
        return (
            <div>

              L'aubergiste s'exprime avec l'accent rocailleux des
              natifs de Ragadorn.  Il vous raconte que la ville est
              gouvernée par Lachelan, le fils de Killean le Suzerain
              qui a été emporté trois ans plus tôt par la peste
              rouge. Votre interlocuteur ne semble pas tenir Lachelan
              en grande estime, il le surnomme en effet le
              «&nbsp;Prince des Voleurs&nbsp;». «&nbsp;Lui et ses hommes
              saignent le peuple à blanc en levant de lourds impôts,
              vous explique-t-il, et si vous avez le malheur de vous
              en plaindre, vous êtes sûr de finir dans les eaux du
              port avec un poignard planté entre les deux épaules.&nbsp;»
              L'homme hoche la tête d'un air sombre et sert une autre
              tournée de bière aux marins ivres. Si vous souhaitez
              louer une chambre pour la nuit, donnez 2 Pièces d'Or à
              l'aubergiste et <L enabled={g>=2}
              onClick={this.pay.bind(this)} to="56">rendez-vous au
              56</L>. Si vous préférez essayer de gagner un peu d'or
              en engageant une partie de bras de fer, <L
              to="276">rendez-vous au 276</L>.

              <p/>
              <HelpGold/>

            </div>
        );
    }
}

export default P226;
