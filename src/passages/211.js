/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P211 extends Component {
    render() {
        return (
            <div>

              «&nbsp;Le consulat du Sommerlund ? » demande-t-il d'un
              air surpris, visiblement déconcerté par votre soudaine
              apparition. Puis, se reprenant : «&nbsp;Oh mais bien sûr
              !  s'exclame-t-il, c'est sur la place Alin, près du
              port. Prenez à droite en sortant et encore à droite au
              bout de l'avenue.  Vous arriverez alors à la porte
              Rouge. Il vous faudra un laissez-passer rouge pour
              entrer, car le consulat se trouve à l'intérieur du
              quartier maritime et la circulation y est
              réglementée.&nbsp;» Vous demandez à l'homme ce qu'il
              convient de faire pour obtenir un laissez-passer rouge.
              «&nbsp;On voit que vous êtes étranger, répond-il, tout
              le monde sait à Port Bax qu'il faut demander cela au
              capitaine de la tour du guet. La tour se trouve au bout
              de la rue, vous ne pouvez pas la manquer, vous tomberez
              dessus dès que vous aurez tourné le coin.&nbsp;» Vous
              remerciez le vieil homme et vous quittez l'hôtel de
              ville. <L to="191">Rendez-vous au 191</L>.

            </div>
        );
    }
}

export default P211;
