/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P188 extends Component {
    render() {
        return (
            <div>

              Votre présence d'esprit et votre adresse vous ont
              épargné une morsure fatale. Et tandis que le serpent
              disparaît dans les hautes herbes de l'autre côté de la
              route, vous ramassez vos affaires et vous grimpez à
              l'arbre dans le feuillage duquel vous passerez la nuit
              en toute sécurité. <L to="312">Rendez-vous au 312</L>.

            </div>
        );
    }
}

export default P188;
