/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P42 extends Component {
    render() {
        return (
            <div>
              
              Vous devinez que quelqu'un se cache au sommet de la
              falaise, juste au-dessus de vous ; vous sentez également
              que vous étiez la victime désignée de cet attentat. On
              veut vous tuer, vous en avez la certitude !  <L
              to="168">Rendez-vous au 168</L>.

            </div>
        );
    }
}

export default P42;
