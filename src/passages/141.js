/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import HelpCharacter from '../HelpCharacter.js';
import EnduranceChange from '../EnduranceChange.js';
import * as bh from '../book_helpers.js';

class P141 extends Component {

    constructor(props) {
        super(props);
        const f = {
            'name': 'a',
            'passage_title': 141,
            'already_done': false,
            'dmg': -2
        };
        this.f = f;
    }

    render() {
        const r = bh.book_get_element(this.props, this.f).already_done;
        return (
            <div>

              Le mât s'écrase sur le pont et un débris de bois vous
              frappe à la tête en vous jetant par-dessus bord. Vous
              vous débattez dans les vagues pour refaire surface puis
              vous vous agrippez à un panneau d'écoutille qui flotte à
              portée de main. A moitié assommé, vous perdez
              <EnduranceChange f={this.f}> 2 points
              d'ENDURANCE</EnduranceChange>. Vous vous hissez ensuite
              sur ce radeau de fortune en vous y cramponnant de toutes
              vos forces : si vous portez une cotte de mailles, il
              faut vous en débarrasser immédiatement, sinon, vous êtes
              sûr de périr noyé. Rayez-la de votre Feuille
              d'Aventure. Vous êtes soudain pris de vertige, vous vous
              sentez mal, et tandis que la forte houle vous ballotte
              en tous sens, vous sombrez peu à peu dans
              l'inconscience.  Lorsque vous vous réveillez au bout de
              plusieurs heures, la tempête s'est calmée. A en juger
              par la position du soleil, l'après-midi touche à sa
              fin. Au loin, vous apercevez un petit bateau de pêche et
              au-delà, un rivage qui se dessine à l'horizon. Il ne
              reste plus du Sceptre Vert que le panneau d'écoutille
              sur lequel vous êtes assis. Si vous voulez essayer de
              signaler votre présence au bateau de pêche en agitant
              votre cape, <L enabled={r} to="278">rendez-vous au
              278</L>. Si vous préférez ne pas vous occuper du bateau
              et tenter de rejoindre la côte en pagayant à l'aide de
              vos seules mains, <L enabled={r} to="337"> rendez-vous
              au 337</L>.

              <p/>
              <HelpCharacter/>

            </div>
        );
    }
}

export default P141;
