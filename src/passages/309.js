/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P309 extends Component {
    render() {
        return (
            <div>

              Lorsque vous avancez sur le pont, les hideuses créatures
              font volte-face et s'enfuient devant la clarté d'or de
              votre Glaive. Ce vaisseau fantôme vous semble alors
              étrange, il vous rappelle quelque chose de familier,
              mais vous ne savez pas quoi exactement. Puis soudain,
              une voix sépulcrale retentit derrière vous en vous
              appelant par votre nom. Vous vous retournez en
              brandissant le Glaive de Sommer et une vision
              terrifiante vous glace alors le sang. <L
              to="26">Rendez-vous au 26</L>.

            </div>
        );
    }
}

export default P309;
