/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P274 extends Component {
    render() {
        const f = {
            passage_title: 274,
            list: [ {id:'masse_d_armes'},
                    {id:'PO', nb:6}
                  ]
        };
        return (
            <div>

              En fouillant rapidement les corps, vous trouvez une
              Épée, 6 Pièces d'Or et une Masse d'Armes. Emportez ce
              que vous voulez le cas échéant, sans oublier de modifier
              en conséquence votre Feuille d'Aventure.  Avant que vous
              ayez eu le temps de sortir par la porte de devant,
              d'autres villageois furieux ont réussi à pénétrer dans
              la boutique et vous devez à présent vous enfuir par la
              fenêtre du premier étage. <L to="132">Rendez-vous au
              132</L>.

              <p/>
              <ItemsPicker items={f}/>

            </div>
        );
    }
}

export default P274;
