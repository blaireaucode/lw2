/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';


import * as bh from '../book_helpers.js';

class P66 extends Component {
    constructor(props) {
        super(props);
        const f = {
            'name': 'f1',
            'passage_title': '66',
            'encounter': "CAPITAINE ZOMBIE",
            'combat_skill':15,
            'endurance':15,
            'bonus_factor_dmg_encounter': 2,
            'no_bonus_psy': true
        };
        this.f = f;
    }

    render() {
        const r = bh.enabled_if_fight_win(this.props, this.f);
        return (
            <div>

              Lorsque vous levez la lame étincelante de votre glaive
              le CAPITAINE ZOMBIE tire de sa veste en lambeaux un
              poignard menaçant. Il vous faut le combattre jusqu'à la
              mort de l'un de vous deux.  La puissance du Glaive de
              Sommer vous permet de multiplier par 2 tous les points
              d'ENDURANCE que le capitaine perdra au cours de
              l'affrontement, mais votre adversaire est insensible à
              la Discipline Kaï de la Puissance Psychique. Si vous
              êtes vainqueur, <L to="218" enabled={r}>rendez-vous au
              218</L>.

              <p/>
              <Fight fight={this.f}/>

            </div>
        );
    }
}

// CAPITAINE ZOMBIE HABILETÉ : 15 ENDURANCE : 15 
export default P66;
