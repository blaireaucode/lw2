/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import update from 'immutability-helper';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import ItemsPicker from '../ItemsPicker.js';
import * as bh from '../book_helpers.js';

class P21 extends Component {

    constructor(props) {
        super(props);
        this.r = {
            passage_title: '21',
            name: 'random_table',
            dice_value: -1
        };
        this.i = {
            'passage_title': '21',
            'name': 'obj1',
            'list': [ {id:'poignard', nb:1} ],
            'already_done': false
        };
    }

    addGold(r) {
        if (r === -1) return;
        const elem = bh.book_get_element(this.props, this.i);
        if (elem.already_done) return;
        if (r === 0) r = 10;
        const n = r*3;
        const g = {id:'PO', nb:n };
        const new_list = bh.item_list_add(elem.list, g);
        const e = update(elem, {
            list: {$set:new_list},
            already_done: {$set:true}});
        this.props.book_set_element(e);
    }

    render() {
        const randtable = bh.book_get_element(this.props, this.r);
        const d = randtable.dice_value;
        return (
            <div>
              
              Le tricheur est étendu raide mort à vos pieds. Vous
              retournez son cadavre, puis vous ôtez des manches de sa
              veste plusieurs cartes qu'il cachait là. Vous les jetez
              sur la table et bientôt la foule se disperse dans la
              taverne qui retrouve instantanément son vacarme et son
              agitation habituels. Les autres joueurs de cartes
              récupèrent leur or et vous laissent ce qui reste. Pour
              savoir quelle quantité d'or vous revient, utilisez la {}
              <RandomTable p={this.r}
              onClick={this.addGold.bind(this)}/> en
              remplaçant exceptionnellement le zéro par le chiffre
              10. Lorsque la Table vous aura donné un chiffre, vous le
              multiplierez par 3 ; le total obtenu représente le
              nombre de Pièces d'Or qui vous appartient désormais.

              <p/>
              <ItemsPicker items={this.i}/>
              <CurrentDice r={d}/>
              <p/>

              Vous pouvez également prendre le Poignard si vous le
              désirez. N'oubliez pas d'inscrire toutes ces nouvelles
              acquisitions sur votre Feuille d'Aventure. Tandis qu'on
              enlève le cadavre du tricheur, vous vous approchez du
              bar et vous appelez l'aubergiste, puis vous lui glissez
              une Pièce d'Or dans le creux de la main en lui demandant
              une chambre pour la nuit. <L to="314">Rendez-vous au
              314</L>.

            </div>
        );
    }
}

export default P21;
