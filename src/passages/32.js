/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

import update from 'immutability-helper';
import HelpCharacter from '../HelpCharacter.js';
import EnduranceChange from '../EnduranceChange.js';

import * as bh from '../book_helpers.js';

class P32 extends Component {

    constructor(props) {
        super(props);
        this.f = {
            'name': 'a',
            'passage_title': '32',
            'already_done': false,
            'dmg':-3
        };
    }

    handleClickMeal() {
        const elem = bh.book_get_element(this.props, this.f);
        if (!elem.rest_already_done) {
            const e = update(elem, {already_done: {$set: true}});
            this.props.book_set_element(e);
            var c = bh.endurance_add(this.props.character, 2);
            c = bh.item_drop(c, 'repas');
            this.props.character_set(c);           
        }        
    }
    
    render() {
        const elem = bh.book_get_element(this.props, this.f);
        const already_done = elem.already_done;
        const index = bh.item_get_index(this.props.character.items, 'repas');
        const meal = (index !== -1);
        return (
            <div>

              Le chant d'un coq vous réveille à l'aube. Les rues
              sinueuses de Ragadorn vous apparaissent alors derrière
              un voile de pluie, une pluie régulière et abondante qui
              martèle le pavé. Il y a six jours que vous avez quitté
              Holmgard et il vous reste 320 kilomètres à parcourir
              avant d'atteindre Port Bax. Vous êtes couché dans le
              grenier d'un vaste relais de diligence. Un groupe
              d'hommes vêtus d'uniformes verts vient d'arriver ; ils
              sont en train de nettoyer l'une des diligences. Vous
              entendez une voix dire que le prochain départ pour Port
              Bax aura lieu à une heure de l'après-midi et que le
              voyage durera sept jours. Vous avez faim et il vous faut
              <L to='32' onClick={this.handleClickMeal.bind(this)}
              enabled={!already_done && meal}> prendre un Repas</L>,
              sinon, vous perdrez <EnduranceChange f={this.f}> 3
              points d'ENDURANCE</EnduranceChange>.

              <p/>
              <HelpCharacter/>
              <p/>

              Après avoir mangé, vous déciderez peut-être d'acheter un
              billet pour Port Bax aux employés de la diligence ; dans
              ce cas, <L to='136' enabled={already_done}>vous vous
              rendrez au 136</L>. Mais vous pouvez également quitter
              le relais en empruntant l'échelle extérieure qui vous
              permettra de descendre directement dans la rue ; <L
              to='238' enabled={already_done}>rendez-vous alors au
              238</L>.

            </div>
        );
    }
}

export default P32;
