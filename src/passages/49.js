/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P49 extends Component {
    render() {
        return (
            <div>

              Pendant trois jours et trois nuits, les navires de la
              flotte de Durenor font voile en direction du golfe de
              Holm. La traversée est rapide mais chacun des bateaux
              est malheureusement frappé par la malchance. Des voiles
              se déchirent, des cordages se dénouent mystérieusement
              et des voies d'eau se forment dans les coques. Les
              hommes entassés à bord se laissent gagner par
              l'énervement, des querelles éclatent, puis des bagarres,
              qui souvent se terminent par la mort d'un des
              adversaires.

              <p/>

              Au bout de la troisième nuit, Lord Axim est
              au bord du désespoir. «Je n'ai jamais subi une aussi
              mauvaise traversée, dit-il, nous n'avons croisé aucun
              ennemi, nous n'avons livré aucune bataille et pourtant,
              la moitié de mes hommes sont malades ou blessés et nous
              avons perdu deux de nos plus beaux navires. La lune nous
              est contraire, une malédiction pèse sur
              nous. Puisse-t-elle se dissiper bientôt car, même si
              nous arrivions à Holmgard cette nuit même, nous
              n'aurions pas la force de repousser l'ennemi qui assiège
              la ville. » Tandis qu'il prononce ces paroles, vous
              voyez l'aube se lever. Vous pensez que ce jour nouveau
              vous apportera peut-être quelque soulagement mais,
              hélas, les eaux calmes qui vous entourent sont
              trompeuses et cachent en fait une menace mortelle. <L
              to="100">Rendez-vous au 100</L>.

            </div>
        );
    }
}

export default P49;
