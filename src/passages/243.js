/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P243 extends Component {
    render() {
        return (
            <div>

              Voyant que leur maître est mort, les Gloks désemparés
              battent en retraite vers la poupe du navire. Le
              capitaine Kelman rassemble alors ses hommes et se lance
              à l'attaque, repoussant les immondes créatures qui, dans
              un concert de grognements rageurs, sont contraintes de
              sauter par-dessus bord pour éviter d'être taillées en
              pièces. Constatant qu'il ont perdu la bataille, les
              Kraans s'envolent des mâts et s'enfuient en direction de
              la côte qu'on aperçoit à l'horizon. «&nbsp;Merci,
              Seigneur Kaï, dit le capitaine en vous serrant la main,
              nous sommes fiers et reconnaissants de vous avoir avec
              nous.&nbsp;» Une longue ovation retentit sur le pont du
              navire : ce sont les marins qui vous rendent hommage en
              même temps que leur capitaine. Vous aidez ensuite à
              soigner les blessés tandis qu'on répare la mâture
              endommagée ; et deux heures plus tard, le navire est
              prêt à repartir, les voiles gonflées de vent : vous
              voici à nouveau en route pour Durenor. <L
              to="240">Rendez-vous au 240</L>.

            </div>
        );
    }
}

export default P243;
