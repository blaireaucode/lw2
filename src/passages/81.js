/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import RandomTable, { CurrentDice } from '../RandomTable.js';
import * as bh from '../book_helpers.js';

class P81 extends Component {
   constructor(props) {
        super(props);
        this.r = {
            passage_title: '81',
            name: 'random_table',
            dice_value: -1
        };
    }

    render() {
        const randtable = bh.book_get_element(this.props, this.r);
        const r = randtable.dice_value;
        return (
            <div>

              Le lendemain matin, vous êtes réveillé par la vigie
              postée dans le nidde-pie : «&nbsp;Canot de sauvetage sur
              bâbord avant», annonce l'homme à grands cris. Vous
              montez sur le pont en affrontant la fraîcheur de la
              brise et vous y rencontrez le capitaine. A une
              cinquantaine de mètres sur bâbord avant, un canot
              endommagé dérive, ballotté par une forte houle. A bord,
              trois hommes serrés les uns contre les autres essaient
              de se protéger de la froideur du vent. Utilisez la {' '}
               <RandomTable p={this.r}> pour obtenir un
              chiffre</RandomTable>. Si vous tirez entre 0 et 4, <L
              to="260" enabled={r>=0 && r<=4}>rendez-vous au
              260</L>. Si vous tirez entre 5 et 9, <L to="281"
              enabled={r>=5 && r<=9}>rendez-vous au 281</L>.

              <CurrentDice r={r}/>
              
            </div>
        );
    }
}

export default P81;
