/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P120 extends Component {
    render() {
        return (
            <div>

              La chance est avec vous ; vous atterrissez en effet sans
              dommage sur le pont du Kalkarm, un vaisseau de guerre de
              la flotte de Durenor. Les marins y ont livré un rude
              combat dont ils sont sortis vainqueurs et ils sont
              occupés pour le moment à détacher les grappins que leur
              avait lancés l'un des bateaux fantômes. Emergeant d'un
              nuage de fumée, Lord Axim apparaît ; son visage est
              ensanglanté et son bouclier porte la trace de coups
              violents. «&nbsp;Dieu merci, vous êtes vivant, Loup
              Solitaire.  La bataille a été sans merci et nos pertes
              sont élevées, mais de vous voir debout devant moi me met
              quelque baume au cœur&nbsp;», dit-il en vous prenant par le
              bras pour vous emmener près du
              bastingage. «&nbsp;Regardez là-bas, poursuit-il, leur
              vaisseau amiral est en feu.&nbsp;» A travers la brume
              noirâtre provoquée par la bataille, vous distinguez
              l'énorme vaisseau fantôme qui sombre sous un panache
              d'épaisse fumée.

              <p/>

              Pendant ce temps les marins du Kalkarm ont réussi à
              détacher leur navire du bateau fantôme qui les avait
              abordés et à l'éloigner des débris parsemant la mer
              alentour. Un vent se lève qui enfle les voiles déchirées
              et dissipe la fumée des combats. Lord Axim ordonne
              bientôt que soit hissé le pavillon aux Armes Royales de
              Durenor afin que les autres navires rescapés puissent
              rallier le Kalkarm. Pour la première fois depuis le
              début de la bataille, vous pouvez distinguer les autres
              navires de la flotte de Durenor et une vision
              stupéfiante s'offre alors à vous car, à présent que le
              vaisseau amiral de la flotte ennemie a sombré dans les
              flots, tous les autres bateaux fantômes retournent au
              fond de la mer d'où les avait tirés la magie des Maîtres
              des Ténèbres. «&nbsp;Le charme est rompu, dit Lord Axim,
              et nous avons remporté la victoire.&nbsp;» Quelques minutes
              plus tard, il ne reste plus un seul vaisseau fantôme à
              la surface de la mer. <L to="225">Rendez-vous au
              225</L>.

            </div>
        );
    }
}

export default P120;
