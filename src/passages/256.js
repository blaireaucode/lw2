/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P256 extends Component {
    render() {
        return (
            <div>

              Vous passez devant les marches de pierre et vous
              poursuivez votre chemin. Vous venez de dépasser la
              plate-forme lorsque vous entendez un bruit au-dessus de
              votre tête. Vous vous immobilisez et vous levez les
              yeux, mais vous ne pouvez rien voir dans l'obscurité du
              tunnel.  <L to="134">Rendez-vous au 134</L>.

            </div>
        );
    }
}

export default P256;
