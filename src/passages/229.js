/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P229 extends Component {
    render() {
        return (
            <div>

              Votre Sixième Sens vous indique que ce chariot dissimule
              une créature malfaisante. Si vous souhaitez aller voir
              de quoi il retourne, montez dans le chariot en  <L
              to='134'> vous rendant au 134</L>. Si vous préférez prendre
              vos jambes à votre cou pour vous enfuir le plus vite
              possible, <L to='208'>rendez-vous au 208</L>. Enfin, si
              vous choisissez de rebrousser chemin jusqu'à la
              bifurcation pour prendre cette fois le tunnel de droite,
              vous pouvez le faire en <L to='164'> vous rendant au
              164</L>.

            </div>
        );
    }
}

export default P229;
