/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P340 extends Component {
    render() {
        return (
            <div>

              Vous continuez de marcher pendant encore une demi-heure
              le long du tunnel avant d'arriver à une bifurcation. Si
              vous souhaitez prendre la voie de gauche, <L to="64">
              rendez-vous au 64</L>. Si vous préférez emprunter la
              voie de droite, <L to="164"> rendez-vous au 164</L>.

            </div>
        );
    }
}

export default P340;
