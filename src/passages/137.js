/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P137 extends Component {
    render() {
        return (
            <div>

              Vous arrivez à un croisement ; la rue du Mendiant tourne
              en direction du sud et aboutit à la rue du Chevalier
              Noir. Quelques mètres plus loin, une autre voie, la rue
              de l'Ancre, mène en direction de l'est. La pluie tombe
              de plus en plus dru, à présent. Si vous souhaitez aller
              vers le sud le long de la rue du Chevalier Noir, <L
              to="259">rendez-vous au 259</L>. Si vous préférez suivre
              la rue de l'Ancre en direction de l'est, <L to="20">
              rendez-vous au 20</L>.

            </div>
        );
    }
}

export default P137;
