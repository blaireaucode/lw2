/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P288 extends Component {
    render() {
        return (
            <div>

              Sans prononcer un mot, le chevalier vous montre du doigt
              la forêt qui s'étend derrière vous et rentre à
              l'intérieur de la tour, dont il referme la porte à
              clé. C'est une forêt touffue où s'enchevêtrent parmi les
              arbres de hautes herbes et des buissons d'épines. Il est
              inutile d'essayer de la traverser à cheval et il ne vous
              reste donc plus qu'à abandonner votre monture pour
              continuer votre chemin à pied. <L to="244">Rendez-vous
              au 244</L>.

            </div>
        );
    }
}

export default P288;
