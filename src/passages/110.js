/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import * as bh from '../book_helpers.js';

class P110 extends Component {
    constructor(props) {
        super(props);
        const f = {
            name: 'f1',
            passage_title: 110,
            encounter: "Garde de la tour",
            combat_skill:15,
            endurance:22
        };
        this.f = f;
    }
    
    render() {
        const r = bh.enabled_if_fight_win(this.props, this.f);
        return (
            <div>

              Le GARDE ne vous croit pas et se rue sur vous, son épée
              à la main.  Si vous ne possédez pas d'arme, retranchez 4
              points de votre total d'habileté pendant toute la durée
              du combat. Vous pouvez prendre la fuite à tout moment <L
              to="65">en vous rendant au 65</L>. Si vous décidez de
              vous battre et que vous êtes vainqueur, <L enabled={r}
              to="331">rendez-vous au 331</L>.

              <p/>
              <Fight fight={this.f}/>

            </div>
        );
    }
}
//GARDE DE LA TOUR HABILETÉ: 15 ENDURANCE: 22
export default P110;
