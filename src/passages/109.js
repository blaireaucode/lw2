/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P109 extends Component {
    render() {
        return (
            <div>

              Vous battez des paupières pour chasser l'eau de vos yeux
              et vous constatez alors que le vaisseau amiral de la
              flotte fantôme est en flammes. Une fumée noire s'échappe
              de ses ponts et des langues de feu orange et jaunes
              jaillissent de sa coque moisie. Hélas, vous n'avez guère
              le loisir de contempler ce spectacle réconfortant ;
              soudain, en effet, vous entendez un battement d'ailes
              au-dessus de votre tête : c'est un Kraan qui fond sur
              vous en essayant de vous saisir entre ses serres
              pointues. Il parvient à refermer ses griffes crochues
              sur l'étoffe de votre cape et vous vous sentez aussitôt
              emporté dans les airs. Le vol cependant est de courte
              durée, car vous dégainez le Glaive de Sommer et vous en
              plongez la lame dans le ventre flasque de la
              créature. Avec un cri de douleur, le Kraan lâche prise
              et vous retombez en priant le ciel que votre chute ne
              soit pas trop douloureuse. <L to="120">Rendez-vous au
              120</L>.

              <p/>
              <SimpleImage width={200} src={'109.png'}/>
              
            </div>
        );
    }
}

export default P109;
