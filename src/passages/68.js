/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P68 extends Component {
    render() {
        return (
            <div>

              Le garde vous jette un regard méprisant. «&nbsp;Je suis
              un soldat de Durenor, dit-il, et votre or ne vous sera
              d'aucun secours avec moi. » Vous avez eu le tort de
              porter atteinte à son honneur et il vous donne une rude
              leçon. <L to="306">Rendez-vous au 306</L>.

            </div>
        );
    }
}

export default P68;
