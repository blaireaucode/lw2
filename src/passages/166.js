/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import SimpleImage from '../SimpleImage.js';
import L from '../L.js';

class P166 extends Component {
    render() {
        return (
            <div>

              Vous montez un escalier et vous vous retrouvez sur le
              pont du navire ; la bataille fait rage tandis que les
              vaisseaux fantômes encerclent la flotte de
              Durenor. Soudain, un éclair de feu jaillit d'une tour
              dressée à l'arrière du bateau fantôme sur lequel vous
              vous trouvez, et vient frapper dans une gigantesque
              explosion le flanc d'un navire de la flotte durnoraise,
              à moins de 50 mètres de distance. Vous voyez alors avec
              horreur les soldats alliés sauter du pont, leurs
              vêtements et leurs cheveux en flammes. Si vous souhaitez
              explorer cette tour, <L to='328'>rendez-vous au
              328</L>. Si vous préférez vous enfuir en sautant
              par-dessus bord, <L to="267">rendez-vous au 267</L>.

              <p/>
              <SimpleImage width={200} src={'166.png'}/>

            </div>
        );
    }
}

export default P166;
