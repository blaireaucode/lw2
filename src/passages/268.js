/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import * as bh from '../book_helpers.js';
import SimpleImage from '../SimpleImage.js';

class P268 extends Component {
    render() {
        const f = {
            passage_title: 268,
            encounter: "BRIGANDS",
            combat_skill: 16,
            endurance: 25
        };
        const r = bh.enabled_if_fight_win(this.props, f);
        const flee = bh.book_get_element(this.props, f).round > 2;
        return (
            <div>

              «&nbsp;Vous êtes sans nul doute un Seigneur Kaï&nbsp;», dit
              l'homme, mais l'expression stupéfaite de son visage se
              transforme bientôt en un ricanement méprisant. «&nbsp;Ou
              plutôt, reprend-il d'une voix ironique, vous étiez un
              Seigneur Kaï !&nbsp;» A peine a-t-il prononcé ces mots
              qu'une porte s'ouvre à la volée juste derrière
              vous. Vous faites volte-face et vous vous retrouvez face
              à trois BRIGANDS qui s'avancent dans votre
              direction. Chacun d'eux est armé d'un cimeterre et vous
              devez les combattre en les considérant comme un seul et
              même ennemi.  Vous aurez le droit de prendre la fuite
              après avoir mené deux assauts ; vous sortirez alors par
              la porte latérale en <L enabled={flee} to='125'>vous
              rendant au 125</L>. Si vous êtes vainqueur, <L
              enabled={r} to="333">rendez-vous au 333</L>.

              <p/>
              <Fight fight={f}/>

              <p/>
              <SimpleImage width={200} src={'268.png'}/>

            </div>
        );
    }
}

// BRIGANDS HABILETÉ: 16 ENDURANCE: 25 
export default P268;
