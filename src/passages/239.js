/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P239 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, "camouflage");
        return (
            <div>

              Vous essayez d'appliquer la paume de vos mains sur la
              poitrine de l'homme blessé, mais les Squalls tirent sur
              les pans de votre cape pour vous éloigner de lui. Si
              vous maîtrisez la Discipline Kaï du Camouflage, <L
              to="77" enabled={d} >rendez-vous au 77</L>. Sinon, il vous faudra
              attaquer les Squalls pour pouvoir ensuite vous occuper
              du blessé ; <L to='28'>rendez-vous alors au 28</L>.

            </div>
        );
    }
}

export default P239;
