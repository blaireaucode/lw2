/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P65 extends Component {
    render() {
        return (
            <div>

              Tandis que vous courez le long de la rue de la Tour de
              Guet, vous entendez derrière vous la voix du garde qui
              pousse des jurons. La voix s'évanouit bientôt et vous
              arrivez sur la place du Tombeau. Devant vous, dans la
              rue du même nom, quatre soldats marchent dans votre
              direction. Vous les évitez en courant vers le sud
              pendant dix minutes environ, le long d'une rue couverte
              de gros pavés. Enfin, vous apercevez une grande écurie
              et un relais de diligence dont les contours se dessinent
              dans l'obscurité, à quelque distance. Vous vous avancez
              dans l'ombre et vous parvenez à grimper sur une échelle
              extérieure qui mène dans un grenier à foin. Personne ne
              vous a vu et vous êtes en sécurité pour la nuit. <L
              to="32">Rendez-vous au 32</L>.

            </div>
        );
    }
}

export default P65;
