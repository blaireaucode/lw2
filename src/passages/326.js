/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import SimpleImage from '../SimpleImage.js';
import * as bh from '../book_helpers.js';

class P326 extends Component {
    render() {
        const f = {
            passage_title: 326,
            encounter: "GUERRIER DRAKKARI",
            combat_skill: 15,
            endurance: 25,
        };
        const w = bh.enabled_if_fight_win(this.props, f);
        return (
            <div>

              Le capitaine ordonne à l'équipage de hisser toutes les
              voiles pour essayer d'échapper aux pirates, mais le
              vaisseau de ces derniers est rapide et ils s'efforcent
              de couper la route du Sceptre Vert. La collision semble
              inévitable. «&nbsp;Attention à l'abordage !&nbsp;»
              s'écrie le capitaine, alors que le flanc du navire aux
              voiles rouges se dresse soudain devant vous.  Dans un
              fracas impres-sionnant, la proue du Sceptre Vert déchire
              le flanc du bateau pirate. Des éclats de bois volent en
              tous sens et vous êtes projeté à plat ventre sur le pont
              sous la violence du choc. Les pirates montent
              immédiatement à l'assaut et vous apercevez avec horreur,
              au milieu de cette horde vociférante, la silhouette
              vêtue de noir d'un GUERRIER DRAKKARIM. Il vous a
              aussitôt repéré et s'avance vers vous, son glaive
              impressionnant levé au-dessus de sa tête. Il vous faut
              l'affronter dans un combat à mort. Si vous remportez la
              victoire, <L enabled={w} to="184">rendez-vous au 184</L>.

              <p/>
              <Fight fight={f}/>
              <p/>
              <SimpleImage width={200} src={'326.png'}/>
              
            </div>
        );
    }
}

// GUERRIER DRAKKARIM HABILETÉ: 15 ENDURANCE: 25

export default P326;
