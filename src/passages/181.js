/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';
import HelpGold from '../HelpGold.js';
import SimpleImage from '../SimpleImage.js';

class P181 extends Component {
    constructor(props) {
        super(props);
        const f = {
            'name': 'obj1',
            'passage_title': 181,
            'list': [
                {id:'epee', cost:4},
                {id:'poignard', cost:2},
                {id:'sabre', cost:3},
                {id:'marteau_guerre', cost:6},
                {id:'lance', cost:5},
                {id:'masse_d_armes', cost:4},
                {id:'couverture', cost:3},
                // {id:'sac_a_dos', cost:1},
            ],
        };
        this.items = f;
    }

    render() {
        return (
            <div>

              Cette rue est encore plus sale et nauséabonde que celle
              que vous venez de quitter. Bientôt, cependant, la
              vitrine en désordre d'une boutique attire votre
              attention; vous y découvrez en effet plusieurs objets
              qui pourraient vous être fort utiles ; chacun de ces
              objets porte une étiquette qui indique son prix.
              
              <p/>
              <ItemsPicker items={this.items}/>
              <p/>
              <HelpGold/>
              <p/>

              Vous pouvez entrer dans cette boutique et acheter ce qui
              vous plaira.  N'oubliez pas d'inscrire vos achats
              éventuels sur votre Feuille d'Aventure et de déduire de
              votre capital le prix que vous aurez payé.  Lorsque vous
              avez terminé vos emplettes, vous poursuivez votre chemin
              le long de la rue du Sage en direction du pont de
              Ragadorn. Ce pont est le seul point de passage entre les
              parties Est et Ouest de la ville ; il est toujours bondé
              et il vous faut jouer des coudes pour parvenir à le
              traverser parmi la foule qui s'y presse. Rendu de
              l'autre côté, vous vous retrouvez dans une avenue
              jonchée d'ordures : c'est le boulevard du Commerce,
              section Est. <L to="186">Rendez-vous au 186</L>.

              <p/>
              <SimpleImage width={250} src={'181.png'}/>

            </div>
        );
    }
}

export default P181;
