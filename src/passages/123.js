/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P123 extends Component {

    constructor(props) {
        super(props);
        const f = {
            'name': 'obj1',
            'passage_title': 123,
            'list': [
                {id:'glaive'},
            ],
        };
        this.items = f;
    }

    render() {
        return (
            <div>

              Une étrange énergie anime votre corps. Instinctivement,
              vous levez le glaive au-dessus de votre tête et un rayon
              de soleil vient frapper l'extrémité de la lame d'où
              jaillit aussitôt une lumière blanche aveuglante. Un
              instant plus tard, cependant, la lumière s'évanouit et
              vous sentez peser sur votre épaule la main de Lord
              Axim. «&nbsp;Venez, Loup Solitaire, dit-il, il y a
              encore beaucoup à faire pour préparer votre retour au
              Royaume du Sommerlund.&nbsp;» Vous rengainez le glaive
              dans son fourreau incrusté de pierreries et vous suivez
              Lord Axim qui vous entraîne hors de la chambre du
              roi. <L to="40">Rendez-vous au 40</L>.

              <p/>
              <ItemsPicker items={this.items}/>
              <p/>

            </div>
        );
    }
}

export default P123;
