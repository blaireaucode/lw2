/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P26 extends Component {
    render() {
        return (
            <div>

            Vous contemplez les orbites vides d'un cadavre
            ambulant. Mais quelque défiguré qu'il soit, vous
            reconnaissez le visage du capitaine Kelman. Vous vous
            trouvez en fait sur le pont du Sceptre Vert qui a sombré
            vingt-quatre jours plus tôt au cours de la tempête. Le
            matin même, l'épave du navire a été arrachée aux
            profondeurs obscures de la mer et sa carcasse ira
            rejoindre la flotte des bateaux fantômes. Le capitaine
            zombie tend vers vous une main aux doigts brisés et vous
            supplie, d'une voix d'outre-tombe, de déposer sur le pont
            du navire le Glaive de Sommer : «&nbsp;Déposez l'épée à
            vos pieds et mon âme alors échappera à son tourment. » Si
            vous souhaitez accéder à sa demande, <L
            to="248">rendez-vous au 248</L>. Si vous préférez attaquer
            le capitaine, <L to="66">rendez-vous au 66</L>.

              <p/>
              <SimpleImage src={'26.png'}/>

            </div>
        );
    }
}

export default P26;
