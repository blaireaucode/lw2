/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P25 extends Component {
    render() {
        const ss = bh.has_discipline(this.props.character, "sixieme_sens");
        const mpm = bh.has_discipline(this.props.character, "maitrise_psychique_de_la_matiere");
        const r = ss || mpm;
        return (
            <div>

              Vous parcourez du regard la taverne où s'entasse une
              foule de buveurs, et vous remarquez que de nombreux
              villageois jouent à des jeux de hasard. Près de l'entrée
              principale, un jeune personnage à l'allure louche est
              assis devant une table sur laquelle trois tasses
              d'argile sont retournées. Il les change sans cesse de
              place en mettant au défi qui veut l'entendre de deviner
              sous laquelle de ces trois tasses est cachée une bille
              de verre. Il promet de donner au gagnant le double de la
              somme que ce dernier aura misée. Si vous possédez les
              Disciplines Kaï du Sixième Sens ou de la Maîtrise
              Psychique de la Matière, <L to="116"
              enabled={r}>rendez-vous au 116</L>. Si ces deux
              Disciplines vous sont étrangères, <L to="153"
              enabled={!r}>rendez-vous au 153</L>.

            </div>
        );
    }
}

export default P25;
