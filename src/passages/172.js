/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P172 extends Component {
    render() {
        return (
            <div>

              Grâce à votre Sixième Sens, vous devinez qu'un péril
              vous menace dans l'ombre de cette plate-forme. Si vous
              souhaitez malgré tout monter les marches et affronter ce
              danger, <L to="52">rendez-vous au 52</L>. Si vous
              préférez vous éloigner rapidement de ces marches et de
              cette plate-forme, <L to="256">rendez-vous au
              256</L>. Enfin, si vous choisissez de revenir en courant
              jusqu'au croisement pour prendre le tunnel de gauche,
              <L to='64'> rendez-vous au 64</L>.

            </div>
        );
    }
}

export default P172;
