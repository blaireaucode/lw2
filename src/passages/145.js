/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import HelpCharacter from '../HelpCharacter.js';
import EnduranceChange from '../EnduranceChange.js';
import * as bh from '../book_helpers.js';

class P145 extends Component {

    constructor(props) {
        super(props);
        const f = {
            'name': 'a',
            'passage_title': 145,
            'already_done': false,
            'dmg': -5
        };
        this.f = f;
    }

    render() {
        const r = bh.book_get_element(this.props, this.f).already_done;
        return (
            <div>
              
              Vous vous sentez de plus en plus faible. Au prix d'un
              effort surhumain, vous cherchez l'herbe de Laumspur que
              vous finissez par trouver ; il vous semble qu'il s'est
              écoulé une éternité de douleur lorsque vous parvenez
              enfin à glisser dans votre bouche quelques feuilles
              sèches que vous vous forcez à avaler. Quelques secondes
              plus tard de violents malaises convulsent votre corps,
              puis la douleur s'apaise et vous sombrez dans un sommeil
              agité. Il s'écoule presque une heure avant votre réveil
              et vous vous sentez encore très mal, si mal que vous
              perdez aussitôt <EnduranceChange f={this.f}> 5 points
              d'ENDURANCE</EnduranceChange>. Peu à peu, cependant, vos
              forces reviennent et votre désarroi se change alors en
              fureur. Vous ramassez vos affaires et vous quittez la
              pièce d'un pas chancelant, bien décidé à retrouver celui
              ou celle qui a tenté de vous assassiner. <L to='200'
              enabled={r}>Rendez-vous au 200</L>.

              <p/>
              <HelpCharacter/>

            </div>
        );
    }
}

export default P145;
