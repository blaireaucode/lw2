/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import * as bh from '../book_helpers.js';

class P296 extends Component {

    constructor(props) {
        super(props);
        this.ff = [];
        this.ff.push( {passage_title: 296,
                       name:'f1',
                       encounter: "Sergent de la GARDE",
                       combat_skill: 13,
                       endurance: 22 });
        this.ff.push( {passage_title: 296,
                       name:'f2',
                       encounter: "Caporal de la GARDE",
                       combat_skill: 12,
                       endurance: 20 });
        this.ff.push( {passage_title: 296,
                       name:'f3',
                       encounter: "1er GARDE",
                       combat_skill: 11,
                       endurance: 19});
        this.ff.push( {passage_title: 296,
                       name:'f4',
                       encounter: "2e GARDE",
                       combat_skill: 11,
                       endurance: 19});
        this.ff.push( {passage_title: 296,
                       name:'f5',
                       encounter: "3e GARDE",
                       combat_skill: 10,
                       endurance: 18});
        this.ff.push( {passage_title: 296,
                       name:'f6',
                       encounter: "4e GARDE",
                       combat_skill: 10,
                       endurance: 17});
        console.log('const ff', this.ff);
    }
    
    render() {
        const f = bh.book_get_current_fight(this.props, this.ff);
        const r = bh.enabled_if_fight_win(this.props, f);
        return (
            <div>

              Les clients fuient la taverne lorsque les six GARDES
              passent à l'attaque.  Vous pouvez prendre la fuite à
              tout moment en sortant par la porte de derrière ; <L
              to='88'> rendez-vous alors au 88</L>. Si vous parvenez à
              tuer tous les gardes, <L enabled={r}
              to="221">rendez-vous au 221</L>.

              <p/> <Fight fight={f}/>

            </div>
        );
    }
}

// HABILETÉ ENDURANCE 
// Sergent de la GARDE 13 22
// Caporal de la GARDE 12 20
// 1er GARDE 11 19
// 2e GARDE 11 9
// 3e GARDE 10 18
// 4e GARDE 10 17

export default P296;
