/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';


import SimpleImage from '../SimpleImage.js';

class P62 extends Component {
    render() {
        const s = this.props.character.items.some(e => e.id === 'sceau_hammardal');
        
        return (
            <div>

              Vous entrez dans une vaste pièce remplie de classeurs et
              de livres de comptes. Face à vous, un homme revêtu d'un
              uniforme d'officier des forces navales de Durenor est
              assis à un grand bureau. Il tient devant lui un énorme
              livre posé debout sur le bureau. A votre entrée, l'homme
              lève les yeux de son livre et vous jette un regard
              inquisiteur. «&nbsp;Vous devez avoir des affaires bien
              urgentes à mener pour solliciter un laissez-passer rouge
              à une heure aussi tardive. Je voudrais voir votre permis
              d'entrée et l'autorisation de votre officier
              commandant. » Si vous avez réuni ces documents au cours
              de votre quête, <L to='126'>rendez-vous au 126</L>. Si
              vous n'avez pas les documents demandés, ou si vous ne
              souhaitez pas les présenter à cet homme, vous devrez
              prendre le risque de lui montrer le Sceau d'Hammardal et
              <L enabled={s} to='263'> vous rendre dans ce cas au
              263</L>.  Si vous n'avez ni le Sceau ni les documents,
              vous pouvez quittez la pièce et retournez dans le hall
              <L to='318'> en vous rendant au 318</L>.

              <p/>
              <SimpleImage width={250} src={'62.png'}/>

            </div>
        );
    }
}

export default P62;
