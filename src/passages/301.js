/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P301 extends Component {
    render() {
        const f = {
            'name': 'obj1',
            'passage_title': 301,
            'list': [
                {id:'PO', nb:3},
                {id:'poignard', nb:3},
                {id:'sabre'},
            ],
        };
        return (
            <div>

              En fouillant les cadavres, vous trouvez 3 Pièces d'Or, 3
              Poignards et 1 Sabre. Si vous souhaitez emporter l'un ou
              l'autre de ces objets, modifiez en conséquence votre
              Feuille d'Aventure. <L to='20'>Rendez-vous ensuite au 20</L>.

              <p/>
              <ItemsPicker items={f}/>

            </div>
        );
    }
}

export default P301;
