/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P64 extends Component {
    render() {
        const d = bh.has_discipline(this.props.character, "sixieme_sens");
        return (
            <div>

              Un peu plus loin, vous apercevrez une diligence
              semblable à celles qui transportent les voyageurs le
              long des côtes menant à Ragadorn. Les chevaux ont été
              dételés et le véhicule semble abandonné. Vous remarquez
              alors les corps de trois soldats étendus entre les
              roues. Leurs uniformes sont tachés de sang. Si vous
              souhaitez fouiller la diligence en espérant y trouver de
              la nourriture ou quelque objet utile, <L
              to="134">rendez-vous au 134</L>. Si vous préférez
              poursuivre votre chemin sans vous attarder, <L
              to="208">rendez-vous au 208</L>. Si enfin vous maîtrisez
              la Discipline Kaï du Sixième Sens, <L enabled={d}
              to="229">rendez-vous au 229</L>.

            </div>
        );
    }
}

export default P64;
