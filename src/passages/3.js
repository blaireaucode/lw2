/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P3 extends Component {

    render() {
        return (
            <div>

              Vous sortez dans une petite ruelle, derrière la boutique et vous
              voyez, tout au bout, un cheval attaché à un piquet par une longe.
              Si vous souhaitez prendre ce cheval et fuir le village, <L
              to="150">rendez-vous au 150</L>. Si le cheval ne vous intéresse
              pas, quittez la ruelle et <L to="19">rendez-vous au 19</L>.

            </div>
        );
    }
}

export default P3;
