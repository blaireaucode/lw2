/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P293 extends Component {
    render() {
        return (
            <div>

              Cette route mène à une cabane abandonnée. L'intérieur en
              est garni de meubles recouverts d'une bonne couche de
              poussière. De toute évidence, il y a plusieurs mois que
              personne n'y est entré. La route ne va pas plus loin,
              c'est un cul-de-sac et vous vous rendez compte à cet
              instant que vous venez de perdre un temps précieux. Il
              ne vous reste plus qu'à rebrousser chemin jusqu'à la
              bifurcation et à prendre la voie de gauche. Hâtez-vous
              de <L to='155'>vous rendre au 155</L>.

            </div>
        );
    }
}

export default P293;
