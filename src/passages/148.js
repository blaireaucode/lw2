/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import update from 'immutability-helper';
import HelpCharacter from '../HelpCharacter.js';
import EnduranceChange from '../EnduranceChange.js';
import * as bh from '../book_helpers.js';

class P148 extends Component {

    constructor(props) {
        super(props);
        this.f = {
            'name': 'a',
            'passage_title': 148,
            'already_done': false,
            'dmg':-3
        };
    }

    handleClickMeal() {
        const elem = bh.book_get_element(this.props, this.f);
        if (!elem.already_done) {
            const e = update(elem, {already_done: {$set: true}});
            this.props.book_set_element(e);
            var c = bh.endurance_add(this.props.character, 2);
            c = bh.item_drop(c, 'repas');
            this.props.character_set(c);
        }        
    }
    
    render() {
        const elem = bh.book_get_element(this.props, this.f);
        const already_done = elem.already_done;
        const index = bh.item_get_index(this.props.character.items, 'repas');
        const meal = (index !== -1);
        return (
            <div>

              Vous vous enveloppez dans votre cape de Seigneur Kaï et
              vous en relevez le capuchon. Le cocher lance un cri puis
              fouette ses chevaux et bientôt la diligence file sur la
              route bordée d'arbres qui longe la côte en direction de
              Ragadorn. Au cours du trajet, vous bavardez avec le
              cocher qui vous donne des renseignements fort utiles
              concernant le port de Ragadorn. Depuis la mort de
              Killean le Suzerain, trois ans auparavant, la ville est
              dirigée (et fort mal, d'après votre interlocuteur) par
              son fils Lachelan. Ses hommes et lui ne sont en fait que
              des brigands qui accablent le peuple d'impôts et
              assassinent quiconque s'oppose à leur pouvoir.

              <p/>

              Tandis que le cocher vous parle, vous vous sentez
              tenaillé par la faim et il vous faut à tout prix <L
              to='148' onClick={this.handleClickMeal.bind(this)}
              enabled={!already_done && meal}> prendre un repas</L>,
              sinon vous <EnduranceChange f={this.f}> 3 points
              d'ENDURANCE</EnduranceChange>. Quelques heures plus
              tard, la ville de Ragadorn se dessine dans le
              lointain. Une cloche sonne les douze coups de midi et
              bientôt la diligence franchit la porte Ouest de la cité
              puis s'arrête au relais. «&nbsp;Si vous voulez vous
              rendre à Durenor, vous devrez prendre une autre
              diligence au relais de la porte Est, mais dépêchezvous,
              car le départ est prévu à une heure.&nbsp;» Vous
              remerciez le cocher pour ces renseignements et vous
              sautez sur la chaussée recouverte de pavés. Vous êtes
              alors frappé par l'effroyable puanteur qui baigne ce
              port sinistre. Une enseigne rouillée accrochée à la
              façade en ruines d'une maison porte ces mots :
              «&nbsp;Bienvenue à Ragadorn&nbsp;». Si vous souhaitez
              aller vers le sud le long de l'avenue de la porte Ouest,
              <L enabled={already_done} to="323"> rendez-vous au
              323</L>. Si vous préférez vous diriger au nord en
              suivant le quai de l'Est, <L enabled={already_done}
              to="122">rendez-vous au 122</L>. Enfin, si vous
              choisissez plutôt d'aller vers l'est en empruntant la
              rue de la Hache, <L enabled={already_done}
              to="257">rendez-vous au 257</L>.

              <p/>
              <HelpCharacter/>

            </div>
        );
    }
}

export default P148;
