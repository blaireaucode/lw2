/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P267 extends Component {
    render() {
        return (
            <div>

              Lorsque vous refaites surface, vous constatez que la
              bataille fait rage tout autour de vous. Nombre de
              cadavres de soldats tués au combat ou jetés par-dessus
              bord et noyés flottent sur la mer. Vous parcourez à la
              nage une trentaine de mètres environ, puis vous vous
              hissez sur le pont d'un navire de la flotte de
              Durenor. Une rude bataille s'y livre car un vaisseau
              fantôme vient de l'aborder et d'y déverser une armée de
              Zombies qui massacrent à tour de bras les soldats
              durenorais frappés de terreur. Si vous estimez opportun
              de dégainer le Glaive de Sommer et de vous lancer à
              l'attaque, <L to="128"> rendez-vous au 128</L>. Si vous
              préférez sauter sur le pont du vaisseau fantôme, <L
              to="309"> rendez-vous au 309</L>.

            </div>
        );
    }
}

export default P267;
