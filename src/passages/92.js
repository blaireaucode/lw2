/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P92 extends Component {
    render() {
        return (
            <div>

              L'épouvantable créature pousse un dernier cri en
              s'écroulant à vos pieds. Vous faites un pas en arrière
              pour échapper à l'odeur putride qui se dégage de son
              corps en décomposition et vous voyez alors trois autres
              Monstres d'Enfer s'avancer vers vous. Rester ici
              relèverait du suicide et vous décidez de prendre la
              fuite en direction du bois après avoir prévenu Rhygar à
              grands cris du danger qui menace. <L
              to='183'>Rendez-vous au 183</L>.


            </div>
        );
    }
}

export default P92;
