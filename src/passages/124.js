/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P124 extends Component {

    constructor(props) {
        super(props);
        const f = {
            'name': 'obj1',
            'passage_title': 124,
            'list': [
                {id:'sabre'},
                {id:'poignard'},
                {id:'PO', nb:42},
            ],
        };
        this.items = f;
    }
    
    render() {
        return (
            <div>

              Vous la fouillez mais vous ne découvrez aucune preuve
              qu'elle était bien celle qui voulait vous
              assassiner. Vous trouvez sur elle 42 Pièces d'Or, un
              Sabre et un Poignard. Prenez ce que vous voulez parmi
              ces objets si le cœur vous en dit et le cas échéant,
              inscrivez-les sur votre Feuille d'Aventure. <L
              to='33'>Rendez-vous ensuite au 33</L>.

              <p/>
              <ItemsPicker items={this.items}/>

            </div>
        );
    }
}

export default P124;
