/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';


import * as bh from '../book_helpers.js';

class P85 extends Component {
    constructor(props) {
        super(props);
        const f = {
            'name': 'f1',
            'passage_title': '85',
            'encounter': "VIVEKA",
            'combat_skill':24,
            'endurance':27
        };
        this.f = f;
    }
    render() {
        const r = bh.enabled_if_fight_win(this.props, this.f);
        return (
            <div>

              VIVEKA renverse la table d'un coup de pied. Elle est
              rapide comme l'éclair et votre effet de surprise est
              complètement raté. Elle a déjà dégainé son épée et se
              jette sur vous.  Si vous remportez la victoire, <L
              enabled={r} to="124">rendez-vous au 124</L>.

              <p/>
              <Fight fight={this.f}/>

            </div>
        );
    }
}

// VIVEKA HABILETÉ : 24 ENDURANCE : 27 
export default P85;
