/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P163 extends Component {
    render() {
        return (
            <div>

            «&nbsp;Nous avons le vaisseau le plus rapide de toutes les
            mers du Nord, il n'est pas de navire qui puisse rattraper
            le Sceptre Vert&nbsp;», affirme le capitaine. Il a raison en
            effet, car bientôt le bateau pirate disparaît à
            l'horizon. «&nbsp;Depuis vingt-cinq ans que je navigue, je
            n'ai jamais vu les pirates Lakuri s'aventurer si loin au
            nord, dit le capitaine en se caressant la barbe d'un air
            songeur, ils doivent être sur la piste d'un bien riche
            butin pour s'éloigner ainsi de leurs îles tropicales.&nbsp;» Et
            tandis que le capitaine descend dans sa cabine, vous
            pensez avec inquiétude que ce « riche butin » pourrait
            bien être vous-même. <L to="240">Rendez-vous au 240</L>.

            </div>
        );
    }
}

export default P163;
