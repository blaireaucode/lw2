/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P269 extends Component {
    render() {
        const f = {
            passage_title: 269,
            list: [ {id:'magical_spear'} ]
        };
        const l = this.props.character.items.some(e => e.id === 'magical_spear');        
        var p = '';
        if (!l) p = (<ItemsPicker items={f}/>);       
        return (
            <div>

              La vision répugnante de la créature qui se tortille sur
              le sol vous remplit de dégoût pour les Maîtres des
              Ténèbres et leurs immondes séides.  Lorsque enfin le
              Monstre d'Enfer s'est entièrement décomposé et que vous
              êtes sûr de l'avoir anéanti à tout jamais, vous arrachez
              de ses restes la Lance Magique dont vous essuyez le fer
              sur l'étoffe fumante de ses vêtements. Vous avez hâte de
              quitter cet endroit et vous courez le long du tunnel
              aussi vite que possible. <L to="349">Rendez-vous au
              349</L>.

              <p/>{p}

            </div>
        );
    }
}

export default P269;
