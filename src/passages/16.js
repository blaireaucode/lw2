/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P16 extends Component {
    render() {
        return (
            <div>

              Vous saisissez un verre de bière et vous le fracassez
              contre le bord de la table. L'éclat de verre que vous
              tenez à présent entre vos doigts est coupant comme un
              rasoir. Vous en passez le tranchant sur le dos de votre
              main gauche et une longue estafilade apparaît aussitôt
              d'où s'écoule un mince filet de sang. Vous pressez
              ensuite la paume de votre main droite contre la blessure
              et vous vous concentrez. Une douceur tiède se répand
              alors sur votre main blessée tandis que votre pouvoir
              guérit la plaie. Lorsque vous ôtez votre main droite, il
              ne reste plus trace de la coupure, pas même la plus
              petite cicatrice. Le marin vous observe d'un air
              stupéfait. <L to="268">Rendez-vous au 268</L>.

            </div>
        );
    }
}

export default P16;
