/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P168 extends Component {
    render() {
        const g = bh.gold_get(this.props.character);
        return (
            <div>

              Un par un, les autres voyageurs s'approchent et
              contemplent avec horreur le corps du cocher de la
              diligence. «&nbsp;Il faut l'enterrer&nbsp;», dit le
              moine. Vous hochez la tête en signe d'approbation et
              vous creusez une tombe pour y déposer le corps. Lorsque
              le malheureux est enterré, tous les voyageurs et
              vous-même revenez près de la diligence pour décider de
              ce qu'il convient de faire. «&nbsp;Je connais la route de Port
              Bax, je peux remplacer le cocher&nbsp;», propose
              Halvore. «&nbsp;J'espère qu'on ne nous accusera pas de l'avoir
              tué&nbsp;», dit le moine avec inquiétude. «&nbsp;Ce sont les
              dieux qui ont décidé de sa mort&nbsp;», assure Dorier. «&nbsp;J'en
              porterai témoignage&nbsp;», déclare Ganon, les Chevaliers de
              la Montagne Blanche ne mentent jamais. Il est vrai qu'au
              royaume de Durenor un authentique chevalier dit toujours
              la vérité, qu'elle lui soit ou non favorable. Ses
              paroles semblent avoir rassuré le moine et bientôt la
              diligence fait route à nouveau en direction de
              l'est.

              <p/>

              L'après-midi touche à sa fin lorsque vous arrivez au
              relais d'un petit village côtier connu sous le nom de
              Crique en Gorn et dont la population se compose
              essentiellement de repris de justice, de voleurs et de
              Squalls. Les villageois se montrent soupçonneux
              lorsqu'on leur annonce la mort du cocher, mais Dorier
              parvient à les convaincre qu'il s'agit bel et bien d'un
              accident. Il n'y a qu'une seule auberge dans tout le
              village; c'est une taverne qui porte un nom peu
              engageant : L'Espoir Déçu. Son état de délabrement est
              typique de la pauvreté qui règne dans ce village du bord
              de mer où abondent les masures en ruines. Une chambre
              pour la nuit coûte 1 Pièce d'Or. Si vous avez les moyens
              de vous offrir une chambre, <L enabled={g>=1}
              to='314'>rendez-vous au 314</L>. Sinon, <L
              enabled={g===0} to="25">rendez-vous au 25</L>.

            </div>
        );
    }
}

export default P168;
