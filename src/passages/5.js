/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Fight from '../Fight.js';
import L from '../L.js';
import * as bh from '../book_helpers.js';

class P5 extends Component {

    constructor(props) {
        super(props);
        const f = {
            'name': 'f1',
            'passage_title': 5,
            'encounter': "Monstre d'enfer",
            'combat_skill':22,
            'endurance':20,
            'no_bonus_psy': true,
            'bonus_factor_dmg_encounter': 2
        };
        this.f = f;
    }
    
    render() { 
        const r = bh.enabled_if_fight_win(this.props, this.f);
        return (
            <div>

              La porte s'ouvre à la volée et un MONSTRE d'ENFER se rue sur vous en
              brandissant son épée. Vous frappez la créature dès qu'elle a pénétré
              dans la cale et sous l'effet du coup, une longue et profonde
              entaille apparaît sur sa poitrine. Le Monstre pousse un cri
              épouvantable, mais, malgré sa blessure, il a encore la force de
              bondir sur vous. Il vous faut engager un combat à mort.

              <p/>

              Le MONSTRE d'ENFER est un être de l'au-delà, un mort vivant, et la
              puissance du Glaive de Sommer vous permet de multiplier par deux
              tous les points d'ENDURANCE qu'il perdra au cours du combat. Il est
              cependant insensible à la Puissance Psychique. Si vous parvenez à
              tuer le Monstre, vous pourrez vous enfuir de la cale par
              l'écoutille. <L enabled={r} to="166">Rendez-vous alors au 166</L>.

              <p/>
              <Fight fight={this.f}/>

            </div>
        );
    }
}

// MONSTRE D'ENFER HABILETÉ: 22 ENDURANCE: 20
export default P5;
