/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P171 extends Component {
    render() {
        return (
            <div>

              Vous apercevez derrière les arbres une large route qui
              longe la côte d'est en ouest. Si vous voulez aller vers
              l'est, <L to="27">rendez-vous au 27</L>. Si vous
              préférez vous diriger vers l'ouest, <L
              to="114">rendez-vous au 114</L>.

            </div>
        );
    }
}

export default P171;
