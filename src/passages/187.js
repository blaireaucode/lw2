/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import ItemsPicker from '../ItemsPicker.js';

class P187 extends Component {

    constructor(props) {
        super(props);
        const f = {
            'name': 'obj1',
            'passage_title': 187,
            'list': [
                {id:'lance', nb:2},
                {id:'epee', nb:2},
                {id:'PO', nb:6},
            ],
        };
        this.items = f;
    }

    render() {
        return (
            <div>

              En fouillant rapidement leurs cadavres, vous découvrez
              les objets suivants : 

              <p/>
              <ItemsPicker items={this.items}/>
              <p/>


              Si vous décidez d'emporter l'un ou l'autre de ces
              objets, n'oubliez pas de modifier votre Feuille
              d'Aventure en conséquence. Vous précipitez ensuite les
              corps des soldats dans les eaux du chenal et vous vous
              hâtez de franchir le pont, de peur que quelqu'un n'ait
              été témoin de la scène. Une fois parvenu de l'autre
              côté, vous marchez pendant une heure sur un chemin
              forestier et vous calez confortablement votre Sac à Dos
              sur vos épaules et vous prenez la direction de l'est. <L
              to="265">Rendez-vous au 265</L>.

            </div>
        );
    }
}

export default P187;
