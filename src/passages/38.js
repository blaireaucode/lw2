/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P38 extends Component {
    render() {
        return (
            <div>

              Vous saisissez la hampe de la lance que vous enfoncez
              dans la cage thoracique du Monstre d'Enfer. Celui-ci se
              met à hurler de douleur et de rage en relâchant
              l'étreinte de ses doigts autour de votre cou. Vous
              roulez alors sur vous-même pour échapper au Monstre
              hideux que vous voyez se tordre sur le sol en essayant
              désespérément d'arracher la lance de sa poitrine. Si
              vous souhaitez empoigner la lance pour l'enfoncer plus
              profondément dans le corps du Monstre d'Enfer, <L
              to='269'>rendez-vous au 269</L>. Si vous préférez
              prendre la fuite le plus vite possible, <L
              to='313'>rendez-vous au 313</L>.

            </div>
        );
    }
}

export default P38;
