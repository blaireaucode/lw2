/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P149 extends Component {
    render() {
        const l = this.props.character.items.some(e => e.id === 'sceau_hammardal');        
        return (
            <div>

              Votre Sixième Sens vous indique que ce garde est un
              soldat loyal du royaume de Durenor. Si vous vous mêliez
              de vouloir le corrompre, il se sentirait gravement
              insulté et vous attaquerait aussitôt. Si vous souhaitez
              lui montrer le Sceau d'Hammardal, <L enabled={l}
              to="223">rendez-vous au 223</L>. Peut-être préférez-vous
              cependant ne pas lui montrer l'anneau ; peut-être même
              n'est-il plus en votre possession ; dans ce cas vous
              pouvez essayer de vous faire passer pour un marchand se
              rendant à Port Bax <L to="250">en allant au 250</L>.

            </div>
        );
    }
}

export default P149;
