/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P322 extends Component {
    render() {
        return (
            <div>

              Ces cavaliers vêtus de capes sont entourés d'une aura
              maléfique et votre Sixième Sens vous avertit qu'il
              serait imprudent de suivre Rhygar et ses hommes. Vous
              leur criez de revenir immédiatement, mais il est trop
              tard : leurs propres cris de guerre et le galop de leurs
              montures couvrent votre voix. <L to="277">Rendez-vous au
              277</L>.

            </div>
        );
    }
}

export default P322;
