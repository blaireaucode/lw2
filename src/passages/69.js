/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';


import HelpCharacter from '../HelpCharacter.js';
import EnduranceChange from '../EnduranceChange.js';
import SimpleImage from '../SimpleImage.js';
import * as bh from '../book_helpers.js';

class P69 extends Component {
    constructor(props) {
        super(props);
        const f = {
            'name': 'a',
            'passage_title': 69,
            'already_done': false,
            'dmg':-2
        };
        this.f = f;
    }
    
    render() {
        const a = bh.book_get_element(this.props, this.f).already_done;
        const d = bh.has_discipline(this.props.character, "bouclier_psychique");
        const ok = a || d;
        return (
            <div>

              L'un des étrangers sort de sous sa cape un épieu noir
              qu'il tend devant lui. D'un cône d'acier fixé à
              l'extrémité de l'épieu s'échappe soudain une flamme
              bleuâtre et un éclair jaillit dans votre direction. Un
              fracas assourdissant retentit lorsque l'éclair vient
              frapper le bouclier de Rhygar. «&nbsp;Pas de quartiers !
              » crie alors le Lieutenant Général en se précipitant sur
              l'étranger à la lance de feu. L'épée de votre compagnon
              transperce la cape de son adversaire mais ce dernier
              reste indemne.

              <p/>

              Vous comprenez alors à qui vous avez affaire ; ces
              créatures vêtues de capes sont en effet des Monstres
              d'Enfer, des êtres cruels au service des Maîtres des
              Ténèbres dont ils sont les capitaines. Ils ont la
              faculté d'adopter une apparence humaine, mais ils
              restent invulnérables aux armes normales. Le Monstre
              d'Enfer que combat le Lieutenant Général pousse un cri
              terrifiant qui vous déchire la tête ; aveuglé par cette
              douleur fulgurante, vous trébuchez et vous tombez dans
              les broussailles épaisses qui recouvrent le flanc boisé
              de la colline.

              <p/>
              <SimpleImage width={250} src={'69.png'}/>
              <p/>

              Si vous ne maîtrisez pas la Discipline Kaï du Bouclier
              Psychique, vous perdez <EnduranceChange f={this.f}> 2
              points d'ENDURANCE</EnduranceChange> sous la violence de
              l'attaque mentale du Monstre.  <L to="311" enabled={ok}
              >Rendez-vous au 311</L>.

              <p/>
              <HelpCharacter/>

            </div>
        );
    }
}

export default P69;
