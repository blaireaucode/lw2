/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P119 extends Component {
    render() {
        return (
            <div>

              Des débris de bois, des planches, des madriers et des
              voiles déchirées flottent sur les vagues parsemées
              d'écume. C'est là tout ce qui reste d'un navire
              marchand. Mais soudain, vous apercevez un homme
              cramponné à un panneau d'écoutille. Une échelle de corde
              lui est aussitôt jetée et le malheureux est ramené à
              bord. «&nbsp;Les pirates ! » dit-il simplement avant de
              s'écrouler sur le pont, à bout de force. Après qu'on l'a
              enveloppé dans une couverture, l'homme est emmené dans
              une cabine. Il a reçu de nombreuses blessures et sa fin
              est proche. «&nbsp;Voici un forfait qui porte la
              signature des pirates Lakuri, vous confie le capitaine,
              mais il est rare qu'on les croise dans ces eaux ; ils
              doivent être sur la piste d'un riche butin pour s'être
              ainsi éloignés de leurs îles tropicales. » Et tandis que
              votre navire reprend sa route en direction de Durenor,
              vous ne pouvez vous empêcher de penser que ce
              «&nbsp;riche butin » pourrait bien être vous-même. <L
              to="240">Rendez-vous au 240</L>.

            </div>
        );
    }
}

export default P119;
