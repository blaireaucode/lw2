/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P290 extends Component {
    render() {
        return (
            <div>

              Ce repas sent délicieusement bon et vous vous apprêtez à
              le dévorer lorsque vous remarquez soudain, sur le bord
              de l'assiette, trois gouttes d'un liquide clair qui vous
              semble tout d'abord être de l'eau. Mais, lorsque vous
              touchez l'une de ces gouttes du bout des doigts, vous
              vous apercevez que le liquide est collant et vous
              reconnaissez aussitôt la consistance de la sève de
              gandum, un poison mortel, inodore et incolore, qui a la
              faveur des assassins de tout poil. Une fureur soudaine
              vous saisit alors et vous vous ruez hors de la chambre
              avec la ferme intention de découvrir quel est celui ou
              celle qui a ainsi tenté de vous supprimer. <L
              to="200">Rendez-vous au 200</L>.

            </div>
        );
    }
}

export default P290;
