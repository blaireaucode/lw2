/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Cont from '../Cont.js';

class P0_4 extends Component {
    render() {
        return (
            <div>

              Le capitaine Gayal et ses gardes vous escortent jusqu'aux portes de
              la citadelle&nbsp;: là, un chariot bâché vous attend. Dès que vous y avez
              pris place, les portes s'ouvrent et le chariot se fraie un chemin
              dans les rues populeuses de Holmgard, la capitale du Sommerlund. Le
              voyage est court, mais très inconfortable. Bientôt, le chariot
              s'arrête et le conducteur vient écarter les pans de la bâche qui
              vous dissimule aux regards. «&nbsp;Voici le quai d'embarquement,
              Seigneur Kaï, annonce le conducteur du chariot, et voici votre
              navire, le Sceptre Vert.&nbsp;»

              <p/>
              <Cont to='1'/>

            </div>
        );
    }
}

export default P0_4;
