/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';
import HelpGold from '../HelpGold.js';

class P339 extends Component {

    pay() {
        const d = bh.get_done(this, 339);
        if (!d) {
            const c = bh.gold_add(this.props.character, -1);
            this.props.character_set(c);
        }
        bh.update_done(this, 339);
    }
    
    render() {
        const g = bh.gold_get(this.props.character);
        const d = bh.get_done(this, 339);
        return (
            <div>

              Une demi-heure plus tard, la diligence est arrêtée par
              des cavaliers en armes. Ils portent l'emblème de
              Lachelan, le Suzerain de Ragadorn : un vaisseau noir
              surmonté d'une crête rouge. Ils exigent de l'or en
              paiement de ce qu'ils appellent une «&nbsp;taxe de
              sortie » : il en coûtera 1 Couronne à chaque
              passager. Vos compagnons de voyage déposent chacun 1
              Pièce d'Or sur une assiette qu'ils vous tendent
              ensuite. Si vous avez les moyens de payer cette taxe,
              déposez à votre tour 1 Couronne sur l'assiette ; la
              diligence alors pourra repartir et vous <L enabled={g>0
              || d} onClick={this.pay.bind(this)} to='249'>vous
              rendrez au 249</L>. Si vous n'avez plus d'or, <L
              enabled={g===0} to="50">rendez-vous au 50</L>.

              <p/>
              <HelpGold/>
              
            </div>
        );
    }
}

export default P339;
