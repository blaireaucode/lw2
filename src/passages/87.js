/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Dead from '../Dead.js';

class P87 extends Component {
    render() {
        return (
            <div>

              Lorsque vous levez votre arme pour en frapper le
              chevalier, vous vous rendez compte trop tard que vous
              avez commis une erreur fatale, car l'homme est un
              escrimeur de toute première force et les soldats
              appartiennent au régiment d'élite de la garde du roi
              Alin IV. Pensant que vous êtes un Monstre d'Enfer, ils
              vous encerclent et vous taillent en pièces. Votre
              mission s'achève tragiquement en même temps que votre
              vie, ici, à Tarnalin.

              <Dead/>

            </div>
        );
    }
}

export default P87;
