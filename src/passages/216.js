/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P216 extends Component {
    render() {
        return (
            <div>

              Pendant trois jours et trois nuits, la puissante flotte
              du royaume de Durenor file à bonne allure en direction
              du golfe de Holm ; un fort vent gonfle les voiles des
              vaisseaux et il se pourrait bien que le voyage soit plus
              court que prévu. Pourtant, le moral des soldats n'est
              pas au plus haut ; il semble que leur confiance en
              eux-mêmes et leur hâte de combattre se soient peu à peu
              évanouies, comme si quelque vampire invisible les avait
              mystérieusement vidés de leur force. Lord Axim en
              éprouve une grande contrariété. «&nbsp;Cette humeur
              sombre qui hante nos navires est l'œuvre des Maîtres des
              Ténèbres, assure-t-il. Je connais l'étendue de leur
              pouvoir lorsqu'il s'agit d'influencer l'esprit des
              hommes, mais la malédiction qu'ils font peser sur nous
              est bien pire encore, il est impossible de conjurer une
              telle sorcellerie. Je prie le ciel que cette malédiction
              soit bientôt levée, car sinon, même si nous arrivons à
              destination, nous n'aurons plus suffisamment de volonté
              pour affronter l'ennemi.&nbsp;»

              <p/>

              Le lendemain à l'aube, la prière de Lord Axim semble
              avoir été entendue. Le moral des hommes remonte en
              effet, et la malédiction paraît avoir pris fin. Mais
              c'est désormais une autre menace qui pèse sur la flotte
              de Durenor, une menace encore plus mortelle dont vous
              connaîtrez la nature en <L to='100'>vous rendant au
              100</L>.

            </div>
        );
    }
}

export default P216;
