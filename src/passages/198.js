/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import * as bh from '../book_helpers.js';
import HelpCharacter from '../HelpCharacter.js';

class P198 extends Component {

    handleClick() {
        const c = bh.endurance_add(this.props.character, -1);
        this.props.character_set(c);        
        bh.update_done(this, 198);
    }
    
    render() {
        const d = bh.get_done(this, 198);
        return (
            <div>
              
              Vous avez à peine parcouru une vingtaine de mètres
              lorsque votre cheval se cabre soudain et s'emballe. Vous
              êtes projeté à terre et vous perdez <L
              onClick={this.handleClick.bind(this)} enabled={!d}
              to='198'>1 point d'ENDURANCE</L>. Vous vous relevez en
              époussetant votre cape et vous lancez un juron à votre
              monture qui disparaît au loin.  Il ne vous reste plus
              qu'à poursuivre votre chemin à pied. <L enabled={d}
              to='138'>Rendez-vous au 138</L>.

              <p/>
              <HelpCharacter/>
              
            </div>
        );
    }
}

export default P198;
