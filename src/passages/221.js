/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P221 extends Component {
    render() {
        return (
            <div>

              Le sol de la taverne est couvert de sang et jonché des
              cadavres de vos adversaires. Dehors, de la grande rue,
              vous parviennent les clameurs d'une foule. Les habitants
              du lieu sont persuadés que vous êtes un tueur fou et ils
              ont la très ferme intention de vous écharper. Vous vous
              enfuyez en toute hâte par la porte de derrière tandis
              que les hurlements de la populace se rapprochent. <L
              to="88">Rendez-vous au 88</L>.

            </div>
        );
    }
}

export default P221;
