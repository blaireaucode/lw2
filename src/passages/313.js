/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import EnduranceChange from '../EnduranceChange.js';
import * as bh from '../book_helpers.js';

class P313 extends Component {
    render() {
        const f = {
            'name': 'a',
            'passage_title': 313,
            'already_done': false,
            'dmg':-4
        };
         const done = bh.book_get_element(this.props, f).already_done;
        return (
            <div>

              Les cris terrifiants des Monstres d'Enfer s'évanouissent
              enfin derrière vous et vous pouvez vous arrêter quelques
              instants pour reprendre votre souffle. Vous grimacez
              alors de douleur, car les doigts de l'épouvantable
              créature vous ont brûlé la gorge, vous infligeant des
              blessures cuisantes qui vous coûtent <EnduranceChange f={f}> 4
              points d'ENDURANCE</EnduranceChange>. Vous déchirez un pan de votre tunique pour
              en faire un bandage, puis vous poursuivez votre route le
              long du tunnel de Tarnalin. <L enabled={done}
              to='149'>Rendez-vous au 349</L>.

            </div>
        );
    }
}

export default P313;
