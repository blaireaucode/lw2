/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import Dead from '../Dead.js';

class P54 extends Component {
    render() {
        return (
            <div>

              Au moment où vous franchissez la porte en courant, une
              lance s'enfonce dans votre poitrine avec une telle force
              que vous en êtes soulevé de terre. Le clair de lune
              s'estompe et la dernière vision que vous emporterez de
              ce monde n'a rien de réconfortant: les visages
              grimaçants de villageois réunis en cercle se penchent
              sur vous et des dizaines de mains vous poignardent à
              mort. Votre quête s'achève ici en même temps que votre
              vie.

              <Dead/>

            </div>
        );
    }
}

export default P54;
