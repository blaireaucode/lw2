/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P263 extends Component {
    render() {
        return (
            <div>

              L'homme contemple le Sceau avec une stupeur mêlée de
              crainte. Sans dire un mot, il se lève alors de son
              fauteuil et vous fait signe de le suivre en haut d'un
              escalier qui mène à une pièce en forme de dôme.  Vous y
              rencontrez le capitaine de la Tour de Guet qui vous
              écoute attentivement tandis que vous lui faites le récit
              des événements qui sont survenus au royaume du
              Sommerlund. Vous lui révélez également le but de votre
              mission. «&nbsp;Donnez immédiatement à cet homme un
              laissez-passer rouge ! Priorité absolue !&nbsp;» ordonne-t-il
              aussitôt. Vous prenez votre laissez-passer, vous quittez
              la tour et vous vous hâtez en direction du poste de
              garde. <L to="246">Rendez-vous au 246</L>.

            </div>
        );
    }
}

export default P263;
