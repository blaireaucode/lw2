/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P51 extends Component {
    render() {
        return (
            <div>

              Environ une heure plus tard, la diligence s'arrête
              devant le sanctuaire de Kalanane. On dit que le
              sanctuaire a été édifié sur la tombe du roi Alin, le
              premier souverain de Durenor et que, tout autour, pousse
              de l'herbe de Laumspur. Si vous souhaitez cueillir un
              peu de cette herbe, <L to="103">rendez-vous au
              103</L>. Sinon, retournez dans la diligence <L
              to="249">en vous rendant au 249</L>.

            </div>
        );
    }
}

export default P51;
