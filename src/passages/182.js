/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P182 extends Component {
    render() {
        return (
            <div>

              Il vous faut trouver un refuge pour la nuit, sinon vous
              risquez d'être arrêté par les gardes de la ville. Votre
              Discipline Kaï vous indique clairement qu'il vous faut
              retourner à la taverne pour y demander une chambre. En y
              passant une bonne nuit, vous serez d'attaque demain
              matin pour établir un plan qui vous permettra
              d'atteindre au plus vite le royaume de Durenor. <L
              to="177">Rendez-vous au 177</L>.

            </div>
        );
    }
}

export default P182;
