/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import SimpleImage from '../SimpleImage.js';

class P257 extends Component {
    render() {
        return (
            <div>

              Cette rue aux pavés jonchés d'ordures est bordée de
              taudis, de masures aux façades moisies et de boutiques
              délabrées. Les quelques passants que vous croisez ont la
              mine sombre, les traits tirés et les yeux hagards.  Ils
              avancent dans le noir en traînant les pieds, le dos
              voûté sous la pluie qui tombe à verse et le regard fixé
              sur le pavé. Vous arrivez bientôt à un croisement ; là,
              la rue de la Hache s'oriente vers le nord et une autre
              rue part vers l'est. Si vous souhaitez aller vers le
              nord et prendre la rue du Chevalier Noir, <L
              to="335">rendez-vous au 335</L>. Si vous préférez
              prendre la direction de l'est en empruntant la rue du
              Sage, <L to="181">rendez-vous au 181</L>.


              <p/>
              <SimpleImage width={200} src={'257.png'}/>

            </div>
        );
    }
}

export default P257;
