/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import * as bh from '../book_helpers.js';

class P162 extends Component {
    
    constructor(props) {
        super(props);
        const f = {
            'name': 'f1',
            'passage_title': 162,
            'encounter': "CHEVALIER DE LA MONTAGNE BLANCHE",
            'combat_skill':20,
            'endurance':27
        };
        this.f = f;
    }
    
    render() {
        const r = bh.enabled_if_fight_win(this.props, this.f);
        return (
            <div>
                
            Il lance son cri de guerre et se rue sur vous.  Vous
            pouvez prendre la fuite à tout moment en vous réfugiant
            dans les bois ; <L to='244'>rendez-vous pour cela au
            244</L>. Si vous sortez vainqueur du combat, <L
            enabled={r} to="302">rendez-vous au 302</L>.

            <p/>
            <Fight fight={this.f}/>

            </div>
                
        );
    }
}

// CHEVALIER DELA MONTAGNE BLANCHE HABILETÉ: 20 ENDURANCE: 27

export default P162;
