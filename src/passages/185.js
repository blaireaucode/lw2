/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';
import Fight from '../Fight.js';
import * as bh from '../book_helpers.js';

class P185 extends Component {
    constructor(props) {
        super(props);
        const f1 = {
            'name': 'f1',
            'passage_title': 185,
            'encounter': "1er DRAKKARIM",
            'combat_skill': 17,
            'endurance':25,
            'continue': true
        };
        const f2 = {
            'name': 'f2',
            'passage_title': 185,
            'encounter': "2e DRAKKARIM",
            'combat_skill': 16,
            'endurance':26
        };
        this.f = [];
        this.f.push(f1);
        this.f.push(f2);
    }

    render() {
        const f = bh.book_get_current_fight(this.props, this.f);
        const r = bh.enabled_if_fight_win(this.props, f);
        return (
            <div>

              Tandis que vous courez sur le pont jonché de cadavres,
              deux guerriers DRAKKARIM apparaissent soudain et vous
              attaquent par surprise. Il vous faut les combattre l'un
              après l'autre.  Vous pouvez prendre la fuite à tout
              moment en plongeant par-dessus bord ; <L
              to='286'>rendez-vous pour cela au 286</L>. Si vous tuez
              vos deux adversaires au cours du combat, vous pourrez
              ensuite sauter sur le pont d'un navire de Durenor qui
              passe à proximité. <L to='120' enabled={r}> Rendez-vous
              alors au 120</L>.

              <p/>
              <Fight fight={f}/>

            </div>
        );
    }
}

// 1er DRAKKARIM HABILETÉ : 17 ENDURANCE : 25
// 2e DRAKKARIM HABILETÉ: 16 ENDURANCE: 26

export default P185;
