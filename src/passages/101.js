/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P101 extends Component {
    render() {
        return (
            <div>

              Vous vous précipitez à l'intérieur de la cabine du
              capitaine ; celui-ci lève les yeux de la carte qu'il
              était en train d'étudier et vous regarde d'un air
              surpris. «&nbsp;Le feu a pris dans la cale ! » Vous avez
              parlé d'une voix haletante, le souffle coupé d'avoir
              tant couru. Un instant plus tard, le capitaine est sorti
              de sa cabine et donne l'ordre à ses hommes de remplir
              des seaux d'eau et de rassembler des couvertures pour
              étouffer l'incendie. Lorsque vous atteignez la cale
              avant, la fumée s'est épaissie et, soudain, une
              véritable frénésie s'empare du navire : des flammes en
              effet viennent de jaillir du panneau d'écoutille. Il
              faut plus d'une heure pour maîtriser le feu et les
              dégâts sont considérables. Les vivres et les réserves
              d'eau douce étaient entreposés dans cette cale : il n'en
              reste plus rien ; de plus, la coque du navire a été
              endommagée.

              <p/>

              Le capitaine remonte de la cale enfumée et s'approche de
              vous, le visage noir de suie. Il porte un paquet sous
              son bras. «Je dois vous parler en privé, my lord»,
              dit-il à voix basse. Sans dire un mot, vous le suivez
              jusqu'à sa cabine. <L to="222">Rendez-vous au 222</L>.

            </div>
        );
    }
}

export default P101;
