/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P153 extends Component {
    render() {
        return (
            <div>

              Vous laissez le fripon à ses tasses et à sa bille, et
              vous vous approchez d'un groupe d'hommes qui jouent aux
              cartes près de l'escalier de la taverne. Au bout d'un
              moment, vous vous apercevez que l'un des joueurs est en
              train de tricher. Si vous voulez défier cet homme, <L
              to="241">rendez-vous au 241</L>. Si vous préférez ne pas
              vous en mêler, <L to='130'>rendez-vous au 130</L>.

            </div>
        );
    }
}

export default P153;
