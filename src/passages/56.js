/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P56 extends Component {
    render() {
        return (
            <div>

              L'aubergiste vous tend une clé. «&nbsp;Chambre 4,
              deuxième porte à gauche en haut de l'escalier»,
              annonce-t-il. Il faudra libérer les lieux une heure
              après le lever du soleil. Votre chambre n'est meublée
              que d'un lit, d'une chaise et d'une petite table. Avant
              d'aller vous coucher, vous verrouillez la porte et vous
              coincez la chaise contre le panneau par mesure de
              sécurité. Dès demain, vous établirez un nouvel
              itinéraire pour rejoindre le royaume de Durenor. <L
              to="127">Rendez-vous au 127</L>.

            </div>
        );
    }
}

export default P56;
