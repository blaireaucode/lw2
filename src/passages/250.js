/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import L from '../L.js';

class P250 extends Component {
    render() {
        const l = this.props.character.items.some(e => e.id === 'sceau_hammardal');
        return (
            <div>

              Le soldat vous regarde d'un air incrédule. «&nbsp;Où
              sont vos marchandises ?  s'étonne-t-il, où est votre
              cheval ? Et votre chariot ? Les marchands ne viennent
              jamais à Port Bax à pied. Vous, un marchand ?
              Laissez-moi rire ! J'ai plutôt l'impression que vous
              êtes un brigand qui essaie de fuir les lieux de quelque
              inavouable forfait. Retournez donc d'où vous venez,
              misérable canaille, nous n'avons pas besoin ici du genre
              de commerce que vous pratiquez.&nbsp;» Si vous voulez
              passer vivant cette frontière, il vous faudra trouver
              autre chose. Qu'allez-vous faire ?  Repartir, faire un
              grand détour et pénétrer dans la forêt plus au sud ?  <L
              to="244"> Rendez-vous au 244</L>. Essayer de corrompre
              le garde ? Il faudra <L to='68'>vous rendre au
              68</L>. Montrer le Sceau d'Hammardal (en admettant qu'il
              soit toujours en votre possession) ? <L enabled={l}
              to='223'>Rendez-vous dans ce cas au 223</L>.


            </div>
        );
    }
}

export default P250;
