/*
 * Copyright 2019
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from './book_default_props.js';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';
import SimpleImage from './SimpleImage.js';
import * as bh from './book_helpers.js';

class Passage extends Component {
    constructor(props) {
        super(props);
        const b = {...this.props.book,
                   current_passage:this.props.path};
        this.props.book_set(b);
    }
    render() {
        /*
        const children = React.Children.map(this.props.children, child => {
            return React.cloneElement(child, {
                character:this.props.character,
                book:this.props.book
            });
        });
        */
        
        var dead = bh.is_dead(this.props.character);
        bh.history_add_passage(this.props, this.props.path);
        // console.log('p', dead, this.props.path, this.props.character, this.props.book);
        if (dead) {          
            dead = (
                <div className="Book-dead">
                  <p/>
                  Vous êtes mort. <p/>
                  <SimpleImage width={80} src={'skull.png'}/>
                  <p/>
                  <Link to='/edit'>Recommencer ...</Link>
                </div>
            );
        }
        else dead = '';

        const children = React.Children.map(this.props.children, child => {
            const c = React.cloneElement(child, {
                character:this.props.character,
                book:this.props.book,
                options:this.props.options,
                character_set: this.props.character_set,
                book_set: this.props.book_set,
                options_set: this.props.options_set,
                book_set_element: this.props.book_set_element
            });
            // console.log('child', child);
            // console.log('child id', child.type.id); // YES !!
            // console.log('chil', c, typeof c);
            // const a = React.Children.toArray(c);
            // console.log('children array', a);            
            return c;
        });
        return (
            <Grid justify="space-between" container>
              {children}
              {dead}
            </Grid>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Passage);
